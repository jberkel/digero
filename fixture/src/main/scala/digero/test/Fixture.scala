package digero.test

import java.io.{BufferedInputStream, File, FileInputStream, InputStream}
import java.net.URL
import java.nio.charset.StandardCharsets
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.{Files, Paths}
import scala.io.{Codec, Source}

trait Fixture:
  def fixture(name: String): String = Fixture.getString(name)
  def fixturePath(name: String): String = Fixture.getPath(name)
  def fixtureFile(name: String): File = Fixture.getFile(name)
  def fixtureStream(name: String): InputStream = Fixture.getStream(name)
  def fixtureWrite(content: String, path: String): Unit = Fixture.write(content, path)

  def temporaryFile(name: String, suffix: String = "fixture"): File =
    File.createTempFile(name,
      if suffix.startsWith(".") then suffix else s".${suffix}",
      null
    )


object Fixture:
  private def getPath(fixture: String): String = getFile(fixture).getAbsolutePath

  private def getFile(fixture: String): File =
    getDirectFile(fixture)
      .orElse(getResourceFile(fixture)) match
      case Some(value) => value
      case None => assert(false, s"missing fixture: $fixture")

  private def getStream(fixture: String): InputStream =
    getResourceURL(fixture).map(_.openStream())
      .orElse(getDirectFile(fixture).map(FileInputStream(_))
                                    .map(BufferedInputStream(_))) match
      case Some(value) => value
      case None => assert(false, s"missing fixture: $fixture")

  private def getResourceURL(fixture: String): Option[URL] =
    val resourcePath = if fixture.startsWith("/") then fixture else "/" + fixture
    val resourceURL = classOf[Fixture].getResource(resourcePath)
    Option(resourceURL)

  private def getResourceFile(fixture: String): Option[File] =
    getResourceURL(fixture).map(url => File(url.getFile))

  private def getDirectFile(fixture: String): Option[File] =
    val directFile = new File(fixture)
    if directFile.exists() then
      Some(directFile)
    else
      None

  private def getString(fixture: String): String =
    Source.fromInputStream(getStream(fixture))(Codec.UTF8).mkString

  private def write(content: String, path: String): Unit =
    Files.write(Paths.get(path), content.getBytes(UTF_8))

