* That which is remembered was predominantly indicated by a genitive object in older usage (until ca. 1940-1950 in writing), nowadays this is restricted to [elevated style](./Appendix:Glossary#higher_register "Appendix:Glossary"). It had also been possible to express that which is reminded with a genitive object, but this usage has been archaic for one or two centuries:

*Er erinnerte mich **dieses Tages**.*

He reminded me of that day.

*Ich erinnere mich **dieses Tages**.*

I remember that day.

* Today both senses are generally indicated with *[an](./an#German "an")* + accusative:

*Er erinnerte mich **an diesen Tag**.*&ZeroWidthSpace;

*Ich erinnere mich **an diesen Tag**.*&ZeroWidthSpace;

* Occasionally, the reflexive verb is replaced with a simple verb + accusative object. Sometimes associated with Northern German, this usage is much less common:

*Ich erinnere **diesen Tag**.*

I remember that day.

But if a passive is desired such use is inevitable, though passive constructions are avoided altogether:

**2001**, [Sebald, Winfried Georg](https://en.wikipedia.org/wiki/W.%20G.%20Sebald "w:W. G. Sebald"), [Austerlitz](https://en.wikipedia.org/wiki/Austerlitz%20(novel) "w:Austerlitz (novel)"), Frankfurt am Main: S. Fischer Verlag, [→ISBN](./Special:BookSources/978-3-596-14864-6 "Special:BookSources/978-3-596-14864-6"), page 173:

Jedenfalls war eines Tages dann unter der Post diese Ansichtskarte aus den zwanziger oder dreißiger Jahren, die eine weiße Zeltkolonie zeigte in der ägyptischen Wüste, ein Bild aus einer **von niemandem mehr erinnerten** Kampagne, \[…\]

(please add an English translation of this quote)