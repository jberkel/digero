package digero.wiktionary.model.parsoid

import digero.test.Fixture
import org.scalatest.matchers.should.Matchers.*
import org.junit.jupiter.api.Test

class ParsoidDataTest extends Fixture:
  val trace = true

  @Test def testParseTemplate(): Unit =
    ParsoidTemplate.fromJSON(fixture("data-mw/data-mw-template.json")) match
      case ParsoidTemplate(Seq(
      Right(DataTemplateContainer(
      DataTemplate(target, params, 0)
      )))) =>
        target should contain("wt" -> "foo")
        target should contain("href" -> "./Template:Foo")

        params should contain("1", DataValue("unused value"))
        params should contain("paramname", DataValue("used value"))

  @Test def testParseComplexTemplate(): Unit =
    val template = ParsoidTemplate.fromJSON(fixture("data-mw/data-mw-template2.json"), trace)
    template.parts should have length 45
    template.parts.filter(_.isLeft) should have length 22
    template.parts.filter(_.isRight) should have length 23

  @Test def parseTemplateWithFunctionTarget(): Unit =
    val template = ParsoidTemplate.fromJSON(
        """
        {"parts":[{"template":{"target":{"wt":"PAGENAME","function":"pagename"},"params":{},"i":0}}]}
        """, trace)
    template.parts should have length 1
    template.parts.head match
      case Right(DataTemplateContainer(DataTemplate(target, params, 0))) =>
        target should contain ("wt" -> "PAGENAME")
        target should contain ("function" -> "pagename")
        params should be (empty)
      case _ => fail("unmatched")

  @Test def parseAttribs(): Unit =
    ParsoidAttribs.fromJSON(fixture("data-mw/data-mw-attribs.json"), trace) match
      case ParsoidAttribs(Seq(Seq(Right(a), Right(b)))) =>
        a should contain ("txt" -> "mw:sortKey")
        b should contain key "html"

  @Test def parseAttribs2(): Unit =
    ParsoidAttribs.fromJSON(fixture("data-mw/data-mw-attribs2.json"), trace) match
      case ParsoidAttribs(Seq(Seq(Left("alt"), Right(b)))) =>
        b should contain key "html"

  @Test def parseErrors(): Unit =
    ParsoidErrors.fromJSON(fixture("data-mw/data-mw-errors.json"), trace) match
      case ParsoidErrors(Seq(
        ParsoidError("apierror-filedoesnotexist", "This image does not exist.")
      )) =>
