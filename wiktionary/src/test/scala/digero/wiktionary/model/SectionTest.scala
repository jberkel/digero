package digero.wiktionary.model

import digero.wiktionary.model.SectionHeading.{Etymology, GlyphOrigin, Noun, Pronunciation, Synonyms}
import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.{assertThat, fail}

class SectionTest:

  @Test
  def testParseSimple(): Unit =
    val section = Section("Etymology")
    assertThat(section.heading).isEqualTo(Some(Etymology))
    assertThat(section.index).isEqualTo(None)

  @Test
  def testParseSectionWithIndex(): Unit =
    val section = Section("Etymology 23")
    assertThat(section.heading).isEqualTo(Some(Etymology))
    assertThat(section.index).isEqualTo(Some(23))

  @Test
  def testParsePronunciationWithIndex(): Unit =
    val section = Section("Pronunciation 1")
    assertThat(section.heading).isEqualTo(Some(Pronunciation))
    assertThat(section.index).isEqualTo(Some(1))

  @Test
  def testIsEtymology(): Unit =
    val section = Section("Etymology 23")
    assertThat(section.isEtymology).isTrue

    val section2 = Section("Glyph origin 2")
    assertThat(section2.isEtymology).isTrue

    val section3 = Section("Noun")
    assertThat(section3.isEtymology).isFalse

    val section4 = Section("XXX")
    assertThat(section4.isEtymology).isFalse

  @Test
  def testParseSectionWithSpacesAndIndex(): Unit =
    val section = Section("Glyph origin 2")
    assertThat(section.heading).isEqualTo(Some(GlyphOrigin))
    assertThat(section.index).isEqualTo(Some(2))

  @Test
  def testParseUnknownHeader(): Unit =
    val section = Section("XXX")
    assertThat(section.name).isEqualTo("XXX")
    assertThat(section.heading).isEqualTo(None)

  @Test
  def testWithChildren(): Unit =
    val section = Section("Noun", heading = Some(Noun),
        subsections = Seq(Section("Synonyms", Level.L3, Some(Synonyms), subsections = Seq(Section("Foo")))))

    section.withChildren.toList match
      case Section(_, _, Some(Noun), _, _, _)
          :: Section(_, _, Some(Synonyms), _, _, _)
          :: Section("Foo", _, None, _,  _, _)
          :: Nil =>
      case e => fail(f"unmatched: $e")

    section.withChildren.head match
      case Section(_, _, Some(Noun), _, _, _) =>
      case e => fail(f"unmatched: $e")
