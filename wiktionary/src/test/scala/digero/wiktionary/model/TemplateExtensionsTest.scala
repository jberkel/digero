package digero.wiktionary.model

import digero.model.Template
import digero.wikitext.TemplateVisitor
import digero.wiktionary.model.TemplateExtensions.*
import digero.wikitext.WtTemplateExtensions._
import digero.wiktionary.model.TemplateExtensionsTest.TemplateCollector
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*
import org.sweble.wikitext.engine.nodes.EngPage
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp
import org.sweble.wikitext.engine.{PageId, PageTitle, WtEngineImpl}
import org.sweble.wikitext.parser.nodes.WtTemplate

import java.util
import java.util.ArrayList

class TemplateExtensionsTest:
  @Test
  def testGetLanguage(): Unit =
    parseTemplate("{{m|en}}").getLanguage should not be empty
    parseTemplate("{{m|jbo}}").getLanguage should not be empty
    parseTemplate("{{m|gem-pro}}").getLanguage should not be empty
    parseTemplate("{{m|xyz}}").getLanguage shouldBe empty

  @Test
  def testGetNonEtymologyLanguage(): Unit =
    parseTemplate("{{m|LL.}}").getNonEtymologyLanguage(1).map(_.getCode) should contain("la")
    parseTemplate("{{m|en}}").getNonEtymologyLanguage(1).map(_.getCode) should contain("en")

  @Test
  def testGetTarget(): Unit =
    parseTemplate("{{m|en|}}").getTarget(2) shouldBe empty
    parseTemplate("{{m|en| - }}").getTarget(2) shouldBe empty
    parseTemplate("{{m|en|foo}}").getTarget(2) should contain("foo")

  private def parseTemplate(wikiText: String): Template =
    val page = parse(wikiText)
    val collector = TemplateCollector()
    collector.visit(page)
    collector.templates should not be empty
    collector.templates.get(0).asTemplate

  private def parse(wikiText: String): EngPage =
    val wikiConfig = DefaultConfigEnWp.generate
    val engine = WtEngineImpl(wikiConfig)
    engine.postprocess(PageId(PageTitle.make(wikiConfig, "test"), 0), wikiText, null).getPage


object TemplateExtensionsTest:
  class TemplateCollector extends TemplateVisitor:
    val templates: util.ArrayList[WtTemplate] = util.ArrayList[WtTemplate]()

    override def visit(template: WtTemplate): Unit =
      templates.add(template)
