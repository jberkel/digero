package digero.wiktionary.model

import digero.wiktionary.model.Level.{L3, L4}
import digero.wiktionary.model.SectionHeading.{Etymology, Noun, Pronunciation}
import digero.wiktionary.parser.FixtureParsing
import org.junit.jupiter.api.Test

class EntryHeaderTest extends FixtureParsing:

  @Test
  def testUnapplySimpleEntry(): Unit =
    val entry = parse(EntryName("simple", "en"), "parser/entry/simple_en_expected.html")
    entry.sections match
      case EntryHeaders(
        EntryHeader(List(Section(_, L3, Some(Etymology), None, _, _), _*), _, Nil)
      ) =>

  @Test
  def testUnapplyNestedEtymology(): Unit =
    val entry = parse(EntryName("bar", "en"), "parser/entry/bar_en_expected.html")
    entry.sections match
      case EntryHeaders(
        EntryHeader(List(Section(_, L3, Some(Etymology), Some(1), _, _), _*), _, Nil),
        EntryHeader(List(Section(_, L3, Some(Etymology), Some(2), _, _), _*), _, Nil),
        EntryHeader(List(Section(_, L3, Some(Etymology), Some(3), _, _), _*), _, Nil),
      ) =>

  @Test
  def testUnapplyPronunciationsWithNestedEtymologies(): Unit =
    // pathological case: has etymologies nested inside pronunciation
    val entry = parse(EntryName("bubo", "tl"), "parser/entry/tl-bubo.html")
    entry.sections match
      case EntryHeaders(
        EntryHeader(
          List(Section(_, L3, Some(Pronunciation), Some(1), _, _), _*),
          _,
          Seq(
            EntryHeader(
              List(Section(_, L4, Some(Etymology), Some(1), _, _), _*),
              _,
              Nil
            ),
            EntryHeader(
              List(Section(_, L4, Some(Etymology), Some(2), _, _), _*),
              _,
              Nil
            )
          )
        ),
        EntryHeader(
          List(Section(_, L3, Some(Pronunciation), Some(2), _, _),
               Section(_, L4, Some(Noun), None, _, _)),
          _,
          Nil
        ),
        EntryHeader(
          List(Section(_, L3, Some(Pronunciation), Some(3), _, _),
               Section(_, L4, Some(Noun), None, _, _)),
          _,
          Nil
        ),
        EntryHeader(
          List(Section(_, L3, Some(Pronunciation), Some(4), _, _), _*),
          _,
          Nil
        )
      ) =>
