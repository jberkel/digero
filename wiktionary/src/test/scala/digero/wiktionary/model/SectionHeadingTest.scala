package digero.wiktionary.model

import digero.wiktionary.model.Conditions.containing
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Condition
import org.jsoup.Jsoup
import org.junit.jupiter.api.Test

class SectionHeadingTest:
  private lazy val document = Jsoup.parse("""
      <html>
        <section id="anagrams">
            <h3>Anagrams</h3>
            <p><a href="some link">Foo</a></p>
        <section>
      </html>
      """)

  private lazy val a = document.selectFirst("a")

  @Test
  def testElementFilterMatching(): Unit =
    assertThat(SectionHeading.Anagrams.elementFilter(a)).isTrue

  @Test
  def testElementFilterNonMatching(): Unit =
    assertThat(SectionHeading.UsageNotes.elementFilter(a)).isFalse
  
  @Test
  def testSection(): Unit =
    assertThat(a.section).is(containing("Anagrams"))
  
  
object Conditions:
  def containing[T](expected: T): Condition[Option[T]] =
    new Condition[Option[T]]:
      override def matches(value: Option[T]): Boolean = value.contains(expected)
