package digero.wiktionary.model.serialization

import digero.wiktionary.model.Sense
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import scala.util.Using

class SerializedEntryTest:
  given TextConverter = (s: String) => Some(s"H: $s")

  @Test
  def testAsJSON(): Unit =
    val entry = SerializedEntry(
      pageTitle = "foo",
      redirects = Seq.empty,
      headwordLines = Seq.empty,
      partOfSpeech = "Noun",
      senses = Seq(
        SerializedSense((1, 0, 0, 0, 0), "sense1", Seq.empty, Seq.empty, Seq.empty, None, None),
        SerializedSense((2, 0, 0, 0, 0), "sense2", Seq.empty, Seq.empty, Seq.empty, None, None),
        SerializedSense((2, 1, 0, 0, 0), "subsense 2.1", Seq.empty, Seq.empty, Seq.empty, None, None)
      ),
      etymology = Some("etymology"),
      pronunciations = Seq.empty,
      translations = Seq.empty,
      usageNotes = None,
      categories = Seq.empty,
      relations = Seq.empty,
      inflections = SerializedInflections(conjugations = Seq.empty)
    )
    CustomPickler.write(entry, indent = 2) shouldEqual
      """{
        |  "pageTitle": "foo",
        |  "redirects": [],
        |  "headwordLines": [],
        |  "partOfSpeech": "Noun",
        |  "senses": [
        |    {
        |      "number": [
        |        1,
        |        0,
        |        0,
        |        0,
        |        0
        |      ],
        |      "definition": "sense1",
        |      "relations": [],
        |      "usexes": [],
        |      "categories": [],
        |      "formOf": null,
        |      "translationGloss": null
        |    },
        |    {
        |      "number": [
        |        2,
        |        0,
        |        0,
        |        0,
        |        0
        |      ],
        |      "definition": "sense2",
        |      "relations": [],
        |      "usexes": [],
        |      "categories": [],
        |      "formOf": null,
        |      "translationGloss": null
        |    },
        |    {
        |      "number": [
        |        2,
        |        1,
        |        0,
        |        0,
        |        0
        |      ],
        |      "definition": "subsense 2.1",
        |      "relations": [],
        |      "usexes": [],
        |      "categories": [],
        |      "formOf": null,
        |      "translationGloss": null
        |    }
        |  ],
        |  "etymology": "etymology",
        |  "pronunciations": [],
        |  "translations": [],
        |  "usageNotes": null,
        |  "categories": [],
        |  "relations": [],
        |  "inflections": {
        |    "conjugations": []
        |  }
        |}""".stripMargin

  @Test
  def testAsJSONWithNull(): Unit =
    val entry = SerializedEntry(
      pageTitle = "foo",
      redirects = Seq.empty,
      partOfSpeech = "Noun",
      headwordLines = Seq.empty,
      senses = Seq.empty,
      etymology = None,
      pronunciations = Seq.empty,
      translations = Seq.empty,
      usageNotes = None,
      categories = Seq.empty,
      relations = Seq.empty,
      inflections = SerializedInflections(conjugations = Seq.empty)
    )

    CustomPickler.write(entry) shouldEqual
      """{"pageTitle":"foo","redirects":[],"headwordLines":[],"partOfSpeech":"Noun","senses":[],"etymology":null,"pronunciations":[],"translations":[],"usageNotes":null,"categories":[],"relations":[],"inflections":{"conjugations":[]}}"""

  @Test def testSerialized(): Unit =
    val senses = Seq[Sense](
      Sense("1", html = "1",
        subsenses = Seq(
          Sense("1.1", html = "1.1"),
          Sense("1.2", html = "1.2")
        )
      ),
      Sense("2", html = "2")
    )

    senses.serialized shouldEqual Seq(
      SerializedSense((1, 0, 0, 0, 0), "H: 1", Seq.empty, Seq.empty, Seq.empty, None, None),
      SerializedSense((1, 1, 0, 0, 0), "H: 1.1", Seq.empty, Seq.empty, Seq.empty, None, None),
      SerializedSense((1, 2, 0, 0, 0), "H: 1.2", Seq.empty, Seq.empty, Seq.empty, None, None),
      SerializedSense((2, 0, 0, 0, 0), "H: 2", Seq.empty, Seq.empty, Seq.empty, None, None)
    )

  @Test def testJavaSerialization(): Unit =
    val entry = SerializedEntry(
      pageTitle = "foo",
      redirects = Seq.empty,
      partOfSpeech = "Noun",
      headwordLines = Seq(SerializedHeadwordLine("foo", None, Seq.empty, Seq.empty, Seq.empty)),
      senses = Seq(SerializedSense((0, 0, 0, 0, 0), "definition", Seq.empty, Seq.empty, Seq.empty, None, None)),
      etymology = None,
      pronunciations = Seq(SerializedPronunciation("foo", Seq.empty)),
      translations = Seq(
        SerializedTranslationGroup(
          "gloss",
          Seq(SerializedTranslation("foo", "en", None)),
          None)
      ),
      usageNotes = Some("usage"),
      categories = Seq.empty,
      relations = Seq.empty,
      inflections = SerializedInflections(conjugations = Seq.empty)
    )

    val byteArrayOutputStream = ByteArrayOutputStream()
    val objectOutputStream = ObjectOutputStream(byteArrayOutputStream)
    objectOutputStream.writeObject(entry)
    objectOutputStream.close()

    val deserializedEntry = Using(ObjectInputStream(
      new ByteArrayInputStream(byteArrayOutputStream.toByteArray))) { is =>
      is.readObject().asInstanceOf[SerializedEntry]
    }.get

    deserializedEntry shouldEqual entry


