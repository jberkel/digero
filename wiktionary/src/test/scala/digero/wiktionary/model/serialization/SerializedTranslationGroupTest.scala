package digero.wiktionary.model.serialization

import digero.wiktionary.model.{TranslationGroup, TranslationSubpage}
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class SerializedTranslationGroupTest:
  @Test
  def testSerializeTranslationGroup(): Unit =
    val group = SerializedTranslationGroup(
        TranslationGroup("gloss",
        Seq.empty,
        Some(TranslationSubpage("page", Some("section"))))
    )
    CustomPickler.write(group, indent=2) shouldEqual
      """{
        |  "gloss": "gloss",
        |  "translations": [],
        |  "subpage": "page#section"
        |}""".stripMargin

