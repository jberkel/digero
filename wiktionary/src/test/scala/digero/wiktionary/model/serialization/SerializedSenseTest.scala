package digero.wiktionary.model.serialization

import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class SerializedSenseTest:

  @Test def testAsJSON(): Unit =
    val sense = SerializedSense(number=(0, 0, 0, 0, 0), "definition", Seq.empty, Seq.empty, Seq.empty, Some("lemma"), Some("translation"))
    CustomPickler.write(sense) should equal(
      """{"number":[0,0,0,0,0],"definition":"definition","relations":[],"usexes":[],"categories":[],"formOf":"lemma","translationGloss":"translation"}"""
    )

  @Test def testAsJSON_NoGloss(): Unit =
    val sense = SerializedSense(number=(0, 0, 0, 0, 0), "definition", Seq.empty, Seq.empty, Seq.empty, None, None)
    CustomPickler.write(sense) should equal(
      """{"number":[0,0,0,0,0],"definition":"definition","relations":[],"usexes":[],"categories":[],"formOf":null,"translationGloss":null}"""
    )