package digero.wiktionary.model.lexical

import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat

class GrammaticalGenderTest:

  @Test
  def testApply(): Unit =
    assertThat(GrammaticalGender("m")).isEqualTo(Some(GrammaticalGender.Masculine))
    assertThat(GrammaticalGender("f")).isEqualTo(Some(GrammaticalGender.Feminine))
    assertThat(GrammaticalGender("xxx")).isEqualTo(None)
