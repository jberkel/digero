package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.lexical.InflectionType.{GenitiveSingular, Plural}
import digero.wiktionary.model.lexical.{GrammaticalGender, Inflection, InflectionType}
import digero.wiktionary.model.{Category, HeadwordLine, Transliterated}
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import scala.compiletime.uninitialized

class HeadwordLineParserTest extends Fixture:
  var subject: HeadwordLineParser = uninitialized

  given ordering: Ordering[Category] = (x: Category, y: Category) => x.name.compare(y.name)

  @BeforeEach
  def prepare(): Unit = subject = HeadwordLineParser()

  @Test
  def testParse(): Unit =
    subject.parse(fixture("parser/headword/headword-played.html")) match
      case HeadwordLine(Left("played"), _, _, _, categories) :: Nil =>
          categories.toSeq.sorted match
            case Category("English non-lemma forms")
              :: Category("English verb forms") :: Nil =>

  @Test
  def testParseGerman(): Unit =
    subject.parse(fixture("parser/headword/headword-haltest_ab.html")) match
      case HeadwordLine(Left("haltest ab"), _, _, _, categories) :: Nil =>
        categories.toSeq.sorted match
          case Category("German non-lemma forms")
            :: Category("German verb forms") :: Nil =>

  @Test
  def testParseRussianTransliteration(): Unit =
    subject.parse(fixture("parser/headword/ru-noun.html")) match
      case HeadwordLine(Right(Transliterated("мир", "Cyrl", "mir" :: Nil)), _, _, _, _) :: Nil =>

  @Test
  def testParseKoreanTransliteration(): Unit =
  subject.parse(fixture("parser/headword/ko-noun.html")) match
    case HeadwordLine(Right(Transliterated("야자수", "Kore", "yajasu" :: Nil)), _, _, _, _) :: Nil =>

  @Test
  def testParseGenderRussian(): Unit =
    subject.parse(fixture("parser/headword/ru-noun.html")) match
      case HeadwordLine(_, _, _, gender, _) :: Nil =>
        gender should contain only(GrammaticalGender.Masculine, GrammaticalGender.Inanimate)

  @Test
  def testParseGenderRussianAnimate(): Unit =
    subject.parse(fixture("parser/headword/ru-noun-animate.html")) match
      case HeadwordLine(_, _, _, gender, _) :: Nil =>
        gender should contain only(GrammaticalGender.Feminine, GrammaticalGender.Animate)

  @Test
  def testParseGermanGender(): Unit =
    subject.parse(fixture("parser/headword/de-noun-Blatt.html")) match
      case HeadwordLine(_, _, _, gender, _) :: Nil =>
        gender should contain only GrammaticalGender.Neuter

  @Test
  def testParseGermanMultipleGenders():Unit =
    subject.parse(fixture("parser/headword/de-noun-multiple-genders-Butter.html")) match
      case HeadwordLine(_, _, _, gender, _) :: Nil =>
        gender should contain only (GrammaticalGender.Feminine, GrammaticalGender.Masculine)

  @Test
  def testParseGermanDiminutive(): Unit =
    subject.parse(fixture("parser/headword/de-noun-Flasche.html")) match
      case HeadwordLine(_, _, _, gender, _) :: Nil =>
        gender should contain only GrammaticalGender.Feminine

  @Test
  def testParseSwedishCommonGender(): Unit =
    subject.parse(fixture("parser/headword/sv-noun.html")) match
      case HeadwordLine(_, _, _, gender, _) :: Nil =>
        gender should contain only GrammaticalGender.Common

  @Test
  def testParseLatinGender(): Unit =
    subject.parse(fixture("parser/headword/la-noun.html")) match
      case HeadwordLine(_, _, _, gender, _) :: Nil =>
        gender should contain only GrammaticalGender.Feminine

  @Test
  def testParseLatinHeadword(): Unit =
    subject.parse(fixture("parser/headword/la-noun.html")) match
      case HeadwordLine(Left("pīla"), _, _, _, _) :: Nil =>

  @Test
  def testParseCatalanPlural(): Unit =
    subject.parse(fixture("parser/headword/ca-noun.html")) match
      case HeadwordLine(_, _, inflections, _, _) :: Nil =>
        inflections should contain(Inflection(Plural, Seq("mesos")))

  @Test
  def testParseEnglishMultiplePlurals(): Unit =
    subject.parse(fixture("parser/headword/en-noun-multiple-plurals.html")) match
      case HeadwordLine(_, _, inflections, _,  _) :: Nil =>
        inflections should contain(Inflection(Plural, Seq("albatross", "albatrosses")))

  @Test
  def testParseGermanGenitive(): Unit =
    subject.parse(fixture("parser/headword/de-noun-Blatt.html")) match
      case HeadwordLine(_, _, inflections, _, _) :: Nil =>
        inflections should contain(Inflection(GenitiveSingular, Seq("Blattes", "Blatts")))

  @Test
  def testParseMultipleHeadwordLines_Wessi(): Unit =
    subject.parse(fixture("parser/headword/multiple-headword-lines-Wessi.html")) match
      case HeadwordLine(_, _, _, genders1, categories1) ::
           HeadwordLine(_, _, _, genders2, categories2) :: Nil =>
            genders1 should contain only GrammaticalGender.Masculine
            genders2 should contain only GrammaticalGender.Feminine

            categories1 should contain only  (Category("German lemmas"),
                                              Category("German nouns"),
                                              Category("German masculine nouns"))

            categories2 should contain only  (Category("German lemmas"),
                                              Category("German nouns"),
                                              Category("German feminine nouns"))

  @Test
  def testParseMultipleHeadwordLines_Butter(): Unit =
    subject.parse(fixture("parser/headword/multiple-headword-lines-Butter.html")) match
      case HeadwordLine(_, _, _, genders1, _) ::
        HeadwordLine(_, _, _, genders2, _) :: Nil =>
        genders1 should contain only GrammaticalGender.Feminine
        genders2 should contain only GrammaticalGender.Masculine

  @Test
  def testParseJapaneseRuby(): Unit =
    subject.parse(fixture("parser/headword/ja-街.html")) match
      case HeadwordLine(Right(Transliterated("街", "Jpan", Seq("machi"))), Seq("まち"), _, _, _) :: Nil =>

  @Test
  def testParseJapaneseNoHeadlines(): Unit =
    subject.parse(fixture("parser/headword/ja-オナニー.html")) match
      case HeadwordLine(Right(Transliterated("オナニー", "Jpan", Seq("onanī"))), _, _, _, _) :: Nil =>
