package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.Category
import digero.wiktionary.parser.JsoupExtensions.*
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class JsoupExtensionsTest extends Fixture with HTMLParser:

  @Test def serializeDocumentDefaultsNoPrettyPrint(): Unit =
    val document = parseDocument("""<div><div>Test</div></div>""")
    val html = document.serialize()
    html should equal("""<div><div>Test</div></div>""")


  @Test def serializeDocumentWithPrettyPrint(): Unit =
    val document = parseDocument("""<div><div>Test</div></div>""")
    val html = document.serialize(true)
    val expected =
        """<div>
          | <div>
          |  Test
          | </div>
          |</div>""".stripMargin
    html should equal(expected)

  @Test def serializeElementWithoutPrettyPrint(): Unit =
    val document = parseDocument("""<div><div id="test"><div>Test</div></div></div>""")
    val html = document.selectFirst("#test").serialize(false)
    html shouldEqual("""<div>Test</div>""")

  @Test def serializeElementWithPrettyPrint(): Unit =
    val document = parseDocument("""<div><div id="test"><div>Test</div></div></div>""")
    val html = document.selectFirst("#test").serialize(true)
    html shouldEqual (
      """|<div>
         | Test
         |</div>""".stripMargin)

  @Test def testParsoidData(): Unit =
    val document = parseDocument(fixture("parser/entry/water.html"))
    for
      element <- document.select("*[data-mw]")
      mwData <- element.attrOpt("data-mw")
    do
      withClue(mwData) { element.parsoidData should not be empty }

  @Test def testAbout(): Unit =
    val document = parseDocument(
      """
      <span about="foo">
        <p class="headword">ABC</p>
        <b about="baz">123</b>
      </span>
      """
    )

    document.select("span").head.about should contain("foo")
    document.select("p").head.about should contain("foo")
    document.select("b").head.about should contain("baz")

  @Test def testCategories(): Unit =
    val document = parseDocument(fixture("parser/headword/ko-noun.html"))
    document.categories should contain only(Category("Korean lemmas"), Category("Korean nouns"))

  @Test def testRemoving(): Unit =
    val document = parseDocument(
      """
        <ol>
          <li>foo<li>
        </ol>
       """)
    document.removing("li").selectFirst("ol").toString should equal("<ol>\n</ol>")

  @Test def testTemplates(): Unit =
    val document = parseDocument(fixture("parser/headword/ko-noun.html"))
    val strong = document.selectFirst("strong")
    strong.templates should not be empty
    strong.templates.map(_.name) should contain("ko-noun")
    strong.templates.flatMap(_.parameters) should contain(Right("hanja") -> "[[椰子樹]]")

  @Test def testWikilinksSingle(): Unit =
    val links = parseDocument(fixture("parser/headword/ko-noun.html")).wikiLinks()
    links should have size 1
    links.headOption.map(_.toString) should contain("[[椰子樹#Korean|null]]")

  @Test def testWikilinksMultiple(): Unit =
    val links = parseDocument(fixture("parser/entry/water.html")).wikiLinks()
    links should have size 1026


  @Test def testPlainSelfLinks(): Unit =
    val link = parseDocument(
      """
        <span class="Latn" lang="fi">
          <strong class="selflink">puhua</strong>
        </span>
      """).plainSelfLinks().head

    link.getTarget shouldEqual "puhua"
    link.getLanguage.getCode shouldEqual "fi"


  @Test def testWikilinksExcludeSelfLinks(): Unit =
    val links = parseDocument(
      """
      <a rel="mw:WikiLink" class="selflink" href="./stain" title="stain" id="mwFA">stains</a>
      """).wikiLinks(includeSelfLinks = false)
    links shouldBe empty

