package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.parser.HTMLPreprocessor.preprocess
import digero.wiktionary.parser.JsoupExtensions.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class HTMLPreprocessorTest extends Fixture:

  @Test def itRemovesUnwantedAttributes(): Unit =
    val input = """<span class="etyl" about="#mwt18" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;der&quot;,&quot;href&quot;:&quot;./Template:der&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;en&quot;},&quot;2&quot;:{&quot;wt&quot;:&quot;zh&quot;},&quot;3&quot;:{&quot;wt&quot;:&quot;福&quot;},&quot;4&quot;:{&quot;wt&quot;:&quot;&quot;},&quot;5&quot;:{&quot;wt&quot;:&quot;[[fortunate]]; [[prosperity]], [[good]] [[luck]]&quot;},&quot;tr&quot;:{&quot;wt&quot;:&quot;fú&quot;}},&quot;i&quot;:0}}]}" id="mwMQ">"""
    assertThat(preprocess(input).serialize()).isEqualTo("""<span class="etyl" typeof="mw:Transclusion"></span>""")

  @Test def itStripsCategoryLinks(): Unit =
    val input = """<link rel="mw:PageProp/Category" href="./Category:Terms_with_manual_transliterations_different_from_the_automated_ones#FOO" about="#mwt19">"""
    assertThat(preprocess(input).serialize()).isEmpty()


  @Test def itStripsInlineReferencesPreservingWhitespace(): Unit =
    val input = """The end.<sup class="mw-ref reference" rel="dc:references"><a href="./dumpster#cite_note-1" style="counter-reset: mw-Ref 1;" id="mwDw"><span class="mw-reflink-text" id="mwEA">[1]</span></a></sup> <span>By</span>"""
    assertThat(preprocess(input).serialize()).isEqualTo("The end. <span>By</span>")

  @Test def itStripsReferenceLists(): Unit =
    val input = "<span>foo</span><div class=\"mw-references-wrap\"><ol class=\"references\"></ol></div>"
    assertThat(preprocess(input).serialize()).isEqualTo("<span>foo</span>")

  @Test def itStripsTermRequests():Unit =
    val input = """foo <small about="#mwt2">[Term?]</small> bar"""
    assertThat(preprocess(input).serialize()).isEqualTo("foo bar")

  @Test def itStripsScriptNeeded(): Unit =
    val input = """foo <small about="#mwt5" typeof="mw:Transclusion" id="mwDw">[script needed]</small> bar"""
    assertThat(preprocess(input).serialize()).isEqualTo("foo bar")

  @Test def itStripsImages(): Unit =
    val input = """foo <span typeof="mw:Transclusion mw:File"><img src="foo"/></span> bar"""
    assertThat(preprocess(input).serialize()).isEqualTo("foo bar")

  @Test def itStripsImageGallery(): Unit =
    val input = """
    <ul class="gallery mw-gallery-traditional" typeof="mw:Extension/gallery">
      <li class="gallerybox">
        <div class="gallerytext" id="mwCQ">some caption</div>
      </li>
    </ul>
    """
    assertThat(preprocess(input).serialize()).isBlank

  @Test def itStripsStyle(): Unit =
    val input = """
    <style>
    .mw-parser-output .tmulti .trow {
        justify-content: center
     }
    </style>
    """
    assertThat(preprocess(input).serialize()).isBlank

  @Test def itStripsInterProject(): Unit =
    val input =
      """
      <span class="interProject">
        <a rel="mw:WikiLink/Interwiki" href="foo" class="extiw">Wikipedia</a>
      </span>
      """
    assertThat(preprocess(input).serialize()).isBlank


  @Test def itStripsSerialComma(): Unit =
    val input = """foo, bar<span class="serial-comma">,</span> and baz"""
    assertThat(preprocess(input).serialize()).isEqualTo("foo, bar and baz")

  /*
  @Test def itStripsCiteRefs(): Unit =
    val input = "<p>Hello<a href=\"#cite_ref-foo\">1</a></p>"
    assertThat(preprocess(input)).isEqualTo("<p>Hello</p>")
   */

  @Test def colorGetsStripped(): Unit =
    assertThat(preprocess("<div class=\"color-panel\"><table></table></div>").serialize()).isEmpty()

  @Test def testMaintenanceLineGetStripped(): Unit =
    assertThat(preprocess("<span class=\"maintenance-line\">foo</span").serialize()).isEmpty()

  @Test def testMaintenanceBoxGetStripped(): Unit =
    assertThat(preprocess("<span class=\"maintenance-box\">foo</span").serialize()).isEmpty()

  @Test def testRequestBoxGetStripped(): Unit =
    assertThat(preprocess("<div class=\"request-box\">foo</div").serialize()).isEmpty()

  @Test def testAttentionGetStripped(): Unit =
    assertThat(preprocess("<span class=\"attentionseeking\">Attention!</span").serialize()).isEmpty()

  @Test def filtersSisterProjects(): Unit =
    val input = "Foo<div class=\"sister-wikipedia sister-project noprint floatright\" style=\"border: 1px solid #aaa; font-size: 90%; background: #f9f9f9; width: 250px; padding: 4px; text-align: left;\">\n" + "</div>Baz"
    assertThat(preprocess(input).serialize()).isEqualTo("FooBaz")

  @Test def filtersWOTD(): Unit =
    val input =
      """
       <div class="was-wotd floatright" style="padding-right: 1em" about="#mwt2" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;was wotd&quot;,&quot;href&quot;:&quot;./Template:was_wotd&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;2009&quot;},&quot;2&quot;:{&quot;wt&quot;:&quot;October&quot;},&quot;3&quot;:{&quot;wt&quot;:&quot;1&quot;}},&quot;i&quot;:0}}]}" id="mwBg">
       <span title="This was a Wiktionary Word of the Day"><small><i><a rel="mw:WikiLink" href="./Wiktionary:Word_of_the_day/Archive/2009/October#1" title="Wiktionary:Word of the day/Archive/2009/October">WOTD –<span typeof="mw:Entity"> </span>1 October 2009</a>
        <link rel="mw:PageProp/Category" href="./Category:Word_of_the_day_archive"></i></small></span>
       </div>
      """
    assertThat(preprocess(input).serialize()).isBlank

  @Test def filtersFigureTags(): Unit =
    assertThat(preprocess("<figure class=\"mw-default-size\" typeof=\"mw:Error mw:Image/Thumb\"></figure>").serialize()).isEmpty()

  @Test def filtersAudioTables(): Unit =
    assertThat(preprocess("<table class=\"audiotable\" style=\"vertical-align: bottom; display:inline-block; list-style:none;line-height: 1em; border-collapse:collapse;\">\n" +
        "<tbody></tbody></table>").serialize()).isEmpty()

  @Test def filtersNumberBoxes(): Unit =
    assertThat(preprocess(
      """
        |<table class="floatright number-box" cellpadding="5" cellspacing="0" rules="all">
        |<caption><b>Irish cardinal numbers</b>
        |</caption>
        |<tbody><tr>
        |<td class="adjacent-slot"><span class="None" lang="ga">&nbsp;&lt;&nbsp;&nbsp;999</span>
        |</td>
        |<th class="current-slot"><span class="None" lang="ga">1000</span>
        |</th>
        |<td class="adjacent-slot"><span class="None" lang="ga">1001&nbsp;&nbsp;&gt;&nbsp;</span>
        |</td></tr>
        |<tr>
        |<td colspan="3" class="form-slot">&nbsp;&nbsp;&nbsp; <i><a href="/wiki/cardinal_number" title="cardinal number">Cardinal</a></i>&nbsp;: <span class="Latn" lang="ga"><strong class="selflink">míle</strong></span><br> &nbsp;&nbsp;&nbsp; <i><a href="/wiki/ordinal_number" title="ordinal number">Ordinal</a></i>&nbsp;: <span class="Latn" lang="ga"><a href="/w/index.php?title=m%C3%ADli%C3%BA&amp;action=edit&amp;redlink=1" class="new" title="míliú (page does not exist)">míliú</a></span>
        |</td></tr>
        |<tr>
        |<td colspan="3" class="footer-slot">
        |</td></tr></tbody></table>
        |""".stripMargin).serialize()).isBlank()

  @Test def filtersAbbrTags(): Unit =
    assertThat(preprocess("<abbr>bar baz</abbr>").serialize()).isEmpty()


  @Test def filtersMultipleImageTags(): Unit =
    // https://en.wiktionary.org/wiki/Module:multiple_images
    assertThat(preprocess("<div class=\"thumb tmulti tright\">Some content</div>").serialize()).isEmpty()

  @Test def removesWhitespaceAroundIgnoredElementsTerm(): Unit =
    assertThat(preprocess(
      """
        |<p>Foo<span/>
        |    <small about="#mwt6">[Term?]</small>.</p>
      """.stripMargin).serialize().strip()).isEqualTo("<p>Foo<span></span>.</p>")

  @Test def removesEtymonTree(): Unit =
    assertThat(preprocess(fixture("parser/preprocessor/etymon.html")).text())
      .isEqualTo("Etymology Borrowed from Arabic إِعْلَام (ʔiʕlām).")