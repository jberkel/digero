package digero.wiktionary.parser.table

import digero.wiktionary.parser.HTMLParser
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class FormTest extends HTMLParser:

  @Test
  def testParseSingle(): Unit =
    val forms = Form.parse(parseDocument(
      """
        <td>
          <span class="Latn form-of lang-es verb-form-achicharrar-form-of     origin-achicharrar   " lang="es">
            <a rel="mw:WikiLink" href="./achicharro#Spanish" title="achicharro">achicharro</a>
          </span>
        </td>
      """))
    forms should have length 1
    forms(0).toString shouldEqual "achicharro"
    forms(0).superscripts shouldBe empty

  @Test
  def testParseMultipleWithSuperscript(): Unit =
    val forms = Form.parse(parseDocument(
      """
        <td>
          <span class="Latn form-of lang-es verb-form-achicharrar-form-of     origin-achicharrar   " lang="es">
            <a rel="mw:WikiLink" href="./achicharras#Spanish" title="achicharras">achicharras</a>
          </span>
          <sup>
            <sup>tú</sup>
          </sup>
          <br>
          <span class="Latn form-of lang-es verb-form-achicharrar-form-of     origin-achicharrar   " lang="es">
            <a rel="mw:WikiLink" href="./achicharrás#Spanish" title="achicharrás">achicharrás</a>
          </span>
          <sup>
            <sup>vos</sup>
          </sup>
        </td>
      """))
    forms should have length 2

    forms(0).toString shouldEqual "achicharras"
    forms(0).superscripts should contain only "tú"
    forms(1).toString shouldEqual "achicharrás"
    forms(1).superscripts should contain only "vos"


  @Test
  def testParseMultipleWithNestedSuperscripts(): Unit =
    val form = Form.parse(parseDocument(
      """
        <td>
          <span class="Latn form-of lang-es verb-form-achicharrar-form-of     origin-achicharrar   " lang="es">
            <a rel="mw:WikiLink" href="./achicharrés#Spanish" title="achicharrés">achicharrés</a>
          </span>
          <sup>
            <sup>
              vos
              <sup style="color:red">2</sup>
            </sup>
          </sup>
        </td>
      """)).head

    form.toString shouldEqual "achicharrés"
    form.superscripts should contain only ("vos", "2")
