package digero.wiktionary.parser.table

import digero.test.Fixture
import digero.wiktionary.parser.HTMLParser
import org.jsoup.nodes.Element
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class HTMLTableParserTest extends Fixture with HTMLParser:
    private lazy val tableElement: Element =
        parseDocument(fixture("parser/inflection/conjugation/es/es-achicharrar.html")).selectFirst("div.NavContent > table")
    private lazy val table = HTMLTableParser.parse(tableElement, sections = Seq(
        TableSection(0 to 4),
        TableSection(5 to 6, global = true, matchColspan = false),
        TableSection(7 to 22)
    ))

    @Test
    def parse_table_should_have_correct_length(): Unit =
        table should have length 23

    @Test
    def headersSingleHeader(): Unit =
        table.headers(row = 0, column = 4).map(_.content) should contain only
          TableHeaderContent("infinitive", Some("infinitivo"))

    @Test
    def headersMultipleHeaders(): Unit =
        table.headers(row = 3, column = 6).map(_.content) should contain only(
          TableHeaderContent("past participle", Some("participio (pasado)")),
          TableHeaderContent("singular", None),
          TableHeaderContent("feminine", Some("femenino"))
        )
        table(3)(6).toString should equal("achicharrada")

    @Test
    def headersMultipleHeaders2(): Unit =
        // indicative, first person singular, present
        table.headers(row = 8, column = 2).map(_.content) should contain only(
          TableHeaderContent("indicative", Some("indicativo")),
          TableHeaderContent("present", Some("presente de indicativo")),
          TableHeaderContent("singular", None),
          TableHeaderContent("1st person", None),
          TableHeaderContent("yo", None)
        )

    @Test
    def headersMultipleHeaders3(): Unit =
        // subjunctive, first person singular, imperfect (ra)
        table.headers(row = 16, column = 2).map(_.content) should contain only(
          TableHeaderContent("subjunctive", Some("subjuntivo")),
          TableHeaderContent("imperfect (ra)", Some("pretérito imperfecto de subjuntivo")),
          TableHeaderContent("singular", None),
          TableHeaderContent("1st person", None),
          TableHeaderContent("yo", None)
        )

    @Test
    def tableHeader(): Unit =
        (table(0)(0): @unchecked) match
            case TableHeader(TableHeaderContent("infinitive", Some("infinitivo")), _, _) =>

    @Test
    def tableDataWithSingleForm(): Unit =
        (table(8)(2): @unchecked) match
            case TableData(forms, _, _) =>
                forms.map(_.toString) should contain only "achicharro"

    @Test
    def tableDataWithMultipleForms(): Unit =
        (table(8)(3): @unchecked) match
            case TableData(forms, _, _) =>
                forms.map(_.toString) should contain only ("achicharras", "achicharrás")


class PortugueseHTMLTableParserTest extends Fixture with HTMLParser:
    private lazy val tableElement: Element =
        parseDocument(fixture("parser/inflection/conjugation/pt/pt-fazer.html")).selectFirst("div.NavContent > table")
    private lazy val table = HTMLTableParser.parse(tableElement, sections = Seq(
        TableSection(0 to 1, global = true),  // singular | first-person...
        TableSection(2 to 4),                 // Infinitive
        TableSection(5 to 6),                 // Gerund
        TableSection(7 to 9),                 // Past participle
        TableSection(10 to 16),               // Indicative
        TableSection(17 to 20),               // Subjunctive
        TableSection(21 to 23)                // Imperative
    ))

    @Test
    def testGlobalSection(): Unit =
        table.headers(8, 2).map(_.content.toString) should contain only(
            "Masculine", "Past participle", "Singular"
        )

    @Test
    def tableHeader(): Unit =
        (table(1)(1): @unchecked) match
            case TableHeader(TableHeaderContent("First-person (eu)", None), _, _) =>

        (table(2)(0): @unchecked) match
            case TableHeader(TableHeaderContent("Infinitive", Some("infinitivo")), _, _) =>

        (table(3)(0): @unchecked) match
            case TableHeader(TableHeaderContent("Impersonal", Some("infinitivo impessoal")), _, _) =>

        (table(5)(0): @unchecked) match
            case TableHeader(TableHeaderContent("Gerund", Some("gerúndio")), _, _) =>

        (table(8)(2): @unchecked) match
            case TableData(Seq(Form(link, _)), /* colSpan */ 3 , /* rowSpan */ 1) =>
                link.toString should equal("[[feito#Portuguese|null]]")

class FinnishHTMLTableParserTest extends Fixture with HTMLParser:
    private lazy val tableElement: Element =
        parseDocument(fixture("parser/inflection/conjugation/fi/fi-puhua.html")).selectFirst("table.inflection-table.fi-conj")

    private lazy val table = HTMLTableParser.parse(tableElement, sections = Seq(
        TableSection(51 to 64, columnRanges = Seq(0 to 3, 4 to 6)),
    ), formSelector = "span[lang]")


    @Test
    def testColumnRanges(): Unit =
        table.headers(53, 2).map(_.content.toString) should contain only(
          "1st", "infinitives"
        )

        table.headers(53, 5).map(_.content.toString) should contain only(
          "present", "participles", "active"
        )

        table.headers(53, 6).map(_.content.toString) should contain only(
          "present", "participles", "passive"
        )

        table.headers(57, 2).map(_.content.toString) should contain only(
          "2nd", "instructive", "infinitives", "active"
        )