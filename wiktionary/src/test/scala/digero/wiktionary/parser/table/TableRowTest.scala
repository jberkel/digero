package digero.wiktionary.parser.table

import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class TableRowTest:
    @Test
    def combine_no_rowspan(): Unit =
        val r1 = TableRow(TableHeader("a1"), TableHeader("b1"))
        val r2 = TableRow(TableHeader("a2"), TableHeader("b2"))
        r1.combine(r2) shouldEqual r1

    @Test
    def combine_with_rowspan(): Unit =
        val r1 = TableRow(TableHeader(TableHeaderContent("a1"), rowSpan = 3), TableHeader("b1"))
        val r2 = TableRow(TableHeader("a2"))

        r2.combine(r1) shouldEqual TableRow(TableHeader(TableHeaderContent("a1"), rowSpan = 2), TableHeader("a2"))

    @Test
    def combine_rowspan_maintains_position(): Unit =
        val r1 = TableRow(TableHeader("a1"), TableHeader(TableHeaderContent("b1"), rowSpan = 2))
        val r2 = TableRow(TableHeader("a2"))

        r2.combine(r1) shouldEqual TableRow(TableHeader("a2"), TableHeader("b1"))
