package digero.wiktionary.parser.table

import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class TableTest:
  @Test
  def expanded_no_expansion(): Unit =
    val a1 = TableHeader(TableHeaderContent("a1"))
    val b1 = TableHeader(TableHeaderContent("b1"))
    val a2 = TableHeader(TableHeaderContent("a2"))
    val b2 = TableHeader(TableHeaderContent("b2"))

    val table = Table(Seq(
      TableRow(a1, b1),
      TableRow(a2, b2)
    ))

    table.expanded should equal(table)

  @Test
  def expanded_single_colspan(): Unit =
    val a1 = TableHeader(TableHeaderContent("a1"), colSpan = 2)
    val a2 = TableHeader("a2")
    val b2 = TableHeader("b2")

    val table = Table(Seq(
      TableRow(a1),
      TableRow(a2, b2)
    ))

    val expanded = table.expanded
    expanded.rows should contain inOrder(
      TableRow(a1, a1),
      TableRow(a2, b2)
    )

  @Test
  def expanded_single_rowspan(): Unit =
    val a1 = TableHeader(TableHeaderContent("a1"), rowSpan = 2)

    val table = Table(Seq(TableRow(a1)))

    table.expanded.rows shouldEqual Seq(
      TableRow(a1)
    )

  @Test
  def expanded_rowspan(): Unit =
    val a1 = TableHeader(TableHeaderContent("a1"), rowSpan = 2)
    val b1 = TableHeader("b1")

    val table = Table(Seq(
      TableRow(a1),
      TableRow(b1)
    ))

    val expanded = table.expanded
    expanded.rows shouldEqual Seq(
      TableRow(a1),
      TableRow(a1.copy(a1.rowSpan - 1), b1)
    )

  @Test
  def expanded_rowspan_across_multiple_rows(): Unit =
    val a1 = TableHeader(TableHeaderContent("a1"), rowSpan = 3)
    val b1 = TableHeader("b1")
    val c1 = TableHeader("c1")

    val table = Table(Seq(
      TableRow(a1),
      TableRow(b1),
      TableRow(c1)
    ))

    val expanded = table.expanded
    expanded.rows shouldEqual Seq(
      TableRow(a1),
      TableRow(a1.copy(a1.rowSpan - 1), b1),
      TableRow(a1.copy(a1.rowSpan - 2), c1)
    )

  @Test
  def expanded_rowspan_and_colspan(): Unit =
    val a1 = TableHeader(TableHeaderContent("a1"), colSpan = 2, rowSpan = 3)
    val a2 = TableHeader("a2")
    val b1 = TableHeader("b1")
    val c1 = TableHeader("c1")

    val table = Table(Seq(
      TableRow(a1, a2),
      TableRow(b1),
      TableRow(c1)
    ))

    val expanded = table.expanded
    expanded.rows shouldEqual Seq(
      TableRow(a1, a1, a2),
      TableRow(a1.copy(a1.rowSpan - 1), a1.copy(a1.rowSpan - 1), b1),
      TableRow(a1.copy(a1.rowSpan - 2), a1.copy(a1.rowSpan - 2), c1)
    )
