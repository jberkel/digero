package digero.wiktionary.parser;

import digero.test.Fixture
import digero.wiktionary.model.{EntryName, Usex}
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class UsexParserTest extends Fixture, HTMLParser:
    private val subject = UsexParser()

    @Test
    def testParse_en_bar(): Unit =
        given EntryContext(EntryName("bar", "en"))
        val results = subject.parse(parseDocument(fixture("parser/usex/en-bar.html")))
        results should contain only Usex("The street was lined with all-night **bars**.", None, None, None)

    @Test
    def testParse_en_Honeyboy(): Unit =
        given EntryContext(EntryName("Honeyboy", "en"))
        val results = subject.parse(parseDocument(fixture("parser/usex/en-Honeyboy.html")))
        results should contain only Usex("""David "**Honeyboy**" Edwards""", None, None, None)

    @Test
    def testParseMissingExample(): Unit =
        given EntryContext(EntryName("empty", "en"))
        val results = subject.parse(parseDocument(fixture("parser/usex/missing_example.html")))
        results shouldBe empty

    @Test
    def testParse_ko_개(): Unit =
        given EntryContext(EntryName("개", "ko"))
        val results = subject.parse(parseDocument(fixture("parser/usex/ko-개.html")))
        results should contain only
          Usex("몇 **개** 필요해요?", Some("How many (things) do you need?"), Some("Myeot **gae** piryohaeyo?"), None)

    @Test
    def testParse_ru_устав(): Unit =
        given EntryContext(EntryName("устав", "ru"))
        val results = subject.parse(parseDocument(fixture("parser/usex/ru-устав.html")))
        results should contain only
          Usex(
              "В чужо́й монасты́рь со свои́м **уста́вом** не хо́дят",
              Some("when in Rome, do as the Romans do"),
              Some("V čužój monastýrʹ so svoím **ustávom** ne xódjat"),
              Some("You don't go to another monastery with your own charter")
          )

    @Test
    def testParseLegacy_en(): Unit =
        given EntryContext(EntryName("inventory", "en"))
        val results = subject.parse(parseDocument(fixture("parser/usex/en-legacy-inventory.html")))
        results should contain only Usex("""This month's **inventory** took nearly three days.""", None, None, None)