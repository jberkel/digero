package digero.wiktionary.parser

import digero.test.Fixture
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.scalatest.matchers.should.Matchers.*

class SimilarityTest extends Fixture:

  @Test
  def testComputeUnigramCounts(): Unit =
    Similarity.computeUnigramCounts("this is a thing and is good") should contain only(
      "a" -> 1,
      "and" -> 1,
      "good" -> 1,
      "is" -> 2,
      "thing" -> 1,
      "this" -> 1
    )

  @Test
  def testComputeTrigramCounts(): Unit =
     Similarity.computeTrigramCounts("this is a thing") should contain only(
      "_a_" -> 1,
      "_is" -> 1,
      "_th" -> 2,
      "hin" -> 1,
      "his" -> 1,
      "ing" -> 1,
      "is_" -> 2,
      "ng_" -> 1,
      "thi" -> 2
    )

  @Test
  def testSimilarityCompletelyEqualReturnsOne(): Unit =
    val text = "this is a test"
    val a = Similarity.computeUnigramCounts(text)
    val b = Similarity.computeUnigramCounts(text)
    Similarity.similarity(a, b) should equal(1)

  @Test
  def testSimilarityCompletelyDifferentReturnsZero(): Unit =
    val a = Similarity.computeUnigramCounts("some text")
    val b = Similarity.computeUnigramCounts("another one")
    Similarity.similarity(a, b) should equal(0)

  @Test
  def testSimilarity(): Unit =
    val a = Similarity.computeUnigramCounts("this is a test")
    val b = Similarity.computeUnigramCounts("this is a another test")
    Similarity.similarity(a, b) should equal(.8888 +- 0.001)

  @ParameterizedTest(name = "findMatching({0})")
  @CsvSource(value = Array("simple", "plain"))
  def testFindMostSimilar(fixtureName: String): Unit =
    val json = fixture(s"similarity/$fixtureName.json")
    val testcase: SimilarityTestcase = upickle.default.read(json)
    for
      (gloss, index) <- testcase.glosses
      mostSimilar = Similarity.findMostSimilar(gloss, testcase.senses)
    do
      withClue(s"unmatched gloss '$gloss':") {
        mostSimilar should not be empty
        testcase.senses.indexOf(mostSimilar.get) should equal(index)
      }

case class SimilarityTestcase(
 senses: Seq[String],
 glosses: Map[String, Int]
) derives upickle.default.Reader



