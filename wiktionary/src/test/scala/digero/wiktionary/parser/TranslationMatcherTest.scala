package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.serialization.SerializedEntry
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class TranslationMatcherTest extends Fixture:

  @Test def testMatchSimpleAdj(): Unit =
      // jq 'select(.pageTitle == "simple")' content_en.ndjson/*json
    matchInput(fixture("serialized/simple_adj.json")) should equal (
      Seq((6, "simple-minded"), (7, "uncomplicated"))
    )

  @Test def testMatchMovement(): Unit =
    matchInput(fixture("serialized/movement.json")) should equal (
      Seq(
        (0, "horology: device that cuts time in equal portions"),
        (2, "impression of motion"),
        (3, "trend in various fields or social categories"),
        (4, "music: division of a larger musical composition"),
        (6, "aviation: aircraft taking off or landing"),
        (7, "baseball: deviation of a pitch"),
        (9, "an act of emptying the bowels")
      )
    )


  @Test def testMatchAsystolic(): Unit =
    matchInput(fixture("serialized/asystolic.json")) should equal(
      Seq( (0, "pertaining to asystole"), (2, "not systolic"))
    )

  @Test def testMatchAniscoria(): Unit =
    matchInput(fixture("serialized/aniscoria.json")) should equal(
      Seq((0, "unequal size of the pupils"))
    )

  private def matchInput(input: String): Seq[(Int, String)] =
    val entry = SerializedEntry.fromJSON(input)
    TranslationMatcher.matchEntry(entry) match
      case Some(matchedEntry) =>
        matchedEntry.senses.zipWithIndex
          .filter(_._1.translationGloss.isDefined)
          .map((sense, index) => (index, sense.translationGloss.get))
      case None => Seq.empty
