package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.serialization.TextConverter
import digero.wiktionary.model.{EntryName, MultiTermTranslation, NormalizedTranslation, SimpleTranslation, Translation, TranslationGroup, TranslationSubpage}
import digero.wiktionary.parser.cmark.CommonMarkConverter
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import scala.compiletime.uninitialized

class TranslationParserTest extends Fixture:
  var subject: TranslationParser = uninitialized

  given TextConverter = CommonMarkConverter().htmlToCommonMark
  given EntryContext(EntryName("test", "en"))

  @BeforeEach
  def prepare(): Unit = subject = TranslationParser()

  @Test
  def testParseTranslationSectionWithLinkToSubpage(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/translation_section_link_to_subpage.html")
    )

    translations match
      case TranslationGroup("", Nil, Some(TranslationSubpage("test", Some("Noun")))) :: Nil =>

  @Test
  def testParseTranslationSection(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/simple_translation_section.html")
    )
    translations match
      case TranslationGroup(gloss1, translations1, None) ::
           TranslationGroup(gloss2, translations2, None) :: Nil =>

          gloss1 should equal("uncomplicated")
          gloss2 should equal("simple-minded")

          translations1 should have length 174
          translations2 should have length 31

          translations1.take(3) match
            case Translation("af", SimpleTranslation("eenvoudig"), None, "Latn", _, false) ::
                 Translation("sq", SimpleTranslation("thjeshtë"), None, "Latn", _, false) ::
                 Translation("ar", NormalizedTranslation("بَسِيط", "بسيط"), Some("basīṭ"), "Arab", _, false) :: Nil =>

          translations1.reverse.take(3) match
            case Translation("zza", SimpleTranslation("gerges"), None, "Latn", _, false) ::
                 Translation("yi", SimpleTranslation("פּשוט"), Some("poshet"), "Hebr", _, false) ::
                 Translation("fy", SimpleTranslation("ienfâldich"), None, "Latn", _, false) :: Nil =>

          translations2.take(3) match
            case Translation("hy", SimpleTranslation("պարզամիտ"), Some("parzamit"), "Armn", _, false) ::
                 Translation("hy", SimpleTranslation("միամիտ"), Some("miamit"), "Armn", _, false) ::
                 Translation("bg", SimpleTranslation("наивен"), Some("naiven"), "Cyrl", _, false) :: Nil =>

          translations2.reverse.take(3) match
            case Translation("sv", SimpleTranslation("enkel"), None, "Latn", _, false) ::
                 Translation("sh", SimpleTranslation("prostodušan"), None, "Latn", _, false) ::
                 Translation("sh", SimpleTranslation("простодушан"), None, "Cyrl", _, false) :: Nil =>

  @Test
  def testParsesNormalizedTranslation(): Unit =
    // <a rel="mw:WikiLink" href="./простодушный#Russian" title="простодушный">простоду́шный</a
    subject.parseTranslationSection(
      fixture("parser/translation/simple_translation_section.html")
    ).find(_.gloss == "simple-minded")
     .map(_.translations.filter(_.language == "ru"))
     .get should contain(
      Translation("ru", NormalizedTranslation("простоду́шный","простодушный"), Some("prostodúšnyj"), "Cyrl", None, false)
    )

  @Test
  def testParseNonIdiomaticTranslation(): Unit =
    val translations = subject.parseTranslationSection(
        fixture("parser/translation/high_island.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language == "de") should contain only
      Translation("de", SimpleTranslation("Vulkaninsel"), None, "Latn", None, false)

    translations.head.translations.filter(_.language == "it") should contain only
      Translation("it", MultiTermTranslation("[isola](./isola#Italian \"isola\") [vulcanica](./vulcanica#Italian \"vulcanica\")"), None, "Latn", None, false)

  @Test
  def testParseNonIdiomaticTranslationWithSingleLink(): Unit =
    val groups = subject.parseTranslationSection(
      fixture("parser/translation/teotwawki.html")
    )
    
    val multiTerms = for
      group <- groups
      translation <- group.translations
      case MultiTermTranslation(commonmark) <- Seq(translation.content)
    yield
      (translation.language, commonmark)

    multiTerms should have length 8
    multiTerms.take(3) should contain allOf(
      ("ca", """la fi del [món](./món#Catalan "món") tal com el coneixem"""),
      ("nl", """het einde van de [wereld](./wereld#Dutch "wereld") zoals we haar kennen"""),
      ("fr", """la fin du [monde](./monde#French "monde") tel que nous le connaissons"""),
    )

    val simpleTerms = for
      group <- groups
      translation <- group.translations
      case SimpleTranslation(commonmark) <- Seq(translation.content)
    yield
      (translation.language, commonmark)

    simpleTerms should contain only (
      ("eo", "la finiĝo de la mondo kiel ni scias ĝin"),
    )

  @Test
  def testParseTranslationWithAttention(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/penumbra.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language == "he") should contain only
      Translation("he", NormalizedTranslation("פְּלַג צֵל", "פלג צל"), None, "Hebr", None, false)

  @Test
  def testParseTranslationChineseVariants(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/Cagayan.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language.startsWith("zh")) should contain only(
      Translation("zh-Hans", SimpleTranslation("卡加延德奥罗"), None, "Hans", None, false),
      Translation("zh-Hant", SimpleTranslation("卡加延德奧羅"), None, "Hant", None, false)
    )

  @Test
  def testParseTranslationIgnoresEmptyLinks(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/Cor_Caroli.html")
    )

    translations should have length 1
    translations.head.translations.filter(_.language == "fr") shouldBe empty
    translations.head.translations.filter(_.language == "it") shouldBe empty

  @Test
  def testSkipTranslationToBeChecked(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/to_be_checked.html")
    )

    translations.map(_.gloss) should contain only "with great ability"

  @Test
  def testGlossWithMarkup(): Unit =
    val translations = subject.parseTranslationSection(
      fixture("parser/translation/libre.html")
    )

    translations.map(_.gloss) should contain only "(software): with very few limitations on distribution or improvement"
