package digero.wiktionary.parser.inflection.fr

import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.inflection.{InflectionParserTestBase, ParsedInflection}
import org.junit.jupiter.api.{Disabled, Test}
import org.scalatest.matchers.should.Matchers.*
import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.GrammaticalFeature.*

class InflectionParserTest extends InflectionParserTestBase:
  given EntryContext(EntryName("test", "fr"))

  @Test
  @Disabled // not linked, just plain text
  def testParseInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))
    parsedInflections should contain(ParsedInflection("faire", Set(Infinitive)))

  @Test
  def testParseGerund(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))
    parsedInflections should contain(ParsedInflection("faisant", Set(Gerund)))

  @Test
  def testParsePastParticiple(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))
    parsedInflections should contain(ParsedInflection("fait", Set(Participle)))

  @Test
  def testParseIndicativePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))

    val indicativePresent = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense).subsetOf(inflection.features)
    )
    indicativePresent should contain only(
      ParsedInflection("fais", Set(Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("fais", Set(Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("fait", Set(Indicative, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("faisons", Set(Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("faites", Set(Indicative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("font", Set(Indicative, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativeImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, PastImperfect).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("faisais", Set(Indicative, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("faisais", Set(Indicative, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("faisait", Set(Indicative, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("faisions", Set(Indicative, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("faisiez", Set(Indicative, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("faisaient", Set(Indicative, PastImperfect, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativePastHistoric(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, Preterite).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("fis", Set(Indicative, Preterite, FirstPerson, Singular)),
      ParsedInflection("fis", Set(Indicative, Preterite, SecondPerson, Singular)),
      ParsedInflection("fit", Set(Indicative, Preterite, ThirdPerson, Singular)),
      ParsedInflection("fîmes", Set(Indicative, Preterite, FirstPerson, Plural)),
      ParsedInflection("fîtes", Set(Indicative, Preterite, SecondPerson, Plural)),
      ParsedInflection("firent", Set(Indicative, Preterite, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativeFuture(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, FutureTense).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("ferai", Set(Indicative, FutureTense, FirstPerson, Singular)),
      ParsedInflection("feras", Set(Indicative, FutureTense, SecondPerson, Singular)),
      ParsedInflection("fera", Set(Indicative, FutureTense, ThirdPerson, Singular)),
      ParsedInflection("ferons", Set(Indicative, FutureTense, FirstPerson, Plural)),
      ParsedInflection("ferez", Set(Indicative, FutureTense, SecondPerson, Plural)),
      ParsedInflection("feront", Set(Indicative, FutureTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseConditional(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, SimpleConditional).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("ferais", Set(Indicative, SimpleConditional, FirstPerson, Singular)),
      ParsedInflection("ferais", Set(Indicative, SimpleConditional, SecondPerson, Singular)),
      ParsedInflection("ferait", Set(Indicative, SimpleConditional, ThirdPerson, Singular)),
      ParsedInflection("ferions", Set(Indicative, SimpleConditional, FirstPerson, Plural)),
      ParsedInflection("feriez", Set(Indicative, SimpleConditional, SecondPerson, Plural)),
      ParsedInflection("feraient", Set(Indicative, SimpleConditional, ThirdPerson, Plural)),
    )

  @Test
  def testParseSubjunctivePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))
    val subjunctivePresent = parsedInflections.filter(
      inflection => Set(Subjunctive, PresentTense).subsetOf(inflection.features)
    )
    subjunctivePresent should contain only(
      ParsedInflection("fasse", Set(Subjunctive, PresentTense, FirstPerson, Singular)),
      ParsedInflection("fasses", Set(Subjunctive, PresentTense, SecondPerson, Singular)),
      ParsedInflection("fasse", Set(Subjunctive, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("fassions", Set(Subjunctive, PresentTense, FirstPerson, Plural)),
      ParsedInflection("fassiez", Set(Subjunctive, PresentTense, SecondPerson, Plural)),
      ParsedInflection("fassent", Set(Subjunctive, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseSubjunctiveImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))
    val subjunctivePresent = parsedInflections.filter(
      inflection => Set(Subjunctive, PastImperfect).subsetOf(inflection.features)
    )
    subjunctivePresent should contain only(
      ParsedInflection("fisse", Set(Subjunctive, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("fisses", Set(Subjunctive, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("fît", Set(Subjunctive, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("fissions", Set(Subjunctive, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("fissiez", Set(Subjunctive, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("fissent", Set(Subjunctive, PastImperfect, ThirdPerson, Plural)),
    )

  @Test
  def testParseImperative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-faire.html"))

    val imperative = parsedInflections.filter(_.features.contains(Imperative))
    imperative should contain only(
      ParsedInflection("fais", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("faisons", Set(Imperative, FirstPerson, Plural)),
      ParsedInflection("faites", Set(Imperative, SecondPerson, Plural)),
    )

  // Some  verbs have alternative imperatives
  @Test
  def testParseAlternativeImperative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-vouloir.html"))

    val imperative = parsedInflections.filter(_.features.contains(Imperative))
    imperative should contain only(
      ParsedInflection("veux", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("veuille", Set(Imperative, SecondPerson, Singular)),

      ParsedInflection("voulons", Set(Imperative, FirstPerson, Plural)),
      ParsedInflection("veuillons", Set(Imperative, FirstPerson, Plural)),

      ParsedInflection("voulez", Set(Imperative, SecondPerson, Plural)),
      ParsedInflection("veuillez", Set(Imperative, SecondPerson, Plural)),
    )

  // Some  verbs lack imperatives
  @Test
  def testParseImperativeNotDefined(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-pouvoir.html"))

    val imperative = parsedInflections.filter(_.features.contains(Imperative))
    imperative shouldBe empty

  // Some  verbs (asseoir, essayer, payer, balayer, etc.) have
  // two conjugations to choose from
  @Test
  def testParseAlternativeIndicativePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-payer.html"))

    val indicativePresent = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense).subsetOf(inflection.features)
    )
    indicativePresent should contain only(
      ParsedInflection("paye", Set(Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("paie", Set(Indicative, PresentTense, FirstPerson, Singular)),

      ParsedInflection("payes", Set(Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("paies", Set(Indicative, PresentTense, SecondPerson, Singular)),

      ParsedInflection("paye", Set(Indicative, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("paie", Set(Indicative, PresentTense, ThirdPerson, Singular)),

      ParsedInflection("payons", Set(Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("payez", Set(Indicative, PresentTense, SecondPerson, Plural)),

      ParsedInflection("payent", Set(Indicative, PresentTense, ThirdPerson, Plural)),
      ParsedInflection("paient", Set(Indicative, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativePresentAvoir(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-avoir.html"))

    val indicativePresent = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense).subsetOf(inflection.features)
    )
    indicativePresent should contain only(
      ParsedInflection("ai", Set(Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("as", Set(Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("a", Set(Indicative, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("avons", Set(Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("avez", Set(Indicative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("ont", Set(Indicative, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseImperativeAvoir(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fr/fr-avoir.html"))

    val imperative = parsedInflections.filter(_.features.contains(Imperative))
    imperative should contain only(
      ParsedInflection("aie", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("ayons", Set(Imperative, FirstPerson, Plural)),
      ParsedInflection("ayez", Set(Imperative, SecondPerson, Plural)),
    )