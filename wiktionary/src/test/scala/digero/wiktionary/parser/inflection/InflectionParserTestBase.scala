package digero.wiktionary.parser.inflection

import digero.test.Fixture
import digero.wiktionary.model.serialization.CustomPickler
import org.junit.jupiter.api.BeforeEach

import scala.compiletime.uninitialized

class InflectionParserTestBase extends Fixture:
  protected var subject: InflectionParser = uninitialized

  @BeforeEach
  def prepare(): Unit = subject = InflectionParser()

  def loadExpected(name: String): Seq[ParsedInflection] =
    CustomPickler.read[Map[String, Seq[ParsedInflection]]](fixture(name))
      .values
      .flatten
      .toSeq

