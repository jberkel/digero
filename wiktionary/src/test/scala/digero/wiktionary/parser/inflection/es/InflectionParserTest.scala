package digero.wiktionary.parser.inflection.es

import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.GrammaticalFeature.Ustedeo
import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.inflection.InflectionParserTestBase
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class InflectionParserTest extends InflectionParserTestBase:
  given EntryContext(EntryName("test", "es"))

  @Test
  def testParse(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/es/es-achicharrar.html"))
    val expectedInflections = loadExpected("parser/inflection/conjugation/es/es-achicharrar.json")

    for
      expected <- expectedInflections if !expected.features.contains(Ustedeo)
    do
      parsedInflections should contain(expected)
      // TODO more idiomatic matcher (contain once) ?
      parsedInflections.filter(_ == expected) should have length 1


