package digero.wiktionary.parser.inflection.fi

import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.model.lexical.GrammaticalFeature.*
import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.inflection.{InflectionParserTestBase, ParsedInflection}
import org.junit.jupiter.api.{Disabled, Test}
import org.scalatest.matchers.should.Matchers.*

class InflectionParserTest extends InflectionParserTestBase:
  given EntryContext(EntryName("test", "fi"))

  @Test
  def testParseIndicativePresentPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhun", Set(Positive, Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("puhut", Set(Positive, Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhuu", Set(Positive, Indicative, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhumme", Set(Positive, Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhutte", Set(Positive, Indicative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhuvat", Set(Positive, Indicative, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhutaan", Set(Positive, Indicative, PresentTense, Passive))
    )

  @Test
  def testParseIndicativePresentNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentNegative = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense, Negative).subsetOf(inflection.features)
    )

    indicativePresentNegative should contain only(
      // positive singular
      ParsedInflection("puhu", Set(Negative, Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("puhu", Set(Negative, Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhu", Set(Negative, Indicative, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhu", Set(Negative, Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhu", Set(Negative, Indicative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhu", Set(Negative, Indicative, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuta", Set(Negative, Indicative, PresentTense, Passive))
    )


  @Test
  def testParseIndicativePerfectPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Indicative, Perfect, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Positive, Indicative, Perfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Indicative, Perfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Indicative, Perfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Positive, Indicative, Perfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Indicative, Perfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Indicative, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Positive, Indicative, Perfect, Passive))
    )

  @Test
  def testParseIndicativePerfectNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentNegative = parsedInflections.filter(
      inflection => Set(Indicative, Perfect, Negative).subsetOf(inflection.features)
    )

    indicativePresentNegative should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Negative, Indicative, Perfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Indicative, Perfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Indicative, Perfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Negative, Indicative, Perfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Indicative, Perfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Indicative, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Negative, Indicative, Perfect, Passive))
    )

  @Test
  def testParseIndicativePastTensePositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Indicative, PastImperfect, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhuin", Set(Positive, Indicative, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("puhuit", Set(Positive, Indicative, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("puhui", Set(Positive, Indicative, PastImperfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuimme", Set(Positive, Indicative, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("puhuitte", Set(Positive, Indicative, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("puhuivat", Set(Positive, Indicative, PastImperfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttiin", Set(Positive, Indicative, PastImperfect, Passive))
    )

  @Test
  def testParseIndicativePastTenseNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Indicative, PastImperfect, Negative).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Negative, Indicative, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Indicative, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Indicative, PastImperfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Negative, Indicative, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Indicative, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Indicative, PastImperfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Negative, Indicative, PastImperfect, Passive))
    )

  @Test
  def testParseIndicativePluperfectPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePluperfectPositive = parsedInflections.filter(
      inflection => Set(Indicative, Pluperfect, Positive).subsetOf(inflection.features)
    )

    indicativePluperfectPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Positive, Indicative, Pluperfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Indicative, Pluperfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Indicative, Pluperfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Positive, Indicative, Pluperfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Indicative, Pluperfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Indicative, Pluperfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Positive, Indicative, Pluperfect, Passive))
    )

  @Test
  def testParseIndicativePluperfectNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePluperfectPositive = parsedInflections.filter(
      inflection => Set(Indicative, Pluperfect, Negative).subsetOf(inflection.features)
    )

    indicativePluperfectPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Negative, Indicative, Pluperfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Indicative, Pluperfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Indicative, Pluperfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Negative, Indicative, Pluperfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Indicative, Pluperfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Indicative, Pluperfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Negative, Indicative, Pluperfect, Passive))
    )

  @Test
  def testParseConditionalPresentPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Conditional, PresentTense, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhuisin", Set(Positive, Conditional, PresentTense, FirstPerson, Singular)),
      ParsedInflection("puhuisit", Set(Positive, Conditional, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhuisi", Set(Positive, Conditional, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuisimme", Set(Positive, Conditional, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhuisitte", Set(Positive, Conditional, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhuisivat", Set(Positive, Conditional, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttaisiin", Set(Positive, Conditional, PresentTense, Passive))
    )

  @Test
  def testParseConditionalPresentNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Conditional, PresentTense, Negative).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhuisi", Set(Negative, Conditional, PresentTense, FirstPerson, Singular)),
      ParsedInflection("puhuisi", Set(Negative, Conditional, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhuisi", Set(Negative, Conditional, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuisi", Set(Negative, Conditional, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhuisi", Set(Negative, Conditional, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhuisi", Set(Negative, Conditional, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttaisi", Set(Negative, Conditional, PresentTense, Passive))
    )


  @Test
  def testParseConditionalPerfectPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Conditional, Perfect, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Positive, Conditional, Perfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Conditional, Perfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Conditional, Perfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Positive, Conditional, Perfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Conditional, Perfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Conditional, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Positive, Conditional, Perfect, Passive))
    )

  @Test
  def testParseConditionalPerfectNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Conditional, Perfect, Negative).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Negative, Conditional, Perfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Conditional, Perfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Conditional, Perfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Negative, Conditional, Perfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Conditional, Perfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Conditional, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Negative, Conditional, Perfect, Passive))
    )


  @Test
  def testParseImperativePresentPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Imperative, PresentTense, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      // –
      ParsedInflection("puhu", Set(Positive, Imperative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhukoon", Set(Positive, Imperative, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhukaamme", Set(Positive, Imperative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhukaa", Set(Positive, Imperative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhukoot", Set(Positive, Imperative, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttakoon", Set(Positive, Imperative, PresentTense, Passive))
    )

  @Test
  def testParseImperativePresentNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Imperative, PresentTense, Negative).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      // –
      ParsedInflection("puhu", Set(Negative, Imperative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhuko", Set(Negative, Imperative, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuko", Set(Negative, Imperative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhuko", Set(Negative, Imperative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhuko", Set(Negative, Imperative, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttako", Set(Negative, Imperative, PresentTense, Passive))
    )


  @Test
  def testParseImperativePerfectPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Imperative, Perfect, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      // –
      // –
      ParsedInflection("puhunut", Set(Positive, Imperative, Perfect, ThirdPerson, Singular)),
      // positive plural
      // –
      // –
      ParsedInflection("puhuneet", Set(Positive, Imperative, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Positive, Imperative, Perfect, Passive))
    )

  @Test
  def testParseImperativePerfectNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Imperative, Perfect, Negative).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      // –
      // –
      ParsedInflection("puhunut", Set(Negative, Imperative, Perfect, ThirdPerson, Singular)),
      // positive plural
      // –
      // –
      ParsedInflection("puhuneet", Set(Negative, Imperative, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Negative, Imperative, Perfect, Passive))
    )

  @Test
  def testParsePotentialPresentPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Potential, PresentTense, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhunen", Set(Positive, Potential, PresentTense, FirstPerson, Singular)),
      ParsedInflection("puhunet", Set(Positive, Potential, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhunee", Set(Positive, Potential, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhunemme", Set(Positive, Potential, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhunette", Set(Positive, Potential, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhunevat", Set(Positive, Potential, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttaneen", Set(Positive, Potential, PresentTense, Passive))
    )

  @Test
  def testParsePotentialPresentNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Potential, PresentTense, Negative).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhune", Set(Negative, Potential, PresentTense, FirstPerson, Singular)),
      ParsedInflection("puhune", Set(Negative, Potential, PresentTense, SecondPerson, Singular)),
      ParsedInflection("puhune", Set(Negative, Potential, PresentTense, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhune", Set(Negative, Potential, PresentTense, FirstPerson, Plural)),
      ParsedInflection("puhune", Set(Negative, Potential, PresentTense, SecondPerson, Plural)),
      ParsedInflection("puhune", Set(Negative, Potential, PresentTense, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttane", Set(Negative, Potential, PresentTense, Passive))
    )


  @Test
  def testParsePotentialPerfectPositive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Potential, Perfect, Positive).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Positive, Potential, Perfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Potential, Perfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Positive, Potential, Perfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Positive, Potential, Perfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Potential, Perfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Positive, Potential, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Positive, Potential, Perfect, Passive))
    )

  @Test
  def testParsePotentialPerfectNegative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val indicativePresentPositive = parsedInflections.filter(
      inflection => Set(Potential, Perfect, Negative).subsetOf(inflection.features)
    )

    indicativePresentPositive should contain only(
      // positive singular
      ParsedInflection("puhunut", Set(Negative, Potential, Perfect, FirstPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Potential, Perfect, SecondPerson, Singular)),
      ParsedInflection("puhunut", Set(Negative, Potential, Perfect, ThirdPerson, Singular)),
      // positive plural
      ParsedInflection("puhuneet", Set(Negative, Potential, Perfect, FirstPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Potential, Perfect, SecondPerson, Plural)),
      ParsedInflection("puhuneet", Set(Negative, Potential, Perfect, ThirdPerson, Plural)),
      // positive passive
      ParsedInflection("puhuttu", Set(Negative, Potential, Perfect, Passive))
    )

  @Test
  def testParseParticiples(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val participles = parsedInflections.filter(
      inflection => Set(Participle).subsetOf(inflection.features)
    )
    participles should contain only(
      ParsedInflection("puhuva", Set(PresentTense, Participle, Active)),
      ParsedInflection("puhuttava", Set(PresentTense, Participle, Passive)),
      ParsedInflection("puhunut", Set(PastImperfect, Participle, Active)),
      ParsedInflection("puhuttu", Set(PastImperfect, Participle, Passive)),

      ParsedInflection("puhuma", Set(Agent, Participle)),
      ParsedInflection("puhumaton", Set(Negative, Participle))
  )

  @Test
  def testParseFirstInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val firstInfinitives = parsedInflections.filter(
      inflection => Set(FirstInfinitive).subsetOf(inflection.features)
    )
    firstInfinitives should contain only ParsedInflection("puhua", Set(Infinitive, FirstInfinitive))

  @Test
  @Disabled
  def testParseLongFirstInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val longFirstInfinitives = parsedInflections.filter(
      inflection => Set(LongFirstInfinitive).subsetOf(inflection.features)
    )
    longFirstInfinitives should contain only(
      ParsedInflection("puhuakseni", Set(Infinitive, LongFirstInfinitive, FirstPerson, Singular)),
      ParsedInflection("puhuaksemme", Set(Infinitive, LongFirstInfinitive, FirstPerson, Plural)),
      ParsedInflection("puhuaksesi", Set(Infinitive, LongFirstInfinitive, SecondPerson, Singular)),
      ParsedInflection("puhuaksenne", Set(Infinitive, LongFirstInfinitive, SecondPerson, Plural)),
      ParsedInflection("puhuakseen", Set(Infinitive, LongFirstInfinitive, ThirdPerson, Singular, Plural)),
      ParsedInflection("puhuaksensa", Set(Infinitive, LongFirstInfinitive, ThirdPerson, Singular, Plural)),
    )

  @Test
  def testParseSecondInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val secondInfinitives = parsedInflections.filter(
      inflection => Set(SecondInfinitive).subsetOf(inflection.features)
    )
    secondInfinitives should contain only(
      ParsedInflection("puhuessa", Set(Infinitive, SecondInfinitive, Inessive, Active)),
      ParsedInflection("puhuttaessa", Set(Infinitive, SecondInfinitive, Inessive, Passive)),
      ParsedInflection("puhuen", Set(Infinitive, SecondInfinitive, Instructive, Active))
  )

  @Test
  def testParseThirdInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val thirdInfinitives = parsedInflections.filter(
      inflection => Set(ThirdInfinitive).subsetOf(inflection.features)
    )
    thirdInfinitives should contain only(
      ParsedInflection("puhuman", Set(Infinitive, ThirdInfinitive, Instructive, Active)),
      ParsedInflection("puhuttaman", Set(Infinitive, ThirdInfinitive, Instructive, Passive)),
      ParsedInflection("puhumassa", Set(Infinitive, ThirdInfinitive, Inessive, Active)),
      ParsedInflection("puhumasta", Set(Infinitive, ThirdInfinitive, Elative, Active)),
      ParsedInflection("puhumaan", Set(Infinitive, ThirdInfinitive, Illative, Active)),
      ParsedInflection("puhumalla", Set(Infinitive, ThirdInfinitive, Adessive, Active)),
      ParsedInflection("puhumatta", Set(Infinitive, ThirdInfinitive, Abessive, Active))
    )

  @Test
  def testParseFourthInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val fourthInfinitives = parsedInflections.filter(
      inflection => Set(FourthInfinitive).subsetOf(inflection.features)
    )
    fourthInfinitives should contain only
      ParsedInflection("puhuminen", Set(Infinitive, FourthInfinitive, VerbalNoun))


  @Test @Disabled
  def testParseFifthInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/fi/fi-puhua.html"))

    val fifthInfinitives = parsedInflections.filter(
      inflection => Set(FifthInfinitive).subsetOf(inflection.features)
    )
    fifthInfinitives should contain only(
      ParsedInflection("puhumaisillani", Set(Infinitive, FifthInfinitive, FirstPerson, Singular)),
      ParsedInflection("puhumaisillamme", Set(Infinitive, FifthInfinitive, FirstPerson, Plural)),
      ParsedInflection("puhumaisillasi", Set(Infinitive, FifthInfinitive, SecondPerson, Singular)),
      ParsedInflection("puhumaisillanne", Set(Infinitive, FifthInfinitive, SecondPerson, Plural)),
      ParsedInflection("puhumaisillaan", Set(Infinitive, FifthInfinitive, ThirdPerson, Singular, Plural)),
      ParsedInflection("puhumaisillansa", Set(Infinitive, FifthInfinitive, ThirdPerson, Singular, Plural)),
    )
