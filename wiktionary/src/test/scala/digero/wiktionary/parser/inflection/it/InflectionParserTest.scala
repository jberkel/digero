package digero.wiktionary.parser.inflection.it

import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.GrammaticalFeature.*
import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.inflection.{InflectionParserTestBase, ParsedInflection}
import org.junit.jupiter.api.{Disabled, Test}
import org.scalatest.matchers.should.Matchers.*

class InflectionParserTest extends InflectionParserTestBase:
  given EntryContext(EntryName("test", "it"))

  @Test
  def testInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))
    parsedInflections should contain(ParsedInflection("fare", Set(Infinitive)))

  @Test
  def testParseIndicativePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))

    val indicativePresent = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense).subsetOf(inflection.features)
    )
    indicativePresent should contain only(
      ParsedInflection("faccio", Set(Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("fai", Set(Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("fa", Set(Indicative, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("facciamo", Set(Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("fate", Set(Indicative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("fanno", Set(Indicative, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativeImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, PastImperfect).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("facevo", Set(Indicative, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("facevi", Set(Indicative, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("faceva", Set(Indicative, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("facevamo", Set(Indicative, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("facevate", Set(Indicative, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("facevano", Set(Indicative, PastImperfect, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativePastHistoric(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, Preterite).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("feci", Set(Indicative, Preterite, FirstPerson, Singular)),
      ParsedInflection("facesti", Set(Indicative, Preterite, SecondPerson, Singular)),
      ParsedInflection("fece", Set(Indicative, Preterite, ThirdPerson, Singular)),
      ParsedInflection("facemmo", Set(Indicative, Preterite, FirstPerson, Plural)),
      ParsedInflection("faceste", Set(Indicative, Preterite, SecondPerson, Plural)),
      ParsedInflection("fecero", Set(Indicative, Preterite, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativeFuture(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, FutureTense).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("farò", Set(Indicative, FutureTense, FirstPerson, Singular)),
      ParsedInflection("farai", Set(Indicative, FutureTense, SecondPerson, Singular)),
      ParsedInflection("farà", Set(Indicative, FutureTense, ThirdPerson, Singular)),
      ParsedInflection("faremo", Set(Indicative, FutureTense, FirstPerson, Plural)),
      ParsedInflection("farete", Set(Indicative, FutureTense, SecondPerson, Plural)),
      ParsedInflection("faranno", Set(Indicative, FutureTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseConditional(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, SimpleConditional).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("farei", Set(Indicative, SimpleConditional, FirstPerson, Singular)),
      ParsedInflection("faresti", Set(Indicative, SimpleConditional, SecondPerson, Singular)),
      ParsedInflection("farebbe", Set(Indicative, SimpleConditional, ThirdPerson, Singular)),
      ParsedInflection("faremmo", Set(Indicative, SimpleConditional, FirstPerson, Plural)),
      ParsedInflection("fareste", Set(Indicative, SimpleConditional, SecondPerson, Plural)),
      ParsedInflection("farebbero", Set(Indicative, SimpleConditional, ThirdPerson, Plural)),
    )

  @Test
  def testParseSubjunctivePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))
    val subjunctivePresent = parsedInflections.filter(
      inflection => Set(Subjunctive, PresentTense).subsetOf(inflection.features)
    )
    subjunctivePresent should contain only(
      ParsedInflection("faccia", Set(Subjunctive, PresentTense, FirstPerson, Singular)),
      ParsedInflection("faccia", Set(Subjunctive, PresentTense, SecondPerson, Singular)),
      ParsedInflection("faccia", Set(Subjunctive, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("facciamo", Set(Subjunctive, PresentTense, FirstPerson, Plural)),
      ParsedInflection("facciate", Set(Subjunctive, PresentTense, SecondPerson, Plural)),
      ParsedInflection("facciano", Set(Subjunctive, PresentTense, ThirdPerson, Plural)),
    )


  @Test
  def testParseSubjunctiveImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))
    val subjunctivePresent = parsedInflections.filter(
      inflection => Set(Subjunctive, PastImperfect).subsetOf(inflection.features)
    )
    subjunctivePresent should contain only(
      ParsedInflection("facessi", Set(Subjunctive, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("facessi", Set(Subjunctive, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("facesse", Set(Subjunctive, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("facessimo", Set(Subjunctive, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("faceste", Set(Subjunctive, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("facessero", Set(Subjunctive, PastImperfect, ThirdPerson, Plural)),
    )


  @Test
  def testParseImperativeAffirmative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))

    val imperative = parsedInflections.filter(_.features.contains(Imperative))
    imperative should contain only(
      ParsedInflection("fai", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("fa'", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("faccia", Set(Imperative, ThirdPerson, Singular)),
      ParsedInflection("facciamo", Set(Imperative, FirstPerson, Plural)),
      ParsedInflection("fate", Set(Imperative, SecondPerson, Plural)),
      ParsedInflection("facciano", Set(Imperative, ThirdPerson, Plural)),
    )

  @Test @Disabled // has 2 headers
  def testParsePastParticiple(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/it/it-fare.html"))
  
    val participle = parsedInflections.filter(_.features.contains(Participle))
    participle should contain only ParsedInflection("fatto", Set(Participle, Masculine, Singular))
