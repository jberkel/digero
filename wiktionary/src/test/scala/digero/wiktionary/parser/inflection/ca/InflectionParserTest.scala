package digero.wiktionary.parser.inflection.ca

import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.inflection.{InflectionParserTestBase, ParsedInflection}
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*
import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.GrammaticalFeature.*

class InflectionParserTest extends InflectionParserTestBase:
  given EntryContext(EntryName("test", "ca"))
  
  @Test
  def testInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))
    parsedInflections should contain(ParsedInflection("fer", Set(Infinitive)))

  @Test
  def testGerund(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))
    parsedInflections should contain(ParsedInflection("fent", Set(Gerund)))

  @Test
  def testParseParticiples(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val participle = parsedInflections.filter(_.features.contains(Participle))
    participle should contain only(
      ParsedInflection("fet", Set(Participle, Masculine, Singular)),
      ParsedInflection("fets", Set(Participle, Masculine, Plural)),
      ParsedInflection("feta", Set(Participle, Feminine, Singular)),
      ParsedInflection("fetes", Set(Participle, Feminine, Plural))
    )

  @Test
  def testIndicativePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val indicativePresent = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense).subsetOf(inflection.features)
    )
    indicativePresent should contain only(
      ParsedInflection("faig", Set(Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("fas", Set(Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("fa", Set(Indicative, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("fem", Set(Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("feu", Set(Indicative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("fan", Set(Indicative, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testIndicativeImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, PastImperfect).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("feia", Set(Indicative, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("feies", Set(Indicative, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("feia", Set(Indicative, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("fèiem", Set(Indicative, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("fèieu", Set(Indicative, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("feien", Set(Indicative, PastImperfect, ThirdPerson, Plural)),
    )

  @Test
  def testIndicativeFuture(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val indicativeFuture = parsedInflections.filter(
      inflection => Set(Indicative, FutureTense).subsetOf(inflection.features)
    )
    indicativeFuture should contain only(
      ParsedInflection("faré", Set(Indicative, FutureTense, FirstPerson, Singular)),
      ParsedInflection("faràs", Set(Indicative, FutureTense, SecondPerson, Singular)),
      ParsedInflection("farà", Set(Indicative, FutureTense, ThirdPerson, Singular)),
      ParsedInflection("farem", Set(Indicative, FutureTense, FirstPerson, Plural)),
      ParsedInflection("fareu", Set(Indicative, FutureTense, SecondPerson, Plural)),
      ParsedInflection("faran", Set(Indicative, FutureTense, ThirdPerson, Plural)),
    )

  @Test
  def testIndicativePreterite(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val indicativePreterite = parsedInflections.filter(
      inflection => Set(Indicative, Preterite).subsetOf(inflection.features)
    )
    indicativePreterite should contain only(
      ParsedInflection("fiu", Set(Indicative, Preterite, FirstPerson, Singular)),
      ParsedInflection("feres", Set(Indicative, Preterite, SecondPerson, Singular)),
      ParsedInflection("feu", Set(Indicative, Preterite, ThirdPerson, Singular)),
      ParsedInflection("férem", Set(Indicative, Preterite, FirstPerson, Plural)),
      ParsedInflection("féreu", Set(Indicative, Preterite, SecondPerson, Plural)),
      ParsedInflection("feren", Set(Indicative, Preterite, ThirdPerson, Plural)),
    )


  @Test
  def testIndicativeConditional(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val indicativeConditional = parsedInflections.filter(
      inflection => Set(Indicative, SimpleConditional).subsetOf(inflection.features)
    )
    indicativeConditional should contain only(
      ParsedInflection("faria", Set(Indicative, SimpleConditional, FirstPerson, Singular)),
      ParsedInflection("faries", Set(Indicative, SimpleConditional, SecondPerson, Singular)),
      ParsedInflection("faria", Set(Indicative, SimpleConditional, ThirdPerson, Singular)),
      ParsedInflection("faríem", Set(Indicative, SimpleConditional, FirstPerson, Plural)),
      ParsedInflection("faríeu", Set(Indicative, SimpleConditional, SecondPerson, Plural)),
      ParsedInflection("farien", Set(Indicative, SimpleConditional, ThirdPerson, Plural)),
    )

  @Test
  def testSubjunctivePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val subjunctivePresent = parsedInflections.filter(
      inflection => Set(Subjunctive, PresentTense).subsetOf(inflection.features)
    )
    subjunctivePresent should contain only(
      ParsedInflection("faci", Set(Subjunctive, PresentTense, FirstPerson, Singular)),
      ParsedInflection("facis", Set(Subjunctive, PresentTense, SecondPerson, Singular)),
      ParsedInflection("faci", Set(Subjunctive, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("fem", Set(Subjunctive, PresentTense, FirstPerson, Plural)),
      ParsedInflection("feu", Set(Subjunctive, PresentTense, SecondPerson, Plural)),
      ParsedInflection("facin", Set(Subjunctive, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testSubjunctiveImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val subjunctiveImperfect = parsedInflections.filter(
      inflection => Set(Subjunctive, PastImperfect).subsetOf(inflection.features)
    )
    subjunctiveImperfect should contain only(
      ParsedInflection("fes", Set(Subjunctive, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("fessis", Set(Subjunctive, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("fes", Set(Subjunctive, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("féssim", Set(Subjunctive, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("féssiu", Set(Subjunctive, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("fessin", Set(Subjunctive, PastImperfect, ThirdPerson, Plural)),
    )


  @Test
  def testParseImperativeAffirmative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/ca/ca-fer.html"))

    val imperative = parsedInflections.filter(_.features.contains(Imperative))
    imperative should contain only(
      ParsedInflection("fes", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("faci", Set(Imperative, ThirdPerson, Singular)),
      ParsedInflection("fem", Set(Imperative, FirstPerson, Plural)),
      ParsedInflection("feu", Set(Imperative, SecondPerson, Plural)),
      ParsedInflection("facin", Set(Imperative, ThirdPerson, Plural)),
    )
