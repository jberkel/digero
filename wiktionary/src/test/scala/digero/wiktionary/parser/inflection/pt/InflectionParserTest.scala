package digero.wiktionary.parser.inflection.pt

import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.inflection.{InflectionParserTestBase, ParsedInflection}
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*
import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.GrammaticalFeature.*

class InflectionParserTest extends InflectionParserTestBase:
  given EntryContext(EntryName("test", "pt"))

  @Test
  def testParseInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))
    parsedInflections should contain(ParsedInflection("fazer", Set(Infinitive)))

  @Test
  def testParsePersonalInfinitive(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))
    val personalInfinitive = parsedInflections.filter(_.features.contains(PersonalInfinitive))

    personalInfinitive should contain only(
      ParsedInflection("fazer", Set(PersonalInfinitive, Singular, FirstPerson)),
      ParsedInflection("fazeres", Set(PersonalInfinitive, Singular, SecondPerson)),
      ParsedInflection("fazer", Set(PersonalInfinitive, Singular, ThirdPerson)),
      ParsedInflection("fazermos", Set(PersonalInfinitive, Plural, FirstPerson)),
      ParsedInflection("fazerdes", Set(PersonalInfinitive, Plural, SecondPerson)),
      ParsedInflection("fazerem", Set(PersonalInfinitive, Plural, ThirdPerson))
    )

  def testParseGerund(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))
    parsedInflections should contain(ParsedInflection("fazendo", Set(Gerund)))

  @Test
  def testParseParticiples(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val participle = parsedInflections.filter(_.features.contains(Participle))
    participle should contain only(
      ParsedInflection("feito", Set(Participle, Masculine, Singular)),
      ParsedInflection("feitos", Set(Participle, Masculine, Plural)),
      ParsedInflection("feita", Set(Participle, Feminine, Singular)),
      ParsedInflection("feitas", Set(Participle, Feminine, Plural))
    )

  @Test
  def testParseIndicativePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val indicativePresent = parsedInflections.filter(
      inflection => Set(Indicative, PresentTense).subsetOf(inflection.features)
    )
    indicativePresent should contain only(
      ParsedInflection("faço", Set(Indicative, PresentTense, FirstPerson, Singular)),
      ParsedInflection("fazes", Set(Indicative, PresentTense, SecondPerson, Singular)),
      ParsedInflection("faz", Set(Indicative, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("fazemos", Set(Indicative, PresentTense, FirstPerson, Plural)),
      ParsedInflection("fazeis", Set(Indicative, PresentTense, SecondPerson, Plural)),
      ParsedInflection("fazem", Set(Indicative, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativeImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val indicativeImperfect = parsedInflections.filter(
      inflection => Set(Indicative, PastImperfect).subsetOf(inflection.features)
    )
    indicativeImperfect should contain only(
      ParsedInflection("fazia", Set(Indicative, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("fazias", Set(Indicative, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("fazia", Set(Indicative, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("fazíamos", Set(Indicative, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("fazíeis", Set(Indicative, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("faziam", Set(Indicative, PastImperfect, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativePreterite(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val preterite = parsedInflections.filter(_.features.contains(Preterite))
    preterite should contain only(
      ParsedInflection("fiz", Set(Indicative, Preterite, FirstPerson, Singular)),
      ParsedInflection("fizeste", Set(Indicative, Preterite, SecondPerson, Singular)),
      ParsedInflection("fez", Set(Indicative, Preterite, ThirdPerson, Singular)),
      ParsedInflection("fizemos", Set(Indicative, Preterite, FirstPerson, Plural)),
      ParsedInflection("fizestes", Set(Indicative, Preterite, SecondPerson, Plural)),
      ParsedInflection("fizeram", Set(Indicative, Preterite, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativePluperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val pluperfect = parsedInflections.filter(_.features.contains(Pluperfect))
    pluperfect should contain only(
      ParsedInflection("fizera", Set(Indicative, Pluperfect, FirstPerson, Singular)),
      ParsedInflection("fizeras", Set(Indicative, Pluperfect, SecondPerson, Singular)),
      ParsedInflection("fizera", Set(Indicative, Pluperfect, ThirdPerson, Singular)),
      ParsedInflection("fizéramos", Set(Indicative, Pluperfect, FirstPerson, Plural)),
      ParsedInflection("fizéreis", Set(Indicative, Pluperfect, SecondPerson, Plural)),
      ParsedInflection("fizeram", Set(Indicative, Pluperfect, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativeFuture(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val indicativeFuture = parsedInflections.filter(
      inflection => Set(Indicative, FutureTense).subsetOf(inflection.features)
    )
    indicativeFuture should contain only(
      ParsedInflection("farei", Set(Indicative, FutureTense, FirstPerson, Singular)),
      ParsedInflection("farás", Set(Indicative, FutureTense, SecondPerson, Singular)),
      ParsedInflection("fará", Set(Indicative, FutureTense, ThirdPerson, Singular)),
      ParsedInflection("faremos", Set(Indicative, FutureTense, FirstPerson, Plural)),
      ParsedInflection("fareis", Set(Indicative, FutureTense, SecondPerson, Plural)),
      ParsedInflection("farão", Set(Indicative, FutureTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseIndicativeConditional(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val conditional = parsedInflections.filter(_.features.contains(SimpleConditional))
    conditional should contain only(
      ParsedInflection("faria", Set(Indicative, SimpleConditional, FirstPerson, Singular)),
      ParsedInflection("farias", Set(Indicative, SimpleConditional, SecondPerson, Singular)),
      ParsedInflection("faria", Set(Indicative, SimpleConditional, ThirdPerson, Singular)),
      ParsedInflection("faríamos", Set(Indicative, SimpleConditional, FirstPerson, Plural)),
      ParsedInflection("faríeis", Set(Indicative, SimpleConditional, SecondPerson, Plural)),
      ParsedInflection("fariam", Set(Indicative, SimpleConditional, ThirdPerson, Plural)),
    )

  @Test
  def testParseSubjunctivePresent(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))
    val subjunctivePresent = parsedInflections.filter(
      inflection => Set(Subjunctive, PresentTense).subsetOf(inflection.features)
    )
    subjunctivePresent should contain only(
      ParsedInflection("faça", Set(Subjunctive, PresentTense, FirstPerson, Singular)),
      ParsedInflection("faças", Set(Subjunctive, PresentTense, SecondPerson, Singular)),
      ParsedInflection("faça", Set(Subjunctive, PresentTense, ThirdPerson, Singular)),
      ParsedInflection("façamos", Set(Subjunctive, PresentTense, FirstPerson, Plural)),
      ParsedInflection("façais", Set(Subjunctive, PresentTense, SecondPerson, Plural)),
      ParsedInflection("façam", Set(Subjunctive, PresentTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseSubjunctiveImperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val subjunctiveImperfect = parsedInflections.filter(
      inflection => Set(Subjunctive, PastImperfect).subsetOf(inflection.features)
    )
    subjunctiveImperfect should contain only(
      ParsedInflection("fizesse", Set(Subjunctive, PastImperfect, FirstPerson, Singular)),
      ParsedInflection("fizesses", Set(Subjunctive, PastImperfect, SecondPerson, Singular)),
      ParsedInflection("fizesse", Set(Subjunctive, PastImperfect, ThirdPerson, Singular)),
      ParsedInflection("fizéssemos", Set(Subjunctive, PastImperfect, FirstPerson, Plural)),
      ParsedInflection("fizésseis", Set(Subjunctive, PastImperfect, SecondPerson, Plural)),
      ParsedInflection("fizessem", Set(Subjunctive, PastImperfect, ThirdPerson, Plural)),
    )

  @Test
  def testParseSubjunctiveFuture(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val subjunctiveFuture = parsedInflections.filter(
      inflection => Set(Subjunctive, FutureTense).subsetOf(inflection.features)
    )

    subjunctiveFuture should contain only(
      ParsedInflection("fizer", Set(Subjunctive, FutureTense, FirstPerson, Singular)),
      ParsedInflection("fizeres", Set(Subjunctive, FutureTense, SecondPerson, Singular)),
      ParsedInflection("fizer", Set(Subjunctive, FutureTense, ThirdPerson, Singular)),
      ParsedInflection("fizermos", Set(Subjunctive, FutureTense, FirstPerson, Plural)),
      ParsedInflection("fizerdes", Set(Subjunctive, FutureTense, SecondPerson, Plural)),
      ParsedInflection("fizerem", Set(Subjunctive, FutureTense, ThirdPerson, Plural)),
    )

  @Test
  def testParseImperativeAffirmative(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-fazer.html"))

    val imperative = parsedInflections.filter(_.features.contains(Imperative))
    imperative should contain only(
      ParsedInflection("faz", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("faze", Set(Imperative, SecondPerson, Singular)),
      ParsedInflection("faça", Set(Imperative, ThirdPerson, Singular)),
      ParsedInflection("façamos", Set(Imperative, FirstPerson, Plural)),
      ParsedInflection("fazei", Set(Imperative, SecondPerson, Plural)),
      ParsedInflection("façam", Set(Imperative, ThirdPerson, Plural)),
    )

  @Test
  def testParseIrregularShortParticiple(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-prender.html"))

    val participle = parsedInflections.filter(
      inflection => Set(Participle, IrregularParticiple).intersect(inflection.features).nonEmpty
    )

    participle should contain only(
      ParsedInflection("preso", Set(IrregularParticiple, Masculine, Singular)),
      ParsedInflection("presos", Set(IrregularParticiple, Masculine, Plural)),
      ParsedInflection("presa", Set(IrregularParticiple, Feminine, Singular)),
      ParsedInflection("presas", Set(IrregularParticiple, Feminine, Plural)),

      ParsedInflection("prendido", Set(Participle, Masculine, Singular)),
      ParsedInflection("prendidos", Set(Participle, Masculine, Plural)),
      ParsedInflection("prendida", Set(Participle, Feminine, Singular)),
      ParsedInflection("prendidas", Set(Participle, Feminine, Plural)),
    )

  @Test
  def testParseIrregularParticiplePluperfect(): Unit =
    val parsedInflections = subject.parse(fixture("parser/inflection/conjugation/pt/pt-prender.html"))

    val pluperfect = parsedInflections.filter(_.features.contains(Pluperfect))
    pluperfect should contain only(
      ParsedInflection("prendera", Set(Indicative, Pluperfect, FirstPerson, Singular)),
      ParsedInflection("prenderas", Set(Indicative, Pluperfect, SecondPerson, Singular)),
      ParsedInflection("prendera", Set(Indicative, Pluperfect, ThirdPerson, Singular)),
      ParsedInflection("prendêramos", Set(Indicative, Pluperfect, FirstPerson, Plural)),
      ParsedInflection("prendêreis", Set(Indicative, Pluperfect, SecondPerson, Plural)),
      ParsedInflection("prenderam", Set(Indicative, Pluperfect, ThirdPerson, Plural)),
    )
