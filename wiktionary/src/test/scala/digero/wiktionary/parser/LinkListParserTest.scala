package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.{EntryName, QualifiedLink}
import org.junit.jupiter.api.{Disabled, Test}
import org.scalatest.matchers.should.Matchers.*


class LinkListParserTest extends Fixture:
  given EntryContext(EntryName("test", "en"))

  private def parse(html:String)(using context: EntryContext): Seq[QualifiedLink] =
    LinkListParser().parse(html)

  @Test
  def testParseSynonymBulletedListWithCommaSeparatedTemplatedLinks(): Unit =
    /*
        ====Synonyms====
        * {{l|en|predictable}}, {{l|en|typical}}, {{l|en|usual}}
     */
    val synonyms = parse(fixture("parser/link_list/bulleted_list_with_comma_separated_templated_links.html"))
    synonyms.map(_.link.toString) should contain only(
      "[[predictable#English|null]]",
      "[[typical#English|null]]",
      "[[usual#English|null]]"
    )

  @Test
  def testParseSynonymBulletedListWithCommaSeparatedUntemplatedLinksEnglish(): Unit =
    /*
        ====Synonyms====
        * [[predictable]], [[typical]]
        * [[usual]]
     */
    val synonyms = parse(fixture("parser/link_list/bulleted_list_with_comma_separated_untemplated_links.html"))
    synonyms.map(_.link.toString) should contain only(
      "[[predictable|null]]",
      "[[typical|null]]",
      "[[usual|null]]"
    )

  @Test
  def testParseSynonymBulletedListWithCommaSeparatedUntemplatedLinksNonEnglish(): Unit =
    /*
        ====Synonyms====
        * [[predictable]], [[typical]]
        * [[usual]]
     */
    given EntryContext(EntryName("test", "fr"))

    val synonyms = parse(fixture("parser/link_list/bulleted_list_with_comma_separated_untemplated_links.html"))
    synonyms shouldBe empty


  @Test
  def testParseMixedLanguageLinksNonEnglish(): Unit =
    /*
        ====Synonyms====
        * {{l|fr|bien}}, {{l|fr|super}}, [[lolz]], {{l|en|noez}}
     */
    given EntryContext(EntryName("test", "fr"))

    val synonyms = parse(fixture("parser/link_list/mixed_language_links.html"))
    synonyms.map(_.link.toString) should contain only(
      "[[bien#French|null]]",
      "[[super#French|null]]"
    )

  @Test
  def testParseBulletedListSynonymsPrefixedWithSense(): Unit =
    /*
        https://en.wiktionary.org/w/index.php?title=apogee&oldid=79059208

        ====Synonyms====
        * {{sense|point in an orbit}} {{l|en|apocenter}}, {{l|en|apoapsis}}, {{l|en|apsis}}
        * {{sense|highest point or state}} {{l|en|acme}}, {{l|en|culmination}}, {{l|en|pinnacle}}, {{l|en|zenith}}, {{l|en|climax}}
        * See also [[Thesaurus:apex]]
     */
    val synonyms = parse(fixture("parser/link_list/bulleted_list_synonyms_prefixed_with_sense.html"))
    synonyms.map(syn => (syn.sense, syn.link.toString)) should contain only(

      (Some("point in an orbit"), "[[apocenter#English|null]]"),
      (Some("point in an orbit"), "[[apoapsis#English|null]]"),
      (Some("point in an orbit"), "[[apsis#English|null]]"),

      (Some("highest point or state"), "[[acme#English|null]]"),
      (Some("highest point or state"), "[[culmination#English|null]]"),
      (Some("highest point or state"), "[[pinnacle#English|null]]"),
      (Some("highest point or state"), "[[zenith#English|null]]"),
      (Some("highest point or state"), "[[climax#English|null]]"),

      /* (None, "[[Thesaurus:apex|null]]") */
    )

  @Test
  def testParseBulletedListAntonymsPrefixedWithAntsense(): Unit =
    /*
        https://en.wiktionary.org/w/index.php?title=apogee&oldid=79059208

        ====Antonyms====
        * {{antsense|a point in an orbit}} {{l|en|periapsis}}
        * {{antsense|a point in an orbit around the Earth}} {{l|en|perigee}}
        * {{antsense|highest point}} {{l|en|nadir}}, {{l|en|perigee}}
        *: [[perigee]] is the etymological antonym (from Ancient Greek).
     */
    val antonyms = parse(fixture("parser/link_list/bulleted_list_antonyms_prefixed_with_sense.html"))
    antonyms.map(ant => (ant.sense, ant.link.toString)) should contain only(

      (Some("a point in an orbit"), "[[periapsis#English|null]]"),

      (Some("a point in an orbit around the Earth"), "[[perigee#English|null]]"),

      (Some("highest point"), "[[nadir#English|null]]"),
      (Some("highest point"), "[[perigee#English|null]]"),
      // contained inside the last list element
      (Some("highest point"), "[[perigee|null]]")
    )

  @Test
  def testParseDerivedTermsCol2Template(): Unit =
    /*
        ====Derived terms====
        {{col2|en
         |acorn aquash
         |{{l|en|opo squash}} (''{{taxfmt|Lagenaria|genus}} spp.'')
         |{{l|en|squash bug}} ({{taxfmt|Coreidae|family}})
         |{{l|en|summer squash}}
         |{{l|en|winter squash}}
        }}
     */
    val derivedTerms = parse(fixture("parser/link_list/derived_col2_template.html"))
    derivedTerms.map(_.link.toString) should contain only(
      "[[acorn squash#English|null]]",
      "[[opo squash#English|null]]",
      "[[Lagenaria|null]]",
      "[[squash bug#English|null]]",
      "[[Coreidae|null]]",
      "[[summer squash#English|null]]",
      "[[winter squash#English|null]]"
    )

  @Test
  def testParseMultipleCol2Templates(): Unit =
    /*
      https://en.wiktionary.org/w/index.php?title=Schloss&oldid=79096976#German

      ====Hyponyms====
      {{col4|de|title=lock|Bügelschloss|Fahrradschloss}}
      {{col4|de|title=gun mechanism|Luntenschloss}}
     */
    given EntryContext(EntryName("Schloss", "de"))

    val hyponyms = parse(fixture("parser/link_list/hyponyms_multiple_col4_templates.html"))
    hyponyms.map(hypo => (hypo.sense, hypo.link.toString)) should contain only(
      (Some("lock"), "[[Bügelschloss#German|null]]"),
      (Some("lock"), "[[Fahrradschloss#German|null]]"),
      (Some("gun mechanism"), "[[Luntenschloss#German|null]]"),
    )

  @Test
  def testParseDerivedTermsCol2TemplateWithTitle(): Unit =
    /*
        ====Derived terms====
        {{col2|en|title=plant and its fruit
          |acorn squash
          |{{l|en|opo squash}} (''{{taxfmt|Lagenaria|genus}} spp.'')
        }}
     */
    val derivedTerms = parse(fixture("parser/link_list/derived_col2_template_with_title.html"))
    derivedTerms.map(der => (der.sense, der.link.toString)) should contain only(
      (Some("plant and its fruit"), "[[acorn squash#English|null]]"),
      (Some("plant and its fruit"), "[[opo squash#English|null]]")
    )

  @Test
  def testParseDerivedTermsCol2TemplateWithTemplatedTitle(): Unit =
    /*
        ====Derived terms====
        {{col2|en|title={{lb|en|computing}} [[algorithm|Algorithm]] for sorting a list of items
          |foo
          |bar
        }}
     */
    val derivedTerms = parse(fixture("parser/link_list/derived_col2_template_with_templated_title.html"))
    derivedTerms.map(der => (der.sense, der.link.toString)) should contain only(
      (Some("(computing) Algorithm for sorting a list of items"), "[[foo#English|null]]"),
      (Some("(computing) Algorithm for sorting a list of items"), "[[bar#English|null]]")
    )

  @Test
  def testParseRelTemplate(): Unit =
    /*
       ====Related terms====
       {{rel-top}}
        * {{l|en|first}}
        * {{l|en|second}}
       {{rel-bottom}}
     */
    parse(fixture("parser/link_list/rel-top.html"))
      .map(syn => (syn.sense, syn.link.toString)) should contain only(
      (None, "[[first#English|null]]"),
      (None, "[[second#English|null]]"),
    )

  @Test @Disabled
  def testParseRelTemplateWithSense(): Unit =
    /*
       ====Related terms====
       {{sense|foo}}
       {{rel-top}}
        * {{l|en|first}}
        * {{l|en|second}}
       {{rel-bottom}}
     */
    parse(fixture("parser/link_list/rel-top-with-sense.html"))
      .map(syn => (syn.sense, syn.link.toString)) should contain only (
        (Some("foo"), "[[first#English|null]]"),
        (Some("foo"), "[[second#English|null]]"),
      )

  @Test
  def testParseUnbulletedCommaSeparatedUntemplatedLinks(): Unit =
    /*
      https://en.wiktionary.org/w/index.php?title=bathers&oldid=78751051#English

      ====Synonyms====
      {{sense|swimsuit}} [[budgie smugglers]], [[cozzie]], [[Speedos]]
     */
    val synonyms = parse(fixture("parser/link_list/unbulleted_comma_separated_untemplated_with_sense.html"))
    synonyms.map(syn => (syn.sense, syn.link.toString)) should contain only(

      (Some("swimsuit"), "[[budgie smugglers|null]]"),
      (Some("swimsuit"), "[[cozzie|null]]"),
      (Some("swimsuit"), "[[Speedos|null]]"),
    )

  @Test
  def testParseSingleUntemplatedLink(): Unit =
    /*
        ====Synonyms====
        [[foo]]
     */
    parse(fixture("parser/link_list/single_untemplated_link.html"))
        .map(syn => (syn.sense, syn.link.toString)) should contain only(
      (None, "[[foo|null]]"),
    )

  @Test
  def testParseNewlineSeparatedUntemplatedLink(): Unit =
    /*
        ====Synonyms====
        [[foo]]

        [[bar]]
     */
    parse(fixture("parser/link_list/newline_separated_untemplated_links.html"))
      .map(syn => (syn.sense, syn.link.toString)) should contain only (
        (None, "[[foo|null]]"),
        (None, "[[bar|null]]"),
      )

  @Test
  def testParseDefinitionListBasedLists(): Unit =
    /*
        ====Synonyms====
        : [[foo]]
        : [[bar]]
     */
    parse(fixture("parser/link_list/dl_based.html"))
      .map(syn => (syn.sense, syn.link.toString)) should contain only(
        (None, "[[foo|null]]"),
        (None, "[[bar|null]]")
    )

  @Test
  def testParseBRSeparatedUntemplatedLinks(): Unit =
    /*
      https://en.wiktionary.org/w/index.php?title=CS_gas&oldid=50769169

      ====Related terms====
      [[CN gas]]<BR>
      [[pepper spray]]<BR>
      [[mace]]
     */
    parse(fixture("parser/link_list/br_separated_untemplated_links.html"))
      .map(syn => (syn.sense, syn.link.toString)) should contain only(
      (None, "[[CN gas|null]]"),
      (None, "[[pepper spray|null]]"),
      (None, "[[mace|null]]"),
    )

  @Test
  def testParseMissingTemplate(): Unit =
    /*
      ====Derived terms====
      {{derl3|en|foo}}
     */
    parse(fixture("parser/link_list/missing_template.html")) shouldBe empty


  @Test
  def testParsePrefixSeeTemplate(): Unit =
    /*
      ====Derived terms====
      {{prefixsee|en|anti-}}
     */
    parse(fixture("parser/link_list/prefixsee.html")) shouldBe empty

  @Test
  def testParsePrefixlanglemma(): Unit =
    /*
      ====See also====
      {{prefixlanglemma|en|foo}}
     */
    parse(fixture("parser/link_list/prefixlanglemma.html")) shouldBe empty

  @Test
  def testParseInvalidURL(): Unit =
    parse(fixture("parser/link_list/invalid_url.html")) shouldBe empty

  @Test
  def testParseNestedDerivedTerms(): Unit =
    /*
      ====Derived terms====
      {{derived terms|en|first|* {{l|en|second}}|a|b|c|d|e|f|g}}
    */
    parse(fixture("parser/link_list/derived_terms_nested.html")).map(_.link.getTarget) should contain only(
      "a", "b", "c", "d", "e", "f", "first", "second", "g"
    )

  @Test
  def testParsePedia(): Unit =
    /*
      ====See also====
      {{pedia}}
    */
    parse(fixture("parser/link_list/see_also_pedia.html")) shouldBe empty

  def testParseCollocationsCoTop(): Unit =
  /*
    ====Collocations====
    {{co-top}}
    * sheer absurdity
    * utter absurdity
    {{co-bottom}}
   */
    parse(fixture("parser/link_list/collocations_co_top.html")) shouldBe empty

  def testParseCollocationsCoTopWithCoi(): Unit =
  /*
    =====Collocations=====
    {{co-top|Adjectives often used with "unsubstantiated"}}
    {{coi|en|claim, allegation, rumor, accusation, report, charge, assumption, gossip, story, statement}}
    {{co-bottom}}
   */
    parse(fixture("parser/link_list/collocations_co_top_coi.html")) shouldBe empty
