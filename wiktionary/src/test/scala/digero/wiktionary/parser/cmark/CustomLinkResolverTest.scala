package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.html.renderer.{LinkType, ResolvedLink}
import digero.wiktionary.parser.cmark.CustomLinkResolverTest.{blueLink, redLink, wikipediaLink}
import org.jsoup.Jsoup
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*


class CustomLinkResolverTest:
  private def resolve(html: String): ResolvedLink =
    val node = Jsoup.parse(html)
    val url = node.selectFirst("a").attr("href")
    val link = new ResolvedLink(LinkType.LINK, url)
    CustomLinkResolver.resolveLink(node, null, link)

  @Test def testResolveBlueLink(): Unit =
    val resolved = resolve(blueLink)
    resolved.getUrl shouldEqual "./stain"

  @Test def testResolveRedLinkRemovesQueryParameter(): Unit =
    val resolved = resolve(redLink)
    resolved.getUrl shouldEqual "./emaculatus#Latin"

  @Test def testResolveWikipediaLink(): Unit =
    val resolved = resolve(wikipediaLink)
    resolved.getUrl shouldEqual "https://en.wikipedia.org/wiki/John%20Hales"


object CustomLinkResolverTest:
  val blueLink =
    """
      <a rel="mw:WikiLink" href="./stain" title="stain" id="mwFA">stains</a>
    """

  val redLink =
    """
      <a rel="mw:WikiLink" href="./emaculatus?action=edit&amp;redlink=1#Latin" title="emaculatus" class="new" typeof="mw:LocalizedAttrs" data-mw-i18n="{&quot;title&quot;:{&quot;lang&quot;:&quot;x-page&quot;,&quot;key&quot;:&quot;red-link-title&quot;,&quot;params&quot;:[&quot;emaculatus&quot;]}}">emaculatus</a>
    """

  val wikipediaLink =
    """
      <a rel="mw:WikiLink/Interwiki" href="https://en.wikipedia.org/wiki/John%20Hales" title="w:John Hales" about="#mwt6" class="extiw">John Hales</a>
    """
