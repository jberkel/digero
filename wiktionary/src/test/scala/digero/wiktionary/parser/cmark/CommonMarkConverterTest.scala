package digero.wiktionary.parser.cmark

import digero.test.Fixture
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import scala.compiletime.uninitialized

class CommonMarkConverterTest extends Fixture:
  var subject: CommonMarkConverter = uninitialized

  val convertHtml: String => String = subject.htmlToCommonMark(_).getOrElse("")

  @BeforeEach
  def setup(): Unit =
    subject = CommonMarkConverter()

  @Test def convertLink(): Unit =
    convertHtml("""<a href="foo">bar</a>""") shouldEqual "[bar](foo)"

  @Test def convertLinkWithDoNotRenderLinks(): Unit =
    subject = CommonMarkConverter(ConversionOptions(doNotRenderLinks = true))
    convertHtml("""<a href="foo">bar</a>""") shouldEqual "bar"

  @Test def convertLinkWithDoNotRenderLinksRetainsOtherFormatting(): Unit =
    subject = CommonMarkConverter(ConversionOptions(doNotRenderLinks = true))
    convertHtml("""<a href="foo"><strong>bar</strong></a>""") shouldEqual "**bar**"

  @Test def convertWiktionaryLink(): Unit =
    convertHtml(
      """
      <a rel="mw:WikiLink" href="./ἱππομανές#Ancient_Greek" title="ἱππομανές">ἱππομᾰνές</a>
      """) shouldEqual """[ἱππομᾰνές](./ἱππομανές#Ancient_Greek "ἱππομανές")"""

  @Test def convertWiktionaryLinkWithDoNotRenderLinks(): Unit =
    subject = CommonMarkConverter(ConversionOptions(doNotRenderLinks = true))
    convertHtml(
      """
      <a rel="mw:WikiLink" href="./ἱππομανές#Ancient_Greek" title="ἱππομανές">ἱππομᾰνές</a>
      """) shouldEqual "ἱππομᾰνές"

  @Test def keepsEnDashesIntact(): Unit =
    convertHtml("–") shouldEqual "–"

  @Test def keepsEmDashesIntact(): Unit =
    convertHtml("—") shouldEqual "—"

  @Test def keepsQuotesIntact(): Unit =
    convertHtml("“”‘’«»") shouldEqual "“”‘’«»"

  @Test def replaceQuoteEntities(): Unit =
    convertHtml("&hellip;") shouldEqual "…"
    convertHtml("&laquo;") shouldEqual "«"
    convertHtml("&raquo;") shouldEqual "»"
    convertHtml("&lsquo;") shouldEqual "‘"
    convertHtml("&rsquo;") shouldEqual "’"
    convertHtml("&ldquo;") shouldEqual "“"
    convertHtml("&rdquo;") shouldEqual "”"
    convertHtml("&apos;") shouldEqual "'"
    convertHtml("&endash;") shouldEqual "–"
    convertHtml("&emdash;") shouldEqual "—"

  @Test def convertEtymology(): Unit =
    val result = convertHtml(fixture("converter/foo_etymology.html"))
//    fixtureWrite(content = result, "foo_etymology.md")
    result shouldEqual fixture("converter/foo_etymology.md")

  @Test def itStripsTheFirstH3Heading(): Unit =
    convertHtml(
      """
      <h3 id="Etymology">Etymology</h3>
      <a href="foo">bar</a>
      """) shouldEqual "[bar](foo)"

  @Test def itStripsTheFirstH4Heading(): Unit =
    convertHtml(
      """
      <h4 id="Etymology">Etymology</h4>
      <a href="foo">bar</a>
      """) shouldEqual "[bar](foo)"

  @Test def itRemovesNewlinesAroundIgnoredElements(): Unit =
    convertHtml(
      """
      <p id="mwKA">From <i class="Latn mention" lang="ga" about="#mwt13" typeof="mw:Transclusion" data-mw="" id="mwKQ"><a rel="mw:WikiLink" href="./fianna#Irish" title="fianna">fianna</a></i><span about="#mwt13"> </span><span class="mention-gloss-paren annotation-paren" about="#mwt13">(</span><span class="ann-pos" about="#mwt13">plural of <i class="Latn mention" lang="ga"><a rel="mw:WikiLink" href="./fiann#Irish" title="fiann">fiann</a></i> <span class="mention-gloss-paren annotation-paren">(</span><span class="mention-gloss-double-quote">“</span><span class="mention-gloss">warrior</span><span class="mention-gloss-double-quote">”</span><span class="mention-gloss-paren annotation-paren">)</span></span><span class="mention-gloss-paren annotation-paren" about="#mwt13">)</span><span about="#mwt13"> +</span><span typeof="mw:Entity" about="#mwt13">‎</span><span about="#mwt13"> </span><i class="Latn mention" lang="ga" about="#mwt13"><a rel="mw:WikiLink" href="./Fáil?action=edit&amp;redlink=1#Irish" title="Fáil" class="new" typeof="mw:LocalizedAttrs" data-mw-i18n="">Fáil</a></i><span about="#mwt13"> </span><span class="mention-gloss-paren annotation-paren" about="#mwt13">(</span><span class="ann-pos" about="#mwt13">genitive of <i class="Latn mention" lang="ga"><a rel="mw:WikiLink" href="./Fál?action=edit&amp;redlink=1#Irish" title="Fál" class="new" typeof="mw:LocalizedAttrs" data-mw-i18n="">Fál</a></i> <span class="mention-gloss-paren annotation-paren">(</span><span class="ann-pos">a legendary name of Ireland</span><span class="mention-gloss-paren annotation-paren">)</span></span><span class="mention-gloss-paren annotation-paren" about="#mwt13">)</span>
        <link rel="mw:PageProp/Category" href="./Category:Irish_compound_nouns#FIANNA%20FAIL" about="#mwt13" id="mwKg">. The original meaning is thus "warriors of Fál", but the phrase is traditionally translated "soldiers of destiny".
      </p>
      """) shouldNot contain('\n')

  @Test def itStripsMaintenance(): Unit =
    convertHtml(
      """
      <p id="mwCA"><span class="maintenance-line" style="color: #777777;" about="#mwt2" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;rfe&quot;,&quot;href&quot;:&quot;./Template:rfe&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;ga&quot;}},&quot;i&quot;:0}}]}" id="mwCQ">(This <a rel="mw:WikiLink" href="./Wiktionary:Etymology" title="Wiktionary:Etymology">etymology</a> is missing or incomplete. Please add to it, or discuss it at the <a rel="mw:WikiLink" href="./Wiktionary:Etymology_scriptorium" title="Wiktionary:Etymology scriptorium">Etymology scriptorium</a>.)</span>  <link rel="mw:PageProp/Category" href="./Category:Requests_for_etymologies_in_Irish_entries#ANFAIS" about="#mwt2" id="mwCg"></p>
      """) shouldBe empty

  @Test def itDoesNotInsertExtraSpacesForSpanTags(): Unit =
    convertHtml(
      """
      <span>(the </span><i class="Latn mention" lang="de" about="#mwt16">Rechtschreibreform</i><span about="#mwt16">)</span
      """) shouldEqual "(the *Rechtschreibreform*)"

  @Test def itDoesNotInsertExtraSpacesForNewlines(): Unit =
    convertHtml(
      """
      Hit the nail on the <b>head</b><span typeof="mw:Entity">!</span>
      """) shouldEqual "Hit the nail on the **head**!"

  @Test def itDoesInsertZWSPBeforeEmphasisWhenNeeded(): Unit =
    convertHtml("girl<i>)</i>") shouldEqual "girl&ZeroWidthSpace;*)*"

  @Test def itDoesNotInsertZWSPBeforeEmphasisWhenNotNeeded(): Unit =
    convertHtml("girl<i>a</i>") shouldEqual "girl*a*"

  @Test def itDoesInsertZWSPAfterEmphasisWhenEndingWithPunctuation(): Unit =
    convertHtml("I was called into the <b>head'</b>s office") shouldEqual "I was called into the **head'**&ZeroWidthSpace;s office"

  @Test def itDoesNotInsertZWSPAfterEmphasisWhenWhitespaceFollowsPunctuation(): Unit =
    convertHtml("I was called into the <b>head'</b> office") shouldEqual "I was called into the **head'** office"

  @Test def itConvertsSubscriptDigitsToUnicode(): Unit =
    convertHtml("""CaCO<sub id="mwIg">3</sub>""") shouldEqual "CaCO₃"

  @Test def itConvertsSubscriptCharacters(): Unit =
    convertHtml("""A<sub id="mwIg">n</sub>""") shouldEqual "Aₙ"

  @Test def itConvertsSuperscriptDigitsToUnicode(): Unit =
    convertHtml("""X<sup id="mwIg">2</sup>""") shouldEqual "X²"

  @Test def itOnlyConvertsIfAllCharactersCanBeMapped(): Unit =
    convertHtml("""X<sub id="mwIg">lolz</sub>""") shouldEqual "X<sub>lolz</sub>"

  @Test def itRemovesUnicodeDirectionMarkers(): Unit =
    val html = """
    <i class="Hebr mention" lang="he" about="#mwt8">
      <a rel="mw:WikiLink" href="./אהב#Hebrew" title="אהב">אָהַב</a>
    </i> <span typeof="mw:Entity" about="#mwt8">‎</span>
    """

    convertHtml(html) shouldEqual "*[אָהַב](./אהב#Hebrew \"אהב\")*"

  @Test def itIgnoresLinebreaksFromCategories(): Unit =
    val html =
      """
      <p id="mwBQ">Metanalyzed
        <link rel="mw:PageProp/Category" href="./Category:Ancient_Greek_rebracketings#ΑΖΩ" about="#mwt2" typeof="mw:Transclusion" id="mwBg"> suffix</p>
      """
    convertHtml(html) shouldEqual "Metanalyzed suffix"


  @Test def itNormalizesWhitespacesInsideP(): Unit =
    val html =
      """
        <p>
        <a href="foo">Foo</a>               <a href="bar">Bar</a>
        </p>
        """
    convertHtml(html) shouldEqual "[Foo](foo) [Bar](bar)"

  @Test def itConvertsDescriptionLists(): Unit =
    val html =
      """
      <p>Leading paragraph</p>
      <dl>
        <dd>Description</dd>
      </dl>
      <p>Following paragraph</p>
      """
    convertHtml(html) shouldEqual "Leading paragraph\nDescription\n\nFollowing paragraph"

  @Test def itStripsInlineImages(): Unit =
    // Foo [[File:Foo.jpg]]
    val result = convertHtml(fixture("converter/image_inline.html"))
    result shouldEqual "Foo"

  @Test def itStripsThumbImages(): Unit =
    // Foo [[File:Foo.jpg|thumb|alt=alt text|some caption]]
    val result = convertHtml(fixture("converter/image_thumb_with_caption_and_alt_text.html"))
    result shouldEqual "Foo"

  @Test def itStripsImagesWithLink(): Unit =
    // Foo [[File:Foo.jpg|link=foo]]
    val result = convertHtml(fixture("converter/image_with_link.html"))
    result shouldEqual "Foo"

  @Test def itStripsImageGalleries(): Unit =
    /*
      Foo
      <gallery>
      File:Foo.jpg|some caption
      </gallery>
     */
    val result = convertHtml(fixture("converter/image_gallery.html"))
    result shouldEqual "Foo"

  @Test def itStripsMultipleImages(): Unit =
    /*
      Foo {{multiple images|image1=Foo.jpg|caption1=some caption}}
    */
    val result = convertHtml(fixture("converter/multiple_images.html"))
    result shouldEqual "Foo"

  @Test def itStripsHiddenInterwikiLinks(): Unit =
    val result = convertHtml(fixture("converter/pedia_link.html"))
    result shouldEqual
      """
      **[Ergativity](https://en.wikipedia.org/wiki/Ergativity#Realization_of_ergativity "w:Ergativity")** on Wikipedia.
      """.strip()

  @Test def itRemovesSerialComma(): Unit =
    // foo, bar{{,}} and baz
    val result = convertHtml(fixture("converter/serial_comma.html"))
    result shouldEqual "foo, bar and baz"

  @Test def convertUsageNotes(): Unit =
    val result = convertHtml(fixture("converter/erinnern_usage-notes.html"))
//    fixtureWrite(content = result, "erinnern_usage-notes.md")
    result shouldEqual fixture("converter/erinnern_usage-notes.md")

  @Test def convertDumpster(): Unit =
    val result = convertHtml(fixture("converter/dumpster_etymology.html"))
//    fixtureWrite(content = result, "dumpster_etymology_expected.md")
    val expected = fixture("converter/dumpster_etymology_expected.md")
    result shouldEqual expected

  @Test def convertMaidan(): Unit =
    val result = convertHtml(fixture("converter/Maidan_sense.html"))
//        fixtureWrite(content = result, "Maidan_sense_expected.md")
    val expected = fixture("converter/Maidan_sense_expected.md")
    result shouldEqual expected

  @Test def convertFitzroy(): Unit =
    val result = convertHtml(fixture("converter/Fitzroy_etymology.html"))
//            fixtureWrite(content = result, "Fitzroy_etymology_expected.md")
    val expected = fixture("converter/Fitzroy_etymology_expected.md")
    result shouldEqual expected

  // convert to plain tests
  @Test def convertToPlain(): Unit =
    subject.commonMarkToPlain(
      """
      ([mineralogy](./mineralogy "mineralogy")) [Homogenous](./homogenous "homogenous").
      """.strip()) should equal("(mineralogy) Homogenous.")

  @Test def convertToPlain2(): Unit =
    subject.commonMarkToPlain(
      """
      ([pathology](./pathology "pathology")) A condition characterized by unequal size of the [pupils](./pupil "pupil") of the [eye](./eye "eye")
      """.strip()) should equal("(pathology) A condition characterized by unequal size of the pupils of the eye")
