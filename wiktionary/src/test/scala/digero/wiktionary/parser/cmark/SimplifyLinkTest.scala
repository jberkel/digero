package digero.wiktionary.parser.cmark

import digero.wiktionary.model.EntryName
import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.cmark.SimplifyLink.simplifyEntryLink
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import scala.compiletime.uninitialized

class SimplifyLinkTest:
  var subject: CommonMarkConverter = uninitialized

  @BeforeEach
  def setup(): Unit =
    subject = CommonMarkConverter()

  @Test def simplifyEntryLinkDifferentTitleReturnsFullLink(): Unit =
    given EntryContext(EntryName("foo", "Ancient Greek"))

    subject.simplifyEntryLink(
      """
        [ἱππομᾰνές](./ἱππομανές#Ancient_Greek "ἱππομανές")
        """.strip()
    ) should equal("""[ἱππομᾰνές](./ἱππομανές#Ancient_Greek "ἱππομανές")""")

  @Test def simplifyEntryLinkSameTitleAndNoOtherContentReturnsJustText(): Unit =
    given EntryContext(EntryName("foo", "English"))

    subject.simplifyEntryLink(
      """
        [foo](./foo#English "foo")
        """.strip()
    ) should equal("foo")


  @Test def simplifyEntryLinkFragmentWithSenseIdReturnsJustText(): Unit =
    given EntryContext(EntryName("bezár", "Hungarian"))

    subject.simplifyEntryLink(
      """
        [zár](./zár#Hungarian:_to_close "zár")
        """.strip()
    ) should equal("zár")

  @Test def simplifyEntryLinkDifferentLanguageIsUnchanged(): Unit =
    given EntryContext(EntryName("foo", "French"))

    subject.simplifyEntryLink(
      """
        [foo](./foo#English "foo")
        """.strip()
    ) should equal("""[foo](./foo#English "foo")""")

  @Test def simplifyEntryLinkSameTitleWithAdditionalTextIsUnchanged(): Unit =
    given EntryContext(EntryName("foo", "English"))

    subject.simplifyEntryLink(
      """
        [foo](./foo#English "foo") Something else
        """.strip()
    ) should equal("""[foo](./foo#English "foo") Something else""")

  @Test def simplifyEntryLinkMultipleLinksIsUnchanged(): Unit =
    given EntryContext(EntryName("foo", "English"))

    subject.simplifyEntryLink(
      """
        [foo](./foo#English "foo") [bar](./bar#English "bar")
        """.strip()
    ) should equal("""[foo](./foo#English "foo") [bar](./bar#English "bar")""")

