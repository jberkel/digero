package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.lexical.RelationType.{Antonym, Synonym}
import digero.wiktionary.model.lexical.{Nym, RelationType}
import digero.wiktionary.model.{Category, EntryName, Sense, SenseKind, Usex}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import scala.compiletime.uninitialized

class SenseParserTest extends Fixture:
  var subject: SenseParser = uninitialized

  @BeforeEach
  def prepare(): Unit = subject = SenseParser()

  @Test
  def testParseKindFormOf(): Unit =
      given EntryContext(EntryName("played", "en"))

      (subject.parse(fixture("parser/sense/played_senses.html")).head: @unchecked) match
        case Sense("simple past tense and past participle of play", _, kind, _, Seq(), Seq(), Seq(), Seq()) if kind.contains(SenseKind.FormOf) =>

  @Test
  def testParseFormOf(): Unit =
    given EntryContext(EntryName("played", "en"))

    (subject.parse(fixture("parser/sense/played_senses.html")).head: @unchecked) match
      case Sense("simple past tense and past participle of play", _, kind, Some("play"), Seq(), Seq(), Seq(), Seq()) =>

  @Test
  def testParseFormOfMultipleLinks(): Unit =
    given EntryContext(EntryName("PFM", "es"))

    (subject.parse(fixture("parser/sense/PFM_senses.html")).head: @unchecked) match
      case Sense("Initialism of proyecto fin de máster (“final master's degree project”).", _, _, None, _, _, _, _) =>

  @Test
  def testParseFormOfMultipleLanguageLinks(): Unit =
    given EntryContext(EntryName("mística", "es"))

    (subject.parse(fixture("parser/sense/mística_senses.html")).head: @unchecked) match
      case Sense("female equivalent of místico (“mystic”)", _, _, Some("místico"), _, _, _, _) =>

  @Test
  def test_empty_senses_are_skipped(): Unit =
    given EntryContext(EntryName("crab", "en"))

    val result = subject.parse(fixture("parser/sense/crab_senses.html"))
    result should have length(10)
    result.head.text should startWith("A crustacean ")

  @Test
  def testParse_simple(): Unit =
    given EntryContext(EntryName("simple", "en"))

    val result = subject.parse(fixture("parser/sense/simple_senses.html"))
    assertThat(result.size).isEqualTo(8)
    result.zipWithIndex.foreach((sense, index) => print(f"${index + 1} $sense"))

    result match
      case Sense("Uncomplicated; taken by itself, with nothing added.", _, _, _, _, Seq(), _, Seq()) ::
           Sense("Without ornamentation; plain.", _, _, _, _, Seq(), _, Seq()) ::
           Sense("Free from duplicity; guileless, innocent, straightforward.", _, _, _, _, Seq(), _, Seq()) ::
           Sense("Undistinguished in social condition; of no special rank.", _, _, _, Seq(Nym(Antonym, "simple", "gentle", None)), Seq(), _, Seq()) ::
           Sense("(now rare ) Trivial; insignificant.", _, _, _, _, Seq(), _, Seq()) ::
           Sense("(now colloquial , euphemistic ) Feeble-minded; foolish.", _, _, _, _, Seq(), _, Seq()) ::
           Sense("(heading, technical) Structurally uncomplicated.", _, _, _, _, _, _,
              Sense("(chemistry , pharmacology ) Consisting of one single substance; uncompounded.", _, _, _, _, Seq(), _, Seq()) ::
              Sense("(mathematics ) Of a group: having no normal subgroup.", _, _, _, _, Seq(), _, Seq()) ::
              Sense("(botany ) Not compound, but possibly lobed.", _, _, _, _, Seq(), _, Seq()) ::
              Sense("(of a steam engine) Using steam only once in its cylinders, in contrast to a compound engine, where steam is used more than once in high-pressure and low-pressure cylinders.", _, _, _, _, Seq(), _, Seq()) ::
              Sense("(zoology ) Consisting of a single individual or zooid; not compound.", _, _, _, _, usexes, _, Seq()) ::
              Sense("(mineralogy ) Homogenous.", _, _, _, _, Seq(), _, Seq()) :: Nil)
           ::
           Sense("(obsolete ) Mere; not other than; being only.", _, _, _, _, Seq(), _, Seq())
           :: Nil =>
              usexes should contain only Usex("a **simple** ascidian", None, None, None)

  @Test
  def testParse_στρύχνον(): Unit =
    given EntryContext(EntryName("στρύχνον", "grc"))
    val result = subject.parse(fixture("parser/sense/στρύχνον_senses.html"))

    result match
      case Sense("name of various nightshades:", _, _, _, Seq(), Seq(), Seq(),
        Sense("winter cherry (Alkekengi officinarum, syn. Physalis alkekengi)", _, _, _, nyms1, Seq(), Seq(), Seq()) ::
        Sense("black nightshade (Solanum nigrum)", _, _, _, Seq(), Seq(), Seq(), Seq()) ::
        Sense("thorn apple (Datura stramonium)", _, _, _, nyms2, Seq(), Seq(), Seq()) ::
        Sense("ashwagandha (Withania somnifera)", _, _, _, nyms3, Seq(), Seq(), Seq()) :: Nil
      ) :: Nil =>

        nyms1 should contain(Nym(Synonym, "στρύχνον", "ἁλικάκκαβον", Some("halikákkabon")))
        nyms2 should contain(Nym(Synonym, "στρύχνον", "[ἱππομᾰνές](./ἱππομανές#Ancient_Greek \"ἱππομανές\")", Some("hippomanés")))
        nyms3 should contain inOrderOnly (Nym(Synonym, "στρύχνον", "ἁλικάκκαβον",Some("halikákkabon")),
                                          Nym(Synonym, "στρύχνον", "κακκαλία", Some("kakkalía")),
                                          Nym(Synonym, "στρύχνον", "μώριος", Some("mṓrios")))


  @Test
  def testParse_usexes_are_parsed_to_correct_senses(): Unit =
    given EntryContext(EntryName("amaze", "en"))

    val result = subject.parse(fixture("parser/sense/en-amaze.html"))
    result match
      case Sense(_, _, _, _, _, usexes1, _,
        Sense(_, _, _, _, _, usexes2, _, Seq()) :: Nil
      ) :: Nil =>
        usexes1 shouldBe empty
        usexes2 should contain only Usex("He was **amazed** when he found that the girl was a robot.", None, None, None)

  @Test
  def testParse_categories_are_assigned_to_correct_senses(): Unit =
    given EntryContext(EntryName("amaze", "en"))

    val result = subject.parse(fixture("parser/sense/en-amaze.html"))
    result match
      case Sense(_, _, _, _, _, _, categories1,
      Sense(_, _, _, _, _, _, categories2, Seq()) :: Nil
      ) :: Nil =>
        categories1 should contain only Category("English transitive verbs")
        categories2 should contain only Category("English terms with usage examples")


  @Test
  def testParse_rfdef_senses_are_ignored(): Unit =
    given EntryContext(EntryName("pionono", "es"))

    val result = subject.parse(fixture("parser/sense/es-pionono-sense-rfdef.html"))
    result match
      case Sense("a type of sweet pastry", _, _, _, _, _, _, Seq(
        Sense("in Latin America, it is similar to a Swiss roll,", _, _, _, _, _, _, Seq()),
        Sense("in Spain, it is a small cylindrical pastry made by dipping sponge cake in a syrupy mixture", _, _, _, _, _, _, Seq())
      )) :: Nil =>

  @Test
  def testParse_rfdef_senses_are_ignored_English(): Unit =
    given EntryContext(EntryName("quasiprimitive", "en"))

    val result = subject.parse(fixture("parser/sense/en-quasiprimitive-sense-rfdef.html"))
    result shouldBe empty

  @Test
  def testParse_legacy_usexes(): Unit =
    given EntryContext(EntryName("inventory", "en"))

    val result = subject.parse(fixture("parser/sense/en-inventory.html"))
    val usexes = for
      sense <- result
      usex <- sense.usexes
      senseText = sense.text.slice(0, 20) + "…"
    yield
      (senseText, usex.text)

    usexes should contain only(
      ("(operations) The sto…", "Due to an undersized **inventory** at the Boston outlet, customers had to travel to Providence to find the item."),
      ("(operations) A detai…", "The **inventory** included several items that one wouldn't normally think to find at a cheese shop."),
      ("(operations) The pro…", "This month's **inventory** took nearly three days."),
      ("A space containing t…", "You can't get through the underground tunnel if there are more than three items in your **inventory**."),
      ("(linguistics , espec…", "Germanic languages have a marked tendency towards large vocalic **inventories**.")
    )
