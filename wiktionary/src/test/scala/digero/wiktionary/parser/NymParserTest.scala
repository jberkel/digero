package digero.wiktionary.parser;

import digero.test.Fixture
import digero.wiktionary.model.EntryName
import digero.wiktionary.model.lexical.RelationType.{AlternativeForm, CoordinateTerm, Imperfective, Perfective, Synonym}
import digero.wiktionary.model.lexical.{Nym, RelationType}
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import scala.compiletime.uninitialized

class NymParserTest extends Fixture:
  var subject: NymParser = uninitialized

  given EntryContext(EntryName("source", "en"))

  @BeforeEach
  def prepare(): Unit = subject = NymParser()

  @Test
  def parseSynonyms(): Unit =
    val nyms = subject.parse(fragment("parser/nym/synonym.html"))
    nyms should contain inOrderOnly (
      Nym(Synonym, "source", "baz", None),
      Nym(Synonym, "source", "bazz", None)
    )

  @Test
  def parseCoordinateTerm(): Unit =
    val nyms = subject.parse(fragment("parser/nym/cot.html"))
    nyms should contain only Nym(CoordinateTerm, "source", "cot", None)

  @Test
  def parseAlternativeForm(): Unit =
    val nyms = subject.parse(fragment("parser/nym/alti.html"))
    nyms should contain only Nym(AlternativeForm, "source", "alt", None)

  @Test
  def parsePerfective(): Unit =
    given EntryContext(EntryName("spočívat", "cs"))

    val nyms = subject.parse(fragment("parser/nym/perfective.html"))
    nyms should contain only Nym(Perfective, "spočívat", "spočinout", None)

  @Test
  def parseImperfective(): Unit =
    given EntryContext(EntryName("bezár", "hu"))

    val nyms = subject.parse(fragment("parser/nym/imperfective.html"))
    nyms should contain only Nym(Imperfective, "bezár", """zár""", None)

  @Test
  def parseTransliteration(): Unit =
    given EntryContext(EntryName("στρύχνον", "grc"))
    subject.parse(fragment("parser/nym/synonym_different_link_target.html")) match
      case Nym(_, _, _, Some("hippomanés")) :: Nil =>

  @Test
  def parseDifferentLinkTargets(): Unit =
    given EntryContext(EntryName("στρύχνον", "grc"))

    val nyms = subject.parse(fragment("parser/nym/synonym_different_link_target.html"))
    nyms should contain only Nym(Synonym, "στρύχνον", "[ἱππομᾰνές](./ἱππομανές#Ancient_Greek \"ἱππομανές\")", Some("hippomanés"))

  @Test
  def ignoreSelfLinks(): Unit =
    val nyms = subject.parse(fragment("parser/nym/synonym_self_link.html"))
    nyms shouldBe empty

  private def fragment(fixturePath: String): Element =
    Jsoup.parseBodyFragment(fixture(fixturePath)).body().firstElementChild()