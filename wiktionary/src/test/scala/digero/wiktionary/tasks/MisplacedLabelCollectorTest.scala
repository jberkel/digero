package digero.wiktionary.tasks

import digero.wiktionary.tasks.MisplacedLabelCollector.{Result, ResultType}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp
import org.sweble.wikitext.engine.{PageId, PageTitle, WtEngineImpl}

import scala.collection.immutable.::

class MisplacedLabelCollectorTest:
  @Test
  def testCorrectLabel(): Unit =
    val results = collect("# {{lb|en|foo}} Bar\n\n")
    assertThat(results.isEmpty).isTrue

  @Test
  def testCollectSimple(): Unit =
    collect("# {{lb|fi|foo}} Bar\n\n") match
      case Result(_, _, _, Some("fi"), _) :: Nil =>

  @Test
  def testLanguageNotResolved(): Unit =
    collect("# {{lb|xxx|foo}} Bar\n\n") match
      case Result(ResultType.LabelContainsUnresolvedLanguageCode, "foo", "en", Some("xxx"), _) :: Nil =>

  @Test
  def testCollectNamedParameterFirst(): Unit =
    collect("{{lb|sort=Baz|fi|foo}} Bar\n\n") match
      case Result(ResultType.LabelMisplaced, "foo", "en", Some("fi"), _) :: Nil =>

  private def collect(wikiText: String) =
    val wikiConfig = DefaultConfigEnWp.generate
    val engine = new WtEngineImpl(wikiConfig)
    val pageId = new PageId(PageTitle.make(wikiConfig, "test"), 0)
    val page = engine.postprocess(pageId, wikiText, null).getPage
    MisplacedLabelCollector.collect("foo", "en", page)
