package digero.wiktionary.tasks.parse

import digero.SparkRunner
import digero.wiktionary.HTMLDumpIntegrationTest
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.api.{BeforeAll, Tag, Test, TestInstance}

import java.io.File
import scala.compiletime.uninitialized
import scala.util.Using

@Tag("integration")
@TestInstance(PER_CLASS)
class ParseHTMLDumpTaskTest extends HTMLDumpIntegrationTest:
  var input: File = uninitialized

  @BeforeAll
  def prepareHTMLDump(): Unit =
    input = importHTMLDump()

  @Test
  def parseHTMLDump(): Unit =
    val output = parseHTMLDump(input)

    Using.resource(SparkRunner("ParseHTMLDumpTest-read", null)) { runner =>
      val dataset = runner.session.read.parquet(output.getAbsolutePath)
      assertThat(dataset.count()).isEqualTo(15)
    }
