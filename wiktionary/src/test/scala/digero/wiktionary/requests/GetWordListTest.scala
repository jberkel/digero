package digero.wiktionary.requests

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import scala.jdk.CollectionConverters.*

class GetWordListTest:

  @Test
  def testApply(): Unit =
    try
      val result = GetWordList("User:Jberkel/lists/wanted/languages")
      assertThat(result.asJava).contains("de", "fr", "en", "ca")
    catch case _:requests.UnknownHostException => ()
