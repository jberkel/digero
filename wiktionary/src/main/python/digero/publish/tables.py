#!/usr/bin/env python
# encoding: utf-8
import json
from textwrap import dedent


def make_language_index(languages):
    return dedent("""\
        {| class="wikitable sortable"
        |-
        ! Count
        ! Language
        ! class="unsortable" | Raw data
        %s
        |-
        |}
    """) % '\n'.join("""|-\n| style="text-align:right;" | %d\n|[[/%s|%s]]\n|%s"""
                     % (count, language_code, canonical_name, raw_data_url)
                     for (count, language_code, canonical_name, raw_data_url, _) in languages)


def make_word_table(words, language):
    return dedent("""\
        {| class="wikitable sortable"
        |-
        ! Count
        ! Term
        %s
        |-
        |}
    """) % '\n'.join("""|-\n| style="text-align:right;" | %d\n| {{l|%s|%s}}"""
                     % (word['count'], language, word['term']) for word in words)


def make_tsv(path, lang_code, top=1000):
    with open(path, 'r') as file:
        rows = (([lang_code, row['target']] + row['sources']) for row in
                (json.loads(line) for line in file.readlines()[:top]))
        return '\n'.join('\t'.join(row) for row in rows)
