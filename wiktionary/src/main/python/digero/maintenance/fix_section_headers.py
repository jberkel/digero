#!/usr/bin/env python3
# encoding: utf-8

import pywikibot
from pywikibot.diff import PatchManager
import mwparserfromhell
import json

# https://en.wiktionary.org/wiki/Wiktionary:Entry_layout
# https://en.wiktionary.org/wiki/User:Erutuon/mainspace_headers/whitelist
mapping = {
    "Adverd": "Adverb",
    "Adjective form": "Adjective",
    "Adjektive": "Adjective",
    "Adjective 1": "Adjective",
    "Adjective 2": "Adjective",

    "Alternatie forms": "Alternative forms",
    "Alternate forms": "Alternative forms",
    "Alterantive forms": "Alternative forms",
    "Alternative terms": "Alternative forms",
    "Alternative Form": "Alternative forms",
    "Alternative Forms": "Alternative forms",
    "Alternative form": "Alternative forms",
    "Alternative name": "Alternative forms",
    "Alternative names": "Alternative forms",
    "Alternative spelling": "Alternative forms",
    "Alternative spellings": "Alternative forms",
    "Alternative versions": "Alternative forms",
    "Alternative": "Alternative forms",
    "Alternative version": "Alternative forms",

    "Anagram": "Anagrams",

    "Clsaaifier": "Classifier",

    "Co-hyponyms": "Coordinate terms",

    "Combining forms": "Combining form",

    "Conjuction": "Conjunction",

    "Conugation": "Conjugation",
    "Conjugaton": "Conjugation",
    "Conjugration": "Conjugation",
    "Conjuugation": "Conjugation",
    # "Prepositions": "Conjugation",

    "Coordinate term": "Coordinate terms",
    "Coordinated term": "Coordinate terms",
    "Coordinated terms": "Coordinate terms",
    "Coordinating terms": "Coordinate terms",
    "Corresponding terms": "Coordinate terms",
    "Corresponding term": "Coordinate terms",
    "Co-ordinate terms": "Coordinate terms",

    "Derived": "Derived terms",
    "Derived words": "Derived terms",
    "Derived forms": "Derived terms",
    "Derived term": "Derived terms",
    "Derive terms": "Derived terms",
    "Derived Terms": "Derived terms",
    "Derived terms=": "Derived terms",
    "Derivedd terms": "Derived terms",
    "Derived terrms": "Derived terms",
    "Derivatives": "Derived terms",
    "Derived times": "Derived terms",
    "Derived terms and phrases": "Derived terms",
    "Derivedterms": "Derived terms",
    "Compound words": "Derived terms",
    "Compound terms": "Derived terms",

    "=Declension": "Declension",
    "Declenasion": "Declension",
    "Decchelsion": "Declension",
    "Declensions": "Declension",

    "Desendants": "Descendants",
    "Desceandants": "Descendants",
    "Descedants": "Descendants",
    "Descednants": "Descendants",
    "Descendant": "Descendants",
    "Descenants": "Descendants",
    "Descendats": "Descendants",
    "Descendents": "Descendants",
    "Deescendants": "Descendants",
    "Descendedants": "Descendants",
    "Deacendants": "Descendants",
    "Descended terms": "Descendants",
    "Borrowings": "Descendants",

    "Demonstrative": "Determiner",

    "Diacritic": "Diacritical mark",

    "Etymology=": "Etymology",
    "Etyymology": "Etymology",
    "Etynology": "Etymology",
    "Etymoology": "Etymology",
    "Etymologys": "Etymology",
    "Etimology": "Etymology",
    "Eymology": "Etymology",
    "Etymoloy": "Etymology",
    "Etymolgy": "Etymology",
    "Etymology1": "Etymology 1",
    "Etymmology 1": "Etymology 1",
    "Etymology2": "Etymology 2",
    "Etymmology 2": "Etymology 2",
    "Etymology3": "Etymology 3",
    "Etymology4": "Etymology 4",

    "Hán tự": "Han character",
    "Holonym": "Holonyms",

    "Hyponym": "Hyponyms",
    "Hyonyms": "Hyponyms",
    "Hyponymns": "Hyponyms",

    "Hyperonym": "Hypernyms",
    "Hyperonyms": "Hypernyms",
    "Hypernym": "Hypernyms",
    "Hypernymns": "Hypernyms",

    "External links": "Further reading",
    "=Further reading": "Further reading",
    "Further readings": "Further reading",
    "Furthter reading": "Further reading",
    "Further Reading": "Further reading",
    "Furter readings": "Further reading",
    "Further reasons": "Further reading",
    "Futher reading": "Further reading",
    "For further information": "Further reading",
    "External Reading": "Further reading",

    "Bopomofo character": "Letter",

    "Sumerogram": "Logogram",

    # "Dependent noun": "Noun",
    "Noun form": "Noun",
    "Plural noun": "Noun",
    "Adjectival noun": "Noun",
    "Relational": "Noun",
    "Noun/Adjectival noun": "Noun",
    "Noun 1": "Noun",
    "Noun 2": "Noun",
    "Noun 3": "Noun",

    "Numerals": "Numeral",
    "Ordinal number": "Numeral",

    "Participles": "Participle",
    "Partiicple": "Participle",

    "Postpositional phrase": "Phrase",

    "Pronucation": "Pronunciation",
    "PRonunciation": "Pronunciation",
    "pronunciation": "Pronunciation",
    "Pronounciation": "Pronunciation",
    "Pronunciattion": "Pronunciation",
    "Pronuniation": "Pronunciation",
    "Pronuncation": "Pronunciation",
    "Pronuciation": "Pronunciation",
    "Pronunciaton": "Pronunciation",
    "Pronuncaition": "Pronunciation",
    "Pronunication": "Pronunciation",
    "Pronucniation": "Pronunciation",
    "Pronuniciation": "Pronunciation",
    "Pronunciations": "Pronunciation",
    "Pronunciation=": "Pronunciation",
    "Pronunciation1": "Pronunciation 1",
    "Pronunciation2": "Pronunciation 2",

    "Proper Noun": "Proper noun",
    "Proper nouns": "Proper noun",

    "Suffix pronoun": "Pronoun",
    "Prepositional pronoun": "Pronoun",

    "Punctuation": "Punctuation mark",

    # "Clitic": "POS",
    # "Enclitic": "POS",
    # "Enclitics": "POS",

    "Quotes": "Quotations",
    "Citations": "Quotations",

    "Related": "Related terms",
    "Reltaed terms": "Related terms",
    "Realted terms": "Related terms",
    "=Related terms": "Related terms",
    "Related words": "Related terms",
    "Reelated terms": "Related terms",
    "Possibly related": "Related terms",
    "Similar terms": "Related terms",

    "Reference": "References",
    "Rererences": "References",
    "Referrences": "References",
    "Refererences": "References",
    "Referenes": "References",
    "References=": "References",
    "Bibliography": "References",
    "Sources": "References",

    "Romaji": "Romanization",

    "Verb root": "Root",

    "See": "See also",
    "See Also": "See also",
    "See also=": "See also",
    "See alos": "See also",
    "Also see": "See also",
    "See more": "See also",
    "Epithets of Krishna": "See also",

    "Stem sets": "Stem set",

    "Categorizing prefix": "Prefix",

    "Primary verb suffix": "Suffix",
    "Verb suffix": "Suffix",
    "Derivational noun suffix": "Suffix",

    "Anyonym": "Antonyms",
    "Antonym": "Antonyms",
    "Synonym": "Synonyms",
    "Syonyms": "Synonyms",
    "Synonums": "Synonyms",
    "Synonymes": "Synonyms",
    "Synopnyms": "Synonyms",
    "Synonysm": "Synonyms",
    "Synonymous": "Synonyms",
    "Parasynonyms": "Synonyms",
    "Dialectal synonyms": "Synonyms",

    "Translation": "Translations",
    "Tranlations": "Translations",
    "Translatons": "Translations",

    "Usage note": "Usage notes",
    "Usage Note": "Usage notes",
    "Usage note:": "Usage notes",
    "Usage terms": "Usage notes",
    "Usage Notes": "Usage notes",
    "Usage notes:": "Usage notes",
    "Use": "Usage notes",
    "Note": "Usage notes",
    "Spelling notes": "Usage notes",
    "Pronunciation notes": "Usage notes",
    "Imperfect Usage Notes": "Usage notes",

    "Adjectival verb": "Verb",
    "Stative verb": "Verb",
    "Veb": "Verb",
    "Verb form": "Verb",
    "Infinitive": "Verb",
    "Converb": "Verb",
    "Concomitant verb": "Verb",
    "Gerund": "Verb"
}


def fix(pages):
    site = pywikibot.Site()

    for page_name in pages:
        page = pywikibot.Page(site, page_name)
        print(page)

        text = page.text
        parsed = mwparserfromhell.parse(text)
        for heading in parsed.filter_headings():
            if mapped := mapping.get(str(heading.title.strip()), None):
                if '=' in heading.title:
                    heading.level += 1
                heading.title = mapped

        replaced = str(parsed)

        if text != replaced:
            page.text = replaced
            PatchManager(text, replaced, context=3).print_hunks()
            page.save(summary='Normalized section header (semi-automatic)', watch='nochange')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Fix section headers.')
    parser.add_argument('sections.ndjson', help='Section overview')
    args = vars(parser.parse_args())

    with open(args['sections.ndjson'], 'r') as file:
        sections = [json.loads(line) for line in file]
        pages = {term for section in sections for term in section['entries'] if 'derived terms' not in term}
        fix(pages)
