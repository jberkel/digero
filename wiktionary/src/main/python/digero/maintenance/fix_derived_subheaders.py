#!/usr/bin/env python3
# encoding: utf-8

import pywikibot
from pywikibot.diff import PatchManager
import mwparserfromhell

# Fixes nesting in the form of:
#
# ====Derived terms====
# =====Verbs=====
# =====Nouns=====
#
# etc. converting it to:
# ====Derived terms====
# ; Verbs
# ; Nouns
if __name__ == '__main__':
    pages: list[str] = []
    site = pywikibot.Site()

    for page_name in pages:
        page = pywikibot.Page(site, page_name)
        print(page)
        text = page.text
        parsed = mwparserfromhell.parse(text)
        derived_level = None
        for heading in parsed.filter_headings():
            if heading.title == 'Derived terms':
                derived_level = heading.level
                continue
            if derived_level:
                if heading.level > derived_level:
                    parsed.replace(heading, '; ' + str(heading.title))
                else:
                    derived_level = None

        replaced = str(parsed)

        if text != replaced:
            page.text = replaced
            PatchManager(text, replaced, context=3).print_hunks()
            page.save(summary='Normalized section header', watch='nochange')
