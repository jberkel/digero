#!/usr/bin/env python
import json
import pyspark
import spacy
from pyspark.sql.functions import col
from spacy.cli import download as download_model

MODELS = {
    'de': 'de_core_news_sm'
}


def load_csv(spark, file):
    """
    :param spark:
    :param file: the CSV file with sentences
    """
    df = spark.read.csv(file, schema='text STRING')
    return [row.text for row in df.collect()]


def load_text(spark, file):
    """
    :param spark:
    :param file: the file with text
    """
    df = spark.read.text(file)
    return [row.value for row in df.collect() if row.value.strip() != '']


def load_index(spark, file, language):
    """
    :param spark:
    :param file: the index file (dump-xml.parquet)
    :param language: the language code
    """
    df = spark.read.parquet(file)
    index = df.select(col('entry'), col('redirect')).where(col('language') == language)
    return {row.entry for row in index.collect()}


def parse(nlp, language: str, index, examples: list[str]):
    def mp_tokenize():
        tokens = []
        for doc in nlp.pipe(examples,
                            disable=['tok2vec', 'tagger', 'parser', 'lemmatizer'],
                            n_process=-1,  # multiprocessing.cpu_count()
                            batch_size=2000):
            for token in doc:
                if token.is_punct or token.is_digit or token.like_num or token.is_space:
                    continue
                if len(token.text) < 2:
                    continue
                if language != 'de' and token.text[0].isupper():
                    continue
                text = token.text.replace('’', "'")
                if text in index or text.lower() in index:
                    pass
                else:
                    tokens.append(text)
        return tokens

    missing: dict[str, int] = {}
    for token in mp_tokenize():
        counter = missing.setdefault(token, 0)
        missing[token] = counter + 1
    return sorted([(count, key) for (key, count) in missing.items()], reverse=True)


def main(index_file: str, text_file: str, language: str, output):
    model_name = MODELS.get(language, f"{language}_core_web_sm")
    try:
        nlp = spacy.load(model_name)
    except IOError:
        download_model(model_name)
        nlp = spacy.load(model_name)

    spark = pyspark.sql.SparkSession.builder.master('local[*]').getOrCreate()

    if text_file.endswith(".csv"):
        text = load_csv(spark, text_file)
    else:
        text = load_text(spark, text_file)

    index = load_index(spark, index_file, language)

    missing = parse(nlp, language, index, text)
    with open(output, 'w') as f:
        for (count, key) in missing:
            json.dump({'count': count, 'term': key}, f, ensure_ascii=False)
            f.write('\n')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Find missing from examples')
    parser.add_argument('dump-xml.parquet', help='parsed XML dump')
    parser.add_argument('examples.[csv|txt]', help='text file, either csv or txt')
    parser.add_argument('language', help='language code')
    parser.add_argument('missing.jsonl', help='Output path')
    args = vars(parser.parse_args())

    main(index_file=args['dump-xml.parquet'],
         text_file=args['examples.[csv|txt]'],
         language=args['language'],
         output=args['missing.jsonl'])
