#!/usr/bin/env python
import sys

import duckdb


def dump_entry_parsed(entry: str, language: str | None, dump: str) -> str:
    relation = duckdb.read_parquet(f"{dump}/**/*.parquet", hive_partitioning=True)
    result = relation.filter(f"entry = '{entry}'")
    if language:
        result = result.filter(f"language = '{language}'")
    if row := result.project('html').fetchone():
        return row[0]
    else:
        raise Exception(f"Entry <{entry}> not found")


def dump_entry_raw(title: str, dump: str) -> str:
    relation = duckdb.read_parquet(f"{dump}/**/*.parquet")
    result = relation.filter(f"title = '{title}'").project('content')
    if row := result.fetchone():
        return row[0]
    else:
        raise Exception(f"Entry <{title}> not found")


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Extract from HTML dump')
    parser.add_argument('dump-html.parquet', help='parsed HTML dump')
    parser.add_argument('[lang:]entry', help='entry')
    parser.add_argument('--raw', help='raw input format', action='store_true')
    args = vars(parser.parse_args())
    entry = args['[lang:]entry']

    if ':' in entry:
        language, entry = entry.split(':', 2)
    else:
        language = None

    try:
        if args['raw']:
            print(dump_entry_raw(entry, args['dump-html.parquet']))
        else:
            print(dump_entry_parsed(entry, language, args['dump-html.parquet']))
    except KeyError:
        print("Entry not found", file=sys.stderr)
        exit(1)
