#!/usr/bin/env python
import itertools
import os

from pywikibot import Category, Site

os.environ['PYWIKIBOT_USERINTERFACE_LANG'] = 'en'

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Get tracking categories from API')

    site = Site('en', 'wiktionary')

    filtered_categories = [
        'Entries using missing taxonomic names',
        'Template tracking',
        'Wiktionary maintenance'
    ]
    subcategories = [
        Category(site, name).subcategories() for name in filtered_categories
    ]

    categories = itertools.chain(*subcategories)

    cats = {category.title(with_ns=False) for category in categories}
    print("\n".join(sorted(cats)))
