#!/usr/bin/env python
import json
from itertools import groupby

from pywikibot import LexemeForm, LexemePage, Site, ItemPage


def _entry(form: LexemeForm, language_code: str) -> tuple[str, list[str]]:
    value = form.representations[language_code]
    features = form.grammaticalFeatures
    feature_ids = [page.getID() for page in features]
    return value, feature_ids


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Get conjugations for a verb from Wikidata')
    parser.add_argument('lexeme_id', help='The Lexeme ID (e.g. L685315)')
    args = vars(parser.parse_args())

    site = Site("wikidata", "wikidata")
    lexeme_page = LexemePage(site, args['lexeme_id'])
    lexeme_page.get()

    language: ItemPage = lexeme_page.language
    language.get()
    language_code = language.claims['P218'][0].getTarget()

    forms = [_entry(form, language_code) for form in lexeme_page.forms]
    keyed_forms = {k: [{'form': k,
                        'features': f[1]} for f in v] for k, v in groupby(sorted(forms), key=lambda x: x[0])}
    print(json.dumps(keyed_forms, ensure_ascii=False, indent=True))
