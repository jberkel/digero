package digero.wiktionary.parser

import digero.wiktionary.parser
import digero.wiktionary.parser.ParsoidHTMLParser.{ElementFilter, Entry, parseURI}
import ipanema.language.model.LanguageData
import ipanema.links.Link
import org.apache.commons.logging.LogFactory
import org.jsoup.{Jsoup, nodes}
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.{Evaluator, QueryParser}

import java.net.URI
import scala.jdk.OptionConverters.*
import digero.wiktionary.parser.JsoupExtensions.*

class ParsoidHTMLParser extends Serializable with HTMLParser:
  /**
   * Splits a Wiktionary HTML page into language sections.
   */
  def split(input: String): Seq[Entry] =
    val document = parse(input)
    document.outputSettings().prettyPrint(false)
    for
      h2Element <- document.getElementsByTag("h2")
      section <- Option(h2Element.parent) if section.tagName == "section"
      language = h2Element.text
      languageCode = ParsoidHTMLParser.languageData.getLanguage(language).toScala.map(_.getCode)

    yield Entry(language, languageCode, section.serialize(false))


  /**
   * Extracts all links from a Wiktionary HTML entry.
   */
  def links(input: String, filter: ElementFilter = _ => true)(using entry: String): Seq[Link] =
    for
      element <- parse(input).select(wikiLinks) if filter(element)
      uri <- parseURI(element.attr("href"))
      link = Link.parse(uri)
    yield link

  private lazy val wikiLinks: Evaluator = QueryParser.parse("a[rel=mw:WikiLink]")

  private def parse(input: String):Document = Jsoup.parse(input, parser)


object ParsoidHTMLParser:
  case class Entry(language: String, code: Option[String], html: String)

  private val LOG = LogFactory.getLog(classOf[ParsoidHTMLParser])
  private val languageData = LanguageData.load()

  case class UnknownLanguageException(language: String) extends Exception

  type ElementFilter = Element => Boolean

  def parseURI(string: String)(using entry: String): Option[URI] =
    val quoted = string
      .replace("^", "%5E")
      .replace("\"", "%22")
      .replace("\\", "%5C")
      .replace("`", "%60")

    try
      Some(URI.create(quoted))
    catch case exception: IllegalArgumentException =>
      LOG.error(s"error parsing link <$string> in entry <$entry>", exception)
      None

extension (filter: ElementFilter)
  def negate(): ElementFilter = x => !filter(x)
