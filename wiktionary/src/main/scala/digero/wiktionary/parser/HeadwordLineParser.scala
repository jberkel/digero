package digero.wiktionary.parser

import digero.wiktionary.model.lexical.{GrammaticalGender, Inflection, InflectionType}
import digero.wiktionary.model.{HeadwordLine, Transliterated}
import digero.wiktionary.parser.JsoupExtensions.*
import org.jsoup.nodes.{Element, Node}
import org.jsoup.select.Elements
import org.jsoup.select.Evaluator.AttributeWithValueMatching
import org.jsoup.select.NodeFilter.FilterResult

class HeadwordLineParser extends HTMLParser:
  private val GenderRegex = """\b(gender|(in)?animate)\b""".r

  def parse(html: String): Seq[HeadwordLine] =
    val document = parseDocument(html)
    for
      headwordElement <- document.select(".headword")
      templateExpansionId <- headwordElement.about
      templateExpansionElements = document.getElementsByAttributeValue("about", templateExpansionId)
    yield
      HeadwordLine(
        text = headword(headwordElement, document),
        alternativeSpellings = spellings(headwordElement),
        inflections = inflections(templateExpansionElements),
        genders = genders(templateExpansionElements),
        categories = templateExpansionElements.categories.toSet)


  private def headword(element: Element, document: Element): Either[String, Transliterated] =
    val script = element.className().split(' ').head
    script match
      case "Latn" => Left(headwordText(element, script))
      case _ =>
        val transliterations = for
            tr <- document.select(".tr")
            text = tr.text().strip() if !text.isBlank
        yield text
        Right(Transliterated(headwordText(element, script), script, transliterations))


  private def headwordText(element: Element, script: String): String =
    script match
      case "Jpan" =>
        element.clone().filter((node: Node, _) =>
          node match
            // filter out ruby annotations for now
            case e: Element if Set("rp", "rt").contains(e.tagName) => FilterResult.REMOVE
            case _ => FilterResult.CONTINUE
        ).text()
      case _ => element.text()


  private def inflections(elements: Elements): Seq[Inflection] =
    InflectionType.values.toSeq.flatMap(inflections(elements, _))

  private def inflections(elements: Elements, inflectionType: InflectionType): Option[Inflection] =
    val forms = elements.select(inflectionType.selector).map(_.text()).filterNot(_.isBlank)

    if forms.isEmpty then
      None
    else
      Some(Inflection(inflectionType, forms))

  private def genders(elements: Elements): Seq[GrammaticalGender] =
    // parse all genders up to the first form-of (which might be unrelated)
    elements.takeWhile(!_.classes.contains("form-of")).flatMap(genders)

  private def genders(element: Element): Seq[GrammaticalGender] =
    val matcher = AttributeWithValueMatching("title", GenderRegex.pattern)
    for
      matched <- element.select(matcher)
      text = matched.text().strip() if !text.isBlank
      gender <- GrammaticalGender(text)
    yield gender

  private def spellings(element: Element): Seq[String] =
  (for
      rt <- element.select("rt")
      text = rt.text()
    yield text)
      match
        case Nil => Nil
        case elements => Seq(elements.mkString)

