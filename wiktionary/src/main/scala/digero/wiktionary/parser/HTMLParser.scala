package digero.wiktionary.parser

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser


trait HTMLParser:
  // ⚠️ the jsoup parser is not threadsafe
  lazy val parser: Parser = Parser.htmlParser

  def parseDocument(html: String, baseURI: String = ""): Document = Jsoup.parse(html, baseURI, parser)
