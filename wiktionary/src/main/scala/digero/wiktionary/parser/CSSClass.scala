package digero.wiktionary.parser

import digero.wiktionary.model.SenseKind
import org.jsoup.nodes.Element

enum CSSClass(val selector: String):
  case FormOfDefinition extends CSSClass(".form-of-definition")

  def senseKind: Option[SenseKind] =
    this match
      case FormOfDefinition => Some(SenseKind.FormOf)


object CSSClass:
  def applicable(element: Element): Set[CSSClass] =
    (for
      css <- CSSClass.values if !element.select(css.selector).isEmpty
    yield css).toSet
