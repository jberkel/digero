package digero.wiktionary.parser

import digero.wiktionary.parser.JsoupExtensions.*
import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element, Node, TextNode}

object HTMLPreprocessor:

  def preprocess(html: String): Document =
    val document = Jsoup.parseBodyFragment(html)

    removeUnwanted(document)
    document

  def removeUnwanted(element: Element): Unit =
    removeUnwantedElements(element)
    removeUnwantedAttributes(element)

  private def removeUnwantedElements(document: Element): Unit =
    for
      filterElement <- FILTER_ELEMENTS
      element <- document.select(filterElement.selector)
    do
      if filterElement.removeWhitespace == RemoveWhitespace.Preceding then
        removeIfBlankText(element.previousSibling())

        if element.previousSibling() != null then
          removeIfBlankText(element.previousSibling().lastChild())

      collapseWhitespace(element.previousSibling(), element.nextSibling())

      element.remove()

  private def collapseWhitespace(left: Node, right: Node): Unit =
    (left, right) match
      case (leftText: TextNode, rightText: TextNode)
        if leftText.text().endsWith(" ") && rightText.text().startsWith(" ") =>

        leftText.text(leftText.text().stripTrailing())

      case _ =>

  private def removeUnwantedAttributes(element: Element): Unit =
    for
      attribute <- FILTER_ATTRIBUTES
      element <- element.getElementsByAttribute(attribute)
    do
      element.removeAttr(attribute)

  private def removeIfBlankText(node: Node): Unit =
    node match
      case text: TextNode if text.text().isBlank => text.remove()
      case _ =>

  private val FILTER_ATTRIBUTES = Array(
    "about",
    "data-mw",
    "id"
  )

  private enum RemoveWhitespace:
    case None
    case Preceding

  private case class FilterElement(selector: String, removeWhitespace: RemoveWhitespace = RemoveWhitespace.None)

  private val FILTER_ELEMENTS: Seq[FilterElement] = Seq(
    FilterElement(".maintenance-line,.maintenance-box"),  // Template:maintenance_line
    FilterElement(".request-box"),                        // Template:request box
    FilterElement(".number-box"),                         // Template:number box
    FilterElement(".PIE-word"),                           // Template:PIE word
    FilterElement(".sister-project"),                     // Template:wikipedia etc.
    FilterElement(".was-wotd"),                           // Template:was_wotd
    FilterElement(".attentionseeking"),                   // Template:attention
    FilterElement(".color-panel"),                        // Template:color_panel
    FilterElement(".audiotable"),                         // Template:audio
    FilterElement("sup.mw-ref"),                          // Reference link
    FilterElement("div.mw-references-wrap"),              // Reference list
    FilterElement("abbr"),                                // gets mangled by html2text
    FilterElement("style"),                               // Extension:TemplateStyles
    FilterElement(".thumb.tmulti"),                       // Template:multiple images
    FilterElement("*[typeof*=\"mw:Extension/gallery\"]"), // <gallery>
    FilterElement("figure"),                              // no images (LEXI-71)
    FilterElement("span[typeof*=\"mw:File\"]"),           // images
    FilterElement("link[rel=\"mw:PageProp/Category\"]"),  // categories
    FilterElement("body > h3, h4, h5, h6"),               // first heading
    FilterElement("table"),                               // workaround for tables w/o specific CSS classes
    FilterElement(".mw-empty-elt"),                       // empty element
    FilterElement("small:containsOwn([Term?])", RemoveWhitespace.Preceding),          // term request
    FilterElement("small:containsOwn([script needed])", RemoveWhitespace.Preceding),  // script request
    FilterElement("span.interProject"),                   // https://en.wiktionary.org/wiki/MediaWiki:Gadget-AggregateInterprojectLinks.js
    FilterElement(".serial-comma"),                       // Template:,
    FilterElement(".etytree")                             // Template:etymon
  )
