package digero.wiktionary.parser

import digero.wiktionary.model.QualifiedLink
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.LinkListParser.{toRemove}
import org.jsoup.nodes.Element

/**
  Needs to handle different cases:

  - list with plain links
    * [[foo]]
    * [[baz]]

  - list with templated links
    * {{l|en|foo}}
    * {{l|en|baz}}

  - list with commas separated links, templated or not
    * [[foo]], [[baz]]
    * {{l|en|foo}}, {{l|en|baz}}

  - list with {{sense}} or {{antsense}}
    * {{sense|quux}} {{l|en|foo}}

  - unbulleted {{sense}}
    {{sense|quux}} {{l|en|foo}}

  - {{sense}} before a list

    {{sense|meat dish}}
    * {{l|en|dak bulgogi}}
    * {{l|en|dwaeji bulgogi}}

  - list with {{qualifier}}
    * {{qualifier|quux}} {{l|en|foo}}

  - list with ad-hoc sense qualifier
    * (quux) {{l|en|foo}}

  - list with appendix link
    * [[Appendix:Glossary of contract bridge terms]]
    See [[Appendix:Collocations of do, have, make, and take]] for [[collocation]]s of nap.

  - category link
    See [[:Category:en:Isolines]] for a full list of isoline types.

  - generated lists (using {{Template:list helper 2}} as backend)
    * {{list:quarks/en}}

    (''quarks'') '''quark''' {{l-self|en|foo}}, {{l-self|en|bar}} <sup>Categories: [[:Category:en:Quarks]]</sup>

  - list with : (<dl>/<dd>)
    : {{sense|assistant}} [[sidekick]]
    <dl>
      <dd>
        <span class="ib-brac qualifier-brac">(</span>
        <span class="ib-content qualifier-content">assistant</span>
        <span class="ib-brac qualifier-brac">)</span>
        <span class="ib-colon sense-qualifier-colon">:</span>
        <a href="/wiki/sidekick" title="sidekick">sidekick</a>
      </dd>
    </dl>

    : [[brace]], [[bitbrace]]

  - list formatted with <br>
    [[CN gas]]<BR>
    [[pepper spray]]<BR>
    [[mace]]

    <p id="mwDQ">
      <a rel="mw:WikiLink" href="./CN_gas" title="CN gas" id="mwDg">CN gas</a>
      <br id="mwDw">
      <a rel="mw:WikiLink" href="./pepper_spray" title="pepper spray" id="mwEA">pepper spray</a>
      <br id="mwEQ">
      <a rel="mw:WikiLink" href="./mace" title="mace" id="mwEg">mace</a>
    </p>

  - list formatted with blank lines
      [[diapause]]

      [[parabiont]]

      [[parabiotic]]

  - unbulleted plain entry links
    [[early voter]], [[early vote]]

  - bulleted {{seeSynonyms}} link to thesaurus page (class=see-cites)
    * {{sense|expression of anger}} {{seeSynonyms|en|dammit}}
    <li id="mwiQ">
      <span class="ib-brac qualifier-brac" about="#mwt119" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;sense&quot;,&quot;href&quot;:&quot;./Template:sense&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;expression of anger&quot;}},&quot;i&quot;:0}}]}" id="mwig">(</span>
      <span class="ib-content qualifier-content" about="#mwt119">expression of anger</span>
      <span class="ib-brac qualifier-brac" about="#mwt119">)</span>
      <span class="ib-colon sense-qualifier-colon" about="#mwt119" id="mwiw">:</span>
      <span class="see-cites" about="#mwt120" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;seeSynonyms&quot;,&quot;href&quot;:&quot;./Template:seeSynonyms&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;en&quot;},&quot;2&quot;:{&quot;wt&quot;:&quot;dammit&quot;},&quot;sense&quot;:{&quot;wt&quot;:&quot;1&quot;}},&quot;i&quot;:0}}]}" id="mwjA">
        <i>See</i> <a rel="mw:WikiLink" href="./Thesaurus:dammit#English" title="Thesaurus:dammit">Thesaurus:dammit</a>
      </span>
    </li>

  - unbulleted {{seeSynonyms}}
    {{seeSynonyms|en|make matters worse}}

  - list with untemplated thesaurus (or wikisaurus) link
    * See also [[Thesaurus:common]]
    * [[WS:male genitalia]]

  - unbulleted + untemplated thesaurus link
    See also [[Thesaurus:easy thing]]
    see [[Thesaurus:tiny]]
    : ''See [[Thesaurus:masturbate]].''

  - ad-hoc link to terms in another entry
    * ''see list in'' '''{{l|en|yestereve}}'''
    : ''see:'' [[autochthonous]]
    See {{m|en|bao}}
    ''See Related terms for'' '''[[divine]]'''
    See ''[[haem-#Derived terms|haem- § Derived terms]]''
    ''For a taxonomic list, see [[w:List of Nebria species]].''
    See {{m|en|tubular#Synonyms|tubular § Synonyms}}.

  - {{synonyms}} (invalid, should be used under definition line)
    {{synonyms|en|delegate general}}
    <p id="mwJg">
      <span class="nyms synonym" about="#mwt17" typeof="mw:Transclusion" data-mw="{&quot;parts&quot;:[{&quot;template&quot;:{&quot;target&quot;:{&quot;wt&quot;:&quot;synonyms&quot;,&quot;href&quot;:&quot;./Template:synonyms&quot;},&quot;params&quot;:{&quot;1&quot;:{&quot;wt&quot;:&quot;en&quot;},&quot;2&quot;:{&quot;wt&quot;:&quot;delegate general&quot;}},&quot;i&quot;:0}}]}" id="mwJw">
        <span class="defdate">Synonym:</span>
        <span class="Latn" lang="en">
          <a rel="mw:WikiLink" href="./delegate_general?action=edit&amp;redlink=1#English" title="delegate general" class="new" typeof="mw:LocalizedAttrs" data-mw-i18n="{&quot;title&quot;:{&quot;lang&quot;:&quot;x-page&quot;,&quot;key&quot;:&quot;red-link-title&quot;,&quot;params&quot;:[&quot;delegate general&quot;]}}">delegate general</a>
        </span>
      </span>
    </p>

  - {{prefixsee}}, {{suffixsee}}, {{circumfixsee}}, {{interfixsee}}}, {{infixsee}}, {{rootsee}} (using Extension:CategoryTree)

    <div class="derivedterms CategoryTreeTag" data-ct-mode="10" data-ct-options="…">
      <div class="CategoryTreeSection">
        <div class="CategoryTreeItem">
          <span class="CategoryTreeBullet">
            <a class="CategoryTreeToggle CategoryTreeToggleHandlerAttached" data-ct-title="English_terms_prefixed_with_back-" href="#" title="expand"></a>
          </span>
          <a href="/wiki/Category:English_terms_prefixed_with_back-" title="">English terms prefixed with back-</a>
        </div>
        <div class="CategoryTreeChildren" style="display:none"/>
      </div>
    </div>

  - {{lookfrom}} (pages starting with "foo")
    {{lookfrom|four}}
    <span class="plainlinks">
      <a class="external text" href="https://en.wiktionary.org/w/index.php?title=Special:Prefixindex&amp;from=four&amp;hideredirects=1&amp;stripprefix=0">Pages starting with “four”</a>
    </span>

  - {{prefixlanglemma}} (English terms starting with “foo”)
    <span class="plainlinks">
      <a class="external text" href="https://en.wiktionary.org/w/index.php?title=Category:English+lemmas&amp;from=mouth#mw-pages" title="">English terms starting with “mouth”</a>
    </span>

  - collapsed list with "show ▼" button ({{rel-top}}, {{rel-bottom}} etc.):
    [sometimes preceded by {{sense}}]
    <div class="NavFrame">
      <div class="NavHead">an activity that is easy</div>
      <div class="NavContent>
        <ul>
          <li><span class="Latn" lang="en"><a href="">breeze</a></li>
        </ul>
      </div>
    </div>

  - {{col2}}-based column layout with "show more ▼" button (also {{col-auto}})
    {{col2|en|foo|bar}}
    <div class="list-switcher">
      <div class="derivedterms term-list ul-column-count" data-column-count="2">
        <ul>
          <li>…</li>
        </ul>
      </div>
    </div>

  - - {{col2}}-based column layout with title (sometimes multiple, e.g. de:Schloss)
    {{col2|en|title=test|foo|bar}}
    <div class="term-list-header">test</div>
    <div class="list-switcher">
    …
    </div>

    {{col2|en|title={{lb|en|computing}} [[algorithm|Algorithm]] for sorting a list of items|…}}

  - a Wikipedia / sister project link
    * {{pedia}}

    <span typeof="mw:File"> <!-- RDFa, image link: https://www.mediawiki.org/wiki/Specs/HTML/2.8.0#Images -->
      <a href="https://en.wikipedia.org/wiki/foo" title="w:foo">
        <img src="//upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/15px-Wikipedia-logo-v2.svg.png" decoding="async" width="15" height="14" class="mw-file-element" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/23px-Wikipedia-logo-v2.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/30px-Wikipedia-logo-v2.svg.png 2x" data-file-width="103" data-file-height="94">
      </a>
    </span>
    <b class="Latn" lang="en">
      <a href="https://en.wikipedia.org/wiki/foo" class="extiw" title="w:foo">foo</a>
    </b>

  - a Wikipedia or other sister project box
    {{wp}}

    <div class="sister-wikipedia sister-project noprint floatright">
    </div>

    {{wikibooks}}

  - Wikipedia category box
    {{wikipedia|cat=Jewish ritual objects}}

  - a <table> / wikitable with arbitrary content
    {{Book-B}}

  - free text
    ''(Note: {{m|en|chock full}} is '''not''' derived from this word. In fact, it is an alteration of the earlier {{m|en|choke-full}}, which most likely derives from a variant of the word {{m|en|cheek}}.)''

  - an image
    [[File:Homonym Euler Diagram Semantics.svg|thumb|Euler diagram of choice semantic relations.]]
    <figure class="mw-default-size" typeof="mw:File/Thumb">
      <a href="/wiki/File:Homonym_Euler_Diagram_Semantics.svg" class="mw-file-description">
        <img src="//upload.wikimedia.org/wikipedia/commons/thumb/7/77/Homonym_Euler_Diagram_Semantics.svg/220px-Homonym_Euler_Diagram_Semantics.svg.png" decoding="async" width="220" height="120" class="mw-file-element" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/7/77/Homonym_Euler_Diagram_Semantics.svg/330px-Homonym_Euler_Diagram_Semantics.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/7/77/Homonym_Euler_Diagram_Semantics.svg/440px-Homonym_Euler_Diagram_Semantics.svg.png 2x" data-file-width="743" data-file-height="405">
      </a>
      <figcaption>Euler diagram of choice semantic relations.</figcaption>
    </figure>

  - misspelled template (class="new" + content starts with "Template:")
    {{derl3|en|foo}}
    <a href="/w/index.php?title=Template:derl3&amp;action=edit&amp;redlink=1" class="new" title="">Template:derl3</a>

  - {{checksense}}
  - {{Webster 1913}}

 */
class LinkListParser(using context: EntryContext) extends HTMLParser with Logger:

  def parse(html: String): Seq[QualifiedLink] =
    val doc = parseDocument(html).removing(toRemove)

    if doc.body().childrenSize() <= 1 then
      Seq.empty
    else
      parse(doc)

  private def parse(element: Element): Seq[QualifiedLink] =
    val listItems = element.select("li")
    if listItems.isEmpty then
      fallbackParseNoList(element)
    else
      val termListLinks = for
        termListHeader <- element.select("div.term-list-header")
        termListTitle = termListHeader.nonBlankText

        nextElement <- List(termListHeader.nextElementSibling())
        termListItem <- nextElement.select("li")

        link <- parseLinks(termListItem, termListTitle)
      yield (link, termListItem)

      val processedElements = termListLinks.map(_._2)

      val otherLinks = for
        li <- listItems if !processedElements.contains(li)
        sense = li.select(".qualifier-content").headOption.map(_.text())
        link <- parseLinks(li, sense)
      yield link

      termListLinks.map(_._1) ++ otherLinks

  private def parseLinks(element: Element, title: Option[String]): Seq[QualifiedLink] =
    for
      // https://www.mediawiki.org/wiki/Specs/HTML/2.8.0#Wiki_links
      wikiLink <- element.wikiLinks(false) if wikiLink.getNamespace == null
      language = Option(wikiLink.getLanguage).map(_.getCode)
      languageMatch = language.map(_ == context.languageCode).getOrElse(context.languageCode == "en")
      link = wikiLink if languageMatch
    yield QualifiedLink(link, title)


  private def fallbackParseNoList(element: Element): Seq[QualifiedLink] =
    // if there's no list, it's most likely just a single paragraph with links
    val qualifier = element.select(".qualifier-content").headOption.map(_.text())

    parseLinks(element, qualifier) match
      case Nil =>
        log.warn(s"no list elements in entry ${context.entry}:\n $element")
        /*
        scala.util.Using(new java.io.FileWriter("out/"+context.entry.name.replace(" ", "_") + ".html")) { fw =>
            fw.write(element.toString)
        }
        */
        Seq.empty
      case other => other

object LinkListParser:
  private val toRemove: String = Seq(
    ".CategoryTreeTag",   // Extension:CategoryTree
    ".sister-project",    // {{wikipedia}} etc.
    "link",               // categories
    ".plainlinks",        // {{lookfrom}}, {{prefixlanglemma}}
    "table:not(.derivedterms)",
    "*[typeof^=mw:File]", // images (File:)
    "div.checksense"      // {{checksense}}
  ).mkString(", ")
