package digero.wiktionary.parser

import org.apache.commons.logging.{Log, LogFactory}

trait Logger:
  val log: Log = LogFactory.getLog(this.getClass)
