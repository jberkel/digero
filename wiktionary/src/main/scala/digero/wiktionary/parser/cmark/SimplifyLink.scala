package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.ast.{Link, Text}
import com.vladsch.flexmark.util.ast.{Node, NodeVisitor, VisitHandler, Visitor}
import digero.wiktionary.parser.EntryContext
import digero.wiktionary.parser.cmark.FlexmarkExtensions.render

import scala.annotation.unused

object SimplifyLink:
  extension (converter: CommonMarkConverter)
    // Simplifies single links which point directly to an entry in a given
    // language to just their textual context
    // [foo](./foo#English "foo") => foo
    def simplifyEntryLink(commonMark: String)(using context: EntryContext): String =
      val document = converter.parser.parse(commonMark)
      val visitor = LinkVisitor(context.languageName.replace(" ", "_"))
      visitor.visit(document)
      if visitor.valid then
        document.render
      else
        // no simplification possible, just return unchanged original
        commonMark


private class LinkVisitor(val language: String):
  private val visitor = NodeVisitor(
    VisitHandler(classOf[Link], visitLink(_)),
    VisitHandler(classOf[Text], visitText(_))
  )

  var valid: Boolean = true

  def visit(node: Node): Unit = visitor.visit(node)

  private def visitText(@unused text: Text): Unit = valid = false

  private def visitLink(link: Link): Unit =
    val targetLanguage = link.getAnchorRef.unescape().split(":").head
    if targetLanguage == language && link.getTitle == link.getText then
      replaceWithText(link)
    else
      valid = false

  private def replaceWithText(link: Link): Unit =
    link.getParent.appendChild(new Text(link.getText.unescape()))
    link.unlink()
