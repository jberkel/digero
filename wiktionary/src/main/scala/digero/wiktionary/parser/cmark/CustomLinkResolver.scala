package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.html.renderer.ResolvedLink
import com.vladsch.flexmark.html2md.converter.{HtmlLinkResolver, HtmlLinkResolverFactory, HtmlNodeConverterContext}
import org.jsoup.nodes.Node as JsoupNode

import java.util

object CustomLinkResolver extends HtmlLinkResolver:
  override def resolveLink(node: JsoupNode,
                           context: HtmlNodeConverterContext,
                           link: ResolvedLink): ResolvedLink =
    import sttp.model.Uri

    if link.getUrl.startsWith("./") then
      val uri = Uri.unsafeParse(link.getUrl)
      if uri.params.get("action").isDefined then
        // remove all query parameters
        link.withUrl(uri.withParams(Map.empty).toString)
      else
        link
    else
      link

  var factory: HtmlLinkResolverFactory = new HtmlLinkResolverFactory:
    override def getAfterDependents: util.Set[Class[?]] = null
    override def getBeforeDependents: util.Set[Class[?]] = null
    override def affectsGlobalScope() = false
    override def apply(context: HtmlNodeConverterContext): HtmlLinkResolver = CustomLinkResolver
