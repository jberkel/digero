package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.html2md.converter.{CustomHtmlNodeRenderer, HtmlMarkdownWriter, HtmlNodeConverterContext}
import digero.wiktionary.parser.Logger
import org.jsoup.nodes.Element

private class CustomMappingRenderer(mapping: Map[Char, Char]) extends CustomHtmlNodeRenderer[Element] with Logger:
  def render(element: Element,
             context: HtmlNodeConverterContext,
             writer: HtmlMarkdownWriter): Unit =
    val text = context.processTextNodes(element)

    if text.forall(mapping.contains) then
      val mapped = text.map(char => mapping.getOrElse(char, char))
      writer.append(mapped)
    else
      context.processWrapped(element, null, true)


object CustomMappingRenderer:
  val superscriptRenderer: CustomMappingRenderer = CustomMappingRenderer(
    Map('0' -> '⁰',
      '1' -> '¹',
      '2' -> '²',
      '3' -> '³',
      '4' -> '⁴',
      '5' -> '⁵',
      '6' -> '⁶',
      '7' -> '⁷',
      '8' -> '⁸',
      '9' -> '⁹',
      '-' -> '⁻',
      '−' -> '⁻',
      '+' -> '⁺',
      'n' -> 'ⁿ',
      '=' -> '⁼',
      't' -> 'ᵗ',
      'h' -> 'ʰ'
    )
  )

  val subscriptRenderer: CustomMappingRenderer = CustomMappingRenderer(
    Map('0' -> '₀',
      '1' -> '₁',
      '2' -> '₂',
      '3' -> '₃',
      '4' -> '₄',
      '5' -> '₅',
      '6' -> '₆',
      '7' -> '₇',
      '8' -> '₈',
      '9' -> '₉',
      '-' -> '₋',
      '−' -> '₋',
      '+' -> '₊',
      'a' -> 'ₐ',
      'e' -> 'ₑ',
      'h' -> 'ₕ',
      'k' -> 'ₖ',
      'm' -> 'ₘ',
      'n' -> 'ₙ',
      'p' -> 'ₚ',
      's' -> 'ₛ',
      't' -> 'ₜ',
      '=' -> '₌',
      'ə' -> 'ₔ'
    )
  )
