package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.html2md.converter.HtmlNodeConverterContext
import org.jsoup.nodes.Node

object HtmlNodeConverterContextExtensions:

  extension (context: HtmlNodeConverterContext)
    private def iterator: Iterator[Node] =
      new Iterator[Node]:
        override def hasNext: Boolean = context.peek(1) != null
        override def next(): Node = context.next()

    def foreach[U](f: Node => U): Unit = iterator.foreach(f)