package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.html2md.converter.internal.HtmlConverterCoreNodeRenderer.isFirstChild
import com.vladsch.flexmark.html2md.converter.{CustomHtmlNodeRenderer, HtmlMarkdownWriter, HtmlNodeConverterContext}
import org.jsoup.internal.StringUtil
import org.jsoup.internal.StringUtil.normaliseWhitespace as normalizeHTMLWhitespace
import org.jsoup.nodes.Element

class CustomPNodeRenderer extends CustomHtmlNodeRenderer[Element]:

  /** based on [[com.vladsch.flexmark.html2md.converter.internal.HtmlConverterCoreNodeRenderer#processP]] */
  override def render(element: Element, context: HtmlNodeConverterContext, out: HtmlMarkdownWriter): Unit =

    val (isItemParagraph, isDefinitionItemParagraph) = element.firstElementSibling match
      case someElement if someElement eq element  => (element.parent.normalName == "li",
                                                      element.parent.normalName == "dd")
      case _ => (false, false)

    out.blankLineIf(!isItemParagraph && !isDefinitionItemParagraph && !isFirstChild(element))

    if element.childNodeSize > 0 then
      val text = context.processTextNodes(element)
      val normalized = normalizeHTMLWhitespace(text)
      out.append(normalized)

    out.line()

    if isItemParagraph || isDefinitionItemParagraph then
      out.tailBlankLine()