package digero.wiktionary.parser.cmark

import com.ibm.icu.text.{UnicodeSet, UnicodeSetSpanner}
import com.vladsch.flexmark.ast.Link
import com.vladsch.flexmark.formatter.{Formatter, MarkdownWriter, NodeFormatter, NodeFormatterContext, NodeFormattingHandler}
import com.vladsch.flexmark.html2md.converter.FlexmarkHtmlConverter.{B_NODE, DL_NODE, I_NODE, P_NODE, SUB_NODE, SUP_NODE}
import com.vladsch.flexmark.html2md.converter.{ExtensionConversion, FlexmarkHtmlConverter, HtmlNodeRenderer, HtmlNodeRendererHandler}
import com.vladsch.flexmark.parser.{Parser, ParserEmulationProfile}
import com.vladsch.flexmark.util.ast.{Document, TextCollectingVisitor}
import com.vladsch.flexmark.util.data.{DataHolder, DataKey, MutableDataSet}
import digero.wiktionary.parser.HTMLPreprocessor
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.cmark.CommonMarkConverter.{FormatterOptions, noLinksNodeFormattingHandler, specialChars}
import org.jsoup.nodes.Element

import java.util
import scala.jdk.CollectionConverters.*

case class ConversionOptions(doNotRenderLinks: Boolean = false)

class CommonMarkConverter(val options: ConversionOptions = ConversionOptions()):

  // converts HTML => CommonMark => Document AST => Text
  def htmlToCommonMark(html: String): Option[String] =
    htmlToCommonMarkDocument(html).map(document =>
      formatter.render(document).stripTrailing()
    )

  // this method is threadsafe
  def commonMarkToPlain(commonMark: String): String =
    val document = parser.parse(commonMark)
    val textCollectingVisitor = new TextCollectingVisitor()
    val text = textCollectingVisitor.collectAndGetText(document)
    text

  private def htmlToCommonMarkDocument(html: String): Option[Document] =
    val preprocessed = HTMLPreprocessor.preprocess(CommonMarkConverter.removeIgnorableCodepoints(html).toString)
    if preprocessed.text().isBlank then
      return None

    val commonMark = htmlConverter.convert(preprocessed.serialize(), 0)
    Option(parser.parse(commonMark))

  // configurable options
  private lazy val parserOptions: MutableDataSet = MutableDataSet()
  private lazy val converterOptions: MutableDataSet = MutableDataSet()
    .set(FlexmarkHtmlConverter.OUTPUT_ATTRIBUTES_ID, false)
    .set(FlexmarkHtmlConverter.TYPOGRAPHIC_REPLACEMENT_MAP, specialChars.asJava)
    // .set(FlexmarkHtmlConverter.DUMP_HTML_TREE, true)
    .set(FlexmarkHtmlConverter.EXT_INLINE_INS, ExtensionConversion.TEXT)

  private lazy val formatterOptions: MutableDataSet = MutableDataSet()
    .set(Formatter.FORMATTER_EMULATION_PROFILE, emulationProfile)
    .set(Formatter.MAX_TRAILING_BLANK_LINES, 0)
    .set(FormatterOptions.DO_NOT_RENDER_LINKS, options.doNotRenderLinks)

  private lazy val emulationProfile = ParserEmulationProfile.COMMONMARK_0_29

  // objects
  private lazy val htmlConverter: FlexmarkHtmlConverter =
    FlexmarkHtmlConverter
      .builder(converterOptions)
      .linkResolverFactory(CustomLinkResolver.factory)
      .htmlNodeRendererFactory((_: DataHolder) => new HtmlNodeRenderer {
        def getHtmlNodeRendererHandlers: util.Set[HtmlNodeRendererHandler[?]] =
          util.Set.of(
            new HtmlNodeRendererHandler[Element](I_NODE, classOf[Element], CustomEmphasisRenderer("*")),
            new HtmlNodeRendererHandler[Element](B_NODE, classOf[Element], CustomEmphasisRenderer("**")),
            new HtmlNodeRendererHandler[Element](SUB_NODE, classOf[Element], CustomMappingRenderer.subscriptRenderer),
            new HtmlNodeRendererHandler[Element](SUP_NODE, classOf[Element], CustomMappingRenderer.superscriptRenderer),
            new HtmlNodeRendererHandler[Element](P_NODE, classOf[Element], CustomPNodeRenderer()),
            new HtmlNodeRendererHandler[Element](DL_NODE, classOf[Element], CustomDlNodeRenderer())
          )
      })
      .build()

  lazy val parser: Parser = Parser.builder(parserOptions).build()

  private lazy val formatter: Formatter = Formatter
    .builder(formatterOptions)
    .nodeFormatterFactory((options: DataHolder) => new NodeFormatter {
      def getNodeFormattingHandlers: util.Set[NodeFormattingHandler[?]] =
        val handlers = util.HashSet[NodeFormattingHandler[?]]()
        if FormatterOptions.DO_NOT_RENDER_LINKS.get(options) then
          handlers.add(CommonMarkConverter.noLinksNodeFormattingHandler)
        handlers

      def getNodeClasses: util.Set[Class[?]] = null
    })
    .build()



object CommonMarkConverter:
  private object FormatterOptions:
    val DO_NOT_RENDER_LINKS: DataKey[Boolean] = DataKey[Boolean]("DO_NOT_RENDER_LINKS", false)

  private val noLinksNodeFormattingHandler = NodeFormattingHandler[Link](
    classOf[Link],
    (node: Link, context: NodeFormatterContext, _: MarkdownWriter) =>
      context.renderChildren(node)
  )

  /**
   * Unicode 9.0, chapter 5, Implementation Guidelines, section 5.21
   *
   * Normally, characters outside the repertoire of supported characters for an implementation
   * would be graphical characters displayed with a fallback glyph, such as a black box.
   * However, certain special-use characters, such as format controls or variation selectors,
   * do not have visible glyphs of their own, although they may have an effect on the display
   * of other characters. When such a special-use character is not supported by an
   * implementation, it should not be displayed with a visible fallback glyph, but instead
   * simply not be rendered at all. The list of such characters which should not be rendered
   * with a fallback glyph is defined by the Default_Ignorable_Code_Point property in the
   * Unicode Character Database.
   */
  private def removeIgnorableCodepoints(text: CharSequence): CharSequence =
    val ignorableSet = UnicodeSet("[\\p{Default_Ignorable_Code_Point}]")
    val spanner = UnicodeSetSpanner(ignorableSet)
    spanner.deleteFrom(text)

  val specialChars: Map[String, String] = Map(
    "&ldquo;" -> "“",
    "&rdquo;" -> "”",
    "&lsquo;" -> "‘",
    "&rsquo;" -> "’",
    "&apos;" -> "'",
    "&laquo;" -> "«",
    "&raquo;" -> "»",
    "&hellip;" -> "…",
    "&endash;" -> "–",
    "&emdash;" -> "—"
  )
