package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.formatter.Formatter
import com.vladsch.flexmark.util.ast.Document
import com.vladsch.flexmark.util.data.MutableDataSet

object FlexmarkExtensions:

  extension (document: Document)
    def render: String =
      val formatOptions = MutableDataSet()
      val formatter = Formatter.builder(formatOptions).build
      formatter.render(document).strip()
