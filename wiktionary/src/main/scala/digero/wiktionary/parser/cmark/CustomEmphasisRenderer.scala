package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.html2md.converter.{CustomHtmlNodeRenderer, HtmlMarkdownWriter, HtmlNodeConverterContext}
import digero.wiktionary.parser.cmark.CustomEmphasisRenderer.{ZWSP, isPunctuation, isWhitespace}
import org.jsoup.nodes.{Element, TextNode}

object CustomEmphasisRenderer:
  private val Punctuation: Set[Int] = Set(
    Character.DASH_PUNCTUATION,
    Character.START_PUNCTUATION,
    Character.END_PUNCTUATION,
    Character.CONNECTOR_PUNCTUATION,
    Character.OTHER_PUNCTUATION
  )

  private val ZWSP = "&ZeroWidthSpace;"

  // A Unicode punctuation character is a character in the Unicode P (punctuation) or S (symbol) general categories.
  inline def isPunctuation(string: String, index: Int): Boolean =
    val codepoint = string.codePointAt(index)
    Punctuation.contains(Character.getType(codepoint))

  // A Unicode whitespace character is a character in the Unicode Zs general category, or a tab (U+0009),
  // line feed (U+000A), form feed (U+000C), or carriage return (U+000D).
  inline def isWhitespace(string: String, index: Int): Boolean =
    val codepoint = string.codePointAt(index)
    Character.getType(codepoint) == Character.SPACE_SEPARATOR ||
      string.charAt(index) == '\t' ||
      string.charAt(index) == '\n' ||
      string.charAt(index) == '\f' ||
      string.charAt(index) == '\r'

class CustomEmphasisRenderer(val wrapMarkup: String) extends CustomHtmlNodeRenderer[Element]:
  // Adds a ZWSP when emphasis is used with punctuation and isn't
  // preceded or followed by whitespace
  // https://spec.commonmark.org/0.31.2/#emphasis-and-strong-emphasis
  def render(element: Element,
             context: HtmlNodeConverterContext,
             writer: HtmlMarkdownWriter): Unit =
    val text = element.text()
    if text.nonEmpty then
      val startsWithPunctuation = isPunctuation(text, 0)

      if startsWithPunctuation && !writer.isPendingSpace then
        writer.append(ZWSP)

    // the default flexmark implementation uses
    // element.nextElementSibling != null
    // for `needSpaceAround`
    context.wrapTextNodes(element, wrapMarkup, false)

    val endsWithPunctuation = text.length > 1 && isPunctuation(text, text.length - 1)
    val whitespaceFollows = context.peek() match
      case textNode: TextNode => isWhitespace(textNode.getWholeText, 0)
      case _ => false

    if endsWithPunctuation && !whitespaceFollows then
      writer.append(ZWSP)