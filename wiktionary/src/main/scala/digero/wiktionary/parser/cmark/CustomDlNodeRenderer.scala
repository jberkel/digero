package digero.wiktionary.parser.cmark

import com.vladsch.flexmark.html2md.converter.FlexmarkHtmlConverter.{DD_NODE, DT_NODE}
import com.vladsch.flexmark.html2md.converter.{CustomHtmlNodeRenderer, FlexmarkHtmlConverter, HtmlMarkdownWriter, HtmlNodeConverterContext}
import digero.wiktionary.parser.cmark.HtmlNodeConverterContextExtensions.foreach
import org.jsoup.nodes.Element

/**
 * Renders <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl">dl</a> (description list) elements.
 *
 * @see A <a href="https://talk.commonmark.org/t/description-list/289/27">discussion</a> about support
 * in CommonMark.
 */
class CustomDlNodeRenderer extends CustomHtmlNodeRenderer[Element]:

  /** based on [[com.vladsch.flexmark.html2md.converter.internal.HtmlConverterCoreNodeRenderer#processDl]] */
  override def render(element: Element, context: HtmlNodeConverterContext, out: HtmlMarkdownWriter): Unit =
    context.pushState(element)

    var lastWasDefinition = true
    var firstItem = true

    out.line()

    for
      node <- context
    do
      node.normalName match {
        case DT_NODE =>
          out.blankLineIf(lastWasDefinition).lineIf(!firstItem)
          context.processTextNodes(node, false)
          out.line()
          lastWasDefinition = false
          firstItem = false

        case DD_NODE =>
          handleDefinition(node.asInstanceOf[Element], context, out)
          lastWasDefinition = true
          firstItem = false

        case _ =>
      }

    context.popState(out)


  private def handleDefinition (element: Element, context: HtmlNodeConverterContext, out: HtmlMarkdownWriter): Unit =
    context.processTextNodes(element, false)
    out.line()