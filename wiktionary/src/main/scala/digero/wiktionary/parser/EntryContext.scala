package digero.wiktionary.parser

import digero.wiktionary.model.EntryName
import ipanema.language.model.LanguageData

case class EntryContext(entry: EntryName):
  def languageCode: String = entry.languageCode
  def languageName: String = LanguageData.load().getLanguage(languageCode).map(_.getCanonicalName).orElseThrow()
    
