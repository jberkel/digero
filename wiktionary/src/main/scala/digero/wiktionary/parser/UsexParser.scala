package digero.wiktionary.parser

import digero.microformats.{Embedded, Parser as MicroformatsParser}
import digero.wiktionary.model.Usex
import digero.wiktionary.parser.cmark.{CommonMarkConverter, ConversionOptions}
import org.jsoup.nodes.Element
import JsoupExtensions._

class UsexParser extends HTMLParser:
  private val microformatsParser = MicroformatsParser()
  private val commonMarkConverter = CommonMarkConverter(ConversionOptions(doNotRenderLinks = true))

  /**
   * @param element the main element of the sense line, usually &lt;li&gt;
   * @param context the entry that is currently being parsed
   */
  def parse(element: Element)(using context: EntryContext): Seq[Usex] =
    parseMicroformats(element) ++ parseLegacy(element)


  private def parseMicroformats(element: Element)(using context: EntryContext): Seq[Usex] =
    for
      mf <- microformatsParser.parse(element)(using baseURI = None, rootLang = Some(context.languageCode))
        if mf.`type`.contains("h-usage-example")

      case Embedded(exampleHtml, _, _) <- mf.get("example")
      example <- commonMarkConverter.htmlToCommonMark(exampleHtml)

      translationHtml = mf.get("translation") match
        case Some(Embedded(html, _, _)) => Some(html)
        case _ => None

      transliterationHtml = mf.get("transliteration") match
        case Some(Embedded(html, _, _)) => Some(html)
        case _ => None

      literallyHtml = mf.get("literally") match
        case Some(Embedded(html, _, _)) => Some(html)
        case _ => None

    yield
      Usex(
        example,
        translationHtml.flatMap(commonMarkConverter.htmlToCommonMark),
        transliterationHtml.flatMap(commonMarkConverter.htmlToCommonMark),
        literallyHtml.flatMap(commonMarkConverter.htmlToCommonMark)
      )


  /**
   * Parses legacy formatted usexes, but only for English entries:
   * {{{
   *  # Some sense.
   *  #: ''A usage example.''
   * }}}
   * @param element
   * @return
   */
  private def parseLegacy(element: Element)(using context: EntryContext): Seq[Usex] =
    if context.languageCode != "en" then
      Seq.empty
    else
      for
        i <- element.select("dl > dd > i:only-child")
        text <- commonMarkConverter.htmlToCommonMark(i.serialize(false))
      yield
        Usex(text, None, None, None)