package digero.wiktionary.parser

import digero.wiktionary.model.lexical.{Nym, RelationType}
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.cmark.CommonMarkConverter
import digero.wiktionary.parser.cmark.SimplifyLink._

import org.jsoup.nodes.Element


// Parses a single "nym" line
class NymParser extends HTMLParser with Logger:
  private val commonMarkConverter = CommonMarkConverter()

  def parse(element: Element)(using context: EntryContext): Seq[Nym] =
    require(element.classes.size >= 2)
    require(element.classes.head == "nyms")

    val nymType = element.classes(1)
    RelationType(nymType) match
      case None =>
        log.warn(s"unknown nym type $nymType in $context")
        Seq.empty
      case Some(relationType) =>
        for
          link <- element.select(s"> span[lang=${context.languageCode}]")
          if link.select(".selflink").isEmpty
          cmark <- commonMarkConverter.htmlToCommonMark(link.serialize(false))
          simplifiedLink = commonMarkConverter.simplifyEntryLink(cmark)
          transliteration = link.nextElementSiblings().select("span.tr").headOption.map(_.text())

        yield Nym(relationType, context.entry.name, simplifiedLink, transliteration)



