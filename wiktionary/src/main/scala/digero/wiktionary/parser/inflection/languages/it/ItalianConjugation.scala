package digero.wiktionary.parser.inflection.languages.it

import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.model.lexical.GrammaticalFeature.*
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.inflection.ParsedInflection
import digero.wiktionary.parser.table.{HTMLTableParser, TableData, TableHeaderContent, TableSection}
import org.jsoup.nodes.Document

object ItalianConjugation:

  private val mapping: Map[TableHeaderContent, Set[GrammaticalFeature]] = Map(
    TableHeaderContent("infinitive", Some("infinito")) -> Set(Infinitive),
    TableHeaderContent("past participle", Some("participio passato")) -> Set(Participle, Masculine, Singular),
    TableHeaderContent("present", Some("presente")) -> Set(PresentTense, Indicative),
    TableHeaderContent("present", Some("congiuntivo presente")) -> Set(PresentTense, Subjunctive),
    TableHeaderContent("present", Some("condizionale presente")) -> Set(SimpleConditional, Indicative),
    TableHeaderContent("imperfect", Some("imperfetto")) -> Set(PastImperfect, Indicative),
    TableHeaderContent("imperfect", Some("congiuntivo imperfetto")) -> Set(PastImperfect, Subjunctive),
    TableHeaderContent("past historic", Some("passato remoto")) -> Set(Preterite, Indicative),
    TableHeaderContent("future", Some("futuro semplice")) -> Set(FutureTense, Indicative),
    TableHeaderContent("imperative", Some("imperativo")) -> Set(Imperative),
    TableHeaderContent("singular", None) -> Set(GrammaticalFeature.Singular),
    TableHeaderContent("plural", None) -> Set(GrammaticalFeature.Plural),
    TableHeaderContent("first", None) -> Set(GrammaticalFeature.FirstPerson),
    TableHeaderContent("second", None) -> Set(GrammaticalFeature.SecondPerson),
    TableHeaderContent("third", None) -> Set(GrammaticalFeature.ThirdPerson),
  )

  def parse(document: Document): Seq[ParsedInflection] =
    (for
      tableElement <- document.selectFirstOption("table.inflection-table").toSeq
      parsedTable = HTMLTableParser.parse(tableElement, sections = Seq(
        TableSection(0 to 2), // infinitive, auxiliary verb, present participle
        TableSection(3 to 4, global = true, matchColspan = false), // person
        TableSection(5 to 9),   // indicative
        TableSection(10 to 11), // conditional
        TableSection(12 to 14), // subjunctive
        TableSection(15 to 16), // imperative
      ), formSelector = "span[lang]")

      (row, rowIndex) <- parsedTable.rows.zipWithIndex
      case (data: TableData, columnIndex) <- row.cells.zipWithIndex

      headers = parsedTable.headers(rowIndex, columnIndex)
      headerFeatures = headers.map(_.content).flatMap(mapping.lift)
        .reduceOption(_ union _)
        .getOrElse(Set.empty)

      form <- data.forms
    yield
      ParsedInflection(form.toString, headerFeatures)
    ).distinct