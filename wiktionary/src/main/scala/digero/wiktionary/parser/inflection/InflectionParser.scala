package digero.wiktionary.parser.inflection

import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.model.serialization.CustomPickler
import digero.wiktionary.parser.{EntryContext, HTMLParser}
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.inflection.languages.ca.CatalanConjugation
import digero.wiktionary.parser.inflection.languages.es.SpanishConjugation
import digero.wiktionary.parser.inflection.languages.fi.FinnishConjugation
import digero.wiktionary.parser.inflection.languages.fr.FrenchConjugation
import digero.wiktionary.parser.inflection.languages.it.ItalianConjugation
import digero.wiktionary.parser.inflection.languages.pt.PortugueseConjugation


private implicit val grammaticalFeatureReadWriter: CustomPickler.ReadWriter[GrammaticalFeature] =
  CustomPickler.readwriter[String].bimap[GrammaticalFeature](
    feature => feature.id,
    id => GrammaticalFeature.values.find(_.id == id) match
        case Some(value) => value
        case None => throw new IllegalArgumentException(s"missing mapping for $id")
  )

case class ParsedInflection(form: String, features: Set[GrammaticalFeature]) derives CustomPickler.ReadWriter

class InflectionParser extends HTMLParser:
  def parse(html: String)(using context: EntryContext): Seq[ParsedInflection] =
    val document = parseDocument(html)

    context.languageCode match
      case "es" => SpanishConjugation.parse(document)
      case "pt" => PortugueseConjugation.parse(document)
      case "ca" => CatalanConjugation.parse(document)
      case "it" => ItalianConjugation.parse(document)
      case "fr" => FrenchConjugation.parse(document)
      case "fi" => FinnishConjugation.parse(document)
      case _ => Seq.empty

    





