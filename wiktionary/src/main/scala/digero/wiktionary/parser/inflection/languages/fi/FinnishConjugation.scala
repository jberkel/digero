package digero.wiktionary.parser.inflection.languages.fi

import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.model.lexical.GrammaticalFeature.*
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.inflection.ParsedInflection
import digero.wiktionary.parser.table.{HTMLTableParser, TableData, TableHeaderContent, TableSection}
import org.jsoup.nodes.Document

object FinnishConjugation:
  private val mapping: Map[TableHeaderContent, Set[GrammaticalFeature]] = Map(
    TableHeaderContent("present", None) -> Set(PresentTense),
    TableHeaderContent("present tense", None) -> Set(PresentTense),
    TableHeaderContent("past tense", None) -> Set(PastImperfect),
    TableHeaderContent("past", None) -> Set(PastImperfect),
    TableHeaderContent("pluperfect", None) -> Set(Pluperfect),
    TableHeaderContent("perfect", None) -> Set(Perfect),

    // cases
    TableHeaderContent("abessive", None) -> Set(Abessive),
    TableHeaderContent("adessive", None) -> Set(Adessive),
    TableHeaderContent("inessive", None) -> Set(Inessive),
    TableHeaderContent("elative", None) -> Set(Elative),
    TableHeaderContent("illative", None) -> Set(Illative),
    TableHeaderContent("instructive", None) -> Set(Instructive),

    TableHeaderContent("positive", None) -> Set(Positive),
    TableHeaderContent("negative", None) -> Set(Negative),

    TableHeaderContent("indicative mood", None) -> Set(Indicative),
    TableHeaderContent("conditional mood", None) -> Set(Conditional),
    TableHeaderContent("imperative mood", None) -> Set(Imperative),
    TableHeaderContent("potential mood", None) -> Set(Potential),

    TableHeaderContent("1st sing.", Some("first-person singular")) -> Set(FirstPerson, Singular),
    TableHeaderContent("2nd sing.", Some("second-person singular")) -> Set(SecondPerson, Singular),
    TableHeaderContent("3rd sing.", Some("third-person singular")) -> Set(ThirdPerson, Singular),

    TableHeaderContent("1st plur.", Some("first-person plural")) -> Set(FirstPerson, Plural),
    TableHeaderContent("2nd plur.", Some("second-person plural")) -> Set(SecondPerson, Plural),
    TableHeaderContent("3rd plur.", Some("third-person plural")) -> Set(ThirdPerson, Plural),

    TableHeaderContent("active", None) -> Set(Active),
    TableHeaderContent("passive", None) -> Set(Passive),
    TableHeaderContent("passive", Some("passive/impersonal")) -> Set(Passive),
    TableHeaderContent("verbal noun", None) -> Set(VerbalNoun),

    // Finnish infinitives
    TableHeaderContent("1st", None) -> Set(FirstInfinitive),
    TableHeaderContent("long 1st", None) -> Set(LongFirstInfinitive),
    TableHeaderContent("2nd", None) -> Set(SecondInfinitive),
    TableHeaderContent("3rd", None) -> Set(ThirdInfinitive),
    TableHeaderContent("4th", None) -> Set(FourthInfinitive),
    TableHeaderContent("5th", None) -> Set(FifthInfinitive),

    TableHeaderContent("infinitives", None) -> Set(Infinitive),
    TableHeaderContent("participles", None) -> Set(Participle),
    TableHeaderContent("agent", None) -> Set(Agent),
  )

  def parse(document: Document): Seq[ParsedInflection] =
    (for
      inflectionTable <- document.select("table.inflection-table.fi-conj")
      columnRanges = Seq(0 to 3, 4 to 6)
      parsedTable = HTMLTableParser.parse(inflectionTable, sections = Seq(
        // Inflection of puhua (Kotus type 52/sanoa, no gradation)
        // indicative mood
        //  present tense  | perfect
        //  past tense     | pluperfect
        // conditional mood
        //  present tense  | perfect
        // imperative mood
        //  present tense  | perfect
        // potential mood
        //  present tense  | perfect

        // nominal forms
        //  infinitives    | participles
        // 1st
        //   lemma
        // long 1st
        //   possessive forms
        // 2nd
        //   inessive (active/passive)
        //   possessive forms
        //   instructive (active)
        // 3rd
        //   inessive (active)
        //   elative (active)
        //   illative (active)
        //   adessive (active)
        //   abessive (active)
        //   instructive (active/passive)
        // 4th
        //   verbal noun
        // 5th
        //   archaic (possessive forms)

        // participles
        //   present (active/passive)
        //   past    (active/passive)
        //   agent
        //   negative

        TableSection(1 to 10,  columnRanges = columnRanges),  // indicative present/perfect
        TableSection(11 to 19, columnRanges = columnRanges, extraHeaders = Seq((1, 0))),  // indicative past/pluperfect
        TableSection(20 to 29, columnRanges = columnRanges),  // conditional present/perfect
        TableSection(30 to 39, columnRanges = columnRanges),  // imperative present/perfect
        TableSection(40 to 49, columnRanges = columnRanges),  // potential  present/perfect
        // nominal forms: infinitives | participles
        TableSection(51 to 64, columnRanges = columnRanges),
      ), formSelector = "span[lang]")

      (row, rowIndex) <- parsedTable.rows.zipWithIndex
      case (data: TableData, columnIndex) <- row.cells.zipWithIndex

      headers = parsedTable.headers(rowIndex, columnIndex)
      headerFeatures = headers.map(_.content).flatMap(mapping.lift)
        .reduceOption(_ union _)
        .getOrElse(Set.empty)

      form <- data.forms
    yield
      ParsedInflection(form.toString, headerFeatures)
    ).distinct


