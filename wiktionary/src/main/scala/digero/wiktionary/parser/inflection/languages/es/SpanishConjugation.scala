package digero.wiktionary.parser.inflection.languages.es

import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.model.lexical.GrammaticalFeature.*
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.inflection.ParsedInflection
import digero.wiktionary.parser.table.*
import org.jsoup.nodes.Document

object SpanishConjugation:

 private val mapping: Map[TableHeaderContent, Set[GrammaticalFeature]] = Map(
  TableHeaderContent("infinitive", Some("infinitivo")) -> Set(Infinitive),
  TableHeaderContent("indicative", Some("indicativo")) -> Set(Indicative),
  TableHeaderContent("subjunctive", Some("subjuntivo")) -> Set(Subjunctive),
  TableHeaderContent("gerund", Some("gerundio")) -> Set(Gerund),

  TableHeaderContent("singular", None) -> Set(Singular),
  TableHeaderContent("plural", None) -> Set(Plural),

  TableHeaderContent("1st person", None) -> Set(FirstPerson),
  TableHeaderContent("2nd person", None) -> Set(SecondPerson),
  TableHeaderContent("3rd person", None) -> Set(ThirdPerson),

  TableHeaderContent("present", Some("presente de indicativo")) -> Set(PresentTense),
  TableHeaderContent("present", Some("presente de subjuntivo")) -> Set(PresentSubjunctive),
  TableHeaderContent("imperfect", Some("pretérito imperfecto (copréterito)")) -> Set(PastImperfect),
  TableHeaderContent("preterite", Some("pretérito perfecto simple (pretérito indefinido)")) -> Set(Preterite),
  TableHeaderContent("future", Some("futuro simple (futuro imperfecto)")) -> Set(FutureTense),
  TableHeaderContent("future", Some("futuro simple de subjuntivo (futuro de subjuntivo)")) -> Set(FutureTense),
  TableHeaderContent("conditional", Some("condicional simple (pospretérito de modo indicativo)")) -> Set(SimpleConditional),

  TableHeaderContent("imperfect (ra)", Some("pretérito imperfecto de subjuntivo")) -> Set(ImperfectSubjunctive, GrammaticalFeature.RaForm),
  TableHeaderContent("imperfect (se)", Some("pretérito imperfecto de subjuntivo")) -> Set(ImperfectSubjunctive, GrammaticalFeature.SeForm),
  TableHeaderContent("imperative", Some("imperativo")) -> Set(Imperative),

  TableHeaderContent("masculine", Some("masculino")) -> Set(Masculine),
  TableHeaderContent("feminine", Some("femenino")) -> Set(Feminine),
  TableHeaderContent("past participle", Some("participio (pasado)")) -> Set(Participle)
 )

 private def features(form: Form): Set[GrammaticalFeature] =
  if form.superscripts.contains("vos") then
    Set(Voseo)
  else
    Set.empty

 def parse(document: Document): Seq[ParsedInflection] =
  (for
   tableElement <- document.selectFirstOption("table").toSeq
   parsedTable = HTMLTableParser.parse(tableElement, sections = Seq(
     TableSection(0 to 4),
     TableSection(5 to 6, global = true, matchColspan = false),
     TableSection(7 to 22)
   ))

   (row, rowIndex) <- parsedTable.rows.zipWithIndex
   case (data: TableData, columnIndex) <- row.cells.zipWithIndex

   headers = parsedTable.headers(rowIndex, columnIndex)
   headerFeatures = headers.map(_.content).flatMap(mapping.lift)
     .reduceOption(_ union _)
     .getOrElse(Set.empty)

   form <- data.forms
   formFeatures = this.features(form)
   features = GrammaticalFeature.normalize(headerFeatures union formFeatures)

  yield
    ParsedInflection(form.toString, features)
  ).distinct