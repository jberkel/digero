package digero.wiktionary.parser.inflection.languages.pt

import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.model.lexical.GrammaticalFeature.{FutureTense, Gerund, PastImperfect}
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.inflection.ParsedInflection
import digero.wiktionary.parser.table.{HTMLTableParser, TableData, TableHeaderContent, TableSection}
import org.jsoup.nodes.Document

object PortugueseConjugation:
  private val mapping: Map[TableHeaderContent, Set[GrammaticalFeature]] = Map(
    TableHeaderContent("Impersonal", Some("infinitivo impessoal")) -> Set(GrammaticalFeature.Infinitive),
    TableHeaderContent("Personal", Some("infinitivo pessoal")) -> Set(GrammaticalFeature.PersonalInfinitive),

    TableHeaderContent("Singular", None) -> Set(GrammaticalFeature.Singular),
    TableHeaderContent("Plural", None) -> Set(GrammaticalFeature.Plural),

    TableHeaderContent("First-person (eu)", None) -> Set(GrammaticalFeature.FirstPerson, GrammaticalFeature.Singular),
    TableHeaderContent("Second-person (tu)", None) -> Set(GrammaticalFeature.SecondPerson, GrammaticalFeature.Singular),
    TableHeaderContent("Third-person (ele / ela / você)", None) -> Set(GrammaticalFeature.ThirdPerson, GrammaticalFeature.Singular),

    TableHeaderContent("First-person (nós)", None) -> Set(GrammaticalFeature.FirstPerson, GrammaticalFeature.Plural),
    TableHeaderContent("Second-person (vós)", None) -> Set(GrammaticalFeature.SecondPerson, GrammaticalFeature.Plural),
    TableHeaderContent("Third-person (eles / elas / vocês)", None) -> Set(GrammaticalFeature.ThirdPerson, GrammaticalFeature.Plural),

    TableHeaderContent("Gerund", Some("gerúndio")) -> Set(GrammaticalFeature.Gerund),

    TableHeaderContent("Masculine", None) -> Set(GrammaticalFeature.Masculine),
    TableHeaderContent("Feminine", None) -> Set(GrammaticalFeature.Feminine),

    TableHeaderContent("Past participle", Some("particípio passado")) -> Set(GrammaticalFeature.Participle),
    TableHeaderContent("Short past participle", Some("particípio irregular")) -> Set(GrammaticalFeature.IrregularParticiple),
    TableHeaderContent("Long past participle", Some("particípio regular")) -> Set(GrammaticalFeature.Participle),

    TableHeaderContent("Indicative", Some("indicativo")) -> Set(GrammaticalFeature.Indicative),
    TableHeaderContent("Present", Some("presente")) -> Set(GrammaticalFeature.PresentTense),
    TableHeaderContent("Imperfect", Some("pretérito imperfeito")) -> Set(GrammaticalFeature.PastImperfect),
    TableHeaderContent("Preterite", Some("pretérito perfeito")) -> Set(GrammaticalFeature.Preterite),
    TableHeaderContent("Pluperfect", Some("pretérito mais-que-perfeito simples")) -> Set(GrammaticalFeature.Pluperfect),
    TableHeaderContent("Future", Some("futuro do presente")) -> Set(GrammaticalFeature.FutureTense),
    TableHeaderContent("Conditional", Some("condicional / futuro do pretérito")) -> Set(GrammaticalFeature.SimpleConditional),

    TableHeaderContent("Subjunctive", Some("conjuntivo (pt) / subjuntivo (br)")) -> Set(GrammaticalFeature.Subjunctive),
    TableHeaderContent("Present", Some("presente do conjuntivo (pt) / subjuntivo (br)")) -> Set(GrammaticalFeature.Subjunctive, GrammaticalFeature.PresentTense),
    TableHeaderContent("Imperfect", Some("pretérito imperfeito do conjuntivo (pt) / subjuntivo (br)")) -> Set(GrammaticalFeature.Subjunctive, PastImperfect),
    TableHeaderContent("Future", Some("futuro do conjuntivo (pt) / subjuntivo (br)")) -> Set(GrammaticalFeature.Subjunctive, FutureTense),

    TableHeaderContent("Imperative", Some("imperativo")) -> Set(GrammaticalFeature.Imperative),
  )

  private val defaultSections: Seq[TableSection] = Seq(
    TableSection(0 to 1, global = true),  // singular | first-person...
    TableSection(2 to 4),                 // Infinitive
    TableSection(5 to 6),                 // Gerund
    TableSection(7 to 9, matchColspan = false), // Past participle
    TableSection(10 to 16),               // Indicative
    TableSection(17 to 20),               // Subjunctive
    TableSection(21 to 23)                // Imperative
  )

  private val irregularParticipleSections: Seq[TableSection] = Seq(
    TableSection(0 to 1, global = true), // singular | first-person...
    TableSection(2 to 4),   // Infinitive
    TableSection(5 to 6),   // Gerund
    TableSection(7 to 9, matchColspan = false),   // Short (irregular) past participle
    TableSection(10 to 12, matchColspan = false), // Long past participle
    TableSection(13 to 19), // Indicative
    TableSection(20 to 23), // Subjunctive
    TableSection(24 to 26)  // Imperative
  )

  def parse(document: Document): Seq[ParsedInflection] =
    (for
      tableElement <- document.select("table")
      inflectionTable = tableElement if tableElement.className().contains("inflection-table")
      tableRows = inflectionTable.select("tr")
      sections = if tableRows.size() == 27 then irregularParticipleSections else defaultSections
      parsedTable = HTMLTableParser.parse(inflectionTable, sections)

      (row, rowIndex) <- parsedTable.rows.zipWithIndex
      case (data: TableData, columnIndex) <- row.cells.zipWithIndex

      headers = parsedTable.headers(rowIndex, columnIndex)
      headerFeatures = headers
        .map(_.content)
        .flatMap(mapping.lift)
        .reduceOption(_ union _)
        .getOrElse(Set.empty)

      features = GrammaticalFeature.normalize(headerFeatures)

      form <- data.forms
    yield
      ParsedInflection(form.toString, features)
    ).distinct

