package digero.wiktionary.parser.inflection.languages.ca

import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.inflection.ParsedInflection
import digero.wiktionary.parser.table.{HTMLTableParser, TableData, TableHeaderContent, TableSection}
import org.jsoup.nodes.Document

object CatalanConjugation:
  private val mapping: Map[TableHeaderContent, Set[GrammaticalFeature]] = Map(
    TableHeaderContent("first", None) -> Set(GrammaticalFeature.FirstPerson),
    TableHeaderContent("second", None) -> Set(GrammaticalFeature.SecondPerson),
    TableHeaderContent("third", None) -> Set(GrammaticalFeature.ThirdPerson),
    TableHeaderContent("singular", None) -> Set(GrammaticalFeature.Singular),
    TableHeaderContent("plural", None) -> Set(GrammaticalFeature.Plural),

    TableHeaderContent("infinitive", Some("infinitiu")) -> Set(GrammaticalFeature.Infinitive),
    TableHeaderContent("gerund", Some("gerundi")) -> Set(GrammaticalFeature.Gerund),
    TableHeaderContent("past participle", Some("participi passat")) -> Set(GrammaticalFeature.Participle),
    TableHeaderContent("masculine", None) -> Set(GrammaticalFeature.Masculine),
    TableHeaderContent("feminine", None) -> Set(GrammaticalFeature.Feminine),

    TableHeaderContent("present", Some("present")) -> Set(GrammaticalFeature.PresentTense),
    TableHeaderContent("imperfect", Some("imperfet")) -> Set(GrammaticalFeature.PastImperfect),
    TableHeaderContent("future", Some("futur")) -> Set(GrammaticalFeature.FutureTense),
    TableHeaderContent("preterite", Some("passat")) -> Set(GrammaticalFeature.Preterite),
    TableHeaderContent("conditional", Some("condicional")) -> Set(GrammaticalFeature.SimpleConditional),

    TableHeaderContent("indicative", Some("indicatiu")) -> Set(GrammaticalFeature.Indicative),
    TableHeaderContent("subjunctive", Some("subjuntiu")) -> Set(GrammaticalFeature.Subjunctive),
    TableHeaderContent("imperative", Some("imperatiu")) -> Set(GrammaticalFeature.Imperative)
  )

  def parse(document: Document): Seq[ParsedInflection] =
    (for
      tableElement <- document.selectFirstOption("table").toSeq
      parsedTable = HTMLTableParser.parse(tableElement, sections = Seq(
        TableSection(0 to 1), // infinitive + gerund
        TableSection(2 to 4), // past participle
        TableSection(5 to 6, global = true, matchColspan = false),
        TableSection(8 to 12),  // indicative
        TableSection(14 to 15), // subjunctive
        TableSection(17 to 18), // imperative
      ))

      (row, rowIndex) <- parsedTable.rows.zipWithIndex
      case (data: TableData, columnIndex) <- row.cells.zipWithIndex

      headers = parsedTable.headers(rowIndex, columnIndex)
      headerFeatures = headers
        .map(_.content)
        .flatMap(mapping.lift)
        .reduceOption(_ union _)
        .getOrElse(Set.empty)

      form <- data.forms
    yield
      ParsedInflection(form.toString, headerFeatures)
    ).distinct