package digero.wiktionary.parser.inflection.languages.fr

import digero.wiktionary.model.lexical.GrammaticalFeature
import digero.wiktionary.model.lexical.GrammaticalFeature.*
import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.inflection.ParsedInflection
import digero.wiktionary.parser.table.{HTMLTableParser, TableData, TableHeaderContent, TableSection}
import org.jsoup.nodes.Document

object FrenchConjugation:

  private val mapping: Map[TableHeaderContent, Set[GrammaticalFeature]] = Map(
    TableHeaderContent("infinitive", Some("infinitif")) -> Set(Infinitive),
    TableHeaderContent("present participle or gerund", Some("participe présent")) -> Set(Gerund),
    TableHeaderContent("past participle", Some("participe passé")) -> Set(Participle),

    TableHeaderContent("singular", None) -> Set(Singular),
    TableHeaderContent("plural", None) -> Set(Plural),
    TableHeaderContent("first", None) -> Set(FirstPerson),
    TableHeaderContent("second", None) -> Set(SecondPerson),
    TableHeaderContent("third", None) -> Set(ThirdPerson),

    TableHeaderContent("indicative", Some("indicatif")) -> Set(Indicative),
    TableHeaderContent("subjunctive", Some("subjonctif")) -> Set(Subjunctive),
    TableHeaderContent("imperative", Some("impératif")) -> Set(Imperative),

    TableHeaderContent("present", Some("présent")) -> Set(Indicative, PresentTense),
    TableHeaderContent("present", Some("subjonctif présent")) -> Set(Subjunctive, PresentTense),
    TableHeaderContent("imperfect", Some("imparfait")) -> Set(Indicative, PastImperfect),
    TableHeaderContent("imperfect", Some("subjonctif imparfait")) -> Set(Subjunctive, PastImperfect),
    TableHeaderContent("past historic", Some("passé simple")) -> Set(Indicative, Preterite),
    TableHeaderContent("future", Some("futur simple")) -> Set(Indicative, FutureTense),
    TableHeaderContent("conditional", Some("conditionnel présent")) -> Set(Indicative, SimpleConditional),
  )

  def parse(document: Document): Seq[ParsedInflection] =
    (for
      tableElement <- document.select("table")
      inflectionTable = tableElement if tableElement.className().contains("inflection-table")
      parsedTable = HTMLTableParser.parse(inflectionTable, sections = Seq(
        TableSection(0 to 1), // infinitive
        TableSection(2 to 3), // gerund
        TableSection(4 to 5), // past participle
        TableSection(5 to 6, global = true, matchColspan = false),
        TableSection(7 to 12, extraHeaders = Seq((7, 0))),  // indicative
        // 13-17 indicative compound tenses
        TableSection(18 to 20, extraHeaders = Seq((18, 0))), // subjunctive
        // 21-22 subjunctive compound tenses
        TableSection(23 to 24, extraHeaders = Seq((23, 0))), // imperative
        // 25 imperative compound tense
        // 26-27 empty
      ))

      (row, rowIndex) <- parsedTable.rows.zipWithIndex
      case (data: TableData, columnIndex) <- row.cells.zipWithIndex

      headers = parsedTable.headers(rowIndex, columnIndex)
      headerFeatures = headers.map(_.content).flatMap(mapping.lift)
        .reduceOption(_ union _)
        .getOrElse(Set.empty)

      form <- data.forms
    yield
      ParsedInflection(form.toString, headerFeatures)
    ).distinct