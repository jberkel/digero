package digero.wiktionary.parser

import digero.wiktionary.model.serialization.{SerializedEntry, SerializedSense}
import digero.wiktionary.parser.cmark.CommonMarkConverter

import java.util.Locale

object TranslationMatcher:
  private val commonMarkConverter = CommonMarkConverter()

  def matchEntry(entry: SerializedEntry): Option[SerializedEntry] =
    val translationGlosses = entry.translations.map(_.gloss)
    if translationGlosses.isEmpty then
      return None

    val candidateSenses = entry.senses.map(sense =>
      (sense.normalizedDefinition, sense)
    ).filterNot((definition, _) => definition.isBlank)

    val mapping = translationGlosses.flatMap(gloss =>
      Similarity.findMostSimilar(gloss, candidateSenses.map(_._1))
        .flatMap(similar => candidateSenses.find(_._1 == similar).map(_._2))
        .map(sense => (sense, gloss))
    ).toMap

    Some(entry.mapSensesEntry(sense =>
      mapping.get(sense) match
        case Some(gloss) => sense.copy(translationGloss = Some(gloss))
        case None => sense
    ))

  extension (entry: SerializedEntry)
    private def mapSensesEntry(f: SerializedSense => SerializedSense): SerializedEntry =
      entry.copy(senses = entry.senses.map(f))


  extension (sense: SerializedSense)
    private def normalizedDefinition: String =
      commonMarkConverter.commonMarkToPlain(sense.definition).toLowerCase(Locale.ENGLISH)


