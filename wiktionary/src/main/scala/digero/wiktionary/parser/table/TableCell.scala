package digero.wiktionary.parser.table

sealed trait TableCell:
  val colSpan: Int
  val rowSpan: Int
  def isEmpty: Boolean
  def copy(newRowSpan: Int): TableCell

/**
 * @param content the textual content of the cell
 * @param titleAttribute the HTML title attribute
 */
case class TableHeaderContent(content: String, titleAttribute: Option[String] = None):
  def isBlank: Boolean = content.isBlank
  override def toString: String = content


case class TableHeader(content: TableHeaderContent, colSpan: Int = 1, rowSpan: Int = 1) extends TableCell:
  def isEmpty: Boolean = content.isBlank
  def copy(newRowSpan: Int): TableHeader = TableHeader(content, colSpan, newRowSpan)

  override def toString: String = content.toString

object TableHeader:
  def apply(content: String): TableHeader = TableHeader(TableHeaderContent(content))

case class TableData(forms: Seq[Form], colSpan: Int = 1, rowSpan: Int = 1) extends TableCell:
  def isEmpty: Boolean = false
  def copy(newRowSpan: Int): TableData = TableData(forms, colSpan, newRowSpan)

  override def toString: String = forms.map(_.toString).mkString(", ")