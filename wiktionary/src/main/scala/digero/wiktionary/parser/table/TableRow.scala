package digero.wiktionary.parser.table

import scala.annotation.targetName

case class TableRow(cells: Seq[TableCell]):
  def map(f: TableCell => TableCell): TableRow = TableRow(cells.map(f))
  def apply(index: Int): TableCell = cells(index)
  def lift(index: Int): Option[TableCell] = cells.lift(index)

  def headers: Seq[(TableHeader, Int)] = cells
    .zipWithIndex
    .flatMap { 
    _ match
      case (header: TableHeader, index) => Some((header, index))
      case _ => None
  }

  def data: Seq[TableData] = cells.flatMap {
    _ match
      case data: TableData => Some(data)
      case _ => None
  }

  def combine(other: TableRow): TableRow =
    val rowSpans = other.cells.zipWithIndex.filter(_._1.rowSpan > 1)
    if rowSpans.isEmpty then
      this
    else
      val columnsList = cells.toBuffer
      for
        (header, index) <- rowSpans
      do
        columnsList.insert(index, header.copy(header.rowSpan - 1))

      TableRow(cells = columnsList.toSeq)

object TableRow:
  @targetName("apply_var")
  def apply(headers: TableHeader*): TableRow = TableRow(cells = headers)
