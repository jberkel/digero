package digero.wiktionary.parser.table


case class TableSection(rowRange: Range,
                        global: Boolean = false,
                        matchColspan: Boolean = true,
                        extraHeaders: Seq[(Int, Int)] = Seq.empty,
                        columnRanges: Seq[Range] = Seq.empty):
  val start: Int = rowRange.start
  def contains(row: Int): Boolean = rowRange.contains(row)

case class Table(rows: Seq[TableRow], sections: Seq[TableSection] = Seq.empty):
  def length: Int = rows.length
  def columnSize = rows.head.cells.size
  def apply(index: Int): TableRow = rows.apply(index)

  def expanded: Table =
    val expandedColSpans = for
      row <- rows
    yield TableRow(row.cells.flatMap(column => Seq.fill(column.colSpan)(column)))

    val expandedRows = expandedColSpans.foldLeft(Seq[TableRow]()) { (memo: Seq[TableRow], row: TableRow) =>
      if memo.isEmpty then
        Seq(row)
      else
        memo appended row.combine(memo.last)
    }

    Table(expandedRows, sections)

  def headers(row: Int, column: Int): Seq[TableHeader] =
    if row < 0 || row > rows.length then return Seq.empty

    val section: Option[TableSection] = sections.find(_.contains(row))
    val sectionStart = section.map(_.start).getOrElse(0)
    val cell = rows(row)(column)
    val matchColspan = section.exists(_.matchColspan)
    val currentRange = section.flatMap(_.columnRanges.find(_.contains(column)))

    val columnHeaders =
      for
        tableRow <- rows.slice(sectionStart, row)
        case header: TableHeader <- tableRow.lift(column)
        matching = header
          // include if header colSpans match, or look like globally applicable headers
          if !matchColspan ||
            cell.colSpan == header.colSpan ||
            header.colSpan == columnSize ||
            header.colSpan == currentRange.map(_.length).getOrElse(0)
      yield matching

    val extraHeaders =
      for
        extraHeader <- section.map(_.extraHeaders).getOrElse(Seq.empty)
        case header: TableHeader  <- rows(extraHeader._1).lift(extraHeader._2)
      yield
        header

    val globalColumnHeaders =
      for
        globalSection <- sections if globalSection.global && row > globalSection.rowRange.end
        sectionRow <- globalSection.rowRange
        case header: TableHeader <- Some(rows(sectionRow)(column))
        matching = header if !globalSection.matchColspan || cell.colSpan == header.colSpan
        
      yield matching

    val sameRowHeaders = rows(row).headers.flatMap {
      case (header, index) if currentRange.forall(_.contains(index)) => Some(header)
      case _ => None
    }

    (sameRowHeaders ++ columnHeaders ++ globalColumnHeaders ++ extraHeaders).distinct.filterNot(_.isEmpty)
