package digero.wiktionary.parser.table

import digero.wiktionary.parser.JsoupExtensions.*
import ipanema.links.Link
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

case class Form(link: Link, superscripts: Seq[String] = Seq.empty):
  override def toString: String = link.getTarget


object Form:
  def makeForms(elements: Elements): Seq[Form] =
    for
      formOf <- elements
      wikiLink <- formOf.wikiLinks() if wikiLink.getNamespace == null
      form = Form(wikiLink, superscripts(formOf))
    yield form

  def parse(element: Element): Seq[Form] =
    makeForms(element.select("span.form-of"))

  private def superscripts(element: Element): Seq[String] =
    Option(element.nextElementSibling()).map(element =>
      element.select("sup").map(_.ownText()).distinct.filterNot(_.isBlank)
    ).getOrElse(Seq.empty)
