package digero.wiktionary.parser.table

import digero.wiktionary.parser.JsoupExtensions.*
import digero.wiktionary.parser.*
import org.jsoup.nodes.Element


object HTMLTableParser:
  
  /**
   * @param table the element declaring the table
   * @param sections the logical content sections of the table
   * @return the parsed table
   */
  def parse(table: Element,
            sections: Seq[TableSection] = Seq.empty,
            formSelector: String = "span.form-of"): Table =
    assert(table.normalName() == "table", s"invalid element: ${table.tagName()}")

    def isHeaderCell(element: Element): Boolean =
      element.normalName() match
        case "th" => true
        case "td" => element.text().isBlank

    val rows = for
      tr <- table.select("> tbody > tr")

      cells: Seq[TableCell] = for
        cell <- tr.select("> th, > td")
        removed = cell.removing("table") // TODO improve

        colSpan = cell.attrOpt("colspan").map(_.toInt).getOrElse(1)
        rowSpan = cell.attrOpt("rowspan").map(_.toInt).getOrElse(1)

      yield
        if isHeaderCell(cell) then
          TableHeader(
             TableHeaderContent(
                cell.removing("sup").text(),
                titleAttribute = cell.selectFirstOption(":not(a)[title]").map(_.attr("title")).map(_.strip)
              ),
              colSpan,
              rowSpan)
        else
          val forms = Form.makeForms(removed.select(formSelector)) 
          val selfForms = removed.plainSelfLinks().map(Form(_))
          TableData(forms ++ selfForms, colSpan, rowSpan)

    yield
      TableRow(cells)

    Table(rows, sections).expanded
