package digero.wiktionary.parser

import digero.model.{Template, TemplateParameter, TemplateParameterMap}
import digero.wiktionary.model.Category
import digero.wiktionary.model.parsoid.{DataTemplateParams, ParsoidAttribs, ParsoidCaption, ParsoidData, ParsoidErrors, ParsoidTemplate, ParsoidTemplateStyle}
import ipanema.links.Link
import org.jsoup.nodes.Document.OutputSettings
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements

import java.net.URI
import scala.collection.WithFilter
import scala.jdk.CollectionConverters.*
import scala.util.Try

object JsoupExtensions:
  private val CategoryRegex = """^\./Category:([^#]+)(#(.*))?$""".r

  extension (elements: Elements)
    def foreach[U](f: Element => U): Unit = elements.asScala.foreach(f)
    def flatMap[B](f: Element => IterableOnce[B]): Seq[B] = toSeq.flatMap(f)
    def map[B](f: Element => B): Seq[B] = toSeq.map(f)
    def withFilter(f: Element => Boolean): WithFilter[Element, Seq] = toSeq.withFilter(f)
    def takeWhile(f: Element => Boolean): Elements = Elements(toSeq.takeWhile(f).asJava)

    def toSeq: Seq[Element] = elements.asScala.toSeq
    def head: Element = elements.first()

    def headOption: Option[Element] =
      if elements.size() == 0 then None else Some(elements.get(0))
    def categories: Seq[Category] = elements.flatMap(_.categories)


  extension [E <: Element](element: E)
    def head: Element = element.child(0)
    def headOption: Option[Element] = if element.childrenSize() == 0 then None else Some(element.child(0))
    def attrOpt(key: String): Option[String] = if element.hasAttr(key) then Some(element.attr(key)) else None
    def classes: Seq[String] = element.classNames().iterator().asScala.toSeq

    // to get the HTML, use this method, don't use html() directly
    def serialize(prettyPrint: Boolean): String =
      require(element != null, "null element")
      require(element.ownerDocument() != null, "element has no ownerDocument")
      val outputSettings = element.ownerDocument().outputSettings()
      outputSettings
        .prettyPrint(prettyPrint)
        .syntax(OutputSettings.Syntax.xml)
        .maxPaddingWidth(-1)
      element.html()

    def selectFirstOption(selector: String): Option[Element] =
      Option(element.selectFirst(selector))

    def removing(cssQuery: String): E =
      val copy = element.clone().asInstanceOf[E]
      copy.select(cssQuery).foreach(_.remove())
      copy

    def nonBlankText: Option[String] =
      if element.text().isBlank then
        None
      else
        Some(element.text())
    
    // gets the element's "about" attribute, falling back to the parents if not found
    def about: Option[String] =
      element.attrOpt("about")
        .orElse(
          element.parents()
            .select("*[about]")
            .headOption
            .flatMap(_.attrOpt("about"))
        )

    def parsoidData: Option[ParsoidData] =
        attrOpt("data-mw").flatMap { data =>
          Try(ParsoidTemplate.fromJSON(data))
            .orElse(Try(ParsoidCaption.fromJSON(data)))
            .orElse(Try(ParsoidTemplateStyle.fromJSON(data)))
            .orElse(Try(ParsoidAttribs.fromJSON(data)))
            .orElse(Try(ParsoidErrors.fromJSON(data)))
            .toOption
        }

    def categories: Seq[Category] =
      for
        link <- element.select("link[rel=mw:PageProp/Category]")
        href <- link.attrOpt("href")
        matched <- CategoryRegex.findFirstMatchIn(href)
        name = matched.group(1).replaceAll("_", " ")
      yield Category(name)

    def templates: Seq[Template] =
      parsoidData match
        case Some(ParsoidTemplate(parts)) =>
          for
            part <- parts
            container <- part.toOption
            template = container.template
            name <- template.name

          yield Template(name, template.params.toTemplateParamMap)
        case _ => Seq.empty

    
    def wikiLinks(includeSelfLinks: Boolean = true): Seq[Link] =
      for
        a <- element.select("a[rel=mw:WikiLink]") if includeSelfLinks || !a.classes.exists(Set("selflink").contains)  
        href <- a.attrOpt("href")
        uri <-  Try(URI.create(href)).toOption 
      yield Link.parse(uri)

    def plainSelfLinks(): Seq[Link] =
      for
        strong <- element.select("strong.selflink")
        target = strong.ownText()
        language = strong.parent().attr("lang")
      yield Link(target, language)

    def preprocessed: String =
      val copy = element.clone()


      HTMLPreprocessor.removeUnwanted(copy)

      val newRoot = Document(element.baseUri())
      newRoot.appendChild(copy)
      copy.serialize(false)

  extension (document: Document)
    def serialize(prettyPrint: Boolean = false): String =
      document.body().serialize(prettyPrint)

  extension (dataTemplateParams: DataTemplateParams)
    private def toTemplateParamMap: TemplateParameterMap =

      val values: Seq[(TemplateParameter, String)] =
        for
          (key, value) <- dataTemplateParams.toSeq
        yield
          key.toIntOption match
            case Some(paramIndex) => (Left(paramIndex), value.wt)
            case None             => (Right(key), value.wt)

      TemplateParameterMap(values)