package digero.wiktionary.parser;

import digero.wiktionary.model.lexical.{Pronunciation, PronunciationType}
import digero.wiktionary.parser.JsoupExtensions.*

class PronunciationParser extends HTMLParser:

  def parse(html: String): Seq[Pronunciation] =
    val document = parseDocument(html)
    for
      span <- document.select("span.IPA") if span.parent.tag().normalName() != "a"
      text = span.text()
      qualifierContent = span.previousElementSiblings().select(".qualifier-content")
      qualifiers = qualifierContent.text().split(",").map(_.strip).filter(!_.isBlank)
    yield Pronunciation(text, PronunciationType.IPA, qualifiers.toSeq)
