package digero.wiktionary.model.lexical

enum InflectionType(val selector: String, val labels: Seq[String]):
  case GenitiveSingular extends InflectionType(".gen\\|s-form-of", labels=Seq("genitive", "genitive singular"))
  case Plural extends InflectionType(".p-form-of", labels=Seq("plural"))

case class Inflection(`type`: InflectionType, forms: Seq[String])
