package digero.wiktionary.model.lexical


enum GrammaticalFeature(val id: String):
  case FirstPerson extends GrammaticalFeature("Q21714344")
  case SecondPerson extends GrammaticalFeature("Q51929049")
  case ThirdPerson extends GrammaticalFeature("Q51929074")
  // verbs without subject
  case Active extends GrammaticalFeature("Q1317831")
  case Passive extends GrammaticalFeature("Q1194697")
  case Agent extends GrammaticalFeature("Q392648")

  case Infinitive extends GrammaticalFeature("Q179230")
  case PersonalInfinitive extends GrammaticalFeature("Q106236131")
  case Gerund extends GrammaticalFeature("Q1923028")

  case Singular extends GrammaticalFeature("Q110786")
  case Plural extends GrammaticalFeature("Q146786")

  // grammatical mood
  case Indicative extends GrammaticalFeature("Q682111")
  case Subjunctive extends GrammaticalFeature("Q473746")
  case Imperative extends GrammaticalFeature("Q22716")
  // https://en.wikipedia.org/wiki/Irrealis_mood#Potential
  case Potential extends GrammaticalFeature("Q2296856")
  case Conditional extends GrammaticalFeature("Q625581")
  case Participle extends GrammaticalFeature("Q814722")

  // used in Portuguese
  case IrregularParticiple extends GrammaticalFeature("XXX")

  // tenses
  case PresentTense extends GrammaticalFeature("Q192613")
  case PastImperfect extends GrammaticalFeature("Q12547192")
  case Pluperfect extends GrammaticalFeature("Q623742")
  case Perfect extends GrammaticalFeature("Q625420")
  case Preterite extends GrammaticalFeature("Q442485")
  case FutureTense extends GrammaticalFeature("Q501405")

  // FutureSubjunctive = FutureTense + Subjunctive
  case SimpleConditional extends GrammaticalFeature("Q65211247")

  case PresentSubjunctive extends GrammaticalFeature("Q3502553")
  case ImperfectSubjunctive extends GrammaticalFeature("Q3502541")
  case FutureSubjunctive extends GrammaticalFeature("Q60550027")

  case Masculine extends GrammaticalFeature("Q499327")
  case Feminine extends GrammaticalFeature("Q1775415")


  // Polarity
  // https://en.wikipedia.org/wiki/Finnish_grammar#Negation_of_verbs
  // affirmative
  case Positive extends GrammaticalFeature("Q109267112")
  // negation
  case Negative extends GrammaticalFeature("Q1478451")

  // in, into
  case Inessive extends GrammaticalFeature("Q282031")
  // without
  case Abessive extends GrammaticalFeature("Q319822")
  // next to by
  case Adessive extends GrammaticalFeature("Q281954")
  case Instructive extends GrammaticalFeature("Q1665275")
  // entering state
  case Elative extends GrammaticalFeature("Q394253")
  // leaving state
  case Illative extends GrammaticalFeature("Q474668")

  // language-specific
  case FirstInfinitive extends GrammaticalFeature("infinitive I")
  case LongFirstInfinitive extends GrammaticalFeature("infinitive I (long)")
  case SecondInfinitive extends GrammaticalFeature("infinitive II")
  case ThirdInfinitive extends GrammaticalFeature("infinitive III")
  case FourthInfinitive extends GrammaticalFeature("infinitive IV")
  case FifthInfinitive extends GrammaticalFeature("infinitive V")

  case Voseo extends GrammaticalFeature("Q1070730")
  case Ustedeo extends GrammaticalFeature("Q6158182")

  case RaForm extends GrammaticalFeature("ra-form")
  case SeForm extends GrammaticalFeature("se-form")
  case VerbalNoun extends GrammaticalFeature("verbalNoun")


object GrammaticalFeature:
  def normalize(features: Set[GrammaticalFeature]): Set[GrammaticalFeature] =
    normalizeSubjunctive(features)

  private def normalizeSubjunctive(features: Set[GrammaticalFeature]): Set[GrammaticalFeature] =
    // implicitly subjunctive
    if features.intersect(Set(PresentSubjunctive, ImperfectSubjunctive, FutureSubjunctive)).nonEmpty then
      features excl Subjunctive
    else
      features