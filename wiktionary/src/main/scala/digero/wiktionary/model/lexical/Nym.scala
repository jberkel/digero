package digero.wiktionary.model.lexical


case class Nym(`type`: RelationType,
               source: String,
               target: String,
               transliteration: Option[String])
