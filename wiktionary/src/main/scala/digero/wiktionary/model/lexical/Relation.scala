package digero.wiktionary.model.lexical

case class Relation(source: String, target: String, `type`: RelationType)
