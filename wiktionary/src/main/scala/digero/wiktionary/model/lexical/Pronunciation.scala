package digero.wiktionary.model.lexical


enum PronunciationType:
  case IPA
  case SAMPA
  case Audio
  case Rhyme

case class Pronunciation(text: String,
                         `type`: PronunciationType,
                         qualifier: Seq[String])
