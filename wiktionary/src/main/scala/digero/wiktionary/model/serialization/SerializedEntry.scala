package digero.wiktionary.model.serialization

import digero.wiktionary.model.lexical.PronunciationType.IPA
import digero.wiktionary.model.lexical.{Pronunciation, Relation}
import digero.wiktionary.model.{HeadwordLine, MultiTermTranslation, NormalizedTranslation, PartOfSpeechSection, SimpleTranslation, TranslationGroup}

type TextConverter = String => Option[String]

case class SerializedInflections(
    conjugations: Seq[SerializedConjugation]
) derives CustomPickler.ReadWriter

case class SerializedEntry(
    pageTitle: String,
    redirects: Seq[String],
    headwordLines: Seq[SerializedHeadwordLine],
    partOfSpeech: String,
    senses: Seq[SerializedSense],
    etymology: Option[String],
    pronunciations: Seq[SerializedPronunciation],
    translations: Seq[SerializedTranslationGroup],
    usageNotes: Option[String],
    categories: Seq[String],
    relations: Seq[SerializedRelation],
    inflections: SerializedInflections,
) derives CustomPickler.ReadWriter

case class SerializedInflection(
   `type`: String,
    forms: Seq[String]
 ) derives CustomPickler.ReadWriter

case class SerializedConjugation(
  form: String,
  features: Seq[String]
) derives CustomPickler.ReadWriter

case class SerializedTranslation(
  text: String,
  language: String,
  script: Option[String] // only set when non-Latin
) derives CustomPickler.ReadWriter

case class SerializedTranslationGroup(
  gloss: String,
  translations: Seq[SerializedTranslation],
  subpage: Option[String]
) derives CustomPickler.ReadWriter

object SerializedTranslationGroup:
  def apply(translationGroup: TranslationGroup): SerializedTranslationGroup =
    SerializedTranslationGroup(
      gloss=translationGroup.gloss,
      translations=translationGroup.translations.map(t =>

        val translation: String = t.content match
          case SimpleTranslation(translation) => translation
          case NormalizedTranslation(_, normalized) => normalized
          case MultiTermTranslation(commonMark) => commonMark

        SerializedTranslation(translation, t.language, t.nonLatinScript)
      ),
      subpage=translationGroup.subpage.map(_.toString)
    )

case class SerializedHeadwordLine(
  headword: String,
  transliteration: Option[String],
  alternativeSpellings: Seq[String],
  inflections: Seq[SerializedInflection],
  gender: Seq[Long]
) derives CustomPickler.ReadWriter

case class SerializedPronunciation(
  text: String,
  qualifier: Seq[String]
) derives CustomPickler.ReadWriter

case class ReverseTranslation(
  language: String,   // target language
  pageTitle: String,  // target Dictionary
  partOfSpeech: String,
  // English
  headword: String,
  gloss: String,
  definition: Option[String]
)

object SerializedPronunciation:
  def apply(pronunciation: Pronunciation): SerializedPronunciation =
    SerializedPronunciation(pronunciation.text, pronunciation.qualifier)

object SerializedHeadwordLine:
  def apply(headwordLine: HeadwordLine): SerializedHeadwordLine =

    SerializedHeadwordLine(
      headwordLine.headword,
      alternativeSpellings = headwordLine.alternativeSpellings,
      transliteration = headwordLine.text.toOption.flatMap(_.transliterations.headOption),
      inflections = headwordLine.inflections.map(inflection => SerializedInflection(inflection.`type`.toString, inflection.forms)),
      gender = headwordLine.genders.map(_.id)
    )

object SerializedEntry extends FromJSON[SerializedEntry]:
  def apply(pageTitle: String,
            redirects: Seq[String],
            etymology: Option[String],
            pronunciations: Seq[Pronunciation],
            partOfSpeech: PartOfSpeechSection)(using textConverter: TextConverter): SerializedEntry =

    SerializedEntry(
      pageTitle,
      redirects,
      headwordLines = partOfSpeech.headwordLines.map(SerializedHeadwordLine(_)),
      partOfSpeech = partOfSpeech.section.name,
      senses = partOfSpeech.senses.serialized,
      etymology = etymology.flatMap(textConverter),
      pronunciations = pronunciations.filter(_.`type` == IPA).map(SerializedPronunciation.apply),
      usageNotes = partOfSpeech.usageNotes.flatMap(textConverter),
      translations = partOfSpeech.translations.map(SerializedTranslationGroup.apply),
      categories = partOfSpeech.headwordCategories.map(_.name).toSeq.sorted,
      relations = partOfSpeech.relations.map(SerializedRelation.apply).distinct,
      inflections = SerializedInflections(
        conjugations = partOfSpeech.inflections.map(inflection => SerializedConjugation(inflection.form, features = inflection.features.map(_.toString).toSeq)) 
      )
    )

