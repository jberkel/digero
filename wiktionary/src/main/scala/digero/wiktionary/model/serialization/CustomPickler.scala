package digero.wiktionary.model.serialization

import upickle.core.{ObjVisitor, Visitor}
import upickle.AttributeTagged


// https://com-lihaoyi.github.io/upickle/#CustomConfiguration
object CustomPickler extends upickle.AttributeTagged:

  given eitherReader[T: Reader]: SimpleReader[Either[String, T]] with
    override def expectedMsg: String = "expected string or dictionary"

    override def visitString(s: CharSequence, index: Int): Either[String, T] = Left(s.toString)

    override def visitObject(length: Int,
                             jsonableKeys: Boolean,
                             index: Int): ObjVisitor[Any, Either[String, T]] =
      val reader = summon[Reader[T]]
      val visitObject = reader.visitObject(length, jsonableKeys, index)

      new ObjVisitor[Any, Either[String, T]]:
        override def visitKey(index: Int): Visitor[?, ?] = visitObject.visitKey(index)
        override def visitKeyValue(v: Any): Unit = visitObject.visitKeyValue(v)
        override def subVisitor: Visitor[?, ?] = visitObject.subVisitor
        override def visitValue(v: Any, index: Int): Unit = visitObject.visitValue(v, index)
        override def visitEnd(index: Int): Either[String, T] = Right(visitObject.visitEnd(index))

  override implicit def OptionWriter[T: Writer]: Writer[Option[T]] =
    summon[Writer[T]].comap[Option[T]] {
      case None => null.asInstanceOf[T]
      case Some(x) => x
    }

  override implicit def OptionReader[T: Reader]: Reader[Option[T]] =
    new Reader.Delegate[Any, Option[T]](summon[Reader[T]].map(Some(_))):
      override def visitNull(index: Int): Option[Nothing] = None

trait FromJSON[T: CustomPickler.Reader]:
  def fromJSON(string: String, trace: Boolean = false): T = CustomPickler.read(string, trace)
