package digero.wiktionary.model

case class Usex(text: String,
                translation: Option[String],
                transliteration: Option[String],
                literally: Option[String])
