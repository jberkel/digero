package digero.wiktionary.model

import ipanema.links.Link

case class QualifiedLink(link: Link,
                         sense: Option[String] = None /* the sense this link belongs to */
                        )
