package digero.wiktionary.model

import digero.wiktionary.model.lexical.Nym

enum SenseKind:
  case FormOf

case class Sense(text: String,
                 html: String,
                 kind: Set[SenseKind] = Set.empty,
                 formOf: Option[String] = None,
                 nyms: Seq[Nym] = Seq.empty,
                 usexes: Seq[Usex] = Seq.empty,
                 categories: Seq[Category] = Seq.empty,
                 subsenses: Seq[Sense] = Seq.empty):

  def isNonLemma: Boolean = kind.contains(SenseKind.FormOf)

  override def toString: String =
    def tree(sense: Sense, level: Int): String =
      f"${"    " * level}${sense.text} (${categories.toSet.size} cats)\n" + sense.subsenses.map(tree(_, level + 1)).mkString("")

    tree(this, 0)