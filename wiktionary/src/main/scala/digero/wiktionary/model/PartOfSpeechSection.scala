package digero.wiktionary.model

import digero.wiktionary.model.lexical.Relation
import digero.wiktionary.parser.inflection.ParsedInflection

/**
 * An individual section containing a POS (such as "Noun"),
 * and the corresponding senses.
 *
 * @param section
 * @param headwordLines normally there is only one per POS header
 * @param senses for all headword lines
 */
case class PartOfSpeechSection(section: SectionHeading,
                               headwordLines: Seq[HeadwordLine],
                               senses: Seq[Sense],
                               relations: Seq[Relation],
                               inflections: Seq[ParsedInflection],
                               translations: Seq[TranslationGroup],
                               usageNotes: Option[String]):

  def name: String = section.name
  def isNonLemma: Boolean = headwordLines.exists(_.isNonLemma)

  def headwordCategories: Set[Category] = headwordLines.map(_.categories).foldLeft(Set.empty)(_ ++ _)
  def senseCategories: Set[Category] = senses.map(_.categories).foldLeft(Set.empty)(_ ++ _)
  
  def categories: Set[Category] = headwordCategories ++ senseCategories