package digero.wiktionary.model


case class Entry[Content](
     name: EntryName,
     etymologySection: Option[Section[Content]] = None,
     posSections: Seq[Section[Content]] = Seq.empty[Section[Content]],
     remainingSections: Seq[Section[Content]] = Seq.empty[Section[Content]]
):
  def pageTitle: String = name.name

object Entry:
  def apply[T](parsedEntry: ParsedEntry[T]): Entry[T] =
    val (etymologies, posSections, rest) = parsedEntry.sections.split(_.isEtymology)
    assert(etymologies.size <= 1)
    Entry(name=parsedEntry.name,
          etymologySection=etymologies.headOption,
          posSections,
          rest)
