package digero.wiktionary.model

import digero.wiktionary.parser.ParsoidHTMLParser.ElementFilter
import digero.wiktionary.model.Level.L3
import digero.wiktionary.model.Level.L4
import digero.wiktionary.model.SectionKind.PartOfSpeech
import org.jsoup.nodes
import org.jsoup.nodes.Element

import scala.collection.immutable.Set

enum Level(val level: Int):
  case L2 extends Level(2)
  case L3 extends Level(3)
  case L4 extends Level(4)
  case L5 extends Level(5)
  case L6 extends Level(6)

object Level:
  def apply(level: Int): Option[Level] =
      Level.values.find(_.level == level)

  def apply(tagName: String): Option[Level] =
     tagName.substring(1).toIntOption.flatMap(Level.apply)


case class Q(id: Int)

enum SectionKind:
  case Etymology
  case PartOfSpeech
  case Declension
  case SupplementaryData
  case Relation
  case Text
  case Transliteration
  case References
  case Default

// https://en.wiktionary.org/wiki/User:Erutuon/mainspace_headers/whitelist
// https://en.wiktionary.org/wiki/Wiktionary:Entry_layout#Part_of_speech
enum SectionHeading(val name: String,
                    val level: Set[Level] = Set.empty,
                    val kind: SectionKind = SectionKind.Default,
                    val languages: Set[String] = Set.empty,
                    val wikidata: Set[Q] = Set.empty,
                    val deprecated: Boolean = false):

  // header
  case AlternativeForms extends SectionHeading("Alternative forms", Set(L3), SectionKind.Relation)
  case AlternativeScripts extends SectionHeading("Alternative scripts", Set(L3), SectionKind.Relation,
    languages=Set("sa", "emk", "pi", "hi", "hoc"))
  case Etymology extends SectionHeading("Etymology", Set(L3), kind = SectionKind.Etymology)

  // Some entries have "Glyph 1"…"Glyph N" L3 headers
  case Glyph extends SectionHeading(name="Glyph", Set(L3), kind = SectionKind.Etymology)

  // Describes how the character obtained its current shape.
  // Only used for single characters
  case GlyphOrigin extends SectionHeading("Glyph origin", Set(L3), kind = SectionKind.Etymology,
    languages=Set("zh", "ja", "egy", "mul", "vi"))
  case Pronunciation extends SectionHeading("Pronunciation", Set(L3))
  case Description extends SectionHeading(name = "Description", Set(L3),
    kind = SectionKind.Etymology,
    languages = Set("mul"))

  // entry
  case Abbreviations extends SectionHeading("Abbreviations", kind = SectionKind.Relation)
  case Antonyms extends SectionHeading("Antonyms", Set(L4), kind = SectionKind.Relation)
  case Collocations extends SectionHeading("Collocations", Set(L4), kind = SectionKind.Relation)
  case Compounds extends SectionHeading("Compounds", kind = SectionKind.Relation,
    languages=Set("zh", "fi", "ja", "ko", "txg"))
  case CoordinateTerms extends SectionHeading("Coordinate terms", Set(L4), kind = SectionKind.Relation)
  case DerivedCharacters extends SectionHeading("Derived characters", Set(L4), kind = SectionKind.Relation,
    languages=Set("mul", "txg", "ja", "vi"))
  case RelatedCharacters extends SectionHeading("Related characters", Set(L4), kind = SectionKind.Relation,
    languages=Set("mul"))
  case DerivedSigns extends SectionHeading("Derived signs", Set(L4), kind = SectionKind.Relation)
  case DerivedTerms extends SectionHeading("Derived terms", Set(L4), kind = SectionKind.Relation)
  case Descendants extends SectionHeading("Descendants", Set(L4), kind = SectionKind.Relation)
  case Holonyms extends SectionHeading("Holonyms", Set(L4), kind = SectionKind.Relation)
  case Hypernyms extends SectionHeading("Hypernyms", Set(L4), kind = SectionKind.Relation)
  case Hyponyms extends SectionHeading("Hyponyms", Set(L4), kind = SectionKind.Relation)
  case Idioms extends SectionHeading("Idioms", Set(L4), kind = SectionKind.Relation,
    languages=Set("ja", "fi", "lt", "lv", "kij"))

  case Meronyms extends SectionHeading("Meronyms", Set(L4), kind = SectionKind.Relation)
  case Paronyms extends SectionHeading("Paronyms", Set(L4), kind = SectionKind.Relation)

  case RelatedTerms extends SectionHeading("Related terms", Set(L4), kind = SectionKind.Relation)
  case SeeAlso extends SectionHeading("See also", Set(L4), kind = SectionKind.Relation)
  case Synonyms extends SectionHeading("Synonyms", Set(L4), kind = SectionKind.Relation)
  case Troponyms extends SectionHeading("Troponyms", Set(L4), kind = SectionKind.Relation)

  case Conjugation extends SectionHeading(name = "Conjugation", Set(L4), kind = SectionKind.Declension)
  case Declension extends SectionHeading(name = "Declension", Set(L4), kind = SectionKind.Declension)

  // https://en.wiktionary.org/wiki/Wiktionary:Beer_parlour/2014/May#New_L3_for_Chinese
  case Definitions extends SectionHeading("Definitions", Set(L3), kind = PartOfSpeech,
    languages = Set("zh", "txg", "ja", "ko", "zkt"))

  // Forms of letters or glyphs.
  case Forms extends SectionHeading("Forms", Set(L4), kind = SectionKind.SupplementaryData,
    languages = Set("mul", "he", "ks"))
  case Gallery extends SectionHeading(name = "Gallery", Set(L4), kind = SectionKind.SupplementaryData)  // Image Gallery
  case Inflection extends SectionHeading(name = "Inflection", Set(L4), kind = SectionKind.Declension)
  case KinshipDeclension extends SectionHeading(name = "Kinship declension", kind = SectionKind.Declension,
    languages = Set("as"))

  case Mutation extends SectionHeading("Mutation", kind = SectionKind.Declension,
    languages = Set("ga", "cy", "sga", "gv", "gd"))
  case Production extends SectionHeading(name = "Production", Set(L3), languages = Set("asl", "icl", "gsg", "fsl", "mul"))
  case Proverbs extends SectionHeading(name = "Proverbs", Set(L4), kind=SectionKind.Relation)
  case Quotations extends SectionHeading(name = "Quotations", Set(L4), kind=SectionKind.SupplementaryData)
  case Readings extends SectionHeading(name = "Readings", Set(L4),
    languages = Set("ja", "ryu", "vi", "mvi", "xug"))
  case Romanization extends SectionHeading(name = "Romanization", Set(L3), kind=SectionKind.Transliteration,
    languages = Set("cmn", "ja", "got", "yue", "sux"))
  case Statistics extends SectionHeading(name = "Statistics", Set(L4), kind=SectionKind.Text)
  case StemSet extends SectionHeading(name = "Stem set", Set(L4), languages = Set("nv"))
  case Translations extends SectionHeading("Translations", Set(L4), kind=SectionKind.Relation)
  case Transliteration extends SectionHeading(name = "Transliteration", Set(L3), kind=SectionKind.Transliteration,
    languages=Set("hit"))

  case Trivia extends SectionHeading(name = "Trivia", Set(L4), kind=SectionKind.Text)
  case UsageNotes extends SectionHeading(name = "Usage notes", Set(L3, L4), kind=SectionKind.Text)
  case Notes extends SectionHeading(name = "Notes", Set(L3, L4), kind=SectionKind.Text)

  case ReconstructionNotes extends SectionHeading(name = "Reconstruction notes", Set(L4), kind=SectionKind.Text,
    languages=Set("oko", "got", "la", "pkc"))

  case DialectalVariants extends SectionHeading(name = "Dialectal variants", Set(L4), kind=SectionKind.Relation,
    languages=Set("oc", "ff", "paw", "kmr"), deprecated=true)

  case SignValues extends SectionHeading(name = "Sign values", Set(L3), languages = Set("akk"))

  case VerbalStems extends SectionHeading(name = "Verbal stems", Set(L4), kind = SectionKind.Relation,
    languages = Set("nv"))

  case AffixedForms extends SectionHeading("Affixed forms", Set(L3), kind = SectionKind.Relation,
    languages = Set("km", "pac", "bdq"))
  case AffixedTerms extends SectionHeading("Affixed terms", Set(L3), kind = SectionKind.Relation,
    languages=Set("id", "ms", "km"))

  case Adjective extends SectionHeading("Adjective", Set(L3), PartOfSpeech)
  case Adnominal extends SectionHeading("Adnominal",
    Set(L3),
    PartOfSpeech,
    languages=Set("ja", "ojp"),
    wikidata=Set(Q(11639843), Q(110688175))
  )
  case Adverb extends SectionHeading("Adverb", Set(L3), PartOfSpeech)
  case Affix extends SectionHeading("Affix", Set(L3), PartOfSpeech)
  case Article extends SectionHeading("Article", Set(L3), PartOfSpeech)
  case Circumfix extends SectionHeading("Circumfix", Set(L3), PartOfSpeech)
  case Circumposition extends SectionHeading("Circumposition", Set(L3), PartOfSpeech,
    languages=Set("kmr", "nl", "de", "zh", "ps"))

  case Classifier extends SectionHeading("Classifier", Set(L3), PartOfSpeech,
    languages=Set("zh", "th", "za", "vi", "as"))

  case CombiningForm extends SectionHeading("Combining form",
    Set(L3),
    PartOfSpeech,
    languages=Set("ru", "ja", "oj", "grc"),
    wikidata = Set(Q(107614077))
  )

  case Conjunction extends SectionHeading("Conjunction", Set(L3), PartOfSpeech)
  case Contraction extends SectionHeading("Contraction", Set(L3), PartOfSpeech)

  case Counter extends SectionHeading("Counter", Set(L3), PartOfSpeech,
    languages=Set("ja", "ko", "ryu", "ug", "jje"))

  case CuneiformSign extends SectionHeading("Cuneiform sign", Set(L3), PartOfSpeech, languages = Set("mul"))
  case Determinative extends SectionHeading("Determinative", Set(L3), PartOfSpeech,
    languages=Set("sux"), wikidata=Set(Q(1201172)))

  case Determiner extends SectionHeading("Determiner", Set(L3), PartOfSpeech)

  case DiacriticalMark extends SectionHeading("Diacritical mark", Set(L3), PartOfSpeech,
    languages = Set("mul", "kn", "km", "he", "ml"))

  case Final extends SectionHeading("Final", Set(L3), PartOfSpeech, languages=Set("oj", "otw"))


  case HanCharacter extends SectionHeading(name = "Han character",
    Set(L3),
    PartOfSpeech,
    languages = Set("mul", "vi", "tyz", "zh"))

  case Hanja extends SectionHeading("Hanja", Set(L3), PartOfSpeech, languages=Set("ko", "zh"))
  case Hanzi extends SectionHeading("Hanzi", Set(L3), PartOfSpeech, languages=Set("cmn", "yue", "zh"))

  case Ideophone extends SectionHeading("Ideophone",
    Set(L3),
    PartOfSpeech,
    languages=Set("ko", "ha", "yo", "mch", "xh", "jje", "okm", "zu", "nup", "ny"),
    wikidata=Set(Q(2302610)))

  case Idiom extends SectionHeading("Idiom", Set(L3), PartOfSpeech, deprecated=true,
    languages=Set("zh", "ja", "vi", "ru", "en", "ko", "sv", "tr", "sh"))

  case Infix extends SectionHeading("Infix", Set(L3), PartOfSpeech)
  case Initial extends SectionHeading("Initial", Set(L3), PartOfSpeech, languages=Set("oj", "otw"))
  case Interfix extends SectionHeading("Interfix", Set(L3), PartOfSpeech)
  case Interjection extends SectionHeading("Interjection", Set(L3), PartOfSpeech)
  case Kanji extends SectionHeading("Kanji", Set(L3), PartOfSpeech,
    languages=Set("ja", "ryu", "mvi", "yoi", "xug", "rys", "yox", "okn", "kzg", "ams"))

  case Letter extends SectionHeading("Letter", Set(L3), PartOfSpeech,
    languages = Set("mul", "te", "crx", "ab", "kk", "ks", "zh", "az", "dv", "mn"))

  case Ligature extends SectionHeading("Ligature", Set(L3), PartOfSpeech,
    languages = Set("mul", "te", "ml", "dv", "hi"))

  case Logogram extends SectionHeading("Logogram", Set(L3), PartOfSpeech,
    languages = Set("akk", "hit", "mul"),
    wikidata=Set(Q(138659)))

  case Noun extends SectionHeading("Noun", Set(L3), PartOfSpeech)
  case Number extends SectionHeading("Number", Set(L3), PartOfSpeech)
  case Numeral extends SectionHeading("Numeral", Set(L3), PartOfSpeech)
  case Participle extends SectionHeading("Participle", Set(L3), PartOfSpeech)
  case Particle extends SectionHeading("Particle", Set(L3), PartOfSpeech)
  case Phoneme extends SectionHeading("Phoneme", Set(L3, L4), PartOfSpeech, languages=Set("hea", "zh"))
  case Phrase extends SectionHeading("Phrase", Set(L3), PartOfSpeech)
  case Postposition extends SectionHeading("Postposition", Set(L3), PartOfSpeech,
    languages = Set("fi", "hu", "nv", "bn", "hi", "az", "izh", "et"))

  case Predicative extends SectionHeading("Predicative", Set(L3), PartOfSpeech,
    languages = Set("ru", "uk", "az", "ba", "kk", "be"),
    wikidata= Set(Q(138659)))

  case Prefix extends SectionHeading("Prefix", Set(L3), PartOfSpeech)
  case Preposition extends SectionHeading("Preposition", Set(L3), PartOfSpeech)
  case PrepositionalPhrase extends SectionHeading("Prepositional phrase", Set(L3), PartOfSpeech)

  case Prenoun extends SectionHeading("Prenoun", Set(L3), PartOfSpeech,
    languages = Set("oj", "ka", "umu", "lzz", "otw", "unm"))

  case Preverb extends SectionHeading("Preverb", Set(L3), PartOfSpeech,
    languages = Set("umu", "otw", "mez", "oj", "unm"))

  case Pronoun extends SectionHeading("Pronoun", Set(L3), PartOfSpeech)
  case ProperNoun extends SectionHeading("Proper noun", Set(L3), PartOfSpeech)
  case Proverb extends SectionHeading("Proverb", Set(L3), PartOfSpeech)

  case PunctuationMark extends SectionHeading("Punctuation mark", Set(L3), PartOfSpeech,
    languages=Set("mul", "ja", "zh", "en", "hy", "ar"))

  case Relative extends SectionHeading("Relative", Set(L3), PartOfSpeech,
    languages = Set("xh", "zu", "ss", "nr", "bnt-phu", "nd", "st", "bnt-lal"))

  case Root extends SectionHeading("Root", Set(L3), PartOfSpeech,
    languages = Set("ar", "sa", "he", "nv", "ko", "aii", "mnt", "ajp", "ff"))

  case Stem extends SectionHeading("Stem", Set(L3), PartOfSpeech, languages = Set("nv", "tyv", "id"))

  case Suffix extends SectionHeading("Suffix", Set(L3), PartOfSpeech)
  case Syllable extends SectionHeading("Syllable", Set(L3), PartOfSpeech,
    languages = Set("ja", "ko", "vai"))

  case Symbol extends SectionHeading("Symbol", Set(L3), PartOfSpeech)
  case Verb extends SectionHeading("Verb", Set(L3), PartOfSpeech)

  case VerbalAdjective extends SectionHeading("Verbal Adjective", Set(L3), PartOfSpeech,
    languages = Set("grc"), deprecated = true)

  case VerbalNoun extends SectionHeading("Verbal noun", Set(L3), PartOfSpeech,
    languages = Set("ka", "oge", "xmf"))

  // Footer
  case ExternalLinks extends SectionHeading("External links", Set(L3), SectionKind.References, deprecated=true)
  case FurtherReading extends SectionHeading("Further reading", Set(L3), SectionKind.References)
  case References extends SectionHeading("References", Set(L3), SectionKind.References)

  case Anagrams extends SectionHeading("Anagrams", Set(L3), SectionKind.Relation)

  val elementFilter: ElementFilter = (element: Element) => element.section.contains(name)

object SectionHeading:
  // Section headers (usually L3) that are used to group entries
  val groupingHeaders: Set[SectionHeading] = Set(Etymology, Pronunciation, Compounds,
                                                Romanization, Glyph, GlyphOrigin, SignValues)

  val ordering: Ordering[SectionHeading] = (x: SectionHeading, y: SectionHeading)
      => groupingHeaders.contains(y) compare groupingHeaders.contains(x) match
          case 0 => x.kind.ordinal compare y.kind.ordinal
          case x => x

  def apply(string: String): Option[SectionHeading] =
    SectionHeading.values.find(_.name == string)

extension (element: nodes.Element)
  def section: Option[String] =
    var current = element
    while
      current = current.parent()
      current != null
    do
      if current.tagName() == "section" then return Option(current.firstElementChild).map(_.text())
    None
