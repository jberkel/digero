package digero.wiktionary.model

case class Transliterated(text: String,
                          script: String,
                          transliterations: Seq[String])
