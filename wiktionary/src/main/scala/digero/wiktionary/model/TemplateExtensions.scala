package digero.wiktionary.model

import digero.model.Template
import ipanema.language.model.{Identifier, Language, LanguageData}

import scala.jdk.OptionConverters._

object TemplateExtensions:
  extension (template: Template)
    def getLanguage: Option[Language] =
        template.getNonBlankParameter(1)
          .flatMap(LanguageData.load.getLanguage(_).toScala)

    def getTarget(index: Int): Option[String] =
      template.getNonBlankParameter(index).filter(_ != "-")

    def getNonEtymologyLanguage(index: Int): Option[Language] =
      template.getNonBlankParameter(index)
          .flatMap((code: String) => LanguageData.load.getLanguage(code).toScala)
          .flatMap(_.getNonEtymological.toScala)
          .filter(_.isInstanceOf[Language])
          .map( _.asInstanceOf[Language])
