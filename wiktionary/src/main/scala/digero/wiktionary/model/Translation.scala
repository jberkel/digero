package digero.wiktionary.model


sealed trait TranslationContent

case class SimpleTranslation(translation: String) extends TranslationContent
case class NormalizedTranslation(translation: String, normalized: String) extends TranslationContent
case class MultiTermTranslation(commonMark: String) extends TranslationContent


case class TranslationSubpage(page: String, section: Option[String]):
  override def toString: String = page + section.map(s => s"#${s}").getOrElse("")

case class TranslationGroup(gloss: String,
                            translations: Seq[Translation],
                            subpage: Option[TranslationSubpage] = None)


case class Translation(language: String,
                       content: TranslationContent,
                       transliteration: Option[String],
                       script: String,
                       qualifier: Option[String],
                       needsVerifying: Boolean):

  def nonLatinScript: Option[String] = script match
    case "" | "Latn" => None
    case script => Some(script)
