package digero.wiktionary.model

case class ParsedEntry[T](name: EntryName, sections: Seq[Section[T]]):
  def asTree(indent: Int = 0): String =
    sections.map(_.asTree(indent)).mkString("\n\n")
