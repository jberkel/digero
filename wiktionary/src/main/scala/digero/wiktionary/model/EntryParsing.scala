package digero.wiktionary.model

import digero.wiktionary.model.SectionHeading.{Antonyms, DerivedTerms, RelatedTerms, Synonyms}
import digero.wiktionary.model.lexical.{Pronunciation, Relation, RelationType}
import digero.wiktionary.model.serialization.TextConverter
import digero.wiktionary.parser.inflection.{InflectionParser, ParsedInflection}
import digero.wiktionary.parser.{EntryContext, HeadwordLineParser, LinkListParser, PronunciationParser, SenseParser, TranslationParser}

object EntryParsing:
  extension (entry: Entry[String])
    def pronunciations: Seq[Pronunciation] =
      for
        pronunciationContent <- entry.remainingSections.filter(_.isPronunciation).flatMap(_.content)
        pronunciation <- PronunciationParser().parse(pronunciationContent)
      yield
        pronunciation

    def partsOfSpeech(using TextConverter): Seq[PartOfSpeechSection] =
      val senseParser = SenseParser()
      val headwordParser = HeadwordLineParser()
      given EntryContext(entry.name)

      for
        posSection <- entry.posSections
        posHeader <- posSection.heading
        posContent <- posSection.content
      yield
        PartOfSpeechSection(
          posHeader,
          headwordParser.parse(posContent),
          senseParser.parse(posContent),
          relations(posSection),
          conjugation(posSection),
          translations(posSection),
          usageNotes(posSection),
        )

    private def usageNotes(posSection: Section[String]): Option[String] =
      posSection.subsections
        .filter(_.isUsageNotes)
        .flatMap(_.content)
        .headOption

    private def relations(posSection: Section[String])(using EntryContext): Seq[Relation] =
      for
        section <- posSection.subsections if section.isRelation && !section.isTranslations
        content <- section.content.toList
        heading <- section.heading.toList
        relation <- parseRelations(heading, content)
      yield relation


    private def parseRelations(heading: SectionHeading, html: String)(using EntryContext): Seq[Relation] =
      for
        link <- LinkListParser().parse(html)
        relationType <- relationTypeMapping.lift(heading)
      yield
        Relation(entry.name.name, link.link.getTarget, relationType)

    private def conjugation(posSection: Section[String])(using EntryContext): Seq[ParsedInflection] =
      for
        conjugationContent <- posSection.subsections.filter(_.isConjugation).flatMap(_.content)
        inflection <- InflectionParser().parse(conjugationContent)
      yield
        inflection

    private def translations(posSection: Section[String])
                            (using TextConverter)
                            (using EntryContext): Seq[TranslationGroup] =
      for
        translationContent <- posSection.subsections.filter(_.isTranslations).flatMap(_.content)
        group <- TranslationParser().parseTranslationSection(translationContent)
      yield
        group

    private def relationTypeMapping: PartialFunction[SectionHeading, RelationType] =
      case Synonyms => RelationType.Synonym
      case Antonyms => RelationType.Antonym
      case DerivedTerms => RelationType.DerivedTerm
      case RelatedTerms => RelationType.RelatedTerm

