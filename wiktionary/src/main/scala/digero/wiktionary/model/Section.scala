package digero.wiktionary.model

import digero.wiktionary.model.Level.L3
import digero.wiktionary.model.SectionHeading.{Conjugation, Pronunciation, Translations, UsageNotes}

case class Section[Content](
     name: String,
     level: Level = L3,
     heading: Option[SectionHeading],
     index: Option[Int] = None,
     content: Option[Content] = None,
     subsections: Seq[Section[Content]] = Seq.empty) extends Ordered[Section[Content]]:

  override def compare(that: Section[Content]): Int =
    (heading, that.heading) match
      case (Some(header1), Some(header2)) => SectionHeading.ordering.compare(header1, header2)
      case (None, Some(_)) => -1
      case (Some(_), None) =>  1
      case _ => 0

  override def toString: String = s"Section[$level $name]"

  def isPronunciation: Boolean = isHeader(Pronunciation)
  def isUsageNotes: Boolean = isHeader(UsageNotes)
  def isEtymology: Boolean = isKind(SectionKind.Etymology)
  def isTranslations: Boolean = isHeader(Translations)
  def isConjugation: Boolean = isHeader(Conjugation)
  def isPartOfSpeech: Boolean =  isKind(SectionKind.PartOfSpeech)
  def isRelation: Boolean = isKind(SectionKind.Relation)
  
  private def isKind(kind: SectionKind): Boolean = heading.exists(_.kind == kind)
  private def isHeader(header: SectionHeading): Boolean = this.heading.contains(header)

  def isGroupingHeader: Boolean = isGroupingHeaderType && hasChildren
  def canContainChildren: Boolean = isGroupingHeaderType || isPartOfSpeech
  private def hasChildren: Boolean = subsections.nonEmpty
  private def isGroupingHeaderType: Boolean = heading.exists(SectionHeading.groupingHeaders.contains)

  def asTree(indent: Int=0): String =
      (subsections.map(_.asTree(indent+2)) prepended
      (" " * indent + s"${heading.getOrElse(name)}")).mkString("\n")

  def withChildren: Seq[Section[Content]] =
    LazyList(this) ++ subsections.flatMap(_.withChildren)

  def mapSections(f: Seq[Section[Content]] => Seq[Section[Content]]): Section[Content] =
    val filteredSubsections = subsections.map(_.mapSections(f))
    Section(name, level, heading, index, content, f(filteredSubsections))

  def filter(p: Section[Content] => Boolean): Seq[Section[Content]] =
    withChildren.filter(p)


object Section:

  private val SectionRegex = """^(?<name>[A-Za-z ]+)(?:\s+(?<number>\d+))?$""".r

  def parse(string: String): (Option[SectionHeading], Option[Int]) =
    string match
      case SectionRegex(name, number: String) =>
        (SectionHeading(name), number.toIntOption)
      case _ =>
        (SectionHeading(string), None)

  def apply(string: String): Section[Nothing] =
    val (heading, index) = parse(string)
    Section[Nothing](string, heading=heading, index=index)
