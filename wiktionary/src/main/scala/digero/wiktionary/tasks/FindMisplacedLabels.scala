package digero.wiktionary.tasks

import digero.wikitext.{SerializationSupport, TemplateVisitor}
import digero.wiktionary.model.TemplateExtensions._
import digero.wikitext.WtTemplateExtensions._
import digero.wiktionary.tasks.MisplacedLabelCollector.{Result, ResultType, collect}
import digero.{DataFrameTask, TaskArguments}
import ipanema.language.model.Language
import org.apache.spark.sql.*
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.types.DataTypes.StringType
import org.apache.spark.sql.types.{DataTypes, StructType}
import org.sweble.wikitext.parser.nodes.{WtNodeList, WtTemplate}

import java.io.File
import scala.collection.mutable

/**
 * Finds labels which are misplaced (that is, the language code specified in the template does not match the language
 * code of the current entry).
 *
 * @see <a href="https://en.wiktionary.org/wiki/User:Jberkel/lists/L2-header-label-mismatch">
 *      User:Jberkel/lists/L2-header-label-mismatch</a>
 */
class FindMisplacedLabels extends DataFrameTask[File]:
  private val rowSchema = new StructType()
    .add("type", StringType)
    .add("entry", StringType)
    .add("entryLanguage", StringType)
    .add("indicatedLanguage", StringType)
    .add("resolvedLanguage", StringType)

  given encoder: Encoder[Row] = ExpressionEncoder(rowSchema)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.nonEmpty)
    val output = arguments.outputFile

    input.filter("namespace = 0 AND redirect is null").flatMap((row: Row) => {
        val entry = row.getAs[String]("entry")
        val language = row.getAs[String]("language")
        val nodeList = SerializationSupport.get.deserialize(row.getAs("parsed"))
        for
          result <- collect(entry, language, nodeList)
        yield Row(
          result.`type`.toString,
          result.entry,
          result.entryLanguage,
          result.indicatedLanguage.getOrElse(""),
          result.resolvedLanguage.map(_.getCanonicalName).getOrElse("")
        )
    }).coalesce(100)
      .distinct
      .repartition(1)
      .write
      .mode(SaveMode.Overwrite)
      .json(output.getPath)

    output


object MisplacedLabelCollector:
  enum ResultType:
    case LabelMisplaced
    case LabelContainsNoLanguageCode
    case LabelContainsUnresolvedLanguageCode

  case class Result(`type`: ResultType,
                    entry: String,
                    entryLanguage: String,
                    indicatedLanguage: Option[String],
                    resolvedLanguage: Option[Language])

  def collect(entry: String, language: String, page: WtNodeList): Seq[Result] =
    val visitor = new MisplacedLabelCollector(entry, language)
    visitor.visit(page)
    visitor.results.toSeq


class MisplacedLabelCollector(private val entry: String,
                              private val language: String) extends TemplateVisitor:
  final private val results = mutable.Buffer[Result]()

  override def visit(wtTemplate: WtTemplate): Unit =
    if !wtTemplate.getName.isResolved then return

      wtTemplate.getName.getAsString match
        case "label" | "lb" | "lbl" =>
          val template = wtTemplate.asTemplate
          val languageCode = template.getParameter(1)
          val resolvedLanguage = template.getLanguage
          determineResult(language, languageCode, resolvedLanguage) match
            case Some(value) => results.append(value)
            case _ =>
        case _ =>

  private def determineResult(sectionLanguage: String,
                              languageCode: Option[String],
                              resolvedLanguage: Option[Language]): Option[Result] =
    if languageCode.isEmpty then
      Some(Result(ResultType.LabelContainsNoLanguageCode, entry, sectionLanguage, languageCode, resolvedLanguage))
    else if resolvedLanguage.isEmpty then
      Some(Result(ResultType.LabelContainsUnresolvedLanguageCode, entry, sectionLanguage, languageCode, resolvedLanguage))
    else if checkLanguageMatch(resolvedLanguage.get, sectionLanguage) then
      None
    else
      Some(Result(ResultType.LabelMisplaced, entry, sectionLanguage, languageCode, resolvedLanguage))

  private def checkLanguageMatch(data: Language, sectionLanguage: String): Boolean = sectionLanguage == data.getCode
