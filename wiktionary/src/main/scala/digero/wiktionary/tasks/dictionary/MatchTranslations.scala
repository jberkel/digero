package digero.wiktionary.tasks.dictionary

import digero.wiktionary.model.serialization.SerializedEntry
import digero.wiktionary.parser.TranslationMatcher
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Encoder, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

/**
 * Matches senses of an entry to translations, based on the similarity of the
 * translation's gloss.
 *
 * **house**
 *
 *  1. A structure built or serving as an abode of human beings.
 *  2. A building where a deliberative assembly meets; […]
 *
 * Translations:
 *
 *  - ± "human abode" […]
 *  - ± "debating chamber for government politicians" […]
 */
class MatchTranslations extends DataFrameTask[File]:
  override def inputSchema: Option[StructType] = Some(summon[Encoder[SerializedEntry]].schema)
  
  /**
   * @param input a dataframe produced with [[ExtractContent]]
   * @return dataframe in the same format, with additional translations
   */
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile

    input
      .as[SerializedEntry]
      .map(entry => transform(entry).getOrElse(entry))
      .write
      .mode(SaveMode.Overwrite)
      .option(JSONOptions.IGNORE_NULL_FIELDS, "false")
      .json(output.getPath)

    output

  private inline def transform(serializedEntry: SerializedEntry): Option[SerializedEntry] =
    TranslationMatcher.matchEntry(serializedEntry)
