package digero.wiktionary.tasks.dictionary

import digero.wiktionary.model.serialization.SerializedEntry
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Encoder, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File
import scala.io.Source
import scala.util.Using

class FilterContent extends DataFrameTask[File]:
  // need to specify the schema here because it cannot always be inferred correctly
  // from the JSON input
  override def inputSchema: Option[StructType] = Some(summon[Encoder[SerializedEntry]].schema)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.length > 1)
    val output = arguments.outputFile
    val excludedCategories = Using(Source.fromFile(File(arguments(1)))) { source =>
      source.getLines().toSet
    }.get

    input
      .as[SerializedEntry]
      .map(entry => filter(entry, excludedCategories))
      .write
      .mode(SaveMode.Overwrite)
      .option(JSONOptions.IGNORE_NULL_FIELDS, "false")
      .json(output.getPath)

    output


  private inline def filter(serializedEntry: SerializedEntry, categories: Set[String]): SerializedEntry =
    serializedEntry.copy(
      categories = serializedEntry.categories.filterNot(categories.contains),
      senses = serializedEntry.senses.map { sense =>
        sense.copy(categories = sense.categories.filterNot(categories.contains))
      }
    )