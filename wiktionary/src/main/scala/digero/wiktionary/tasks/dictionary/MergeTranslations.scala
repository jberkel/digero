package digero.wiktionary.tasks.dictionary

import digero.wiktionary.model.serialization.{SerializedEntry, SerializedTranslationGroup}
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.functions.{col, not}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Encoder, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

/**
 * Merges the translations from `entry/translations` subpages back into the entries
 */
class MergeTranslations extends DataFrameTask[File]:

  override def inputSchema: Option[StructType] = Some(summon[Encoder[SerializedEntry]].schema)
  
  /**
   * @param input dataset produced with [[ExtractContent]].
   * @param arguments the output file in the same format
   */
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    val translationsPageSuffix = "/translations"

    val isTranslationPage = col("pageTitle").endsWith(translationsPageSuffix)
    val translationEntries = input
      .where(isTranslationPage)
      .as[SerializedEntry]
      .collect()
      .toSeq
      .groupMap(_.pageTitle.replace(translationsPageSuffix, ""))(identity)

    val broadcast = session.sparkContext.broadcast(translationEntries)

    input
      .as[SerializedEntry]
      .map(entry => broadcast.value.get(entry.pageTitle) match
        case Some(translationEntries) => entry.mergeTranslations(translationEntries)
        case None => entry
      )
      .where(not(isTranslationPage))
      .write
      .mode(SaveMode.Overwrite)
      .option(JSONOptions.IGNORE_NULL_FIELDS, "false")
      .json(output.getAbsolutePath)

    output

extension (entry: SerializedEntry)
  def mergeTranslations(candidates: Seq[SerializedEntry]): SerializedEntry =
    val mergedTranslations: Seq[SerializedTranslationGroup] = entry
      .translations
      .flatMap(group => group.subpage match
        case Some(_) =>
          candidates
            .find(_.partOfSpeech == entry.partOfSpeech)
            .map(_.translations)
            .getOrElse(Seq(group))

        case None => Seq(group)
      )

    entry.copy(translations = mergedTranslations)