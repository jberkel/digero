package digero.wiktionary.tasks.dictionary

import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.functions.{col, collect_list, first, struct}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.io.File

class CombineContent extends DataFrameTask[File]:

  /** Groups all entries by pageTitle */
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    input
      .drop("translations")
      .groupBy("pageTitle")
      .agg(
        first(col("redirects")).as("redirects"),
        collect_list(
          struct("pageTitle", "headwordLines", "partOfSpeech", "senses", "etymology", "pronunciations",
            "usageNotes", "categories", "relations", "inflections")
        ).as("entries")
      )
      .write
      .option(JSONOptions.IGNORE_NULL_FIELDS, true)
      .mode(SaveMode.Overwrite)
      .json(output.getPath)

    output