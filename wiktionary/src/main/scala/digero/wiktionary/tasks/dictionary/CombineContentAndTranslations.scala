package digero.wiktionary.tasks.dictionary

import digero.wiktionary.model.serialization.ReverseTranslation
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.functions.{col, collect_list, first, struct}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

class CombineContentAndTranslations extends DataFrameTask[File]:
  /**
   * @param input dataframe of [[SerializedEntry]]s, dataframe of [[ReverseTranslation]]s
   * @return a dataframe keyed by `pageTitle` containing entries and translations
   */
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.length > 2)
    val output = File(arguments(1))
    val language = arguments(2)

    val translations = session.read
      .json(arguments(0))
      .filter(col("language") === language)
      .as[ReverseTranslation]
      .groupBy("pageTitle")
      .agg(
        collect_list(
          struct("headword", "partOfSpeech", "gloss", "definition")
        ).as("translations")
      )

    input
      .drop("translations")
      .groupBy("pageTitle")
      .agg(
        first(col("redirects")).as("redirects"),
        collect_list(
          struct("pageTitle", "headwordLines", "partOfSpeech", "senses", "etymology", "pronunciations",
                 "usageNotes", "categories", "relations", "inflections")
        ).as("entries")
      )
      .join(translations, usingColumn = "pageTitle", joinType = "left")
      .write
      .option(JSONOptions.IGNORE_NULL_FIELDS, true)
      .mode(SaveMode.Overwrite)
      .json(output.getPath)

    output

