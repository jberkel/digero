package digero.wiktionary.tasks.dictionary

import digero.wiktionary.model.serialization.{ReverseTranslation, SerializedSense, SerializedTranslation, SerializedTranslationGroup}
import digero.wiktionary.parser.cmark.CommonMarkConverter
import digero.wiktionary.tasks.dictionary.ExtractTranslations.commonMarkConverter
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

object ExtractTranslations:
  private val commonMarkConverter = CommonMarkConverter()

class ExtractTranslations extends DataFrameTask[File]:

  /**
   * @param input a dataframe produced with [[ExtractContent]] with English [[digero.wiktionary.model.serialization.SerializedEntry]]s
   * @return a dataframe of [[ReverseTranslation]]s
   */
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    require(output.getPath.nonEmpty)

    input
      .selectExpr("pageTitle", "partOfSpeech", "senses", "explode(translations)")
      .as[(String, String, Seq[SerializedSense], SerializedTranslationGroup)]
      .flatMap { case (title, pos, senses, translationGroup) =>
        translationGroup.translations.map(translation =>
          ReverseTranslation(
            language = translation.language,
            pageTitle = translation.text,
            partOfSpeech = pos,
            headword = title,
            gloss=translationGroup.gloss,
            definition = senses.find(_.translationGloss.contains(translationGroup.gloss)).map(
                sense => commonMarkConverter.commonMarkToPlain(sense.definition)
            )
          )
        )
      }
      .write
      .partitionBy("language")
      .mode(SaveMode.Overwrite)
      .json(output.getAbsolutePath)

    output