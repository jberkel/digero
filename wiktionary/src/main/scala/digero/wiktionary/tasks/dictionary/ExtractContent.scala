package digero.wiktionary.tasks.dictionary

import digero.spark.{getOpt, getSeqOpt}
import digero.wiktionary.model.EntryParsing.{partsOfSpeech, pronunciations}
import digero.wiktionary.model.serialization.{SerializedEntry, SerializedTranslationGroup, TextConverter}
import digero.wiktionary.model.{EntryGroup, EntryName}
import digero.wiktionary.parser.EntryParser
import digero.wiktionary.parser.cmark.CommonMarkConverter
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.catalyst.json.JSONOptions
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import scala3encoders.encoder

import java.io.File

class ExtractContent extends DataFrameTask[File]:
  private val parser = EntryParser()

  /**
   * @param input Wiktionary HTML dump
   * @param arguments language to extract content for
   * @return rows of [[SerializedEntry]]
   */
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    val language = arguments.language.getOrElse("en")

    require(output.getPath.nonEmpty)
    given TextConverter = CommonMarkConverter().htmlToCommonMark

    (for
      row <- input.filter(s"language = \"$language\"")
      html: String <- row.getOpt("html")
      entryName: EntryName <- Some(EntryName(row))
      entry <- EntryGroup(parser.parse(entryName, html))
      partOfSpeech <- entry.partsOfSpeech
      redirects: Option[Seq[String]] = row.getSeqOpt("redirects")
      etymology = entry.etymologySection.flatMap(_.content)
      serializedEntry = SerializedEntry(entry.pageTitle,
                                        redirects.getOrElse(Seq.empty),
                                        etymology,
                                        entry.pronunciations,
                                        partOfSpeech)
    yield serializedEntry)
      .write
      .mode(SaveMode.Overwrite)
      .option(JSONOptions.IGNORE_NULL_FIELDS, "false")
      .json(output.getAbsolutePath)

    output

