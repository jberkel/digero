package digero.wiktionary.tasks.wanted

import digero.wiktionary.AppSchema
import digero.wiktionary.model.SectionHeading
import digero.wiktionary.parser.{ParsoidHTMLParser, negate}
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.*
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.functions.col

import java.io.File
import scala.jdk.CollectionConverters.*

class CreateLinkGraph extends DataFrameTask[File]:
  private val parser = ParsoidHTMLParser()
  private val filter = SectionHeading.Anagrams.elementFilter.negate()

  given encoder: Encoder[Row] = ExpressionEncoder(AppSchema.links)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    require(output.getPath.nonEmpty)

    input.flatMap(row => {
      val html = row.getAs[String]("html")
      val entry = row.getAs[String]("entry")
      val language = row.getAs[String]("language")

      parser.links(html, filter)(using entry)
        .filter(_.getNamespace == null)
        .map(link => (link.getTarget, Option(link.getLanguage).map(_.getCode)))
        .distinct
        .map(x => Row(entry, language, x._1, x._2.orNull))
    })(using encoder)
     .repartition(col("targetLanguage"))
     .write
     .mode(SaveMode.Overwrite)
     .partitionBy("targetLanguage")
     .parquet(output.getAbsolutePath)

    output
