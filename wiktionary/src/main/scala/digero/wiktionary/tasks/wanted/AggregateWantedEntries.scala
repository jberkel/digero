package digero.wiktionary.tasks.wanted

import digero.wiktionary.requests.GetWordList
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.File
import java.nio.file.Files

class AggregateWantedEntries extends DataFrameTask[File]:

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.nonEmpty)
    val output = arguments.outputPath
    input.createOrReplaceTempView("wanted")

    for language <- GetWordList("User:Jberkel/lists/wanted/languages")
      do
        println(s"=> generating ${language}")
        val agg = session.sql(
          s"""
            |SELECT
            |  count(source) as count,
            |  sort_array(collect_list(source)) AS sources,
            |  target FROM wanted
            |  WHERE targetLanguage = '${language}'
            |  GROUP BY target
            |  ORDER BY count DESC, target ASC
            |""".stripMargin)

        agg.show()
        Files.createDirectories(output)

        val jsonFile = output.resolve(language)
        println(s"=> writing json dir ${jsonFile}")
        agg.coalesce(1).write.json(jsonFile.toString)

    output.toFile


