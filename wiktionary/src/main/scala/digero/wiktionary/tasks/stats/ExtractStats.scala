package digero.wiktionary.tasks.stats

import digero.wiktionary.model.EntryParsing.partsOfSpeech
import digero.wiktionary.model.serialization.TextConverter
import digero.wiktionary.model.{EntryGroup, EntryName}
import digero.wiktionary.parser.EntryParser
import digero.wiktionary.parser.cmark.CommonMarkConverter
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.*
import org.apache.spark.sql.SaveMode.Overwrite
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DataTypes.{StringType, createArrayType}
import org.apache.spark.sql.types.StructType

import java.io.File

class ExtractStats extends DataFrameTask[File]:
  private val parser = EntryParser()

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    require(output.getPath.nonEmpty)

    val filteredInput = if arguments.length > 1 then
      val filter = arguments(1)
      log.info(s"filter: \"$filter\"")
      input.filter(filter)
    else
      input

    given TextConverter = CommonMarkConverter().htmlToCommonMark

    filteredInput.flatMap(row =>
      val html = row.getAs[String]("html")
      val entry = EntryName(row)

      val partsOfSpeech = for
        entry <- EntryGroup(parser.parse(entry, html))
        partOfSpeech <- entry.partsOfSpeech
      yield partOfSpeech

      val headwords = for
        partOfSpeech <- partsOfSpeech
        headwordLine <- partOfSpeech.headwordLines
        tags = if headwordLine.isNonLemma then Seq("NonLemma") else Seq.empty
      yield Row(entry.name, entry.languageCode, "headword", partOfSpeech.name, headwordLine.headword, tags)

      val senses = for
        partOfSpeech <- partsOfSpeech
        sense <- partOfSpeech.senses
        tags = if sense.isNonLemma || partOfSpeech.isNonLemma then Seq("NonLemma") else Seq.empty
      yield Row(entry.name, entry.languageCode, "sense", partOfSpeech.name, sense.text, tags)

      headwords ++ senses
    )
      .repartition(col("language"))
      .write
      .partitionBy("language", "type")
      .mode(Overwrite)
      .json(output.getAbsolutePath)

    output

  given encoder: Encoder[Row] = ExpressionEncoder((new StructType)
    .add("entry", StringType)
    .add("language", StringType)
    .add("type", StringType)
    .add("part_of_speech", StringType)
    .add("content", StringType)
    .add("tags", createArrayType(StringType))
  )
