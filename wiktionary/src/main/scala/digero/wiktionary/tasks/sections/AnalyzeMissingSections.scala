package digero.wiktionary.tasks.sections

import digero.wiktionary.model.EntryName
import digero.wiktionary.parser.EntryParser
import digero.{DataFrameTask, TaskArguments}
import org.apache.spark.sql.*
import org.apache.spark.sql.SaveMode.Overwrite
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.functions.{col, count}
import org.apache.spark.sql.types.DataTypes.StringType
import org.apache.spark.sql.types.StructType

import java.io.File

/**
 * Finds sections in a HTML-dump which can't be parsed.
 */
class AnalyzeMissingSections extends DataFrameTask[File]:
  private val parser = EntryParser()

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    require(output.getPath.nonEmpty)

    given encoder: Encoder[Row] = ExpressionEncoder((new StructType)
        .add("section", StringType)
        .add("entry", StringType)
    )

    input.flatMap(row =>
      val entry = EntryName(row)
      val html = row.getAs[String]("html")
      val parsed = parser.parse(entry, html)

      parsed.sections
        .flatMap(_.withChildren)
        .filter(_.heading.isEmpty)
        .map(section => Row(section.name, entry))
    ).groupBy("section")
     .agg(count("section").as("count"))
     .orderBy(col("count").desc)
     .write
     .mode(Overwrite)
     .json(output.getAbsolutePath)

    output
