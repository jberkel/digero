package digero.wiktionary.requests
import java.net.URLEncoder
import java.nio.charset.Charset
import ujson.read
import requests.get

object GetWordList:

  def apply(page: String): Seq[String] =
    val encodedPage = URLEncoder.encode(page, Charset.defaultCharset())
    val body = requests.get(s"https://en.wiktionary.org/w/rest.php/v1/page/$encodedPage").text()
    val json = ujson.read(body)
    val source = json.obj("source").str
    source.split("\\R").toSeq
