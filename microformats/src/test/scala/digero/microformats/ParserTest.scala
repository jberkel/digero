package digero.microformats

import digero.test.Fixture
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import java.net.URI
import scala.annotation.nowarn
import scala.compiletime.uninitialized

class ParserTest extends Fixture, HTMLParser:
  var subject: Parser = uninitialized
  given baseURI: Option[URI] = Some(URI.create("https://base.com"))

  @BeforeEach
  def setUp(): Unit = subject = Parser()

  @Test def testParseSimpleMicroformat(): Unit =
    subject.parse(
      """
        <div class="h-simple">
            <p class="p-name">Name</p>
            <a href="/foo" class="u-url">Foo</a>
        </div>
      """) match
      case Microformats(Seq(Microformat(None, Seq("h-simple"), properties, None, None, None, Seq())), _) =>
        properties should contain only (
          "name" -> Seq(StringValue("Name")),
          "url" -> Seq(StringValue("https://base.com/foo")),
        )

  @Test def testParseChildMicroformat(): Unit =
    subject.parse(
      """
        <div class="h-parent">
            <p class="p-name">Parent</p>
            <div class="h-child">
              <p class="p-name">Child</p>
            </div>
        </div>
      """) match
      case Microformats(Seq(Microformat(None, Seq("h-parent"), properties, None, None, None, Seq(child))), _) =>
        properties should contain only ("name" -> Seq(StringValue("Parent")))

        child.properties should contain only ("name" -> Seq(StringValue("Child")))
        child.value shouldBe empty


  @Test @nowarn def testParseChildMicroformatInProperties(): Unit =
    subject.parse(
      """
        <div class="h-parent">
            <p class="p-name">Parent</p>
            <div class="p-child h-child" id="child-id">
              <p class="p-name">Child</p>
            </div>
        </div>
      """) match
      case Microformats(Seq(Microformat(None, Seq("h-parent"), properties, None, None, None, Seq())), _) =>

        properties.get("name") match
          case Some(Seq(StringValue("Parent"))) =>

        properties.get("child") match
          case Some(Seq(NestedMicroformat(
            Microformat(Some("child-id"), Seq("h-child"), childProperties, Some("Child"), None, None, Seq()))
          )) => childProperties should contain only ("name" -> Seq(StringValue("Child")))



  @Test def testParseEmbedded(): Unit =
    subject.parse(
      """
        <div id="some-id" class="h-usage-example">
            <i class="Latn mention e-example" lang="en">The street was lined with all-night <b>bars</b>.</i>
        </div>
      """) match
      case Microformats(Seq(Microformat(Some("some-id"), Seq("h-usage-example"), properties, None, None, None, Seq())), _) =>
        properties should contain only (
          "example" -> Seq(Embedded(
            html = "The street was lined with all-night <b>bars</b>.",
            value = "The street was lined with all-night bars.",
            lang = Some("en")
          ))
          )

  @Test def testParseDtValueClassWithDefaultDate(): Unit =
    subject.parse(
      """
        <div class="h-event">
           <h1 class="p-name">Implied Date wo Timezone</h1>
           <p> When:
             <span class="dt-start">
               <span class="value" title="May 21, 2014">2014-05-21</span>
               <span class="value" title="18:30">18:30</span>
               –
               <span class="dt-end">19:30</span>
             </span> (local time)
           </p>
        </div>
        """) match
      case Microformats(Seq(Microformat(None, Seq("h-event"), properties, None, None, None, Seq())), _) =>
        properties("start") should contain only StringValue("2014-05-21 18:30")
        properties("end") should contain only StringValue("2014-05-21 19:30")

  @Test def testParseImpliedNamePropertyTitle(): Unit =
    subject.parse(
      """
        <abbr class="h-abbr" title="Title">
            <p>Some text</p>
        </abbr>
      """) match
      case Microformats(Seq(Microformat(None, Seq("h-abbr"), properties, None, None, None, Seq())), _) =>
        properties should contain only(
          "name" -> Seq(StringValue("Title")),
        )

  @Test def testParseImpliedNamePropertyImg(): Unit =
    subject.parse(
      """
        <img class="h-image" alt="Alt"/>
      """) match
      case Microformats(Seq(Microformat(None, Seq("h-image"), properties, None, None, None, Seq())), _) =>
        properties should contain only (
          "name" -> Seq(StringValue("Alt")),
        )

  @Test def testParseImpliedPhotoPropertyImg(): Unit =
    subject.parse(
      """
        <img class="h-image" alt="Alt" src="/foo.png"/>
      """) match
      case Microformats(Seq(Microformat(None, Seq("h-image"), properties, None, None, None, Seq())), _) =>
        properties should contain only (
          "name" -> Seq(StringValue("Alt")),
          "photo" -> Seq(ImpliedPhoto(URI.create("https://base.com/foo.png"), Some("Alt"))),
        )

  @Test def testParseImpliedPhotoPropertyImgEmptySrc(): Unit =
    val result = subject.parse("""<img class="h-image" src=""/>""")
    result(0)("photo") should contain only ImpliedPhoto("https://base.com")

  @Test def testParseImpliedUrlPropertyA(): Unit =
    subject.parse("""<a class="h-link" href="/baz">Target</a>""") match
      case Microformats(Seq(Microformat(None, Seq("h-link"), properties, None, None, None, Seq())), _) =>
        properties should contain only (
          "url" -> Seq(StringValue("https://base.com/baz")),
          "name" -> Seq(StringValue("Target")),
        )

  @Test def testParseUrlInput(): Unit =
    val result = subject.parse("""<p class="h-foo"><input class="u-url" value="/baz"/></p>""")
    result(0)("url") should contain only StringValue("https://base.com/baz")

  @Test def testParseRel(): Unit =
    subject.parse(
      """
         <a rel="example" href="/foo">bar</a>
      """) match
      case Microformats(Seq(), rel) =>
        rel.rels should contain only(
          "example" -> Seq(URI.create("https://base.com/foo"))
        )

  @Test def testParseRelLocation(): Unit =
    subject.parse(
        """
           <a rel="example" href="/foo">bar</a>
        """) match
      case Microformats(Seq(), rel) =>
        rel.rels should contain only (
          "example" -> Seq(URI.create("https://base.com/foo"))
        )

  @Test def parseRel(): Unit =
    subject.parseRel(parseElement(
      """
          <a rel="baz buz" href="/foo" type="application/pdf" hreflang="fr" title="title" media="paper">
           Link-Content
         </a>
       """)) should contain(
      Rel(
        rels = Map(
          "baz" -> Seq(URI.create("https://base.com/foo")),
          "buz" -> Seq(URI.create("https://base.com/foo"))
        ),
        urls = Map(
          URI.create("https://base.com/foo") -> RelEntry(
            rels = Seq("baz", "buz"),
            text = "Link-Content",
            media = Some("paper"),
            hrefLang = Some("fr"),
            `type` = Some("application/pdf"),
            title = Some("title")
          )
        )
      )
    )

  @Test def parseRelEmpty(): Unit =
    subject.parseRel(parseElement("""<p>no rel</p>""")) shouldBe empty


class ParseResultTest:

  @Test
  def testMerge(): Unit =
    val a = Parser.ParsingResult(properties = Map("foo" -> Seq(PropertyValue("a")),
                                                  "bar" -> Seq(PropertyValue("b"))),
                                 microformats = Seq(),
                                 types = Set("a"))

    val b = Parser.ParsingResult(properties = Map("foo" -> Seq(PropertyValue("b"))),
                                 microformats = Seq(),
                                 types = Set("a", "b"))

    val result = a ++ b

    result.properties should contain only(
      "foo" -> Seq(PropertyValue("a"), PropertyValue("b")),
      "bar" -> Seq(PropertyValue("b"))
    )
    result.types should contain only("a", "b")
    result.microformats shouldBe empty

