package digero.microformats

import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class MicroformatsTest:

  @Test
  def testRoots(): Unit =
    Microformats.roots(
      Seq("h-foo", "bar", "e-lol")
    ) should contain only("h-foo")

  @Test
  def testIsRoot(): Unit =
    Microformats.isRoot(Seq("h-book")) shouldBe(true)
    Microformats.isRoot(Seq("book")) shouldBe(false)


  @Test
  def testClasses(): Unit =
    val result = Microformats.classes(Seq("e-element", "p-prop", "dt-datetime", "e-element-two", "h-some-root", "foo"))

    result should contain only(
      "e" -> Seq("element", "element-two"),
      "p" -> Seq("prop"),
      "dt" -> Seq("datetime"),
      "h" -> Seq("h-some-root")
    )