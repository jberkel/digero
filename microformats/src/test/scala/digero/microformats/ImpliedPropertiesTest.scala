package digero.microformats

import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import java.net.URI
import scala.compiletime.uninitialized

class ImpliedPropertiesTest extends HTMLParser:
  var subject: Parser = uninitialized
  given baseURI: Option[URI] = Some(URI.create("https://base.com"))

  @BeforeEach
  def setUp(): Unit = subject = Parser()

  @Test
  def testImplyNameImgAlt(): Unit =
    subject.implyName(parseElement("""<img alt="Alt Text"/>""")) should equal("Alt Text")

  @Test
  def testImplyNameAreaAlt(): Unit =
    subject.implyName(parseElement("""<area alt="Alt Text"/>""")) should equal("Alt Text")

  @Test
  def testImplyNameAbbrTitle(): Unit =
    subject.implyName(parseElement("""<abbr title="Abbr Title"/>""")) should equal("Abbr Title")

  @Test
  def testImplyNameOnlyChildImgAlt(): Unit =
    subject.implyName(parseElement(
      """
        <div>
          <img alt="Alt Text"/>
        </div>
      """)) should equal("Alt Text")

  @Test
  def testImplyNameNotOnlyChildImgAlt(): Unit =
    subject.implyName(parseElement(
      """
        <div>
          <img alt="Alt Text"/>
          <p>Text</p>
        </div>
      """)) should equal("Alt Text\nText")

  @Test
  def testImplyNameOnlyChildMicroformatImgAlt(): Unit =
    subject.implyName(parseElement(
      """
        <div>
          <img alt="Alt Text" class="h-foo"/>
        </div>
      """)) should equal("Alt Text")

  @Test
  def testImplyNameOnlyGrandChildImgAlt(): Unit =
    subject.implyName(parseElement(
      """
        <div>
          <div>
            <img alt="Alt Text"/>
          </div>
        </div>
      """)) should equal("Alt Text")

  @Test
  def testImplyNameOnlyChildAbbrTitle(): Unit =
    subject.implyName(parseElement(
      """
        <div>
          <abbr title="Abbr Title"/>
        </div>
      """)) should equal("Abbr Title")

  @Test
  def testImplyNameOnlyGrandChildAbbrTitle(): Unit =
    subject.implyName(parseElement(
      """
        <div>
          <div>
            <abbr title="Abbr Title"/>
           </div>
        </div>
      """)) should equal("Abbr Title")

  @Test
  def testImplyNameTextContentFallback(): Unit =
    subject.implyName(parseElement("""<p>  Foo    </p>""")) should equal("Foo")


  @Test
  def testImplyUrlAHref(): Unit =
    subject.implyURL(parseElement("""<a href="foo"/>""")) should contain(URI.create("https://base.com/foo"))

  @Test
  def testImplyUrlAreaHref(): Unit =
    subject.implyURL(parseElement("""<area href="foo"/>""")) should contain(URI.create("https://base.com/foo"))

  @Test
  def testImplyUrlAreaHrefWithoutBaseURI(): Unit =
    subject.implyURL(parseElement("""<area href="foo"/>"""))(using None) should contain(URI.create("foo"))

  @Test
  def testImplyUrlSingleNestedAHref(): Unit =
    subject.implyURL(parseElement(
      """
        <div>
          <a href="foo"/>
        </div>
      """)) should contain(URI.create("https://base.com/foo"))

  @Test
  def testImplyUrlMultipleNestedAHref(): Unit =
    subject.implyURL(parseElement(
      """
        <div>
          <a href="foo"/>
          <a href="foo2"/>
        </div>
      """)) shouldBe empty

  @Test
  def testImplyUrlSingleNestedAHrefWithMicroformat(): Unit =
    subject.implyURL(parseElement(
      """
        <div>
          <a href="foo" class="h-foo"/>
        </div>
      """)) shouldBe empty

  @Test
  def testImplyUrlGrandChild(): Unit =
    subject.implyURL(parseElement(
      """
        <div>
          <div>
            <a href="foo"/>
          </div>
        </div>
      """)) should contain(URI.create("https://base.com/foo"))

  @Test
  def testImplyUrlGrandChildMicroformat(): Unit =
    subject.implyURL(parseElement(
      """
        <div>
          <div class="h-baz">
            <a href="foo"/>
          </div>
        </div>
      """)) shouldBe empty

  @Test
  def testImplyUrlInvalidUrl(): Unit =
    subject.implyURL(parseElement("""<a href="https://en.wikipedia.org/wiki/David%20&quot;Honeyboy&quot;%20Edwards"/>""")) shouldBe empty

  @Test
  def testImplyPhotoImg(): Unit =
    subject.implyPhoto(parseElement("""<img src="/foo.png" alt="lol"/>""")) should contain(
      ImpliedPhoto(URI.create("https://base.com/foo.png"), Some("lol"))
    )

  @Test
  def testImplyPhotoImgNoAlt(): Unit =
    subject.implyPhoto(parseElement("""<img src="/foo.png"/>""")) should contain(
      ImpliedPhoto(URI.create("https://base.com/foo.png"), None)
    )

  @Test
  def testImplyPhotoData(): Unit =
    subject.implyPhoto(parseElement("""<object data="foo.png"/>""")) should contain(
      ImpliedPhoto(URI.create("https://base.com/foo.png"), None)
    )

  @Test
  def testImplyPhotoNestedImg(): Unit =
    subject.implyPhoto(parseElement("""<div><img src="/foo.png" alt="lol"/></div>""")) should contain(
      ImpliedPhoto(URI.create("https://base.com/foo.png"), Some("lol"))
    )

  @Test
  def testImplyPhotoDoublyNestedImg(): Unit =
    subject.implyPhoto(parseElement("""<div><div><img src="/foo.png" alt="lol"/></div></div>""")) should contain(
      ImpliedPhoto(URI.create("https://base.com/foo.png"), Some("lol"))
    )

  @Test
  def testImplyNestedPhotoData(): Unit =
    subject.implyPhoto(parseElement("""<div><object data="foo.png"/></div>""")) should contain(
      ImpliedPhoto(URI.create("https://base.com/foo.png"), None)
    )

  @Test
  def testDoublyImplyNestedPhotoData(): Unit =
    subject.implyPhoto(parseElement("""<div><div><object data="foo.png"/></div></div>""")) should contain(
      ImpliedPhoto(URI.create("https://base.com/foo.png"), None)
    )

