package digero.microformats

import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import java.net.URI
import scala.compiletime.uninitialized

class PropertiesTest extends HTMLParser:
  var subject: Parser = uninitialized
  given baseURI: Option[URI] = Some(URI.create("https://base.com"))
  given rootLang: Option[String] = Some("en")

  @BeforeEach def setUp(): Unit = subject = Parser()

  // parse_p-
  @Test def testParse_pValuePattern(): Unit =
    subject.`parse_p-`(parseElement("""<p><span class="value">lol</span>Some</p""")) should equal("lol")

  @Test def testParse_pAbbr(): Unit =
    subject.`parse_p-`(parseElement("""<abbr title="lol">content</abbr>""")) should equal("lol")

  @Test def testParse_pLink(): Unit =
    subject.`parse_p-`(parseDocument("""<link title="lol"/>""").selectFirst("link")) should equal("lol")

  @Test def testParse_pData(): Unit =
    subject.`parse_p-`(parseElement("""<data value="lol">content</data>""")) should equal("lol")

  @Test def testParse_pImg(): Unit =
    subject.`parse_p-`(parseElement("""<img src="lol.png" alt="lol"/>""")) should equal("lol")

  @Test def testParse_pArea(): Unit =
    subject.`parse_p-`(parseElement("""<area alt="lol"/>""")) should equal("lol")

  @Test def testParse_pFallBack(): Unit =
    subject.`parse_p-`(parseElement("<p>Some</p")) should equal("Some")

  // parse_u-
  @Test def testParse_uHref(): Unit =
    subject.`parse_u-`(parseElement("""<a href="https://bar.com">Link<a/>""")) should equal(StringValue("https://bar.com"))

  @Test def testParse_uImg(): Unit =
    subject.`parse_u-`(parseElement("""<img src="https://bar.com">Image<img/>""")) should equal(ImpliedPhoto("https://bar.com"))

  @Test def testParse_uData(): Unit =
    subject.`parse_u-`(parseElement("""<data value="https://bar.com">Data<data/>""")) should equal(StringValue("https://bar.com"))

  @Test def testParse_uValuePattern(): Unit =
    subject.`parse_u-`(parseElement("""<p><span class="value">lol</span>Some</p""")) should equal(StringValue("https://base.com/lol"))

  @Test def testParse_uFallback(): Unit =
    subject.`parse_u-`(parseElement("""<p>lol</p""")) should equal(StringValue("https://base.com/lol"))

  // parse_e-
  @Test def testParse_e(): Unit =
    subject.`parse_e-`(parseElement("""<p> <span>lol</span> </p""")) should equal(
      Embedded(
        value="lol",
        html="""<span>lol</span>""",
        lang=Some("en")
      )
    )

  @Test def testParse_eUsesLang(): Unit =
    subject.`parse_e-`(parseElement("""<p lang="fr"> <span>lol</span> </p""")) should equal(
      Embedded(
        value="lol",
        html="<span>lol</span>",
        lang=Some("fr")
      )
    )

  @Test def testParse_eResolvesUrls(): Unit =
    subject.`parse_e-`(parseElement("""<p> <a href="/foo">lol</a> </p"""))(using baseURI, None) should equal(
      Embedded(
        value="lol",
        html="""<a href="https://base.com/foo">lol</a>""",
        lang=None
      )
    )

