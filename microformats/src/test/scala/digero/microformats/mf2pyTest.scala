package digero.microformats

import digero.test.Fixture
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

import java.net.URI
import scala.annotation.nowarn
import scala.compiletime.uninitialized

// Tests ported from
// https://github.com/microformats/mf2py/blob/main/test/test_parser.py
class mf2pyTest extends Fixture:
  private var subject: Parser = uninitialized
  given baseURI: Option[URI] = None

  @BeforeEach def setup(): Unit = subject = Parser()

  @Test def test_simple_parse(): Unit =
    val result = subject.parse(fixture("simple_person_reference.html"))
    result(0).properties should contain only("name" -> Seq(StringValue("Frances Berriman")))

  @Test def test_simple_person_reference_same_element(): Unit =
    val result = subject.parse(fixture("simple_person_reference_same_element.html"))
    result(0).properties should contain only ("name" -> Seq(StringValue("Frances Berriman")))

  @Test def test_person_with_url(): Unit =
    val result = subject.parse(fixture("person_with_url.html"))
    val item = result(0)

    item.properties should contain only (
      "name" -> Seq(StringValue("Tom Morris")),
      "url" -> Seq(StringValue("http://tommorris.org/"))
    )

  @Test def test_vcp(): Unit =
    val result = subject.parse(fixture("value_class_person.html"))
    result(0)("tel") should contain only StringValue("+44 1234 567890")

  @Test def test_multiple_root_classnames(): Unit =
    val result = subject.parse(fixture("nested_multiple_classnames.html"))
    // order does not matter
    result.items should have length 1
    result(0).`type` should contain only("h-entry", "h-as-note")

  @Test def test_property_nested_microformat(): Unit =
    val result = subject.parse(fixture("nested_multiple_classnames.html"))
    result.items should have length 1
    val item = result(0)
    item.properties.keySet should contain("author")

    item("author") match
      case Seq(NestedMicroformat(microformat)) =>
        microformat("name") should contain only StringValue("Tom Morris")
        microformat("adr") match
          case Seq(NestedMicroformat(adr)) =>
            adr("city") should contain only StringValue("London")

    item("reviewer") match
      case Seq(NestedMicroformat(microformat)) =>
        microformat("name") should contain only StringValue("Tom Morris")


  @Test def test_plain_child_microformat(): Unit =
    subject.parse(fixture("nested_multiple_classnames.html")) match
      case Microformats(Seq(microformat), _) => microformat match
          case Microformat(_, _, _, _, _, _, Seq(child)) =>
            child("name") should contain only StringValue("Some Citation")
          case _ => fail()

  @Test def test_datetime_parsing(): Unit =
    val result = subject.parse(fixture("datetimes.html"))
    val item = result(0)
    item("start") should contain only StringValue("2014-01-01T12:00:00+0000")
    item("end") should contain only StringValue("3014-01-01T18:00:00+0000")
    item("duration") should contain only StringValue("P1000Y")
    item("updated") should equal(Seq(
      StringValue("2011-08-26T00:01:21+0000"),
      StringValue("2011-08-26T00:01:21+0000")
    ))

  @Test def test_datetime_vcp_parsing(): Unit =
    val result = subject.parse(fixture("datetimes.html"))
    result.items should have length 16

    result(1)("published") should contain only StringValue("3014-01-01 01:21Z")
    result(2)("updated") should contain only StringValue("2014-03-11 09:55")
    result(3)("published") should contain only StringValue("2014-01-30 15:28")
    result(4)("published") should contain only StringValue("9999-01-14T11:52+0800")
    result(5)("published") should contain only StringValue("2014-06-01 12:30-0600")
    result(8)("start") should contain only StringValue("2014-06-01 12:30-0600")
    result(9)("start") should contain only StringValue("2014-06-01 12:30-0600")

    result(10)("start") should equal(Seq(
      StringValue("2014-06-01 00:30-0600"), StringValue("2014-06-01 00:30-0600")
    ))
    // should use default date from previous
    result(10)("end") should equal(Seq(
      StringValue("2014-06-01 12:15"), StringValue("2014-06-01 12:15")
    ))
    result(11)("start") should contain only StringValue("2016-03-02 00:30-0600")
    result(12)("start") should contain only StringValue("2014-06-01 12:30-600")
    result(13)("start") should contain only StringValue("2014-06-01 12:30+600")
    result(14)("start") should contain only StringValue("2014-06-01 12:30Z")
    result(15)("start") should contain only StringValue("2014-06-01 12:30-600")

  @Test def test_dt_end_implied_date(): Unit =
    val result = subject.parse(fixture("datetimes.html"))
    val event_wo_tz = result(6)
    event_wo_tz("start") should contain only StringValue("2014-05-21 18:30")
    event_wo_tz("end") should contain only StringValue("2014-05-21 19:30")

    val event_w_tz = result(7)
    event_w_tz("start") should contain only StringValue("2014-06-01 12:30-0600")
    event_w_tz("end") should contain only StringValue("2014-06-01 19:30-0600")

  @Test @nowarn def test_embedded_parsing(): Unit =
    val result = subject.parse(fixture("embedded.html"))
    val content = result(0)("content")
    content match
      case Seq(Embedded(html, value, _)) =>
        html should equal("<p>Blah blah blah blah blah.</p>\n<p>Blah.</p>\n<p>Blah blah blah.</p>")
        value should equal("Blah blah blah blah blah.\n\nBlah.\n\nBlah blah blah.")

  @Test @nowarn def test_hoisting_nested_hcard(): Unit =
    subject.parse(fixture("nested_hcards.html")) match
      case Microformats(Seq(microformat), _) => microformat match
        case Microformat(_, Seq("h-entry"), properties, None, _, _, Seq()) =>
          properties("author") match
            case Seq(NestedMicroformat(Microformat(_, Seq("h-card"), cardProperties, Some("KP1"), _, _, Seq()))) =>
              cardProperties should contain only("name" -> Seq(StringValue("KP1")))

          properties("in-reply-to") match
            case Seq(NestedMicroformat(Microformat(_, Seq("h-cite"), citeProperties, Some("KP"), _, _, Seq()))) =>
              citeProperties should contain only ("name" -> Seq(StringValue("KP")))

  @Test def test_html_tag_class(): Unit =
    subject.parse(fixture("hfeed_on_html_tag.html")) match
      case Microformats(Seq(Microformat(_, Seq("h-feed"), _, None, _, _, Seq(
        Microformat(_, _, childProp1, _, _, _, Seq()),
        Microformat(_, _, childProp2, _, _, _, Seq()),
      ))), _) =>
        childProp1("name") should contain only StringValue("entry1")
        childProp2("name") should contain only StringValue("entry2")


  @Test def test_string_strip(): Unit =
    val result = subject.parse(fixture("string_stripping.html"))
    result(0)("name") should contain only StringValue("Tom Morris")

  @Test def test_template_parse(): Unit =
    val result = subject.parse(fixture("template_tag.html"))
    result.items shouldBe empty

  @Test def test_template_tag_inside_e_value(): Unit =
    subject.parse(fixture("template_tag_inside_e_value.html")) match
      case Microformats(Seq(Microformat(_, _, properties, _, _, _, Seq())), _) =>
        properties("content") match
          case Seq(Embedded(html, value,_)) =>
            html should equal("This is a Test with a <code>template</code> tag after this:")
            value should equal("This is a Test with a template tag after this:")

  @Test def test_ordering_dedup(): Unit =
    val result = subject.parse(fixture("ordering_dedup.html"))
    val item = result(0)

    item.`type` should contain inOrderOnly("h-entry", "h-feed", "h-product", "h-x-test")
    item("url") should contain inOrderOnly(
      StringValue("example.com"), StringValue("example.com/2")
    )
    item("name") should contain inOrderOnly(
      StringValue("name"), StringValue("URL name")
    )
    result.rel.urls(URI.create("example.com/rel")).rels should contain inOrderOnly(
      "author", "bookmark", "me"
    )
    result.rel.urls(URI.create("example.com/lang")).hrefLang should contain("de")

  // test that only classes with letters and possibly numbers in the vendor prefix part are used
  @Test def test_class_names_format(): Unit =
    val result = subject.parse(fixture("class_names_format.html"))
    val item = result(0)

    item.`type` should contain inOrderOnly("h-feed", "h-p3k-entry", "h-x-test")
    item.properties.keySet should contain("url")
    item.properties.keySet should contain("p3k-url")

    item.properties.keySet shouldNot contain("Url")
    item.properties.keySet shouldNot contain("-url")
    item.properties.keySet shouldNot contain("url-")


  @Test def test_area_uparsing(): Unit =
    subject.parse(fixture("area.html")) match
      case Microformats(Seq(Microformat(_, _, properties, _, _, _, Seq())), _) =>
        properties("url") should contain only StringValue("http://suda.co.uk")
        properties("name") should contain only StringValue("Brian Suda")
        // not official in the microformats spec
        // assert "shape" in result["items"][0]
        // assert "coords" in result["items"][0]


  @Test @nowarn def test_src_equiv(): Unit =
    val result = subject.parse(fixture("test_src_equiv.html"))
    for
      item <- result.items
    do
      item.properties.keySet should contain("x-example")
      item("x-example") match
        case Seq(ImpliedPhoto(url, _)) => url.toString should equal("http://example.org/")
        case Seq(StringValue(s)) => s should equal("http://example.org/")

  @Test def test_rels(): Unit =
    val result = subject.parse(fixture("rel.html"))
    result.rel.rels should contain allOf (
      "in-reply-to" -> Seq(URI.create("http://example.com/1"), URI.create("http://example.com/2")),
      "author" -> Seq(URI.create("http://example.com/a"), URI.create("http://example.com/b")),
      "alternate" -> Seq(URI.create("http://example.com/fr")),
      "home" -> Seq(URI.create("http://example.com/fr")),
    )

    result.rel.urls should contain allOf (
      URI.create("http://example.com/1") -> RelEntry(text = "post 1", rels = Seq("in-reply-to")),
      URI.create("http://example.com/2") -> RelEntry(text = "post 2", rels = Seq("in-reply-to")),
      URI.create("http://example.com/a") -> RelEntry(text = "author a", rels = Seq("author")),
      URI.create("http://example.com/b") -> RelEntry(text = "author b", rels = Seq("author")),
      URI.create("http://example.com/fr") -> RelEntry(
        text = "French mobile homepage",
        media = Some("handheld"),
        hrefLang = Some("fr"),
        rels = Seq("alternate", "home")
      )
    )

  @Test def test_alternates(): Unit =
    val result = subject.parse(fixture("rel.html"))
    result.rel.alternates should contain only Alternate(
      rel = "home",
      url = URI.create("http://example.com/fr"),
      media = Some("handheld"),
      text = "French mobile homepage",
      hrefLang = Some("fr")
    )


  @Test def test_enclosures(): Unit =
    val result = subject.parse(fixture("rel_enclosure.html"))
    result.rel.rels should contain (
      "enclosure" -> Seq(URI.create("http://example.com/movie.mp4"))
    )
    result.rel.urls should contain (
      URI.create("http://example.com/movie.mp4") -> RelEntry(
        rels = Seq("enclosure"),
        text = "my movie",
        `type` = Some("video/mpeg")
      )
    )

  @Test def test_empty_href(): Unit =
    val result = subject.parse(fixture("hcard_with_empty_url.html"))(using Some(URI.create("http://foo.com")))
    for
      hcard <- result.items
    do
      hcard("url") should contain only StringValue("http://foo.com")

  @Test def test_link_with_u_url(): Unit =
    subject.parse(fixture("link_with_u-url.html"))(using Some(URI.create("http://foo.com"))) match
      case Microformats(Seq(Microformat(None, Seq("h-card"), properties, None, _, _, Seq())), _) =>
        properties("name") should contain only StringValue("")
        properties("url") should contain only StringValue("http://foo.com/")

  @Test def test_broken_url(): Unit =
    an [IllegalArgumentException] should be thrownBy
      subject.parse(fixture("broken_url.html"))(using Some(URI.create("http://foo.com")))
    // assert result["items"][0]["properties"]["relative"][0] == "http://example.com/foo.html"
    // assert result["items"][0]["properties"]["url"][0] == "http://www.[w3.org/"
    // assert result["items"][0]["properties"]["photo"][0] == "http://www.w3].org/20[08/site/images/logo-w3c-mobile-lg"

  // When parsing h-* e-* properties, we should fold `{"value":..., "html":...}`
  // into the parsed microformat object, instead of nesting it under an
  // unnecessary second layer of "value"
  @Test def test_complex_e_content(): Unit =
    subject.parse(fixture("complex_e_content.html")) match
      case Microformats(Seq(Microformat(_, Seq("h-entry"), properties, None, None, None, Seq())), _) =>
        properties("content") match
          case Seq(NestedMicroformat(Microformat(None, Seq("h-card"), childProps, value, html, None, Seq()))) =>
            childProps should have size 1
            childProps("name") should contain only StringValue("Hello")

            value should contain("Hello")
            html should contain("<p>Hello</p>")

  // When parsing e-* properties, make relative URLs absolute.
  @Test def test_relative_url_in_e(): Unit =
    val result = subject.parse(fixture("relative_url_in_e.html"))
    val item = result(0)
    val content = item("content")

    content match
      case Seq(Embedded(html, _, _)) =>
        html should equal(
          """
          <p><a href="http://example.com/cat.html">Cat <img src="http://example.com/cat.jpg"></a></p>
          """.strip)

  // When parsing nested microformats, check that value is the value of
  // the simple property element
  @Test @nowarn def test_nested_values(): Unit =
    val result = subject.parse(fixture("nested_values.html"))
    val entry = result(0)

    entry("author") match
      case Seq(NestedMicroformat(microformat)) =>
        microformat match
          case Microformat(None, Seq("h-card"), properties, Some("Kyle"), _, _, Seq()) =>
            properties("name") should contain only StringValue("Kyle")
            properties("url") should contain only StringValue("http://about.me/kyle")

    entry("like-of") match
      case Seq(NestedMicroformat(microformat)) =>
        microformat match
          case Microformat(None, Seq("h-cite"), properties, Some("http://example.com/foobar"), _, _, Seq()) =>
            properties("name") should contain only StringValue("foobar")
            properties("url") should contain only StringValue("http://example.com/foobar")

    entry.children match
      case Seq(microformat) => microformat match
          case Microformat(None, Seq("h-card"), properties, None, _, _, Seq()) =>
            properties("name") should contain only StringValue("George")
            properties("url") should contain only StringValue("http://people.com/george")

  @Test def test_implied_name(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_properties.html"))
    for i <- 0 until 7
    do result(i)("name") should contain only StringValue("Tom Morris")

  @Test def test_implied_photo(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_photo.html"))

    for i <- 0 until 12
    do
      val photos = result(i)("photo")
      photos should have length 1
      photos.head should equal(ImpliedPhoto("http://example.com/photo.jpg"))

    // tests for no photo
    for i <- 12 until 23
    do result(i).properties.keySet shouldNot contain("photo")

  @Test def test_implied_photo_relative_url(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_photo_relative_url.html"))
    result(0)("photo") should contain only ImpliedPhoto("http://example.com/jane-img.jpeg", Some("Jane Doe"))
    result(1)("photo") should contain only ImpliedPhoto("http://example.com/jane-object.jpeg")


  @Test def test_implied_url(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_url.html"))

    for i <- 0 until 12
    do
      result(i)("url") should contain only StringValue("http://example.com")

    // tests for no url
    for i <- 12 until 23
    do result(i).properties.keySet shouldNot contain("url")

  @Test def test_stop_implied_url(): Unit =
    val result = subject.parse(fixture("implied_properties/stop_implied_url.html"))
    for i <- 0 until 6
    do result(i).properties.keySet shouldNot contain("url")

    for i <- 6 until 10
    do result(i)("url") should contain only StringValue("http://example.com/")


  @Test def test_implied_nested_photo(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_properties.html"))(using Some(URI.create("http://bar.org")))

    result(2)("photo") should contain only ImpliedPhoto("http://tommorris.org/photo.png", Some(""))
    result(3)("photo") should contain only ImpliedPhoto("http://tommorris.org/photo.png")
    result(4)("photo") should contain only ImpliedPhoto("http://tommorris.org/photo.png", Some("Tom Morris"))
    result(6)("photo") should contain only ImpliedPhoto("http://bar.org")

  @Test def test_implied_nested_photo_alt_name(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_properties.html"))
    result(3)("name") should contain only StringValue("Tom Morris")

  @Test def test_implied_image(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_properties.html"))
    result(4)("photo") should contain only
      ImpliedPhoto("http://tommorris.org/photo.png", Some("Tom Morris"))
    result(4)("name") should contain only StringValue("Tom Morris")

  @Test @nowarn def test_implied_name_empty_alt(): Unit =
    // An empty alt text should not prevent us from including other
    //  children in the implied name.
    val result = subject.parse(fixture("implied_properties/implied_name_empty_alt.html"))
    val hcard = result(0)
    hcard match
      case Microformat(None, Seq("h-card"), properties, None, None, None, Seq()) =>
        properties("name") should contain only StringValue("@kylewmahan")
        properties("url") should contain only StringValue("https://twitter.com/kylewmahan")
        properties("photo") should contain only ImpliedPhoto("https://example.org/test.jpg", Some(""))

  @Test def test_relative_datetime(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_relative_datetimes.html"))
    result(0)("updated") should contain only StringValue("2015-01-02 05:06")

  @Test def test_stop_implied_name_nested_h(): Unit =
    val result = subject.parse(fixture("implied_properties/stop_implied_name_nested_h.html"))
    result(0).properties.keySet shouldNot contain("name")

  @Test def test_stop_implied_name_e_content(): Unit =
    val result = subject.parse(fixture("implied_properties/stop_implied_name_e_content.html"))
    result(0).properties.keySet shouldNot contain("name")

  @Test def test_stop_implied_name_p_content(): Unit =
    val result = subject.parse(fixture("implied_properties/stop_implied_name_p_content.html"))
    result(0).properties.keySet shouldNot contain("name")

  @Test def test_implied_properties_silo_pub(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_properties_silo_pub.html"))
    result(0).properties.keySet shouldNot contain("name")

  @Test def test_simple_person_reference_implied(): Unit =
    val result = subject.parse(fixture("implied_properties/simple_person_reference_implied.html"))
    result(0)("name") should contain only StringValue("Frances Berriman")

  @Test @nowarn def test_implied_name_alt(): Unit =
    val result = subject.parse(fixture("implied_properties/implied_name_alt.html"))
    result(0).children(0) match
      case Microformat(None, Seq("h-card"), properties, _, _, _, Seq()) =>
        properties("name") should contain only StringValue("Avatar of Stephen")
        properties("photo") should contain only ImpliedPhoto("avatar.jpg", Some("Avatar of"))

  @Test @nowarn def test_value_name_whitespace(): Unit =
    val result = subject.parse(fixture("value_name_whitespace.html"))
    for i <- 0 until 3
    do
      result(i)("name") should contain only StringValue("Hello World")
      result(i)("content").head match
        case Embedded(_, "Hello World", _) =>

    for i <- 3 until 7
    do
      result(i)("name") should contain only StringValue("Hello\nWorld")
      result(i)("content").head match
          case Embedded(_, "Hello\nWorld", _) =>

    // <p>Hello</p><p>World</p>
    result(7)("name") should contain only StringValue("Hello\n\nWorld")
    result(7)("content").head match
        case Embedded(_, "Hello\n\nWorld", _) =>

    // <pre>
    result(8)("name") should contain only StringValue("One\nTwo\nThree")
    result(8)("content").head match
      case Embedded(_, "One\nTwo\nThree", _) =>

    // <p>One</p>
    // <p>Two</p>
    // <p>Three</p>
    result(9)("name") should contain only StringValue("One\n\nTwo\n\nThree")
    result(9)("content").head match
      case Embedded(_, "One\n\nTwo\n\nThree", _) =>

    // Hello World
    // <pre>
    //  one
    //  two
    //  three
    // </pre>
    result(10)("name") should contain only StringValue("Hello World      one\n      two\n      three")
    result(10)("content").head match
      case Embedded(_, "Hello World      one\n      two\n      three", _) =>
    // TODO should be  "Hello World      one\n      two\n      three\n    "

    // <span class="p-name">Correct name</span><span class="p-summary">Correct summary</span>
    result(11)("name") should contain only StringValue("Correct name")
    result(11)("content").head match
      case Embedded(_, "Correct name Correct summary", _) =>

  @Test def test_whitespace_with_tags_inside_property(): Unit =
    // Whitespace should only be trimmed at the ends of textContent, not inside.
    val result = subject.parse(fixture("tag_whitespace_inside_p_value.html"))
    result(0)("name") should contain only StringValue("foo bar")

  @Test def test_plaintext_p_whitespace(): Unit =
    // Whitespace should only be trimmed at the ends of textContent, not inside.
    val result = subject.parse(fixture("plaintext_p_whitespace.html"))
    result(0)("content") match
      case Seq(Embedded(_, "foo\nbar baz", _)) =>

    result(1)("content") match
      case Seq(Embedded(_, "foo\nbar baz", _)) =>

    result(2)("content") match
      case Seq(Embedded(_, "foo bar\nbaz", _)) =>

  @Test @nowarn def test_plaintext_img_whitespace(): Unit =
    val result = subject.parse(fixture("plaintext_img_whitespace.html"))
    result(0)("content").head match
      case Embedded(_, "selfie At some tourist spot", _) =>

    result(1)("content").head match
      case Embedded(_, "At another tourist spot", _) =>

    result(2)("content").head match
      case Embedded(_, "https://example.com/photo.jpg At yet another tourist spot", _) =>

  @Test def test_photo_with_alt(): Unit =
    val result = subject.parse(fixture("img_with_alt.html"))
    // simple img with u -*
    result(0)("photo") should contain only ImpliedPhoto("/photo.jpg", None)
    result(1)("url") should contain only ImpliedPhoto("/photo.jpg", Some("alt text"))
    result(2)("in-reply-to") should contain only ImpliedPhoto("/photo.jpg", Some(""))

    // img with u-* and h-* example
    result(3)("in-reply-to") match
      case Seq(NestedMicroformat(Microformat(_, Seq("h-cite"), properties, _, _, _, _))) =>
        properties("photo") should contain only ImpliedPhoto("/photo.jpg", None)

    result(4)("in-reply-to") match
      case Seq(NestedMicroformat(Microformat(_, Seq("h-cite"), properties, _, _, _, _))) =>
        properties("photo") should contain only ImpliedPhoto("/photo.jpg", Some("alt text"))

    result(5)("in-reply-to") match
      case Seq(NestedMicroformat(Microformat(_, Seq("h-cite"), properties, _, _, _, _))) =>
        properties("photo") should contain only ImpliedPhoto("/photo.jpg", Some(""))

  @Test def test_photo_with_srcset(): Unit =
    val result = subject.parse(fixture("img_with_srcset.html"))
    result.items should have length 9
    // TODO implement srcset

  @Test def test_photo_with_srcset_with_base(): Unit =
    val result = subject.parse(fixture("img_with_srcset_with_base.html"))
    result.items should have length 1
    // TODO implement srcset

  @Test def test_parse_id(): Unit =
    subject.parse(fixture("parse_id.html")) match
      case Microformats(
      Seq(Microformat(Some("recentArticles"), _, properties, _, _, _,
      // children
      Seq(Microformat(Some("article"), _, _, _, _, _, _),
      Microformat(None /* id="" */ , _, _, _, _, _, _))
      )), _) => properties("author") match
        case Seq(NestedMicroformat(Microformat(Some("theAuthor"), _, _, _, _, _, _))) =>

  @Test @nowarn def test_all_u_cases(): Unit =
    // test variations of u- parsing and that relative urls are always resolved
    val result = subject.parse(fixture("u_all_cases.html"))
    val urls = result(0)("url")
    urls should have length 28
    for i <- 0 until 28
    do
      urls(i) match
        case StringValue(s) => s should equal("http://example.com/test")
        case ImpliedPhoto(url, _) => url.toString should equal("http://example.com/test")

  @Test def test_language(): Unit =
    subject.parse(fixture("language.html")) match
      case Microformats(Seq(
        Microformat(_, _, _, _, _, Some("it"), _),
        Microformat(_, _, properties1, _, _, Some("it"), _),
        Microformat(_, _, properties2, _, _, Some("sv"), _)), _) =>

          properties1("content") match
            case Seq(Embedded(_, _, Some("en")),
                     Embedded(_, _, Some("it"))) =>

          properties2("content") match
            case Seq(Embedded(_, _, Some("en")),
                     Embedded(_, _, Some("sv"))) =>