package digero.microformats

import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers._

class ValueClassPatternTest extends HTMLParser:

  @Test
  def testGetText(): Unit =
    ValueClassPattern.text(parseElement("""<p class="value">foo</p>""")) should contain("foo")

  @Test
  def testGetTextEmpty(): Unit =
    ValueClassPattern.text(parseElement("""<p>foo</p>""")) shouldBe empty

  @Test
  def testGetTextMultiple(): Unit =
    ValueClassPattern.text(parseElement(
      """
       <div>
          <p class="value">foo</p>
          <p>quux</p>
          <p class="value">bar</p>
        </div>
      """)) should contain("foobar")

  @Test
  def testGetTextValueTitle(): Unit =
    ValueClassPattern.text(parseElement("""<p class="value-title" title="title">foo</p>""")) should contain("title")

  @Test
  def testDatetimeImg(): Unit =
    ValueClassPattern.datetime(parseElement("""<img class="value" alt="3014-01-01" src="onethousandyears.png"/>""")) match
      case Some("3014-01-01", Some("3014-01-01")) =>
      case unmatched => fail(f"unmatched $unmatched")

  @Test
  def testDatetimeOnlyTime(): Unit =
    ValueClassPattern.datetime(parseElement("""<data class="value" value="01:21Z">1:21 am</data>""")) match
      case Some("01:21Z", None) =>
      case unmatched => fail(f"unmatched $unmatched")

  @Test
  def testDatetimeOnlyTimeWithDefaultDate(): Unit =
    ValueClassPattern.datetime(parseElement("""<time class="value">1:21 pm</time>"""), defaultDate = Some("2001-01-01")) match
      case Some("2001-01-01 13:21", Some("2001-01-01")) =>
      case unmatched => fail(f"unmatched $unmatched")

  @Test
  def testDatetimeCombined(): Unit =
    ValueClassPattern.datetime(parseElement(
      """
        <span>
          <time class="value">3:28pm</time> on <time class="value">2014-01-30</time>
        </span>
      """)) match
      case Some("2014-01-30 15:28", Some("2014-01-30")) =>
      case unmatched => fail(f"unmatched $unmatched")

  @Test
  def testDatetimeWithTimeZone(): Unit =
      ValueClassPattern.datetime(parseElement(
        """
          <span class="dt-published">
               <span class="value" title="June 1, 2014">2014-06-01</span>
               <span class="value" title="12:30">12:30<span style="display: none;">-06:00</span></span>
           </span>
        """)) match
      case Some("2014-06-01 12:30-0600", Some("2014-06-01")) =>
      case unmatched => fail(f"unmatched $unmatched")

  @Test
  def testDatetimeWithDaysPerYear(): Unit =
    ValueClassPattern.datetime(parseElement(
      """
        <span class="dt-start">
             <span class="value">2016-062</span>
             <span class="value">12:30AM</span>
             (UTC<span class="value">-06:00</span>)
        </span>
      """)) match
      case Some("2016-03-02 00:30-0600", Some("2016-062")) =>
      case unmatched => fail(f"unmatched $unmatched")