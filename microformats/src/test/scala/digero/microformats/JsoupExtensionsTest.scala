package digero.microformats

import digero.microformats.JsoupExtensions.*
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class JsoupExtensionsTest extends HTMLParser:

  @Test def testTextPreservingWhitespaceParagraphConversion(): Unit =
    parseElement("""<div><p>foo</p><img src="pic.png" alt="bar">baz</div>""").textPreservingWhitespace() should equal("foo\nbaz")

  @Test def testTextPreservingWhitespaceParagraphConversionLeadingSpace(): Unit =
    parseElement("""<div><p>foo</p> baz</div>""").textPreservingWhitespace() should equal("foo\nbaz")

  @Test def testTextPreservingWhitespacePre(): Unit =
    // TODO should be  "Hello World      one\n      two\n      three\n    "
    parseElement(
      """
        |<div">
        |  Hello World
        |  <pre>
        |      one
        |      two
        |      three
        |  </pre>
        | </div>
        |""".stripMargin).textPreservingWhitespace() should equal("Hello World      one\n      two\n      three\n")

  @Test def testTextPreservingWhitespaceBreakConversion(): Unit =
    parseElement("""<div>foo<br>baz</div>""").textPreservingWhitespace() should equal("foo\nbaz")

  @Test def testTextPreservingWhitespaceLeadingTrailing(): Unit =
    parseElement("<div> Foo \n Bar </div>").textPreservingWhitespace() should equal("Foo Bar")