package digero.microformats

import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*
import java.net.URI

class RelTest:
  
  @Test def test_rel_concat(): Unit =
    val r1 =Rel(rels=Map.empty, urls=Map(
      URI.create("/foo") -> RelEntry(
        rels = Seq("a", "b"),
        text = "text"
      ) 
    ))
    val r2 = Rel(rels = Map.empty, urls = Map(
      URI.create("/foo") -> RelEntry(
        rels = Seq("b", "c"),
        text = "text"
      )
    ))
    
    val result = r1 ++ r2
    result.urls should contain only(
      URI.create("/foo") -> RelEntry(
        rels = Seq("a", "b", "c"),
        text = "text"
      ) 
    )
        
    
