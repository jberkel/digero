package digero.microformats

import digero.microformats.TextVisitor.NewlineElements
import org.jsoup.internal.StringUtil
import org.jsoup.nodes.{Document, Element, Node, TextNode}
import org.jsoup.select.{Elements, NodeTraversor, NodeVisitor}
import org.jsoup.select.NodeFilter.FilterResult

import java.net.URI
import java.lang.StringBuilder as JavaStringBuilder
import scala.collection.WithFilter
import scala.jdk.CollectionConverters.*

object JsoupExtensions:
  private val UnwantedTagNames = Seq("script", "style", "template")

  extension (document: Document)

    def lang: Option[String] =
      Option(document.firstElementChild()).flatMap(_.attrOpt("lang"))

    def baseURI: Option[URI] =
      document.location() match
        case "" => document.selectAttribute("base[href]", "href").map(URI.create)
        case location => Some(URI.create(location))

  extension (element: Element)
    def classes: Seq[String] = element.classNames().iterator().asScala.toSeq
    def attrOpt(key: String): Option[String] = if element.hasAttr(key) then Some(element.attr(key)) else None
    def isRoot: Boolean = Microformats.isRoot(element.classes)
    def selectFirstOption(query: String): Option[Element] = Option(element.selectFirst(query))
    def selectAttribute(selector: String, attrKey: String): Option[String] =
      element
        .select(selector)
        .headOption
        .map(_.attr(attrKey))

    def filterUnwanted(): Element =
      element.filter { (head, _) => head match
        case e: Element if UnwantedTagNames.contains(e.tagName()) => FilterResult.REMOVE
        case _ => FilterResult.CONTINUE
      }

    def textPreservingWhitespace(): String =
      val builder = StringUtil.borrowBuilder
      NodeTraversor.traverse(TextVisitor(builder), element)
      StringUtil.releaseBuilder(builder)
        .split("\n")
        // remove <space> if it is first and last or if it is preceded by a <space> or <p> open/close
        .map(line => if raw"^\s\S.*".r.matches(line) then line.substring(1) else line)
        .map(_.stripTrailing())
        .mkString("\n")

  extension (elements: Elements)
    def foreach[U](f: Element => U): Unit = elements.asScala.foreach(f)
    def flatMap[B](f: Element => IterableOnce[B]): Seq[B] = elements.asScala.toSeq.flatMap(f)
    def map[B](f: Element => B): Seq[B] = elements.asScala.toSeq.map(f)
    def withFilter(f: Element => Boolean): WithFilter[Element, Seq] = elements.asScala.toSeq.withFilter(f)

    private def headOption: Option[Element] =
      if elements.size() == 0 then None else Some(elements.get(0))


private class TextVisitor(val builder: JavaStringBuilder) extends NodeVisitor:
  import digero.microformats.TextVisitor.{appendNormalisedWhitespace, stripTrailing}

  def head(node: Node, depth: Int): Unit =
    node match
      case textNode: TextNode =>
        textNode.parentNode() match
          case element: Element if element.tagName() == "pre" =>
            builder.stripTrailing().append(textNode.getWholeText)
          case _ =>
            builder.appendNormalisedWhitespace(textNode.getWholeText)

      case element: Element if NewlineElements.contains(element.tagName()) && !builder.isEmpty =>
        builder.append("\n")
      case _ =>

  override def tail(node: Node, depth: Int): Unit =
    node match
      case element: Element if element.tagName() == "p" => builder.append("\n")
      case _ =>

private object TextVisitor:
  private val NewlineElements = Set("br", "p")

  extension (builder: JavaStringBuilder)
    def stripTrailing(): JavaStringBuilder =
      while lastCharIsWhitespace
      do builder.deleteCharAt(builder.length - 1)
      builder

    def appendNormalisedWhitespace(text: String): Unit =
      StringUtil.appendNormalisedWhitespace(builder, text, lastCharIsWhitespace)

    private def lastCharIsWhitespace: Boolean = !builder.isEmpty && builder.charAt(builder.length - 1) == ' '
