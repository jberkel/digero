package digero.microformats

import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import digero.microformats.JsoupExtensions.*

// Functions to parse the properties of elements according to the value class pattern
// https://microformats.org/wiki/value-class-pattern

// The value class pattern only applies to properties which are simple strings, enumerated values, telephone numbers, and datetimes.
// The value class pattern does not affect parsing of properties of type email, URL, URI, UID.
object ValueClassPattern:
  // https://microformats.org/wiki/value-class-pattern#Basic_Parsing
  def text(element: Element): Option[String] =
    require(element != null)
    val result = get_vcp_children(element).map(get_vcp_value).mkString("").strip()
    if result.nonEmpty then
      Some(result)
    else
      None

  // https://microformats.org/wiki/value-class-pattern#Date_and_time_values
  // Return a tuple (string string): (datetime, date)
  def datetime(element: Element, defaultDate: Option[String] = None): Option[DateTime] = {
    val children = get_vcp_children(element)
    if children.isEmpty then
      None
    else
      datetime(children, defaultDate)
  }

  def datetime(elements: Elements, defaultDate: Option[String]): Option[DateTime] =
    Dateparser.parse(getDateParts(elements), defaultDate)

  private def get_vcp_children(element: Element): Elements =
    if element == null then
      Elements()
    else
      element.select("*[class~=value], *[class~=value-title]")

  private def get_vcp_value(element: Element): String =
    if element.classes.contains("value-title") then
      element.attr("title")
    else
      element.text()

  private def getDateParts(elements: Elements): Seq[String] =
    for
      elt <- elements
      vcp_value = if elt.classes.contains("value-title") then elt.attrOpt("title") else None

      part = vcp_value.getOrElse(elt.tagName() match
        case "img" | "area"=> elt.attrOpt("alt").getOrElse(elt.text())
        case "data" => elt.attrOpt("value").getOrElse(elt.text())
        case "abbr" => elt.attrOpt("title").getOrElse(elt.text())
        case "del" | "ins" | "time" => elt.attrOpt("datetime").getOrElse(elt.text())
        case _ => elt.text())

      stripped = part.strip()
      text = stripped if stripped.nonEmpty
    yield
      text