package digero.microformats

import java.text.SimpleDateFormat
import java.util.regex.Matcher

type DateTime = (String, Option[String])

object Dateparser:
  private val DateDaysInYearRe = raw"\d{4}-\d{3}".r
  private val DateRe = raw"(\d{4}-\d{2}-\d{2})|(%s)".formatted(DateDaysInYearRe).r
  private val SecRe = raw"(:(?<second>\d{2})(\.\d+)?)".r
  private val RawtimeRe = raw"(?<hour>\d{1,2})(:(?<minute>\d{2})%s?)?".formatted(SecRe).r
  private val AM_PM_Re = raw"am|pm|a\.m\.|p\.m\.|AM|PM|A\.M\.|P\.M\.".r
  private val TimeZoneRe = raw"Z|[+-]\d{1,2}:?\d{2}?".r
  private val TimeRe = raw"(?<rawtime>%s)( ?(?<ampm>%s))?( ?(?<tz>%s))?".formatted(RawtimeRe, AM_PM_Re, TimeZoneRe).r
  private val DateTimeRe = raw"(?<date>%s)(?<separator>[T ])(?<time>%s)".formatted(DateRe, TimeRe).r

  def parse(parts: Seq[String], defaultDate: Option[String]=None): Option[DateTime] =
    fulldate(parts).orElse(parsePartial(parts, defaultDate))

  def parseFullDate(string: String): DateTime =
    val matcher = DateTimeRe.pattern.matcher(string)
    (
      normalizeDate(string, Some(matcher)),
      if matcher.matches() then Option(matcher.group("date")) else None
    )

  def parseTime(string: String, defaultDate: String): Option[DateTime] =
    if TimeRe.matches(string) then
      Some((normalizeDate(defaultDate + " " + string), Some(defaultDate)))
    else
      None

  private def normalizeDate(string: String, reuseMatcher: Option[Matcher] = None): String =
    val matcher = reuseMatcher.getOrElse(DateTimeRe.pattern.matcher(string))
    if !matcher.matches() then
      return string

    val date = matcher.group("date")
    val hours = matcher.group("hour")
    val minute = Option(matcher.group("minute")).getOrElse("00")
    val seconds = Option(matcher.group("second"))
    val ampm = Option(matcher.group("ampm"))
    val separator = matcher.group("separator")
    val timezone = Option(matcher.group("tz"))

    // convert ordinal date YYYY-DDD to YYYY-MM-DD
    val reformattedDate = convertDaysInYear(date)

    // 12 to 24 time conversion
    val reformattedHour = ampm.map(ampm =>
      (ampm.headOption.map(_.toLower), hours.toInt) match
        case (Some('a'), 12) => "00"
        case (Some('p'), hour) if hour < 12 => (hour + 12).toString
        case _ => hours
    )
    reformattedDate.getOrElse(date) + separator + reformattedHour.getOrElse(hours) + ":" + minute +
      seconds.map(s => f":$s").getOrElse("") +
      timezone.map(_.replace(":", "")).getOrElse("")

  private def parsePartial(parts: Seq[String], defaultDate: Option[String]=None): Option[DateTime] =
    val timePart = parts.find(TimeRe.matches)
    val datePart = parts.find(DateRe.matches).orElse(defaultDate)
    val timezone = parts.find(TimeZoneRe.matches).getOrElse("")

    val dateTimeValue = (datePart, timePart) match
      case (Some(dateTime), Some(time)) => "%s %s%s".formatted(dateTime, time, timezone)
      case _ => datePart.orElse(timePart).getOrElse("").concat(timezone)

    Some((normalizeDate(dateTimeValue), datePart))


  private def fulldate(parts: Seq[String]): Option[DateTime] =
    val result = for
      p <- parts
      matcher = DateTimeRe.pattern.matcher(p) if matcher.matches()
      date = Option(matcher.group("date"))
    yield (p, date, matcher) match
      case (p, date, matcher) =>
        (normalizeDate(p, Option(matcher)), date)

    result.headOption

  private def convertDaysInYear(date: String): Option[String] =
    if DateDaysInYearRe.matches(date) then
      try
        val parsedDate = SimpleDateFormat("y-D").parse(date)
        Option(SimpleDateFormat("yyyy-MM-dd").format(parsedDate))
      catch case _: java.text.ParseException => None
    else
      None
