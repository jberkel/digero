package digero.microformats

import java.net.URI
import scala.collection.SortedSet
import scala.util.matching.Regex

sealed trait PropertyValue

case class StringValue(value: String) extends PropertyValue
case class ImpliedPhoto(url: URI, alt: Option[String] /*, srcset: Map[String, String] */) extends PropertyValue
case class NestedMicroformat(microformat: Microformat) extends PropertyValue
case class Embedded(html: String, value: String, lang: Option[String]) extends PropertyValue

object PropertyValue:
  def apply(uri: URI): PropertyValue = StringValue(uri.toString)
  def apply(string: String): PropertyValue = StringValue(string)

object ImpliedPhoto:
  def apply(string: String, alt: Option[String]=None): ImpliedPhoto = ImpliedPhoto(URI.create(string), alt)

case class Microformats(items: Seq[Microformat], rel: Rel):
  def ++(other: Microformats): Microformats =
    Microformats(items ++ other.items, rel ++ other.rel)

  def apply(index: Int): Microformat = items.apply(index)

type Properties = Map[String, Seq[PropertyValue]]
type HtmlClasses = Seq[String]

case class Microformat(
  id: Option[String],         // element's id attribute
  `type`: Seq[String],        // array of unique microformat "h-*" type(s) on the element sorted alphabetically
  properties: Properties,     // properties of this microformat
  value: Option[String],      // only set for microformat in properties: first `p-name`, `u-url`, `e-value`
  html: Option[String],       // only set for nested `e-` microformats
  lang: Option[String],       // the item's language
  children: Seq[Microformat], // all contained microformats that are not properties
):
  def apply(name: String): Seq[PropertyValue] = properties.apply(name)
  def get(name: String): Option[PropertyValue] = properties.get(name).flatMap(_.headOption)

case class RelEntry(
  rels: Seq[String],
  text: String,
  media: Option[String] = None,
  hrefLang: Option[String] = None,
  `type`: Option[String] = None,
  `title`: Option[String] = None
):
  def ++(other: RelEntry): RelEntry =
    RelEntry(
      SortedSet(rels ++ other.rels*).toSeq,
      text, media, hrefLang, `type`, `title`
    )

case class Alternate(
   url: URI,
   text: String,
   rel: String,
   media: Option[String] = None,
   hrefLang: Option[String] = None,
   `type`: Option[String] = None,
   `title`: Option[String] = None
)

// "rels": {
//   "foo": ["http://foo.com/1", "http://foo.com/2"],
// },
// "rel-urls" {
//   "http://foo.com/1": {
//     "rels: ["foo"],
//     "text": "foo"
//   }
//   "http://foo.com/2":  { … }
// }
case class Rel(rels: Map[String, Seq[URI]] = Map.empty,
               urls: Map[URI, RelEntry] = Map.empty):
  def ++(other: Rel): Rel =
    Rel(
      rels = rels ++ other.rels.map {
        case (k, v) => (k, rels.getOrElse(k, Seq.empty) ++ v)
      },
      urls = urls ++ other.urls.map {
        case (k, v) => (k, urls.get(k).map(_ ++ v).getOrElse(v))
      }
    )

  def alternates: Seq[Alternate] =
    urls
      .filter((_, rel) => rel.rels.contains("alternate"))
      .map((uri, rel) =>
          Alternate(
            uri,
            text=rel.text,
            rel=rel.rels.filter(_ != "alternate").mkString(" "),
            media=rel.media,
            hrefLang=rel.hrefLang,
            `type`=rel.`type`,
            title=rel.title
          )
      ).toSeq

object Microformats:
  private val classes: Regex = """(p|e|u|dt|h)-((:?[a-z0-9]+-)?[a-z]+(:?-[a-z]+)*)$""".r
  private val roots: Regex = """h-(:?[a-z0-9]+-)?[a-z]+(:?-[a-z]+)*$""".r

  def roots(htmlClasses: HtmlClasses): Set[String] = htmlClasses.filter(roots.matches).toSet

  // detect classes that are valid names for mf2, sort in dictionary by prefix
  def classes(htmlClasses: HtmlClasses): Map[String, Seq[String]] =
    (for
      htmlClass <- htmlClasses
      matched <- classes.findFirstMatchIn(htmlClass)
      (microformatType, microformatName) = (matched.group(1), matched.group(2))
      prefixedName = if microformatType == "h" then "h-" + microformatName else microformatName
    yield
      (microformatType, prefixedName)
    ).groupMap(_._1)(_._2)


  def isRoot(classes: Seq[String]): Boolean = roots(classes).nonEmpty
