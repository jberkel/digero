package digero.microformats


import org.jsoup.nodes.{Document, Element, TextNode}
import digero.microformats.Parser.ParsingResult
import digero.microformats.JsoupExtensions.*

import java.net.URI
import scala.jdk.StreamConverters.*
import scala.util.Try


object Parser:
  case class ParsingResult(properties: Properties,
                           microformats: Seq[Microformat],
                           types: Set[String]):
    def ++(other: ParsingResult): ParsingResult =

      val mergedProperties = this.properties ++ other.properties.map {
        case (k, v) => (k, properties.getOrElse(k, Seq.empty)  ++ v)
      }

      ParsingResult(
        properties=mergedProperties,
        microformats=this.microformats ++ other.microformats,
        types=this.types ++ other.types
      )

    def implyName(impliedValue: => Option[PropertyValue]): Properties = imply("name", Set("p", "e", "h"), impliedValue)
    def implyPhoto(impliedValue: => Option[PropertyValue]): Properties = imply("photo", Set("u", "h"), impliedValue)
    def implyURL(impliedValue: => Option[PropertyValue]): Properties = imply("url", Set("u", "h"), impliedValue)

    private def imply(name: String, types: Set[String], impliedValue: => Option[PropertyValue]): Properties =
      if !properties.contains(name) && this.types.intersect(types).isEmpty then
        impliedValue.map(v => properties.updated(name, Seq(v))).getOrElse(properties)
      else
        properties


// http://microformats.org/wiki/microformats2-parsing
class Parser extends HTMLParser:
  // `defaultDate` exists to provide implementation for rules described
  //  in legacy value-class-pattern. basically, if you have two dt-
  // properties and one does not have the full date, it can use the
  // existing date as a template.
  // see value-class-pattern#microformats2_parsers on wiki.
  // see also the implied_relative_datetimes testcase.
  private var defaultDate: Option[String] = None

  def parse(html: String)(using baseURI: Option[URI]): Microformats =
    val doc = parseDocument(html)
    parse(doc)(using doc.baseURI.orElse(baseURI), doc.lang)

  def parse(document: Document)(using baseURI: Option[URI], rootLang: Option[String]): Microformats =
    // To parse a document for microformats, follow the HTML parsing rules and do the following:
    // - start with an empty JSON "items" array and "rels" & "rel-urls" hashes:

    // - parse the root element for class microformats, adding to the JSON items array accordingly
    val microformats = parse(document.asInstanceOf[Element])

    // - parse all hyperlink (`<a>` `<area>` `<link>`) elements for rel microformats, adding to the
    //   JSON rels & rel-urls hashes accordingly
    val rels = for
      element <- document.select("a[rel~=.+], area[rel~=.+], link[rel~=.+]")
      rel <- parseRel(element)
    yield rel

    // - return the resulting JSON
    Microformats(microformats, rels.foldLeft(Rel())(_ ++ _))

  def parse(element: Element)(using baseURI: Option[URI], rootLang: Option[String]): Seq[Microformat] =
    if element.tagName() == "template" then
      return Seq.empty

    // To parse an element for class microformats:
    // - parse element class for root class name(s) `h-*`
    val potentialMicroformats = Microformats.roots(element.classes)
    if potentialMicroformats.isEmpty then
      // - if none found, parse child elements for microformats (depth first, doc order)
      element.children().map(parse).foldLeft(Seq.empty)(_ ++ _)
    else
      Seq(handleMicroformat(potentialMicroformats.toSeq, element))


  // Handles a (possibly nested) microformat, i.e. h-*
  private def handleMicroformat(rootClassNames: Seq[String],
                                element: Element,
                                valuePropertyName: Option[String] = None,
                                simpleValue: Option[PropertyValue] = None)
                                (using baseURI: Option[URI], defaultRootLang: Option[String]): Microformat =
    // - start parsing a new microformat:
    //  -  create a new `{}` structure with:
    //       type: [array of unique microformat "h-*" type(s) on the element sorted alphabetically],
    //       properties: {}
    //       id: string value of element's id attribute, if present

    //  - parse child elements (document order) by:
    //     - parse a child element class for property class names `p-`, `u-`, `dt-`, `e-`
    //     - if such classes are found, it is a property element: add to current `properties {}` structure
    //     - parse a child element for microformats (recurse)
    //       - if that child element itself has a microformat (`h-`) and is a property element, add it into the array
    //          of values for that property as a `{}` structure, add to that `{}` structure:
    //           - `value:`
    //             -  if it's a `p-` property element, use the first p-name of the `h-` child
    //             -  else if it's an `e-` property element, re-use its `{}` structure with existing `value:` inside.
    //             -  else if it's a `u-` property element and the `h-` child has a `u-url`, use the first such `u-url`
    //             -  else use the parsed property value per `p-`, `u-`, `dt-` parsing respectively
    //       - else add found elements that are microformats to the "children" array
    //  - imply properties for the found microformat
    val rootLang: Option[String] = element.attrOpt("lang").orElse(defaultRootLang)
    defaultDate = None

    // parse for properties and children
    val merged = (for
      child <- element.children()
      result = parseProps(child)(using baseURI, rootLang)
    yield
      result
    ).foldLeft(ParsingResult(Map.empty, Seq.empty, Set.empty))(_ ++ _)

    // complex h-* objects can take their "value" from the
    // first explicit property ("name" for p-* or "url" for u-*)
    val simpleValueProperty: Option[PropertyValue] = simpleValue.orElse(valuePropertyName.flatMap(value =>
      merged.properties.get(value).flatMap(_.headOption)
    ))

    val value: Option[String] = simpleValueProperty match
      case Some(StringValue(s)) => Some(s)
      case Some(Embedded(_, value, _)) => Some(value)
      case _ => None

    // fold embedded html into microformat
    val html: Option[String] = simpleValue match
      case Some(Embedded(html, _, _)) => Some(html)
      case _ => None

    // add implied properties
    val properties =
       merged.implyName(Some(StringValue(implyName(element)))) ++
       merged.implyPhoto(implyPhoto(element)) ++
       merged.implyURL(implyURL(element).map(PropertyValue.apply))

    val id = element.attrOpt("id").flatMap(id => if id.strip().isEmpty then None else Some(id))

    Microformat(
      id = id,
      `type` = rootClassNames.sorted,
      value = value,
      html = html,
      properties = properties,
      lang = rootLang,
      children = merged.microformats
    )

  // Parse the properties from a single element
  private def parseProps(element: Element)
                        (using baseURI: Option[URI], rootLang: Option[String]): ParsingResult =
    val filteredClasses = Microformats.classes(element.classes)
    val rootClassNames = filteredClasses.getOrElse("h", Seq.empty)
    val isRootClass = rootClassNames.nonEmpty

    // Parse plaintext p-* properties.
    val p_propertyValues: Seq[(String, PropertyValue)] = for
      propName <- filteredClasses.getOrElse("p", Seq.empty)
      text = `parse_p-`(element)
    yield
      if isRootClass then
        (propName, NestedMicroformat(handleMicroformat(rootClassNames, element,
                                                       valuePropertyName=Some("name"),
                                                       simpleValue=Some(StringValue(text)))))
      else
        (propName, StringValue(text))

    // Parse URL u-* properties.
    val u_propertyValues: Seq[(String, PropertyValue)] = for
      propName <- filteredClasses.getOrElse("u", Seq.empty)
      value = `parse_u-`(element)
    yield
      if isRootClass then
        (propName, NestedMicroformat(handleMicroformat(rootClassNames, element,
                                                       valuePropertyName=Some("url"),
                                                       simpleValue=Some(value))))
      else
        (propName, value)

    // Parse datetime dt-* properties.
    val dt_propertyValues: Seq[(String, PropertyValue)] = for
      propName <- filteredClasses.getOrElse("dt", Seq.empty)
      datetime = `parse_dt-`(element)
    yield
      if isRootClass then
        (propName, NestedMicroformat(handleMicroformat(rootClassNames, element,
                                                       valuePropertyName=None,
                                                       simpleValue=Some(StringValue(datetime)))))
      else
        (propName, StringValue(datetime))

    // Parse embedded markup e-* properties.
    val e_propertyValues: Seq[(String, PropertyValue)] = for
      propName <- filteredClasses.getOrElse("e", Seq.empty)
      embedded = `parse_e-`(element)
    yield
      if isRootClass then
        (propName, NestedMicroformat(handleMicroformat(rootClassNames, element, simpleValue=Some(embedded))))
      else
        (propName, embedded)

    val properties = (p_propertyValues ++
                      u_propertyValues ++
                      dt_propertyValues ++
                      e_propertyValues).groupMap(_._1)(_._2)

    val parsedTypes = Seq(
      ("h", rootClassNames),
      ("e", e_propertyValues),
      ("dt", dt_propertyValues),
      ("p", p_propertyValues),
      ("u", u_propertyValues)
    ).filter(_._2.nonEmpty).map(_._1).toSet

    val isPropertyEl = parsedTypes.diff(Set("h")).nonEmpty

    if isRootClass then
        if isPropertyEl then
          ParsingResult(properties, Seq.empty, parsedTypes)
        else
          // if this is not a property element, but it is a h-* microformat,
          // add it to our list of children
          ParsingResult(properties, Seq(handleMicroformat(rootClassNames, element)), parsedTypes)
    else
      // parse child tags
      (for
        child <- element.children()
        result = parseProps(child)
      yield
        result
      ).foldLeft(ParsingResult(properties, Seq.empty, parsedTypes))(_ ++ _ )


  // parse a plain text property, e.g. `p-name`, `p-summary`
  // http://microformats.org/wiki/microformats2-parsing#parsing_a_p-_property
  def `parse_p-`(element: Element)(using baseURI: Option[URI]): String =
    // Parse the element for the value-class-pattern. If a value is found, return it.
    ValueClassPattern.text(element)
      .orElse(
        // If `abbr.p-x[title]` or `link.p-x[title]`, return the `title` attribute.
        element.selectAttribute("abbr:root[title], link:root[title]", "title")
      )
      .orElse(
        // if `data.p-x[value]` or `input.p-x[value]`, then return the `value` attribute
        element.selectAttribute("data:root[value~=.+], input:root[value~=.+]", "value")
      )
      .orElse(
        // else if `img.p-x[alt]` or `area.p-x[alt], then return the `alt` attribute
        element.selectAttribute("img:root[alt~=.+], area:root[alt~=.+]", "alt")
      )
      .getOrElse(
        // else return the textContent of the element
        textContent(element, replaceImg = false, imgToSrc = true)
      )

  // parse a URL property, e.g. `u-photo`
  // http://microformats.org/wiki/microformats2-parsing#parsing_a_u-_property
  def `parse_u-`(element: Element)(using baseURI: Option[URI]): PropertyValue =
    def href: String =
      // if `a.u-x[href]` or `area.u-x[href]` or `link.u-x[href]`, then get the `href` attribute
      element.selectAttribute("a:root[href], area:root[href], link:root[href]", "href")
        /*
        .orElse(
          // else if `img.u-x[src]` return `parseImg()`
          element.selectAttribute("img:root[src~=.+]", "src")
        )
         */
        .orElse(
          // else if `audio.u-x[src]` or `video.u-x[src]` or `source.u-x[src]` or `iframe.u-x[src]`, get `src`
          element.selectAttribute(
            "audio:root[src~=.+], video:root[src~=.+], source:root[src~=.+], iframe:root[src~=.+]",
            "src"
          )
        )
        .orElse(
          // else if `video.u-x[poster]`, then get the `poster` attribute
          element.selectAttribute("video:root[poster~=.+]", "poster")
        )
        .orElse(
          // else if `object.u-x[data]`, then get the `data` attribute
          element.selectAttribute("object:root[data~=.+]", "data")
        )
        .orElse(
          // else parse the element for the value-class-pattern.
          ValueClassPattern.text(element)
        )
        .orElse(
          // else if `abbr.u-x[title], then get the `title` attribute
          element.selectAttribute("abbr:root[title~=.+]", "title")
        )
        .orElse(
          // else if `data.u-x[value]` or `input.u-x[value]`, then get the `value` attribute
          element.selectAttribute("data:root[value~=.+], input:root[value~=.+]", "value")
        )
        // else get the textContent of the element
        .getOrElse(textContent(element, imgToSrc = true, replaceImg = false))

    if element.tagName() == "img" && element.hasAttr("src") then
      parseImg(element)
    else
      // transform value into absolute URL
      try
        val uri = URI.create(href)
        StringValue(baseURI.map(_.resolve(uri)).getOrElse(uri).toString)
      catch case _: IllegalArgumentException => StringValue(href)

  // parse an element tree (or embedded markup) property, e.g. `e-note`
  // The entire contained element hierarchy is the value.
  // http://microformats.org/wiki/microformats2-parsing#parsing_an_e-_property
  def `parse_e-`(element: Element)(using baseURI: Option[URI], rootLang: Option[String]): Embedded =
    def resolveContainedUrls(base: URI): Element =
      val attributes = Seq("href", "src", "cite", "data", "poster")
      val cloned = element.clone()
      for
        element <- cloned.stream().toScala(LazyList)
        attrKey <- attributes
        value <- element.attrOpt(attrKey)
        uri = URI.create(value)
      do
        element.attr(attrKey, base.resolve(uri).toString)
      cloned

    // return a dictionary with two keys:
    // `html` the `innerHTML` of the element, with leading/trailing spaces removed
    // `value`: `the `textContent` of the element
    Embedded(
      html=baseURI.map(resolveContainedUrls).getOrElse(element).filterUnwanted().html(),
      value=textContent(element, replaceImg=true, imgToSrc=true),
      lang=element.attrOpt("lang").orElse(rootLang)
    )

  // parse a datetime property, e.g. `dt-bday`, `dt-start`, `dt-end`
  // http://microformats.org/wiki/microformats2-parsing#parsing_a_dt-_property
  private def `parse_dt-`(element: Element)(using baseURI: Option[URI]): String =
    // parse the element for the value-class-pattern, including the date and time parsing rules.
    // If a value is found, then return it.
    val (datetime, newDefault) = ValueClassPattern.datetime(element, defaultDate).getOrElse {
      // if `time.dt-x[datetime]` or `ins.dt-x[datetime]` or `del.dt-x[datetime]`, then return the `datetime` attribute
      // else if `abbr.dt-x[title]`, then return the `title` attribute
      // else if `data.dt-x[value]` or `input.dt-x[value]`, then return the `value` attribute
      // else return `textContent`
      val propValue =
          element.selectAttribute("time[datetime~=.+], ins[datetime~=.+], del[datetime~=.+]", "datetime")
            .orElse(element.selectAttribute("abbr[title~=.+]", "title"))
            .orElse(element.selectAttribute("date[value~=.+], input[value~=.+]", "value"))
            .getOrElse(textContent(element, replaceImg = false, imgToSrc = true))

      // if this is just a time, augment with default date
      defaultDate
        .flatMap(default => Dateparser.parseTime(propValue, default))
        .getOrElse(Dateparser.parseFullDate(propValue))
    }
    this.defaultDate = newDefault
    datetime

  // parse a hyperlink element for rel microformats
  // http://microformats.org/wiki/microformats2-parsing#parse_a_hyperlink_element_for_rel_microformats
  def parseRel(element: Element)(using baseURI: Option[URI]): Option[Rel] =
    // if the "rel" attribute of the element is empty then exit
    element.attrOpt("rel").map { rel =>
      // set url to the value of the "href" attribute of the element, normalized and resolved
      val url = element.attrOpt("href").map(URI.create).get
      val resolvedURL = baseURI.map(_.resolve(url)).getOrElse(url)

      // treat the `rel` attribute of the element as a space separate set of `rel` values
      val relComponents = rel.split("\\s+").toSeq

      // for each rel value
      //   - if there is no key rel-value in the rels hash then create it with an empty array as its value
      //   - if url is not in the array of the key rel-value in the rels hash then add url to the array
      val rels = relComponents.map(_ -> resolvedURL).groupMap(_._1)(_._2)

      // if there is no key with name url in the top-level "rel-urls" hash then add a key with name url there, with an empty hash
      // add keys to the hash of the key with name url for each of these attributes (if present) and key not already set:
      //   - `hreflang`
      //   - `media`
      //   - `title`
      //   - `type`
      //   - `text`
      val relEntry = RelEntry(
        text = element.text().strip(),
        rels = relComponents,
        media = element.attrOpt("media"),
        hrefLang = element.attrOpt("hreflang"),
        `type` = element.attrOpt("type"),
        title = element.attrOpt("title"),
      )
      // if there is no "rels" key in that hash, add it with an empty array value
      // set the value of that "rels" key to an array of all unique items in the set of rel values unioned with the current
      // array value of the "rels" key, sorted alphabetically.
      Rel(
        rels,
        Map(resolvedURL -> relEntry)
      )
    }

  def implyName(element: Element)(using baseURI: Option[URI]): String =
    element
      // if `img.h-x` or `area.h-x`, then use its `alt` attribute
      .selectAttribute("img:root[alt~=.+], area:root[alt~=.+]", "alt")
      .orElse(
        // if `abbr.h-x[title]` then use its `title` attribute
        element.selectAttribute("abbr[title~=.+]", "title")
      )
      .orElse(
        // if `.h-x>img:only-child[alt]:not([alt=""]):not[.h-*]` then use that img's `alt` for name
        element.selectAttribute("> img:only-child[alt~=.+]:not([class~=h-.+])", "alt")
      )
      .orElse(
        // if .h-x>area:only-child[alt]:not([alt=""]):not[.h-*] then use that area's alt for name
        element.selectAttribute("> area:only-child[alt~=.+]:not([class~=h-.+])", "alt")
      )
      .orElse(
        // if .h-x>abbr:only-child[title]:not([title=""]):not[.h-*] then use that abbr title for name
        element.selectAttribute("> abbr:only-child[title~=.+]:not([class~=h-.+])", "title")
      )
      .orElse(
        // if .h-x>:only-child:not[.h-*]>img:only-child[alt]:not([alt=""]):not[.h-*] then use that img’s alt for name
        element.selectAttribute("> *:only-child:not([class~=h-.+]) > img:only-child[alt~=.+]:not([class~=h-.+])", "alt")
      )
      .orElse(
        // if .h-x>:only-child:not[.h-*]>area:only-child[alt]:not([alt=""]):not[.h-*] then use that area’s alt for name
        element.selectAttribute("> *:only-child:not([class~=h-.+]) > area:only-child[alt~=.+]:not([class~=h-.+])", "alt")
      )
      .orElse(
        // if .h-x>:only-child:not[.h-*]>abbr:only-child[title]:not([title=""]):not[.h-*] use that abbr’s title for name
        element.selectAttribute("> *:only-child:not([class~=h-.+]) > abbr:only-child[title~=.+]:not([class~=h-.+])", "title")
      )
      .getOrElse(
        // else use the `textContent` of the `.h-x` for `name` after:
        //  - dropping any nested `<script>` & `<style>` elements;
        //  - replacing any nested `<img>` elements with their `alt` attribute, if present;
        //  - remove all leading/trailing spaces
        textContent(element, replaceImg = true, imgToSrc = false)
      )

  def implyURL(element: Element)(using baseURI: Option[URI]): Option[URI] =
    implyHref(element)
      .flatMap(href => Try(URI.create(href)).toOption)
      .map(uri => baseURI.map(_.resolve(uri)).getOrElse(uri))

  private def implyHref(element: Element): Option[String] =
    // - if `a.h-x[href]` or `area.h-x[href]` then use that `[href]` for url
    element.selectAttribute("a:root[href], area:root[href]", "href")
      .orElse(
        // if .h-x>a[href]:only-of-type:not[.h-*], then use that [href] for url
        element.selectAttribute("> a[href]:only-of-type:not([class~=h-.+])", "href")
      )
      .orElse(
        // if .h-x>area[href]:only-of-type:not[.h-*], then use that [href] for url
        element.selectAttribute("> area[href]:only-of-type:not([class~=h-.+])", "href")
      )
      .orElse(
        // .h-x>:only-child:not[.h-*]>a[href]:only-of-type:not[.h-*], then use that [href] for url
        element.selectAttribute("> *:only-child:not([class~=h-.+]) > a[href]:only-of-type:not([class~=h-.+])", "href")
      )
      .orElse(
        // .h-x>:only-child:not[.h-*]>area[href]:only-of-type:not[.h-*], then use that [href] for url
        element.selectAttribute("> *:only-child:not([class~=h-.+]) > area[href]:only-of-type:not([class~=h-.+])", "href")
      )

  def implyPhoto(element: Element)(using baseURI: Option[URI]): Option[ImpliedPhoto] =
    val imgResolve: String => ImpliedPhoto = attr => ImpliedPhoto(resolve(attr, baseURI), alt=None)

    // if img.h-x[src], then use the result of "parse an img element for src and alt" (see Sec.1.5) for photo
    element.selectFirstOption("img:root[src]").map(parseImg)
      .orElse(
        // if object.h-x[data] then use data for photo
        element.selectAttribute("*:root[data~=.+]", "data").map(imgResolve)
      )
      .orElse(
        // if .h-x>img[src]:only-of-type:not[.h-*]
        element.selectFirstOption("> img[src]:only-of-type:not([class~=h-.+])").map(parseImg)
      )
      .orElse(
        // if h-x>object[data]:only-of-type:not[.h-*] then use that object’s data for photo
        element.selectAttribute("> object[data~=.+]:only-of-type:not([class~=h-.+])", "data").map(imgResolve)
      )
      .orElse(
        // if.h-x>:only-child:not[.h-*]>img[src]:only-of-type:not[.h-*]
        element.selectFirstOption("> *:only-child:not([class~=h-.+]) > img[src]:only-of-type:not([class~=h-.+])").map(parseImg)
      )
      .orElse(
        // .h-x>:only-child:not[.h-*]>object[data]:only-of-type:not[.h-*]
        element.selectAttribute("> *:only-child:not([class~=h-.+]) > object[data~=.+]:only-of-type:not([class~=h-.+])", "data").map(imgResolve)
      )


  private def textContent(element: Element, replaceImg: Boolean, imgToSrc: Boolean)(using baseURI: Option[URI]): String =
    def replacedImages(): Element =
      val cloned = element.clone()
      for
        img <- cloned.select("img")
      do
        // replacing any nested <img> elements with their alt attribute, if present;
        // otherwise their src attribute, if present, adding a space at the beginning and end, resolving the URL if it’s relative;
        val imgSrc = URI.create(img.attr("src"))
        val imgSrcText = if imgToSrc then Some(baseURI.map(_.resolve(imgSrc)).getOrElse(imgSrc).toString) else None

        val imgAlt = img.attrOpt("alt")
        val replacementText = imgAlt.orElse(imgSrcText).map(text => s" $text ")

        val textNode = TextNode(replacementText.getOrElse(""))

        img.replaceWith(textNode)
      cloned

    val filteredElement = if replaceImg then replacedImages() else element

    filteredElement
      // remove all leading/trailing spaces and nested `<script>` & `<style>`
      .filterUnwanted()
      .textPreservingWhitespace()
      // removing all leading/trailing spaces
      .strip()

  private def parseImg(element: Element)(using baseURI: Option[URI]): ImpliedPhoto =
    // To parse an img element for src and alt attributes:
    //  - if img[alt]
    //      return a new {} structure with (value, alt)
    //  - else
    //     return the element's src attribute as a normalized absolute URL.
    assert(element.tagName() == "img")
    val src = element.attr("src")
    val alt = element.attrOpt("alt")

    ImpliedPhoto(resolve(src, baseURI), alt)

  private def resolve(href: String, baseURI: Option[URI]): URI =
    val uri = URI.create(href)
    baseURI.map(_.resolve(uri)).getOrElse(uri)
