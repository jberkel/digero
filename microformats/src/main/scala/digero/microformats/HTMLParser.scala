package digero.microformats

import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element}
import org.jsoup.parser.Parser

trait HTMLParser:
  private lazy val parser: Parser = Parser.htmlParser

  def parseDocument(html: String, baseURI: String = ""): Document = Jsoup.parse(html, baseURI, parser)
  def parseElement(html: String): Element = parseDocument(html).body.firstElementChild
