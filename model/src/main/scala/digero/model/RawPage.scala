package digero.model

import java.io.Serializable
import java.time.OffsetDateTime
import javax.annotation.Nullable
import scala.beans.BeanProperty
import scala.compiletime.uninitialized

@SerialVersionUID(7909355701674974804L)
object RawPage:
  var CONTENT_MODEL_WIKITEXT = "wikitext"

@SerialVersionUID(7909355701674974804L)
class RawPage extends Serializable:
  // TODO: everything be immutable
  @BeanProperty var namespace = 0
  @BeanProperty var title: String = uninitialized
  @BeanProperty var text: String = uninitialized
  @BeanProperty @Nullable var redirect: String = uninitialized
  @BeanProperty var timestamp: OffsetDateTime = uninitialized

  /**
   * The content model for this page.
   * Usually one of wikitext|text|css|javascript|json|Scribunto
   *
   * @see <a href="https://www.mediawiki.org/wiki/Content_handlers">Content handlers</a>
   */
  @BeanProperty var contentModel: String = uninitialized
  @BeanProperty var id: Long = 0L
  @BeanProperty var revisionId: Long = 0L
  @BeanProperty var contributorId = 0L
  @BeanProperty var contributorName: String = uninitialized

  def isEmpty: Boolean = (title == null || title.isEmpty) || (text == null || text.isEmpty)

  override def toString: String =
    s"RawPage{namespace=$namespace, title='$title', text='$text', redirect='$redirect', " +
      s"contentModel='$contentModel', revisionId=$revisionId, contributorId=$contributorId}"
