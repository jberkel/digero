package digero.model

case class Template(name: String,
                    parameters: TemplateParameterMap = TemplateParameterMap()) extends Iterable[String]:

  val anonymousParameters: Seq[String] = parameters
    .flatMap((k, v) => k.swap.map( (_, v)).toOption)
    .toSeq
    .sortWith( (a, b) => a._1 < b._1 )
    .map(_._2)

  /**
   * @param index 1-based index
   */
  def getParameter(index: Int): Option[String] =
    assert(index >= 1)
    parameters.get(Left(index))

  def headParameter: Option[String] = parameters.values.headOption
  def tailParameter: Option[String] = if parameters.size > 1 then parameters.values.lastOption else None

  def getParameter(key: String): Option[String] = parameters.get(Right(key))
  def getNonBlankParameter(index: Int): Option[String] = getParameter(index).filter((s: String) => s.trim.nonEmpty)
  def iterator: Iterator[String] = anonymousParameters.iterator

  override def toString: String = f"Template{name=$name, parameters=$parameters}"
