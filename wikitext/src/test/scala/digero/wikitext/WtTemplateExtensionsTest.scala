package digero.wikitext

import digero.model.Template
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*
import org.sweble.wikitext.engine.nodes.EngPage
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp
import org.sweble.wikitext.engine.{PageId, PageTitle, WtEngineImpl}
import org.sweble.wikitext.parser.nodes.WtTemplate
import digero.wikitext.WtTemplateExtensions._

import scala.collection.mutable.ListBuffer


class WtTemplateExtensionsTest:
  @Test
  def testGetParameter(): Unit =
    val t = WtTemplateExtensionsTest.parseTemplate("{{foo|a|b}}")

    t.parameters should have size 2
    t.getParameter(1) should contain("a")
    t.getParameter(2) should contain("b")

  @Test
  def testIterable(): Unit =
    (for
      x <- WtTemplateExtensionsTest.parseTemplate("{{foo|a|b}}")
    yield x).toSeq should contain inOrderOnly ("a", "b")

  @Test
  def testGetParameterByKey(): Unit =
    val t = WtTemplateExtensionsTest.parseTemplate("{{foo|a|b|c=value}}")
    t.getParameter("c") should contain("value")

  @Test
  def testGetParameterByIndex(): Unit =
    val t = WtTemplateExtensionsTest.parseTemplate("{{foo|c=value|a|d=foo|b|}}")
    t.getParameter(1) should contain("a")
    t.getParameter(2) should contain("b")

  @Test
  def testGetNonBlankParameter(): Unit =
    val t = WtTemplateExtensionsTest.parseTemplate("{{foo|a| |c}}")
    t.getNonBlankParameter(1) should contain("a")
    t.getNonBlankParameter(2) shouldBe empty

  @Test
  def testAnonymousParameterOrderingIsPreserved(): Unit =
    val t = WtTemplateExtensionsTest.parseTemplate("{{foo|a|b| |c|foo=bar|d}}")
    t.anonymousParameters should contain inOrderOnly("a", "b", "", "c", "d")

object WtTemplateExtensionsTest:
  class TemplateCollector extends TemplateVisitor:
    final val templates = new ListBuffer[WtTemplate]
    override def visit(template: WtTemplate): Unit = templates.append(template)

  private def parseTemplate(wikiText: String): Template =
    val page = parse(wikiText)
    val collector = new WtTemplateExtensionsTest.TemplateCollector
    collector.visit(page)
    collector.templates should not be empty
    collector.templates.headOption.map(_.asTemplate).get

  private def parse(wikiText: String): EngPage =
    val wikiConfig = DefaultConfigEnWp.generate
    val engine = new WtEngineImpl(wikiConfig)
    engine.postprocess(
      new PageId(PageTitle.make(wikiConfig, "test"), 0), wikiText, null
    ).getPage
