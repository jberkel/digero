package digero.wikitext

import de.fau.cs.osr.ptk.common.AstVisitor
import org.sweble.wikitext.parser.nodes.*

import scala.annotation.unused

abstract class TemplateVisitor extends AstVisitor[WtNode]:
  def visit(n: WtNodeList): Unit = iterate(n)

  def visit(template: WtTemplate): Unit
  def visit(@unused n: WtNode): Unit = {
    // catch all
  }

  def visit(section: WtSection): Unit = iterate(section.getBody)
  def visit(argument: WtTemplateArgument): Unit = iterate(argument)
