package digero.wikitext

import digero.model.{Template, TemplateParameterMap}
import digero.model.TemplateParameterMap._
import org.sweble.wikitext.parser.nodes
import org.sweble.wikitext.parser.nodes.{WtNode, WtTemplate, WtTemplateArgument}
import digero.wikitext.WtNodeExtensions.getText
import scala.jdk.CollectionConverters.*

object WtTemplateExtensions:
  extension (wtTemplate: WtTemplate)

    def asTemplate: Template =
      val parameters = TemplateParameterMap()
      var paramIndex = 1
      for
        case argument: WtTemplateArgument <- wtTemplate.getArgs.asScala.toSeq
      do
        (argument.getName.getText, argument.getValue.getText) match
          case (Some(key), Some(value)) => parameters.put(key, value)
          case (None, value) =>
            parameters.put(paramIndex, value.getOrElse(""))
            paramIndex += 1
          case _ =>

      Template(wtTemplate.getName.getAsString, parameters)
