package digero.wikitext

import digero.model.RawPageTitle
import org.sweble.wikitext.engine.nodes.EngPage

import java.time.OffsetDateTime

case class ParsedPage(rawTitle: RawPageTitle, page: EngPage, timestamp: OffsetDateTime)
