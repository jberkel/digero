package digero.wikitext

import digero.model.RawPage.CONTENT_MODEL_WIKITEXT
import digero.model.{RawPage, RawPageTitle}
import digero.{RDDTask, TaskArguments}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp
import org.sweble.wikitext.engine.{PageId, PageTitle, WtEngine, WtEngineImpl}

/**
 * Transforms the XML-parsed dump further and parses the individual pages.
 */
class WikitextTask extends RDDTask[RDD[(String, ParsedPage)]]:

  def process(context: SparkContext,
              input: RDD[(String, RawPage)],
              arguments: TaskArguments): RDD[(String, ParsedPage)] =
    val wikiConfig = DefaultConfigEnWp.generate
    val engineBroadcast = context.broadcast[WtEngine](WtEngineImpl(wikiConfig))

    input.filter((_, page) => CONTENT_MODEL_WIKITEXT == page.contentModel && !page.isEmpty)
      .mapValues((rawPage: RawPage) => {
        val engine = engineBroadcast.value
        val title = PageTitle.make(engine.getWikiConfig, rawPage.title)
        val pageId = new PageId(title, rawPage.revisionId)
        val engPage = engine.postprocess(pageId, rawPage.text, null).getPage

        ParsedPage(
          new RawPageTitle(rawPage.title, rawPage.redirect, rawPage.namespace),
          engPage,
          rawPage.timestamp
        )
    })

  override def kryoRegistrator: String = classOf[SerializationSupport].getName
