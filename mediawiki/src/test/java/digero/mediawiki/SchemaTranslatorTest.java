package digero.mediawiki;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SchemaTranslatorTest {
    private SchemaTranslator subject;

    @BeforeEach
    void setUp() {
        subject = new SchemaTranslator();
    }

    @Test
    void testTranslateEmpty() {
        assertThat(subject.translate("")).isEqualTo("");
    }

    @Test
    void testTranslateCreateTableBinaryToBlob() {
        assertThat(subject.translate(
                "CREATE TABLE user (a binary(14) b varbinary(1));")).isEqualTo(
                "CREATE TABLE user (a BLOB b BLOB);"
        );
    }

    @Test
    void testTranslateCreateTableRemoveUnsigned() {
        assertThat(subject.translate(
                "CREATE TABLE page (page_id int unsigned);")).isEqualTo(
                "CREATE TABLE page (page_id INTEGER );"
        );
    }

    @Test
    void testTranslateCreateTableFloatingPointToReal() {
        assertThat(subject.translate(
                "CREATE TABLE page (page_random double unsigned);")).isEqualTo(
                "CREATE TABLE page (page_random REAL );"
        );
    }

    @Test
    void testTranslateCreateTableVarcharToText() {
        assertThat(subject.translate(
                "CREATE TABLE user (user_name varchar(255));")).isEqualTo(
                "CREATE TABLE user (user_name TEXT);"
        );
    }

    @Test
    void testTranslateCreateTableTextNormalization() {
        assertThat(subject.translate(
                "CREATE TABLE user (a tinytext b mediumtext c longtext);")).isEqualTo(
                "CREATE TABLE user (a TEXT b TEXT c TEXT);"
        );
    }

    @Test
    void testTranslateCreateTableBlobNormalization() {
        assertThat(subject.translate(
                "CREATE TABLE user (a tinyblob b mediumblob c longblob);")).isEqualTo(
                "CREATE TABLE user (a BLOB b BLOB c BLOB);"
        );
    }

    @Test
    void testTranslateCreateTableBoolToInteger() {
        assertThat(subject.translate(
                "CREATE TABLE user (a boolean b bool);")).isEqualTo(
                "CREATE TABLE user (a INTEGER b INTEGER);"
        );
    }

    @Test
    void testTranslateCreateTableDateTimeToText() {
        assertThat(subject.translate(
                "CREATE TABLE user (a datetime b timestamp);")).isEqualTo(
                "CREATE TABLE user (a TEXT b TEXT);"
        );
    }

    @Test
    void testTranslateCreateTableNoEnum() {
        assertThat(subject.translate(
                "CREATE TABLE user (a ENUM(\"a\", \"b\"));")).isEqualTo(
                "CREATE TABLE user (a TEXT);"
        );
    }

    @Test
    void testTranslateCreateTableRemoveBinaryCollation() {
        assertThat(subject.translate(
                "CREATE TABLE user (a varchar(225) binary);")).isEqualTo(
                "CREATE TABLE user (a TEXT );"
        );
    }

    @Test
    void testTranslateCreateTableAutoIncrement() {
        assertThat(subject.translate(
                "CREATE TABLE user (user_id int NOT NULL PRIMARY KEY AUTO_INCREMENT);")).isEqualTo(
                "CREATE TABLE user (user_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT);"
        );
    }

    @Test
    void testTranslateCreateIndexNoFulltext() {
        assertThat(subject.translate(
                "CREATE FULLTEXT INDEX a ON b (b_id);")).isEqualTo(
                "CREATE  INDEX a ON b (b_id);"
        );
    }

    @Test
    void testTranslateCreateIndexNoTruncatedIndexes() {
        assertThat(subject.translate(
                "CREATE INDEX a ON b (b_id(20));")).isEqualTo(
                "CREATE INDEX a ON b (b_id);"
        );
    }
}
