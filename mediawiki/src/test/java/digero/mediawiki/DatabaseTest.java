package digero.mediawiki;

import org.junit.jupiter.api.Test;
import digero.model.RawPage;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

public class DatabaseTest {
    @Test
    void testCreate() throws Exception {
        final File file = File.createTempFile("test", ".sqlite");
        try (Database database = new Database(file, true)) {
            assertThat(file).canRead();
            assertThat(file.length()).isGreaterThan(0);

            assertThat(database.count("SELECT count(*) FROM page")).isEqualTo(0);
            assertThat(database.count("SELECT count(*) FROM actor")).isEqualTo(1);
            assertThat(database.count("SELECT count(*) FROM comment")).isEqualTo(1);
        }

        try (Database existing = new Database(file, false)) {
            assertThat(existing.count("SELECT count(*) FROM page")).isEqualTo(0);
        }
    }

    @Test
    void testAddUser() throws Exception {
        final File file = File.createTempFile("test", ".sqlite");
        try (Database database = new Database(file, true)) {
            database.addUser("Admin", "password", new String[] {"sysop", "interface admin"});

            assertThat(database.count("SELECT count(*) FROM user")).isEqualTo(1);
            assertThat(database.count("SELECT count(*) FROM actor")).isEqualTo(2);
            assertThat(database.count("SELECT count(*) FROM user_groups")).isEqualTo(2);
        }
    }

    @Test
    void testInsert() throws Exception {
        final File file = File.createTempFile("test", ".sqlite");
        try (Database database = new Database(file, true)) {
            RawPage page = new RawPage();
            page.setTitle("Test");
            page.setText("Some text");
            page.setContentModel("wikitext");
            page.setId(1);
            page.setRevisionId(1);
            page.setNamespace(2);

            assertThat(database.insert(new DatabasePage(page))).isFalse();

            RawPage page2 = new RawPage();
            page2.setTitle("Test 2");
            page2.setText("Some text 2");
            page2.setContentModel("wikitext");
            page2.setId(2);
            page2.setRevisionId(2);
            page2.setNamespace(2);

            assertThat(database.insert(new DatabasePage(page2))).isFalse();

            assertThat(database.count("SELECT count(*) FROM page")).isEqualTo(2);
            assertThat(database.count("SELECT count(*) FROM revision")).isEqualTo(2);
            assertThat(database.count("SELECT count(*) FROM revision_comment_temp")).isEqualTo(2);
            assertThat(database.count("SELECT count(*) FROM revision_actor_temp")).isEqualTo(2);
            assertThat(database.count("SELECT count(*) FROM redirect")).isEqualTo(0);
            assertThat(database.count("SELECT count(*) FROM content")).isEqualTo(2);
            assertThat(database.count("SELECT count(*) FROM slots")).isEqualTo(2);
        }
    }

    @Test
    void testInsertSanitizedCssContentModel() throws Exception {
        final File file = File.createTempFile("test", ".sqlite");
        try (Database database = new Database(file, true)) {
            RawPage page = new RawPage();

            page.setTitle("Test");
            page.setText("Some text");
            page.setContentModel("sanitized-css");
            page.setId(1);
            page.setRevisionId(1);
            page.setNamespace(2);

            assertThat(database.insert(new DatabasePage(page))).isFalse();

            assertThat(database.count("SELECT count(*) FROM page")).isEqualTo(1);
            assertThat(database.count("SELECT count(*) FROM content")).isEqualTo(1);
            assertThat(database.count("SELECT count(*) FROM slots")).isEqualTo(1);
        }
    }

    @Test
    void testInsertRedirect() throws Exception {
        final File file = File.createTempFile("test", ".sqlite");
        try (Database database = new Database(file, true)) {
            RawPage page = new RawPage();
            page.setTitle("Test");
            page.setRedirect("somewhere else");
            page.setText("#REDIRECT [[somewhere else]]");
            page.setContentModel("wikitext");
            page.setId(1);
            page.setRevisionId(1);
            page.setNamespace(2);

            assertThat(database.insert(new DatabasePage(page))).isFalse();

            assertThat(database.count("SELECT count(*) FROM redirect")).isEqualTo(1);
        }
    }
}
