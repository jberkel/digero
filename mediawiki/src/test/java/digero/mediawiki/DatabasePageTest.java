package digero.mediawiki;

import org.junit.jupiter.api.Test;
import digero.model.RawPage;

import static org.assertj.core.api.Assertions.assertThat;

public class DatabasePageTest {

    @Test
    void testTitleNamespacePrefixGetsRemoved() {
        RawPage rawPage = new RawPage();
        rawPage.setTitle("Foo:Bar");
        rawPage.setNamespace(1);
        DatabasePage page = new DatabasePage(rawPage);

        assertThat(page.title).isEqualTo("Bar");
        assertThat(page.namespace).isEqualTo(1);
    }

    @Test
    void testSpacesInTitleReplacedWithUnderscores() {
        RawPage rawPage = new RawPage();
        rawPage.setTitle("Foo Bar");

        DatabasePage page = new DatabasePage(rawPage);

        assertThat(page.title).isEqualTo("Foo_Bar");
    }
}
