package digero.mediawiki;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

/**
 * Adapted from
 * <a href="https://github.com/wikimedia/mediawiki/blob/master/includes/libs/rdbms/database/DatabaseSqlite.php#L932">
 *     DatabaseSqlite.php</a>
 */
public class SchemaTranslator {
    private static final Pattern CREATE_TABLE = Pattern.compile("^\\s*(CREATE|ALTER) TABLE", CASE_INSENSITIVE);
    private static final Pattern CREATE_INDEX = Pattern.compile("^\\s*CREATE (\\s*(?:UNIQUE|FULLTEXT)\\s+)?INDEX", CASE_INSENSITIVE);

    public String translate(final String input) {
        if (CREATE_TABLE.matcher(input).find()) {
            return Create.replace(input);
        } else if (CREATE_INDEX.matcher(input).find()) {
            return Index.replace(input);
        } else {
            return input;
        }
    }

    enum Create {
        BinaryToBlob(Pattern.compile("\\b(var)?binary(\\(\\d+\\))", CASE_INSENSITIVE), "BLOB"),
        RemoveUnsigned(Pattern.compile("\\b(un)?signed\\b", CASE_INSENSITIVE), ""),
        Integer(Pattern.compile("\\b(tiny|small|medium|big|)int(\\s*\\(\\s*\\d+\\s*\\)|\\b)", CASE_INSENSITIVE),
                "INTEGER"),
        FloatingPointToReal(Pattern.compile("\\b(float|double(\\s+precision)?)(\\s*\\(\\s*\\d+\\s*(,\\s*\\d+\\s*)?\\)|\\b)",
                CASE_INSENSITIVE), "REAL"),
        VarCharToText(Pattern.compile("\\b(var)?char\\s*\\(.*?\\)", CASE_INSENSITIVE), "TEXT"),
        TextNormalization(Pattern.compile("\\b(tiny|medium|long)text\\b", CASE_INSENSITIVE), "TEXT"),
        BlobNormalization(Pattern.compile("\\b(tiny|small|medium|long|)blob\\b", CASE_INSENSITIVE), "BLOB"),
        BoolToInteger(Pattern.compile("\\bbool(ean)?\\b", CASE_INSENSITIVE), "INTEGER"),
        DateTimeToText(Pattern.compile("\\b(datetime|timestamp)\\b", CASE_INSENSITIVE), "TEXT"),
        NoEnumType(Pattern.compile("\\benum\\s*\\([^)]*\\)", CASE_INSENSITIVE), "TEXT"),
        RemoveBinaryCollation(Pattern.compile("\\bbinary\\b", CASE_INSENSITIVE), ""),
        AutoIncrement(Pattern.compile("\\bauto_increment\\b", CASE_INSENSITIVE), "AUTOINCREMENT"),
        NoExplicitOptions(Pattern.compile("\\)[^);]*(;?)\\s*$", CASE_INSENSITIVE), ")$1"),
        AutoincrementPrimaryKey(Pattern.compile("primary key (.*?) autoincrement", CASE_INSENSITIVE),
                "PRIMARY KEY AUTOINCREMENT $1");

        private final Pattern pattern;
        final String replacement;

        Create(Pattern pattern, String replacement) {
            this.pattern = pattern;
            this.replacement = replacement;
        }

        public static String replace(final String input) {
            String modified = input;
            for (Create c : values()) {
                Matcher matcher = c.pattern.matcher(modified);
                if (matcher.find()) {
                    modified = matcher.replaceAll(c.replacement);
                }
            }
            return modified;
        }
    }

    enum Index {
        NoTruncatedIndexes(Pattern.compile("\\(\\d+\\)", CASE_INSENSITIVE), ""),
        NoFullText(Pattern.compile("\\bfulltext\\b", CASE_INSENSITIVE), "");

        private final Pattern pattern;
        final String replacement;

        Index(Pattern pattern, String replacement) {
            this.pattern = pattern;
            this.replacement = replacement;
        }

        public static String replace(final String input) {
            String modified = input;
            for (Index c : values()) {
                Matcher matcher = c.pattern.matcher(modified);
                if (matcher.find()) {
                    modified = matcher.replaceAll(c.replacement);
                }
            }
            return modified;
        }
    }
}
