package digero.mediawiki.jdbc;

import java.sql.SQLException;

public class RuntimeSQLException extends RuntimeException {
    private static final long serialVersionUID = 6665039959941556886L;

    public RuntimeSQLException(Throwable cause) {
        super(cause);
    }

    public static void silenceNoArg(SQLNoArgFunction f) {
        silence(() -> { f.apply(); return null; });
    }

    public static <R> R silence(SQLFunction<R> f) {
        try {
            return f.apply();
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    @FunctionalInterface
    interface SQLNoArgFunction {
        void apply() throws SQLException;
    }

    @FunctionalInterface
    interface SQLFunction<R> {
        R apply() throws SQLException;
    }
}
