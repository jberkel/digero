package digero.mediawiki.jdbc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sqlite.SQLiteConfig;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.function.Function;

import java.util.stream.Stream;

import static digero.mediawiki.jdbc.ExtendedPreparedStatement.wrap;
import static digero.mediawiki.jdbc.RuntimeSQLException.silence;
import static digero.mediawiki.jdbc.RuntimeSQLException.silenceNoArg;
import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class SQLiteHelper implements AutoCloseable {
    private final Log log = LogFactory.getLog("SQLiteHelper");
    private final Connection connection;
    private final File dbFile;

    public SQLiteHelper(File dbFile) {
        this(dbFile, false);
    }

    public SQLiteHelper(File dbFile, boolean readOnly) {
        if (dbFile == null) {
            throw new IllegalArgumentException("dbFile is null");
        }
        final SQLiteConfig config = new SQLiteConfig();
        config.setReadOnly(readOnly);
        this.dbFile = dbFile;
        connection = silence(() -> DriverManager.getConnection("jdbc:sqlite:" + dbFile.getAbsolutePath(), config.toProperties()));
        setForeignKeysEnabled(true);
        log.info(String.format("SQLiteHelper (%1s): %2s", getVersion(), toString()));
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException ignore) {
            log.warn("close()", ignore);
        }
    }

    public boolean isClosed() {
        return silence(connection::isClosed);
    }

    public void beginTransaction() {
        silenceNoArg(() -> connection.setAutoCommit(false));
    }

    public void commit()  {
        silenceNoArg(connection::commit);
    }

    void rollback() {
        silenceNoArg(connection::rollback);
    }


    public <R> R transaction(Function<SQLiteHelper, R> function) {
        beginTransaction();
        R result = function.apply(this);
        commit();
        return result;
    }

    final String getVersion() {
        try (Stream<ExtendedResultSet> stream = prepare("SELECT sqlite_version()").executeStream()) {
            return stream
                    .findFirst()
                    .map(rs -> rs.stringValue(1))
                    .orElse("N/A");
        }
    }

    /**
     * As of SQLite version 3.6.19, the default setting for foreign key enforcement is OFF.
     *
     * @param enabled whether foreign keys should be enabled
     * @see <a href="https://www.sqlite.org/pragma.html#pragma_foreign_keys">PRAGMA foreign_keys</a>
     */
    final void setForeignKeysEnabled(boolean enabled) {
        execute("PRAGMA foreign_keys = "+ (enabled ? "1" : "0") + ";");
    }

    public final void execute(String sql) {
        log.info("execute(" + sql + ")");
        try (ExtendedPreparedStatement statement = prepare(sql)) {
            statement.executeNoThrow();
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    public final ExtendedPreparedStatement prepare(String sql) {
        return silence(() -> wrap(connection.prepareStatement(sql, RETURN_GENERATED_KEYS)));
    }

    public long count(String query) {
        return prepare(query).longValue();
    }

    public boolean exists(String table) {
        try (ExtendedPreparedStatement statement = prepare("PRAGMA table_info("+table+")") ){
            try (Stream<ExtendedResultSet> stream = statement.executeStream()) {
                return stream.count() > 0;
            }
        } catch (SQLException e) {
            return false;
        }
    }

    public File getDbFile() {
        return dbFile;
    }

    @Override
    public final String toString() {
        return "SQLiteHelper{" +
                "connection=" + connection +
                ", dbFile=" + dbFile +
                '}';
    }
}
