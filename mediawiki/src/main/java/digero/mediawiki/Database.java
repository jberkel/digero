package digero.mediawiki;

import digero.mediawiki.jdbc.ExtendedPreparedStatement;
import digero.mediawiki.jdbc.SQLiteHelper;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @see <a href="https://www.mediawiki.org/wiki/Manual:Database_layout">Database layout</a>
 */
public class Database implements Closeable {
    private final Log log = LogFactory.getLog("Database");

    private static final String SCHEMA_SQL = "/tables.sql";
    private final SQLiteHelper sqliteHelper;
    private final static long DEFAULT_ACTOR_ID = 1;
    private final static long DEFAULT_COMMENT_ID = 1;
    private final static String MAIN_SLOT_NAME = "main";
    private final static long MAIN_SLOT_ID = 1;

    private final static Map<String, Long> CONTENT_MODELS = new HashMap<>();
    private final static String WIKITEXT_CONTENT_MODEL = "wikitext";
    private final static String SCRIBUNTO_CONTENT_MODEL = "Scribunto";

    public final static String[] DEFAULT_ADMIN_GROUPS = {
        "sysop", "bureaucrat", "interface admin"
    };

    static  {
        // mediawiki/includes/Defines.php
        CONTENT_MODELS.put(WIKITEXT_CONTENT_MODEL, 1L);
        CONTENT_MODELS.put(SCRIBUNTO_CONTENT_MODEL, 2L);
        CONTENT_MODELS.put("javascript", 3L);
        CONTENT_MODELS.put("css", 4L);
        CONTENT_MODELS.put("text", 5L);
        CONTENT_MODELS.put("json", 6L);
        CONTENT_MODELS.put("sanitized-css", 7L); // https://www.mediawiki.org/wiki/Extension:TemplateStyles
    }

    public Database(File file, boolean create) throws IOException {
        sqliteHelper = new SQLiteHelper(file, false);
        if (create) {
            final InputStream in = getClass().getResourceAsStream(SCHEMA_SQL);
            if (in == null) {
                throw new RuntimeException("schema not found (SCHEMA_SQL="+SCHEMA_SQL+", " +
                        "classloader="+getClass().getClassLoader()+")");
            }
            createTables(in);
            createDefaults();
        }
    }

    public synchronized boolean insert(DatabasePage databasePage) {
        return sqliteHelper.transaction(helper -> {
            final long textId = insertText(databasePage.text);
            final Long contentModelId = CONTENT_MODELS.get(databasePage.contentModel);
            if (contentModelId != null) {
                final long contentId = insertContent(databasePage.text.length(), contentModelId,
                        makeAddressFromTextId(textId));
                insertSlot(databasePage.revisionId, contentId);
            } else {
                log.warn("unknown content model "+databasePage.contentModel);
            }
            insertRevision(databasePage, textId);
            linkRevisionToComment(databasePage.revisionId);
            linkRevisionToActor(databasePage.revisionId, databasePage.id);

            if (databasePage.redirect != null) {
                insertRedirect(databasePage);
            }
            return insertPage(databasePage);
        });
    }

    public synchronized long addUser(String username, String password, String[] groups) {
        final ExtendedPreparedStatement user = sqliteHelper.prepare(
                "INSERT INTO user (user_name, user_password, user_token, user_newpassword, user_email) " +
                        "VALUES(?, ?, ?, '', '')"
        );

        final ExtendedPreparedStatement actor = sqliteHelper.prepare(
                "INSERT INTO actor (actor_user, actor_name) " +
                        "VALUES(?, ?)"
        );
        final long userId = user.bind(1, username)
                .bind(2, crypt(password))
                .bind(3 , generateUserToken())
                .insert();

        final ExtendedPreparedStatement user_groups = sqliteHelper.prepare(
            "INSERT INTO user_groups (ug_user, ug_group) VALUES(?, ?)"
        );

        actor.bind(1, userId)
            .bind(2, username)
            .insert();

        if (groups != null) {
            for (String group : groups) {
                user_groups
                        .bind(1, userId)
                        .bind(2, group)
                        .insert();
            }
        }
        return userId;
    }

    public long count(String query) {
        return sqliteHelper.count(query);
    }

    @Override
    public void close() {
        sqliteHelper.close();
    }

    private void createDefaults() {
        insertDefaultActor();
        insertDefaultComment();
        insertMainSlotRole();
        insertDefaultContentModels();
    }

    private void createTables(InputStream in) throws IOException {
        final SchemaTranslator schemaTranslator = new SchemaTranslator();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line;
            StringBuilder command = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                if (line.trim().startsWith("--"))
                    continue;
                command.append(line);
                if (line.endsWith(";")) {
                    sqliteHelper.execute(schemaTranslator.translate(command.toString()));
                    command = new StringBuilder();
                }
            }
        }
    }

    private long insertText(String pageText) {
        final ExtendedPreparedStatement text = sqliteHelper.prepare(
            "INSERT INTO text (old_text, old_flags) VALUES(?, 'utf-8')"
        );
        return text.bind(1, pageText).insert();
    }

    private long insertContent(long contentSize, long contentModelId, String contentAddress) {
        final ExtendedPreparedStatement content = sqliteHelper.prepare(
                "INSERT INTO content (content_size, content_sha1, content_model, content_address) " +
                    "VALUES(?, ?, ?, ?)"
        );
        return content.bind(1, contentSize)
            .bind(2, 0)
            .bind(3, contentModelId)
            .bind(4, contentAddress)
            .insert();
    }

    // mediawiki/includes/Storage/SqlBlobStore.php
    private String makeAddressFromTextId(long textId) {
        return  "tt:"+textId;
    }

    private void insertSlot(long revisionId, long contentId) {
        final ExtendedPreparedStatement slot = sqliteHelper.prepare(
            "INSERT INTO slots (slot_revision_id, slot_role_id, slot_content_id, slot_origin) " +
                    "VALUES(?, ?, ?, ?)"
        );
        slot.bind(1, revisionId)
            .bind(2, MAIN_SLOT_ID)
            .bind(3, contentId)
            .bind(4, revisionId)
            .executeNoThrow();
    }

    private void insertDefaultComment() {
        final ExtendedPreparedStatement comment = sqliteHelper.prepare(
            "INSERT INTO comment (comment_id, comment_hash, comment_text) VALUES(?, ?, ?)"
        );
        comment.bind(1, DEFAULT_COMMENT_ID)
               .bind(2, 0)
               .bind(3, "default comment")
               .executeNoThrow();
    }

    private void insertDefaultActor() {
        final ExtendedPreparedStatement actor = sqliteHelper.prepare(
                "INSERT INTO actor (actor_id, actor_user, actor_name) VALUES(?, ?, ?)"
        );
        actor.bind(1, DEFAULT_ACTOR_ID)
            .bindNull(2) // anon
            .bind(3, "127.0.0.1")
            .executeNoThrow();
}

    private void insertMainSlotRole() {
        final ExtendedPreparedStatement actor = sqliteHelper.prepare(
            "INSERT INTO slot_roles (role_id, role_name) VALUES(?, ?)"
        );
        actor.bind(1, MAIN_SLOT_ID)
            .bind(2, MAIN_SLOT_NAME)
            .executeNoThrow();
    }

    private void insertDefaultContentModels() {
        final ExtendedPreparedStatement actor = sqliteHelper.prepare(
            "INSERT INTO content_models (model_id, model_name) VALUES(?, ?)"
        );
        CONTENT_MODELS.forEach((name, id) ->
            actor.bind(1, id)
            .bind(2, name)
            .executeNoThrow()
        );
    }

    private void insertRevision(DatabasePage databasePage, long textId) {
        final ExtendedPreparedStatement revision =  sqliteHelper.prepare(
                "INSERT INTO revision(" +
                        "rev_id, rev_page, rev_text_id, rev_content_model) " +
                        "VALUES(?, ?, ?, ?)");
        revision.bind(1, databasePage.revisionId)
                .bind(2, databasePage.id)
                .bind(3, textId)
                .bind(4, databasePage.contentModel)
                .executeNoThrow();
    }

    private void linkRevisionToActor(long revisionId, long pageId) {
        final ExtendedPreparedStatement revision_actor_temp = sqliteHelper.prepare(
            "INSERT INTO revision_actor_temp (revactor_rev, revactor_actor, revactor_page) VALUES(?, ?, ?)"
        );
        revision_actor_temp.bind(1, revisionId)
            .bind(2, DEFAULT_ACTOR_ID)
            .bind(3, pageId)
            .executeNoThrow();
}

    private void linkRevisionToComment(long revisionId) {
        final ExtendedPreparedStatement revision_comment_temp = sqliteHelper.prepare(
            "INSERT INTO revision_comment_temp (revcomment_rev, revcomment_comment_id) VALUES(?, ?)"
        );
        revision_comment_temp.bind(1, revisionId)
            .bind(2, DEFAULT_COMMENT_ID)
            .executeNoThrow();
    }

    private void insertRedirect(DatabasePage databasePage) {
        final ExtendedPreparedStatement redirect = sqliteHelper.prepare(
                "INSERT INTO redirect (rd_from, rd_namespace, rd_title) VALUES(?, ?, ?)"
        );
        redirect.bind(1, databasePage.id)
                .bind(2, databasePage.namespace)
                .bind(3, databasePage.redirect)
                .executeNoThrow();
    }

    private boolean insertPage(DatabasePage databasePage) {
        final ExtendedPreparedStatement page = sqliteHelper.prepare(
        "INSERT INTO page(" +
                " page_id, page_namespace, page_title, page_restrictions, page_random, page_latest, page_len," +
                " page_is_redirect, page_content_model)" +
                " VALUES(?, ?, ?, '', 0, ?, ?, ?, ?)");

        return page.bind(1, databasePage.id)
                .bind(2, databasePage.namespace)
                .bind(3, databasePage.title)
                .bind(4, databasePage.revisionId)
                .bind(5, databasePage.text.length())
                .bind(6, databasePage.redirect == null ? 0 : 1)
                .bind(7, databasePage.contentModel)
                .executeNoThrow();
    }

    /**
     * See $wgPasswordConfig in includes/DefaultSettings.php for prefixes and implementations.
     * Because the database is not mean for production the simplest password mechanism is used.
     */
    private String crypt(String password) {
        // MwOldPassword, MD5 hash of password, no salt
        return ":A:" + md5(password);
    }

    private String md5(String text) {
        try {
            final MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(text.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * A pseudorandomly generated value that is stored in cookie.
     */
    private String generateUserToken() {
        return md5(String.valueOf(System.currentTimeMillis()));
    }
}
