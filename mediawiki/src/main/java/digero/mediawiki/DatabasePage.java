package digero.mediawiki;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import digero.model.RawPage;

public class DatabasePage {
    public final long id;
    public final long revisionId;
    public final String title;

    public final int namespace;
    @Nonnull public final String text;
    @Nullable public final String redirect;
    public final String contentModel;

    public DatabasePage(RawPage rawPage) {
        this.id = rawPage.getId();
        this.revisionId = rawPage.getRevisionId();
        this.text = rawPage.getText();
        this.contentModel = rawPage.getContentModel();
        this.redirect = rawPage.getRedirect();
        int colon = rawPage.title().indexOf(":");
        if (colon > 0) {
            this.title = underscore(rawPage.getTitle().substring(colon + 1));
        } else {
            this.title = underscore(rawPage.getTitle());
        }
        this.namespace = rawPage.getNamespace();
    }

    private String underscore(String title) {
        return title.replace(" ", "_");
    }
}
