package digero

import digero.tasks.ImportHTMLDumpTask
import digero.test.Fixture
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{Tag, Test}

import scala.util.Using

@Tag("integration")
class ImportHTMLDumpTaskTest extends Fixture:

  @Test
  def testImportHTMLDump(): Unit =
    val outputPath = temporaryFile("import-html.parquet")

    runHTMLTask(ImportHTMLDumpTask(),
      fixtureFile("enwiktionary-NS0-test-ENTERPRISE-HTML.json.tar.gz").getAbsolutePath,
      outputPath.getAbsolutePath)

    assertThat(outputPath).exists()

    Using.resource(SparkRunner("testImportHTMLDump", null)) { runner =>
      val dataset = runner.session.read.parquet(outputPath.getAbsolutePath)
      assertThat(dataset.count()).isEqualTo(15)
    }

    println(s"wrote file to ${outputPath}")