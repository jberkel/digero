package digero

import digero.tasks.{CleanHTMLDumpTask, ImportHTMLDumpTask}
import digero.test.Fixture
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{Tag, Test}

import scala.util.Using

@Tag("integration")
class CleanHTMLDumpTaskTest extends Fixture:

  @Test
  def testCleanHTMLDump(): Unit =
    val outputPath = temporaryFile("import-html.parquet")
    val cleanedPath = temporaryFile("import-html-cleaned.parquet")

    runHTMLTask(ImportHTMLDumpTask(),
      fixtureFile("enwiktionary-NS0-test-ENTERPRISE-HTML.json.tar.gz").getAbsolutePath,
      outputPath.getAbsolutePath)


    Using.resource(SparkRunner("testImportHTMLDump", null)) { runner =>
      val dataset = runner.session.read.parquet(outputPath.getAbsolutePath)
      assertThat(dataset.count()).isEqualTo(15)

      new CleanHTMLDumpTask().process(runner.session, dataset, TaskArguments(cleanedPath.getAbsolutePath))

      val cleanedSet = runner.session.read.parquet(cleanedPath.getAbsolutePath)
      assertThat(cleanedSet.count()).isEqualTo(15)
    }






