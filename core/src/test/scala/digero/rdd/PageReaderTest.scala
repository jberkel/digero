package digero.rdd

import digero.test.Fixture
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.io.IOUtils
import org.apache.hadoop.mapreduce.TaskAttemptID
import org.apache.hadoop.mapreduce.lib.input.FileSplit
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets.UTF_8

class PageReaderTest extends Fixture:
  private val MULTISTREAM = "enwiktionary-20150224-pages-articles-multistream.xml.bz2"
  private val SINGLESTREAM = "enwiktionary-20150224-pages-articles.xml.bz2"

  // from enwiktionary-20150224-pages-articles-index.txt.bz2
  private val OFFSETS = Array[Long](654, 261373, 387572)

  @Test
  def testProgress(): Unit =
    val reader = getPageReader(MULTISTREAM, OFFSETS(0), OFFSETS(1) - OFFSETS(0))
    assertThat(reader.getProgress).isLessThan(1.0f)

    while reader.nextKeyValue() do {}

    assertThat(reader.getProgress).isEqualTo(1.0f)

  @Test
  def testNext(): Unit =
    val reader = getPageReader(MULTISTREAM, OFFSETS(0), OFFSETS(1) - OFFSETS(0))
    assertThat(reader.nextKeyValue()).isTrue
    assertThat(reader.getCurrentKey).isEqualTo(reader.getCurrentValue.title)
    assertThat(reader.getCurrentValue.title).isEqualTo("Wiktionary:Welcome, newcomers")

  @Test
  def testParseWholeChunk(): Unit =
    val reader = getPageReader(MULTISTREAM, OFFSETS(0), OFFSETS(1) - OFFSETS(0))
    var n = 0
    while reader.nextKeyValue() do n += 1
    assertThat(n).isEqualTo(100)

  @Test
  def testParseSingleStream(): Unit =
    val fileSize = fixtureFile(SINGLESTREAM).length
    val reader = getPageReader(SINGLESTREAM, 0, fileSize)
    var n = 0
    while reader.nextKeyValue() do n += 1
    assertThat(n).isEqualTo(300)

  @Test
  def testLastChunk(): Unit =
    val fileSize = fixtureFile(MULTISTREAM).length
    val reader = getPageReader(MULTISTREAM, OFFSETS(OFFSETS.length - 1), fileSize - OFFSETS(OFFSETS.length - 1))
    while reader.nextKeyValue() do {}

  @Test
  def testClose(): Unit =
    val reader = getPageReader(MULTISTREAM, 654, 261373 - 654)
    reader.close()

  @Test
  def testGetInputStreamAtOffset(): Unit =
    val configuration = Configuration()
    val file = fixturePath("enwiktionary-20150224-pages-articles-multistream.xml.bz2")
    val stream = FileSystem.getLocal(configuration).open(Path(file))
    val inputStream = PageReader.getInputStreamAtOffset(stream, 654)
    val bos = ByteArrayOutputStream()
    IOUtils.copyBytes(inputStream, bos, 8192, true)
    val decoded = bos.toString(UTF_8.name)
    assertThat(decoded).startsWith("<mediawiki>")
    assertThat(decoded).endsWith("</mediawiki>")
    assertThat(decoded.length).isEqualTo(987279)

  private def getPageReader(resource: String, offset: Long, length: Long): PageReader =
    val file = fixturePath(resource)
    val reader = PageReader()
    val conf = Configuration()
    reader.initialize(
      new FileSplit(Path(file), offset, length, Array[String]()),
      TaskAttemptContextImpl(conf, TaskAttemptID())
    )
    reader
