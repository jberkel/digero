package digero

import digero.model.RawPage
import digero.test.Fixture
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.DataFrame
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

@Tag("integration")
class ProcessXMLDumpTest extends Fixture:

  @ParameterizedTest(name = "runDatasetTask({0}, {1})")
  @CsvSource(value = Array(
      "enwiktionary-20150224-pages-articles-multistream.xml.bz2,enwiktionary-20150224-pages-articles-multistream-index.txt.bz2",
      "enwiktionary-20150224-pages-articles.xml.bz2,")
  )
  def testRunDatasetTask(dumpResource: String, indexResource: String): Unit =
    val task: DataFrameTask[Unit] = (_, input: DataFrame, _) =>
      assertThat(input.count).isEqualTo(300)

    val inputs =
      for resource <- Seq(dumpResource, indexResource) if resource != null
        yield fixturePath(resource)

    runXMLTask(task, TaskArguments(inputs.mkString(",")))

  @ParameterizedTest(name = "runRDDTask({0}, {1})")
  @CsvSource(value = Array(
    "enwiktionary-20150224-pages-articles-multistream.xml.bz2,enwiktionary-20150224-pages-articles-multistream-index.txt.bz2",
    "enwiktionary-20150224-pages-articles.xml.bz2,")
  )
  def testRunRDDTask(dumpResource: String, indexResource: String): Unit =
    val task: RDDTask[Unit] = (_, input: RDD[(String, RawPage)], _) =>
      assertThat(input.count).isEqualTo(300)

    val inputs =
      for resource <- Seq(dumpResource, indexResource) if resource != null
      yield fixturePath(resource)

    runXMLTask(task, TaskArguments(inputs.mkString(",")))
