package digero.datasources.mediawiki

import digero.XMLDumpSchema
import digero.test.Fixture
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{BeforeEach, Tag, Test}

import scala.compiletime.uninitialized
import scala.util.Using

@Tag("integration")
class MediawikiDumpDataSourceV2Test extends Fixture:
  private var testConf: SparkConf = uninitialized

  @BeforeEach def setUp(): Unit =
    testConf = new SparkConf(false).setAppName("test").setMaster("local")

  @Test def testLoadData(): Unit =
    val dump = fixturePath("enwiktionary-20150224-pages-articles-multistream.xml.bz2")
    val index = fixturePath("enwiktionary-20150224-pages-articles-multistream-index.txt.bz2")

    val schema = XMLDumpSchema.output

    Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
        val dataset = session.read.format("mediawiki").load(dump, index)
        assertThat(dataset.count).isEqualTo(300)
        val first = dataset.first
        assertThat(first.size).isEqualTo(9)

        assertThat(first.get(schema.fieldIndex("revisionId"))).isEqualTo(24557508L)
        assertThat(first.get(schema.fieldIndex("namespace"))).isEqualTo(4)
        assertThat(first.get(schema.fieldIndex("title"))).isEqualTo("Wiktionary:Welcome, newcomers")
        assertThat(first.get(schema.fieldIndex("timestamp"))).isEqualTo(java.sql.Timestamp(1388411449L * 1000))
        assertThat(first.get(schema.fieldIndex("content"))).isNotNull
        assertThat(first.get(schema.fieldIndex("contentModel"))).isEqualTo("wikitext")
        assertThat(first.get(schema.fieldIndex("redirect"))).isNull()
        assertThat(first.get(schema.fieldIndex("contributorId"))).isEqualTo(14633L)
        assertThat(first.get(schema.fieldIndex("contributorName"))).isEqualTo("Biblbroks")
    }

  @Test def testDataSourceFilter(): Unit =
    val dump = fixturePath("enwiktionary-20150224-pages-articles-multistream.xml.bz2")
    val index = fixturePath("enwiktionary-20150224-pages-articles-multistream-index.txt.bz2")
    Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
      val dataset = session.read.format("mediawiki").load(dump, index).filter("namespace = 4")
      assertThat(dataset.count).isEqualTo(19)
    }
