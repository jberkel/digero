package digero.datasources.cbor

import digero.test.Fixture
import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.junit.jupiter.api.Test

class CBORSchemaTest extends Fixture:
  @Test def readSchema(): Unit =
    val transliteration = fixtureFile("transliteration.cbor").getAbsolutePath
    val `type` = new CBORSchema().readSchema(transliteration)
    assertThat(`type`.fields).hasSize(2)
    assertThat(`type`.fields(0).toString).isEqualTo("StructField(title,StringType,true)")
    assertThat(`type`.fields(1).toString).isEqualTo("StructField(templates,ArrayType(StructType(StructField(name,StringType,true),StructField(parameters,MapType(StringType,StringType,true),true),StructField(text,StringType,true)),true),true)")
