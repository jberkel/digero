package digero.datasources.cbor

import digero.test.Fixture
import org.apache.spark.SparkConf
import org.apache.spark.sql.types.{ArrayType, StructType}
import org.apache.spark.sql.{Row, SparkSession}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{BeforeEach, Tag, Test}

import java.io.File
import scala.compiletime.uninitialized
import scala.util.Using

@Tag("integration") class CBORDataSourceV2Test extends Fixture:
  private var testConf: SparkConf = uninitialized

  @BeforeEach def setUp(): Unit =
    testConf = new SparkConf(false).setAppName("test").setMaster("local")

  @Test
  def testLoadDataParsesSchema(): Unit =
      val transliteration = fixturePath("transliteration.cbor")
      Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
        val dataset = session.read.format("cbor").load(transliteration)
        val schema = dataset.schema
        assertThat(schema.fieldIndex("title")).isEqualTo(0)
        assertThat(schema.treeString).isEqualTo("root\n" +
            " |-- title: string (nullable = true)\n" +
            " |-- templates: array (nullable = true)\n" +
            " |    |-- element: struct (containsNull = true)\n" +
            " |    |    |-- name: string (nullable = true)\n" +
            " |    |    |-- parameters: map (nullable = true)\n" +
            " |    |    |    |-- key: string\n" +
            " |    |    |    |-- value: string (valueContainsNull = true)\n" +
            " |    |    |-- text: string (nullable = true)\n")
      }

  @Test
  def testLoadDataParsesSchemaWithNullField(): Unit =
        val transliteration = fixturePath("combining_form_of.cbor")
        Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
          val dataset = session.read.format("cbor").load(transliteration)
          val schema = dataset.schema
          val first = dataset.first
          val templates = first.getList[Row](schema.fieldIndex("templates"))
          val row = templates.get(0)
          val templateStruct = schema.apply("templates").dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType]
          val text = row.getString(templateStruct.fieldIndex("text"))
          assertThat(text).isNull()
        }

  @Test def testLoadDataGetsAllRows(): Unit =
      val transliteration = fixturePath("transliteration.cbor")
      Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
        val dataset = session.read.format("cbor").load(transliteration)
        assertThat(dataset.count).isEqualTo(86)
      }

  @Test def testCBORIsParsedCorrectly(): Unit =
      val transliteration = fixturePath("transliteration.cbor")
      Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
        val dataset = session.read.format("cbor").load(transliteration)
        val schema = dataset.schema
        val first = dataset.first
        val title = first.getString(schema.fieldIndex("title"))
        assertThat(title).isEqualTo("Qatar")
        val templates = first.getList[Row](schema.fieldIndex("templates"))
        assertThat(templates).hasSize(1)
        val row = templates.get(0)
        val templateStruct = schema.apply("templates").dataType.asInstanceOf[ArrayType].elementType.asInstanceOf[StructType]
        val text = row.getString(templateStruct.fieldIndex("text"))
        val name = row.getString(templateStruct.fieldIndex("name"))
        assertThat(name).isEqualTo("translit")
        assertThat(text).isEqualTo("{{translit|en|ar|قَطَر|id=country}}")
      }

  @Test def testPathGlobbing(): Unit =
      val resourceDir = fixtureFile("transliteration.cbor").getParentFile
      val globPath = new File(resourceDir, "*.cbor")
      Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
        val dataset = session.read.format("cbor").load(globPath.getAbsolutePath)
        assertThat(dataset.count).isEqualTo(135)
      }