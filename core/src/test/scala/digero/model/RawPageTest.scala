package digero.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class RawPageTest:

  @Test def testIsEmpty(): Unit =
    val empty = new RawPage
    assertThat(empty.isEmpty).isTrue
    empty.text = "text"
    assertThat(empty.isEmpty).isTrue
    empty.title = "title"
    assertThat(empty.isEmpty).isFalse
