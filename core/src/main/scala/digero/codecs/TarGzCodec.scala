package digero.codecs

import digero.codecs.TarGzCodec.TarDecompressorStream
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.apache.hadoop.io.compress.{CompressionCodec, CompressionInputStream, CompressionOutputStream, Compressor, Decompressor, DecompressorStream}

import java.io.{InputStream, OutputStream}

/**
 * Decompresses directly all entries contained in a .tar.gz file.
 * Note: This is not splittable and therefore very slow. It is faster to untar all the files before running the job.
 *
 * @see <a href="https://stackoverflow.com/a/48136762/60606">How to load tar.gz files in streaming datasets?</a>
 */
class TarGzCodec extends CompressionCodec:
  override def createInputStream(inputStream: InputStream): CompressionInputStream =
    TarDecompressorStream(new TarArchiveInputStream(GzipCompressorInputStream(inputStream)) {
        getNextEntry()
    })

  override def createInputStream(inputStream: InputStream, decompressor: Decompressor): CompressionInputStream =
    createInputStream(inputStream)

  override def getDefaultExtension = ".tar.gz"
  override def createDecompressor: Decompressor = null

  override def getDecompressorType: Class[? <: Decompressor] = null
  override def getCompressorType: Class[? <: Compressor] = ???
  override def createCompressor: Compressor = ???
  override def createOutputStream(outputStream: OutputStream): CompressionOutputStream = ???
  override def createOutputStream(outputStream: OutputStream, compressor: Compressor): CompressionOutputStream = ???

private object TarGzCodec:
  class TarDecompressorStream(in: TarArchiveInputStream) extends DecompressorStream(in):
    override protected def checkStream(): Unit =
      super.checkStream()
      if in.getCurrentEntry != null && in.available <= 0 then
        in.getNextEntry

    override def read: Int =
      checkStream()
      in.read

    override def read(b: Array[Byte], off: Int, len: Int): Int =
      checkStream()
      in.read(b, off, len)
