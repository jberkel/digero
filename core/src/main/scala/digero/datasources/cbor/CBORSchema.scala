package digero.datasources.cbor

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.{JsonNodeType, ObjectNode}
import com.fasterxml.jackson.dataformat.cbor.CBORFactory
import com.fasterxml.jackson.dataformat.cbor.databind.CBORMapper
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.types.DataTypes.{createArrayType, createMapType}
import org.apache.spark.sql.types.{DataType, DataTypes, StructType}

import java.io.InputStream
import scala.jdk.CollectionConverters.*

class CBORSchema:
  def readSchema(pathS: String): StructType =
      val path = new Path(pathS)
      val stream = path.getFileSystem(new Configuration).open(path)
      try
        val parser = new CBORFactory().createParser(stream.asInstanceOf[InputStream])
        val node: JsonNode = new CBORMapper().readTree(parser)
        node match
          case objectNode: ObjectNode => makeStructType(objectNode)
          case _ =>
            throw new RuntimeException("Expect a map as top-level object, got "+node);
      finally
        stream.close()

  private def makeStructType(map: ObjectNode): StructType =
    map.fields().asScala.foldLeft(new StructType) { (struct, entry) =>
      struct.add(entry.getKey, getDataType(entry.getValue))
    }

  private def getDataType(node: JsonNode): DataType =
    node.getNodeType match {
      case JsonNodeType.ARRAY =>
        if (node.size > 0)
          return createArrayType(getDataType(node.get(0)))
        else
          return createArrayType(DataTypes.NullType)
      case JsonNodeType.OBJECT =>
        val objectNode = node.asInstanceOf[ObjectNode]
        val valueTypes = objectNode.elements().asScala.map(_.getNodeType).toSet
        if (valueTypes.size == 1)
          return createMapType(DataTypes.StringType, mapPrimitiveDataType(objectNode.elements.next))
        else
          return makeStructType(objectNode)
      case JsonNodeType.BINARY => return DataTypes.BinaryType
      case JsonNodeType.BOOLEAN => return DataTypes.BooleanType
      case JsonNodeType.MISSING =>
      case JsonNodeType.POJO => throw new UnsupportedOperationException
      case JsonNodeType.NULL => return DataTypes.NullType
      case JsonNodeType.NUMBER => return DataTypes.IntegerType
      case JsonNodeType.STRING => return DataTypes.StringType
    }
    throw new RuntimeException

  private def mapPrimitiveDataType(node: JsonNode) = node.getNodeType match
    case JsonNodeType.BINARY => DataTypes.BinaryType
    case JsonNodeType.BOOLEAN => DataTypes.BooleanType
    case JsonNodeType.NUMBER => DataTypes.IntegerType
    case JsonNodeType.STRING => DataTypes.StringType
    case JsonNodeType.NULL => DataTypes.NullType
    case _ =>
      throw new IllegalArgumentException("Complex type: " + node.getNodeType)
