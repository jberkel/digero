package digero.datasources.cbor

import org.apache.hadoop.fs.FileStatus
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.expressions.Expression
import org.apache.spark.sql.catalyst.{FileSourceOptions, InternalRow}
import org.apache.spark.sql.connector.catalog.Table
import org.apache.spark.sql.connector.read.{PartitionReader, PartitionReaderFactory, Scan, ScanBuilder}
import org.apache.spark.sql.connector.write.{LogicalWriteInfo, WriteBuilder}
import org.apache.spark.sql.execution.datasources.binaryfile.BinaryFileFormat
import org.apache.spark.sql.execution.datasources.v2.*
import org.apache.spark.sql.execution.datasources.{FileFormat, PartitionedFile, PartitioningAwareFileIndex}
import org.apache.spark.sql.internal.SQLConf
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.util.CaseInsensitiveStringMap
import org.apache.spark.util.SerializableConfiguration

import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{FileSystems, FileVisitResult, Files, Path, SimpleFileVisitor}
import scala.collection.mutable.ListBuffer
import scala.jdk.CollectionConverters.*

class CBORDataSourceV2 extends FileDataSourceV2:
  override def fallbackFileFormat: Class[? <: FileFormat] = classOf[CBORFileFormat]
  override def shortName() = "cbor"

  override protected def getTable(options: CaseInsensitiveStringMap): Table =
    val paths = getPaths(options)
    val tableName = getTableName(options, paths)
    val optionsWithoutPaths = getOptionsWithoutPaths(options)
    CBORTable(tableName, sparkSession, optionsWithoutPaths, paths, None, fallbackFileFormat)

  override protected def getTable(options: CaseInsensitiveStringMap, schema: StructType): Table =
    val paths = getPaths(options)
    val tableName = getTableName(options, paths)
    val optionsWithoutPaths = getOptionsWithoutPaths(options)
    CBORTable(tableName, sparkSession, optionsWithoutPaths, paths, Option(schema), fallbackFileFormat)

  override def inferSchema(options: CaseInsensitiveStringMap): StructType =
    val paths = getPaths(options)
    CBORSchema().readSchema(paths.head)

  override protected def getPaths(map: CaseInsensitiveStringMap): Seq[String] =
    val path = super.getPaths(map)
    globPath(path.head)

  // Hadoop's built-in globbing doesn't support colons in filenames, need to implement our own
  // https://issues.apache.org/jira/browse/HADOOP-3257
  private def globPath(pathAndGlob: String): Seq[String] =
    val fs = FileSystems.getDefault
    val path = fs.getPath(pathAndGlob)
    val pathMatcher = fs.getPathMatcher("glob:" + path.getFileName)
    val foundPaths = ListBuffer[Path]()
    Files.walkFileTree(path.getParent, new SimpleFileVisitor[Path]() {
      override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult =
        if pathMatcher.matches(file.getFileName) then foundPaths += file
        FileVisitResult.CONTINUE
    })
    foundPaths.map(p => p.toAbsolutePath.toString).toSeq


case class CBORTable(
  name: String,
  sparkSession: SparkSession,
  options: CaseInsensitiveStringMap,
  paths: Seq[String],
  userSpecifiedSchema: Option[StructType],
  fallbackFileFormat: Class[? <: FileFormat]) extends FileTable(sparkSession, options, paths, userSpecifiedSchema):

  override def inferSchema(files: Seq[FileStatus]): Option[StructType] =
    files.headOption.map(option => CBORSchema().readSchema(option.getPath.toString))

  override def formatName: String = "CBOR"

  override def newScanBuilder(options: CaseInsensitiveStringMap): ScanBuilder =
    CBORScanBuilder(sparkSession, fileIndex, dataSchema, options)

  override def newWriteBuilder(info: LogicalWriteInfo): WriteBuilder =
    throw UnsupportedOperationException("not implemented")


class CBORScanBuilder(
    sparkSession: SparkSession,
    fileIndex: PartitioningAwareFileIndex,
    dataSchema: StructType,
    options: CaseInsensitiveStringMap)
  extends FileScanBuilder(sparkSession, fileIndex, dataSchema):

  override def build(): Scan =
    CBORScan(sparkSession, fileIndex, dataSchema, readDataSchema(), readPartitionSchema(), options)


case class CBORScan(
    sparkSession: SparkSession,
    fileIndex: PartitioningAwareFileIndex,
    dataSchema: StructType,
    readDataSchema: StructType,
    readPartitionSchema: StructType,
    options: CaseInsensitiveStringMap,
    partitionFilters: Seq[Expression] = Seq.empty,
    dataFilters: Seq[Expression] = Seq.empty) extends FileScan:

  override def createReaderFactory(): PartitionReaderFactory =
    val caseSensitiveMap = options.asCaseSensitiveMap.asScala.toMap
    val hadoopConf = sparkSession.sessionState.newHadoopConfWithOptions(caseSensitiveMap)
    val broadcastedConf = sparkSession.sparkContext.broadcast(
      SerializableConfiguration(hadoopConf))

    CBORPartitionReaderFactory(
      sparkSession.sessionState.conf,
      broadcastedConf,
      dataSchema,
      readDataSchema,
      readPartitionSchema
    )


case class CBORPartitionReaderFactory(
   sqlConf: SQLConf,
   broadcastedConf: Broadcast[SerializableConfiguration],
   dataSchema: StructType,
   readDataSchema: StructType,
   partitionSchema: StructType) extends FilePartitionReaderFactory:

  override def options: FileSourceOptions = FileSourceOptions(Map.empty)

  override def buildReader(partitionedFile: PartitionedFile): PartitionReader[InternalRow] =
    CBORPartitionReader(partitionedFile.filePath, dataSchema)


class CBORFileFormat extends BinaryFileFormat {
}
