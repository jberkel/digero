package digero.datasources.mediawiki

import digero.rdd.PageReader
import org.apache.hadoop.mapred.{JobConf, TaskAttemptContextImpl, TaskAttemptID}
import org.apache.hadoop.mapreduce.lib.input.FileSplit
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.connector.read.PartitionReader
import org.apache.spark.sql.sources.Filter
import org.apache.spark.unsafe.types.UTF8String

case class MediawikiPartitionReader(fileSplit: FileSplit, filters: Array[Filter])
  extends PartitionReader[InternalRow]:

  lazy val pageReader: PageReader = {
      val reader = PageReader(filters*)
      reader.initialize(fileSplit, TaskAttemptContextImpl(JobConf(), TaskAttemptID()))
      reader
  }

  override def next: Boolean = pageReader.nextKeyValue()
  override def close(): Unit = pageReader.close()

  override def get: InternalRow =
    val page = pageReader.getCurrentValue
    InternalRow(
      page.revisionId,
      page.namespace,
      UTF8String.fromString(page.title),
      page.timestamp.toEpochSecond * 1000 * 1000, // expects microseconds
      UTF8String.fromString(page.text),
      UTF8String.fromString(page.contentModel),
      UTF8String.fromString(page.redirect),
      page.contributorId,
      UTF8String.fromString(page.contributorName)
    )