package digero.rdd

import digero.model.RawPage
import org.apache.commons.logging.LogFactory
import org.apache.hadoop.fs.{FileStatus, FileSystem}
import org.apache.hadoop.io.compress.bzip2.CBZip2InputStream
import org.apache.hadoop.mapreduce.lib.input.{FileInputFormat, FileSplit}
import org.apache.hadoop.mapreduce.{InputSplit, JobContext, RecordReader, TaskAttemptContext}

import java.io.{IOException, InputStream}
import java.util
import java.util.Collections.singletonList
import java.util.List
import scala.io.Source
import scala.jdk.CollectionConverters.*
import scala.util.Using

class MediawikiDumpInputFormat extends FileInputFormat[String, RawPage]:
  private val LOG = LogFactory.getLog(classOf[MediawikiDumpInputFormat])

  override def getSplits(job: JobContext): util.List[InputSplit] =
    // ensures that filenames are ordered
    job.getConfiguration.setInt("mapreduce.input.fileinputformat.list-status.num-threads", 1)
    val fileStatuses = listStatus(job)

    if fileStatuses.isEmpty then throw new IOException("expected at least one input file")
    val dump = fileStatuses.get(0)
    val index: FileStatus = if fileStatuses.size() > 1 then fileStatuses.get(1) else null

    if index != null then
      readFileSplitsFromIndexFile(FileSystem.get(job.getConfiguration),
        dump, index).toIndexedSeq.asJava
    else
      LOG.warn("reading dump without index file: \uD83D\uDC0C")
      singletonList(new FileSplit(dump.getPath, 0, dump.getLen, null))

  override def createRecordReader(split: InputSplit, context: TaskAttemptContext): RecordReader[String, RawPage] =
    PageReader()

  private def readFileSplitsFromIndexFile(fileSystem: FileSystem,
                                          dump: FileStatus, index: FileStatus): Array[InputSplit] =
    LOG.info(s"reading offsets/splits from $index")
    val offsetPairs = Using.resource(fileSystem.open(index.getPath)) { indexStream =>
      collectOffsets(indexStream, dump.getLen)
        .sliding(2, 1)
        .map(s => (s.head, s(1)))
        .toArray
    }
    for
      (start, end) <- offsetPairs
      length = end - start
    yield FileSplit(dump.getPath, start, length, null)

  private def collectOffsets(stream: InputStream, fileLength: Long): Iterator[Long] =
    if stream.skip(2) != 2 then throw IOException()

    val offsets = for
      line <- Source.fromInputStream(CBZip2InputStream(stream)).getLines()
      parts = line.split(":", 3)
      firstPart <- parts.headOption
    yield firstPart

    offsets.distinct.map(_.toLong).concat(Seq(fileLength))
