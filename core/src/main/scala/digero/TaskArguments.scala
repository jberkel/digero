package digero

import java.io.File
import java.nio.file.Path

case class TaskArguments(arguments: Array[String]):
  val nonEmpty: Boolean = arguments.nonEmpty
  val length: Int = arguments.length
  def head: String = arguments.head
  def apply(i: Int): String = arguments.apply(i)

  private lazy val output: Option[String] = arguments.headOption
  
  lazy val outputPath: Path = output.map(Path.of(_)).getOrElse(throw IllegalArgumentException("missing output"))
  lazy val outputFile: File = output.map(File(_)).getOrElse(throw IllegalArgumentException("missing output"))
  lazy val skipFirst: TaskArguments = if arguments.isEmpty then this else TaskArguments(arguments.drop(1))
  lazy val language: Option[String] = if length >= 2 then Some(apply(1)) else None 
  
  override def toString: String = arguments.mkString("TaskArguments(", ", ", ")")


object TaskArguments:
  val empty: TaskArguments = TaskArguments(Array.empty[String])

  def apply(argument: String): TaskArguments = TaskArguments(Array(argument))
  def apply(arguments: Seq[String]): TaskArguments = TaskArguments(arguments=arguments.toArray)