package digero

import digero.model.RawPage
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
 * A task which operates on a [[RDD]].
 */
trait RDDTask[Output] extends Task:
  def process(context: SparkContext, input: RDD[(String, RawPage)], arguments: TaskArguments): Output
