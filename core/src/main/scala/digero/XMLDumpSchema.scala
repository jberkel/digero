package digero

import org.apache.spark.sql.types.DataTypes.{IntegerType, LongType, StringType, TimestampType}
import org.apache.spark.sql.types.StructType

object XMLDumpSchema:
  // Output schema for classic Mediawiki XML dumps
  lazy val output: StructType = new StructType()
    .add("revisionId", LongType)
    .add("namespace", IntegerType)
    .add("title", StringType)
    .add("timestamp", TimestampType)
    .add("content", StringType)
    .add("contentModel", StringType)
    .add("redirect", StringType) // redirect target
    .add("contributorId", LongType)
    .add("contributorName", StringType)
