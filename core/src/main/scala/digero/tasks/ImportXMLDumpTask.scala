package digero.tasks

import digero.{DataFrameTask, TaskArguments, XMLDumpSchema}
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.functions.{col, length, sha1}
import org.apache.spark.sql.{DataFrame, Encoder, Row, SaveMode, SparkSession}

import java.io.File

class ImportXMLDumpTask extends DataFrameTask[File]:
  given encoder: Encoder[Row] = ExpressionEncoder(XMLDumpSchema.output)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    log.info(s"ImportXMLDump($arguments)")
    require(arguments.nonEmpty, "empty arguments")

    val output = arguments.outputFile
    require(!output.getPath.isBlank, "need output dir") // Spark will happily trash the local directory :(

    (for 
      row <- input
    yield row)
      .withColumn("content_hash", sha1(col("content")))
      .withColumn("content_length", length(col("content")))
      .coalesce(100)
      .write
      .mode(SaveMode.Overwrite)
      .partitionBy("namespace")
      .option("compression", "snappy")
      .option("header", "true")
      .parquet(output.getPath)

    output