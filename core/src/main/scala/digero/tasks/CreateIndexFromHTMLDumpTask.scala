package digero.tasks

import digero.Task.copyMerge
import digero.{DataFrameTask, TaskArguments}
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.*
import org.apache.spark.sql.functions.*

import java.io.File

/**
 * Creates an index directly from a HTML dump.
 */
class CreateIndexFromHTMLDumpTask extends DataFrameTask[File]:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.nonEmpty)

    val output = arguments.outputFile
    val outputWork = File(output.getParentFile, output.getName + ".work")

    input.select(col("title"))
         .coalesce(100)
         .sort(col("title"))
         .write
         .mode(SaveMode.Overwrite)
         .text(outputWork.getAbsolutePath)

    copyMerge(FileSystem.get(session.sparkContext.hadoopConfiguration), srcDir=outputWork, output)
    output

/**
 * Creates an index directly from a XML dump.
 */
class CreateIndexFromXMLDump extends DataFrameTask[File]:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    require(arguments.nonEmpty)

    val output = arguments.outputFile
    val outputWork = File(output.getParentFile, output.getName + ".work")

    input.filter("namespace = 0")
         .filter("redirect is null")
         .select(col("title"))
         .coalesce(100)
         .sort(col("title"))
         .write
         .mode(SaveMode.Overwrite)
         .text(outputWork.getAbsolutePath)

    copyMerge(FileSystem.get(session.sparkContext.hadoopConfiguration), srcDir=outputWork, output)
    output
