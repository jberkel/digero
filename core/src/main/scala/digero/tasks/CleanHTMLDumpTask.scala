package digero.tasks

import digero.{DataFrameTask, HTMLDumpSchema, TaskArguments}
import org.apache.spark.sql.*
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{broadcast, col, explode, nth_value}

import java.io.File

/**
 * Fixes some consistency issues with HTML dumps:
 *  - removes duplicated titles (keep latest revision)
 *  - removes entries that are also used as redirects
 *  - removes duplicate (revision) ids
 */
class CleanHTMLDumpTask extends DataFrameTask[File]:

  given encoder: Encoder[Row] = ExpressionEncoder(HTMLDumpSchema.simplifiedOutput)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    log.info(s"CleanHTMLDump($arguments)")
    require(arguments.length >= 1, "no arguments")

    val output = arguments.outputFile
    require(!output.getPath.isBlank, "need output dir") // Spark will happily trash the local directory :(

    val redirects = input
      .select(explode(col("redirects")).as("title"))
      .dropDuplicates()
      .cache()

    val window = Window.partitionBy(col("title")).orderBy(col("id").desc)
    val uniqueRevisions = input
      .withColumn("id", nth_value(col("id"), 1).over(window))

    input
      .dropDuplicates("id")
      .join(broadcast(uniqueRevisions), usingColumn = "id", joinType = "left_semi")
      .join(broadcast(redirects), usingColumn = "title", joinType = "left_anti")
      .write
      .mode(SaveMode.Overwrite)
      .option("compression", "snappy")
      .option("header", "true")
      .parquet(output.getPath)

    output
