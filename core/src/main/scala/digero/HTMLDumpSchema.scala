package digero

import org.apache.spark.sql.types.{ArrayType, DataTypes, StructType}
import org.apache.spark.sql.types.DataTypes.*

import java.time.OffsetDateTime
import java.time.Instant
import java.time.format.DateTimeFormatter

object HTMLDumpSchema:
  // The input schema for "enterprise" Mediawiki HTML dumps
  // https://dumps.wikimedia.org/other/enterprise_html/
  lazy val input: StructType = new StructType()
    .add("additional_entities", ArrayType(entityType))
    .add("article_body", articleBodyType)
    .add("categories", ArrayType(namedUrlType))
    .add("date_modified", timestampStringType)
    .add("event", eventType)
    .add("identifier", LongType) // page_id
    .add("in_language", stringIdentifierType) // "English", "en"
    .add("is_part_of", stringIdentifierType)  // "Wiktionary", "enwiktionary"
    .add("license", ArrayType(stringIdentifierType))
    .add("name", StringType)
    .add("namespace", namespaceType)
    .add("protection", ArrayType(protectionType), nullable = true)
    .add("redirects", ArrayType(namedUrlType), nullable = true)
    .add("templates", ArrayType(namedUrlType))
    .add("url", StringType)
    .add("version", versionType)

  /** The output schema used by [[ImportHTMLDump]] */
  lazy val simplifiedOutput: StructType = new StructType()
    .add("id", LongType, nullable = false)
    .add("title", StringType, nullable = false)
    .add("namespace", IntegerType, nullable = false)
    .add("last_modified", TimestampType, nullable = false)
    .add("redirects", ArrayType(StringType))
    .add("content", StringType)
    .add("editor_name", StringType, nullable = true)
    .add("content_hash", StringType)
    .add("content_length", LongType)

  def parseTimestamp(text: String): Instant =
      DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(text).query(OffsetDateTime.from).toInstant

  private lazy val entityType = new StructType()
    .add("aspects", ArrayType(StringType)) // ?
    .add("identifier", StringType)         // "Q123"
    .add("url", StringType)                // "https://www.wikidata.org/wiki/Q123"

  private lazy val articleBodyType = new StructType()
    .add("html", StringType, nullable = true) 
    .add("wikitext", StringType)

  private lazy val namespaceType = new StructType()
    .add("identifier", IntegerType) // 0
    .add("name", StringType)        // "Article"

  private lazy val namedUrlType = new StructType()
    .add("name", StringType)
    .add("url", StringType)

  private lazy val stringIdentifierType = new StructType()
    .add("identifier", StringType)
    .add("name", StringType)

  private lazy val protectionType = new StructType()
    .add("expiry", StringType) // infinity
    .add("level", StringType)  // sysop, autoconfirmed, etc.
    .add("type", StringType)   // edit, move, or create

  private lazy val editorType = new StructType()
    .add("name", StringType)  // user name or IP address if is_anonymous is true
    .add("identifier", LongType, nullable = true) // user id, only set when not anon
    .add("is_anonymous", BooleanType, nullable = true)

  private lazy val versionType = new StructType()
    .add("comment", StringType)
    .add("editor", editorType)
    .add("identifier", LongType)
    .add("is_minor_edit", BooleanType, nullable = true)
    .add("tags", ArrayType(StringType), nullable = true)  // visualeditor-wikitext, etc.

  private lazy val eventType = new StructType()
    .add("identifier", StringType)
    .add("type", StringType)
    .add("date_created", timestampStringType)

  private lazy val timestampStringType = StringType // 2019-10-15T01:56:12Z