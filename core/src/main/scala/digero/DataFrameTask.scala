package digero

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * A task which operates on a [[DataFrame]].
 */
trait DataFrameTask[Output] extends Task:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): Output

  /** Optional schema of the input, if not set, Spark will try to infer it automatically */
  def inputSchema: Option[StructType] = None
