package digero

import org.apache.commons.logging.LogFactory

import scala.util.Using

@main
def ProcessHTMLDump(className: String, rest: String*): Unit =
  val task = Task.getTask[DataFrameTask[?]](className)
  runHTMLTask(task, rest*)

def runHTMLTask(task: DataFrameTask[?], args: String*): Unit =
  val log = LogFactory.getLog("ProcessHTMLDump")
  val (inputs, rest) = args.partition(x => x.endsWith(".ndjson") || x.endsWith(".json.tar.gz"))

  log.info(s"runHTMLTask(input=${inputs}, args=${rest})")

  require(inputs.nonEmpty, "empty input")

  Using.resource(SparkRunner(task)) { runner =>
    val dataset = runner.session.read.schema(HTMLDumpSchema.input).json(inputs*)
    task.process(runner.session, dataset, TaskArguments(rest.toArray))
  }
