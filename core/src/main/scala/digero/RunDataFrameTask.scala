package digero

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.File
import scala.util.Using

/**
 * Runner class for a [[DataFrameTask]], using a previously parsed dump.
 * See [[digero.ProcessHTMLDump]] and [[digero.ProcessXMLDump]] for the runner class to process a dump.
 */
@main
def RunDataFrameTask(className: String, file: String, rest: String*): Unit =
  val task: DataFrameTask[?] = Task.getTask(className)
  RunDataFrameTask(task, new File(file), rest)

private def RunDataFrameTask(task: DataFrameTask[?], file: File, rest: Seq[String]): Unit =
  Using.resource(SparkRunner(task)) { runner =>
    task.process(runner.session, getDataFrame(runner.session, file, task.inputSchema), TaskArguments(rest))
  }

private def getDataFrame(session: SparkSession, file: File, schema: Option[StructType]): DataFrame =
  val reader = schema.map(session.read.schema(_)).getOrElse(session.read)

  if file.getName.endsWith(".json") ||
    file.getName.endsWith(".jsonl") ||
    file.getName.endsWith(".ndjson") then

    if file.isDirectory then
      val containedFiles = file.listFiles()
      val partitionedDirs = containedFiles
          .filter(_.isDirectory)
          .filter(_.getName.contains("="))

      if partitionedDirs.isEmpty then
        val dataFiles = containedFiles
          .filter(_.isFile)
          .filterNot(_.getName.startsWith("."))
          .filterNot(_.getName.startsWith("_"))
          .map(_.getAbsolutePath)

        reader.json(dataFiles*)
      else
        // use basePath
        reader.json(file.getAbsolutePath)
    else
      reader.json(file.getAbsolutePath)
  else
    reader.parquet(file.getAbsolutePath)
