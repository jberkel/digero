package digero.parser

import digero.model.RawPage
import digero.parser.MediawikiParser.{CONTRIBUTOR, DUMP_VERSION, ID, IP, MODEL, NS, PAGE, REDIRECT, REVISION, ROOT, TEXT, TIMESTAMP, TITLE, USERNAME}
import org.apache.spark.sql.sources.{EqualTo, Filter, In, IsNotNull, IsNull}

import java.io.{IOException, InputStream}
import java.nio.charset.StandardCharsets.UTF_8
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import javax.xml.stream.XMLStreamConstants.{CHARACTERS, END_ELEMENT, START_ELEMENT}
import javax.xml.stream.{Location, XMLInputFactory, XMLStreamException, XMLStreamReader}
import scala.collection.mutable


class MediawikiParser(inputStream: InputStream, filters: Filter*) extends AutoCloseable:
  private lazy val reader: XMLStreamReader =
    XMLInputFactory.newInstance.createXMLStreamReader(inputStream, UTF_8.name())

  @throws[XMLStreamException]
  def next(page: RawPage): Boolean =
    while reader.hasNext do
      if reader.next() == START_ELEMENT then
        reader.getName.getLocalPart match {
          case ROOT => checkVersion()
          case PAGE => return readPage(page)
          case _ =>
        }
    false

  @throws[IOException]
  def close(): Unit =
    try reader.close()
    catch case e: XMLStreamException => throw IOException(e)


  private def readPage(page: RawPage): Boolean =
    page.redirect = null
    page.contributorName = null
    page.contributorId = 0L
    var inRevision, inContributor = false

    while reader.hasNext do
      reader.next() match {
        case START_ELEMENT =>
          reader.getName.getLocalPart match {
            case TITLE => page.title = readContent
            case NS => handleNS(page)
            case ID => handleID(page, inRevision, inContributor)
            case REDIRECT => page.redirect = readRedirect
            case REVISION =>
              if includeRedirect(page.redirect) then
                inRevision = true
              else
                skipToPageEnd()
                page.redirect = null // important to reset state here
            case MODEL if inRevision => page.contentModel = readContent
            case TIMESTAMP if inRevision => page.timestamp = readDate
            case CONTRIBUTOR if inRevision => inContributor = true
            case TEXT if inRevision => page.text = readContent
            case USERNAME if inContributor => page.contributorName = readContent
            case IP if inContributor => page.contributorName = readContent
            case _ =>
          }
        case END_ELEMENT =>
          reader.getName.getLocalPart match {
            case PAGE => return true
            case REVISION => inRevision = false
            case CONTRIBUTOR => inContributor = false
            case _ =>
          }
        case _ =>
    }
    false

  private def handleID(page: RawPage, inRevision: Boolean, inContributor: Boolean): Unit =
    if inContributor then
      page.contributorId = readLong.getOrElse(-1)
    else if inRevision then
      page.revisionId = readLong.getOrElse(throw XMLStreamException("Error parsing revision id"))
    else
      page.id = readLong.getOrElse(throw XMLStreamException("Error parsing page id"))

  private def handleNS(page: RawPage): Unit =
    val namespace = readInt.getOrElse(throw XMLStreamException("Error parsing ns id"))
    if includeNamespace(namespace) then
      page.namespace = namespace
    else
      skipToPageEnd()

  private def checkVersion(): Unit =
    Option(reader.getAttributeValue(null, "version")) match {
      case Some(version) if version != DUMP_VERSION =>
        throw MediawikiParser.UnsupportedDumpVersion(reader.getLocation, version)
      case _ =>
    }

  private def skipToPageEnd(): Unit =
    while reader.hasNext do
      reader.next() match {
        case END_ELEMENT if reader.getName.getLocalPart == PAGE => return
        case _ =>
      }

  private def readRedirect = reader.getAttributeValue(null, "title")
  private def readLong: Option[Long] = readContent.trim.toLongOption
  private def readInt: Option[Int] = readContent.trim.toIntOption
  private def readDate: OffsetDateTime =
    DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(readContent).query(OffsetDateTime.from)

  private def readContent: String =
    val builder = mutable.StringBuilder()
    while reader.hasNext do
      reader.next() match {
        case CHARACTERS => builder ++= reader.getText
        case END_ELEMENT => return builder.result
        case _ =>
      }
    builder.result

  private def includeNamespace(namespace: Int): Boolean =
    filters.forall {
      case EqualTo("namespace", value: Int)    => value == namespace
      case In("namespace", values: Array[Any]) => values.contains(namespace)
      case _ => true
    }

  private def includeRedirect(redirect: String): Boolean =
    filters.forall {
      case IsNull("redirect")    => redirect == null
      case IsNotNull("redirect") => redirect != null
      case _ => true
    }

object MediawikiParser:
  class UnsupportedDumpVersion(location: Location, version: String)
    extends XMLStreamException(s"Unsupported dump version: $version", location)

  private val TEXT = "text"
  private val TITLE = "title"
  private val NS = "ns"
  private val ID = "id"
  private val REVISION = "revision"
  private val MODEL = "model"
  private val TIMESTAMP = "timestamp"
  private val CONTRIBUTOR = "contributor"
  private val USERNAME = "username"
  private val IP = "ip"
  private val REDIRECT = "redirect"
  private val PAGE = "page"
  private val ROOT = "mediawiki"
  private val DUMP_VERSION = "0.10"
